function [ind min1] = firstMin(L)
Ltmp = L;

while min(Ltmp) == -Inf
     [val ind] = min(Ltmp);
     Ltmp(ind) = Inf;
end

[ind min1] = min(Ltmp);