function [cutValueF dimension data1 data2 e1 e2 t1 t2 dist] = Split2 (data, errors, tags, sample, dimInput)
%
% Copyright (c) 2009 Adrien Baranes and Pierre-Yves Oudeyer, INRIA Bordeaux - Sud-Ouest
%               351 Cours de la Liberation, 33400, Talence,
%               France, http://flowers.inria.fr
%


 [~, leng] = size(data) ;
 [~, ne] = size(errors) ;
 
 if (leng ~= ne) 
     data
     errors
     error(['SPLIT: error : data and errors have incompatible width : data : ',num2str(leng), ' while errors: ',num2str(ne)]);
 end
 

Interval = leng/sample:leng/sample:leng-leng/sample;
dist     = -Inf*ones(dimInput,length(Interval));
Ind      = zeros(dimInput,length(Interval));

for dimension = 1:dimInput
    E = sort(data(dimension,:));
    cpt = 0;
    
    for cutValueI = Interval
        if(E(ceil(cutValueI))>100 || E(ceil(cutValueI))<0)
            error('Split2: error E(ceil(cutValue)) is out of bound. It should be between 0 and 1');
        end
        
        [data1, data2, e1, e2, t1, t2] = split(data, errors, tags, E(ceil(cutValueI)),dimension);
        if ~isempty(e1) && ~isempty(e2) && ~isempty(data1) && ~isempty(data2)
            try
                cpt = cpt + 1;
                Ind(dimension,cpt) = E(ceil(cutValueI));
              
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % INTEREST 1 %%%%%%%
                n1 = size(e1,2);
                errorsDim1{1} = e1;
                Interest1 = ComputeDerivative(errorsDim1, 1);
                
                
                %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                % INTEREST 2 %%%%%%%
                n2 = size(e2,2);
                errorsDim2{1} = e2;
                Interest2 = ComputeDerivative(errorsDim2, 1);
                
                delta = (Interest1 - Interest2).^2;
                
                
                
                if n1 > size(errors,2)/10 && n2 > size(errors,2)/10
                    dist(dimension,cpt) = n1*n2*delta;
                else
                    dist(dimension,cpt) = -Inf;
                end
              
            catch
                dist(dimension,cpt) = -Inf;
            end  % end try catch
        end   % empty(e1) ...
        
    end
end


[maxi,indMax] = max(dist(:));
[IndiceDistMaxDim,IndiceDistMaxCut] = ind2sub(size(dist),indMax);


cutValueF = Ind(IndiceDistMaxDim,IndiceDistMaxCut);

dimension = IndiceDistMaxDim;

%[data1, data2, e1, e2, t1, t2] = split(data, errors, tags, cutValueF,dimension);
dist = maxi;



%{
% test Split2
dim = 5;
nbData = 10;
data    = ones(dim,nbData);
data(2,:) = [1:nbData];
errors = [ones(1,5) 3*ones(1,5)];
tags   = ones(1,nbData).*[1:nbData];

[cutResult, dimension, data1, data2, e1, e2, t1, t2, dist] = Split2(data, errors, tags, 10, dim)


%{ 
outputs

cutResult =

     6


dimension =

     2


data1 =

     1     1     1     1     1
     1     2     3     4     5
     1     1     1     1     1
     1     1     1     1     1
     1     1     1     1     1


data2 =

     1     1     1     1     1
     6     7     8     9    10
     1     1     1     1     1
     1     1     1     1     1
     1     1     1     1     1


e1 =

     1     1     1     1     1


e2 =

     3     3     3     3     3


t1 =

     1     2     3     4     5


t2 =

     6     7     8     9    10


dist =

   100

%}
%}

end






