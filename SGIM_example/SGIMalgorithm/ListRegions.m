classdef ListRegions < handle
   % Copyright (c) 2012 Sao Mai Nguyen,
   %               INRIA Bordeaux - Sud-Ouest
   %               351 Cours de la Liberation, 33400, Talence,
   %               France, http://nguyensmai.free.fr
   %

   
    properties
        numLReg
        LReg  % table of [[derivative of competence for each strategy],...
              % [limitMin limitMax] matrix]
        LRegMin
    end
    
    
    methods(Static=false)
        
        %constructor
        function listReg = ListRegions(nbStrat)
            listReg.numLReg = 1;
            listReg.LReg    = ListRegions.createLReg(nbStrat);
            listReg.LRegMin = ListRegions.createLReg(nbStrat);
        end
        
        function limitsRegions(obj, dim)
            %display the min and max limits of the regions for the given dimension
            nbRegions = size(obj.LReg,2);
            dispMin = ['min is '];
            dispMax = ['max is '];
            for i = 2 :nbRegions
                Tmp = obj.LReg{i}{2};
                dispMin = [dispMin, num2str(Tmp(dim,1)), ' '];
                dispMax = [dispMax, num2str(Tmp(dim,2)), ' '];
            end
            disp(dispMin);
            disp(dispMax);
        end
        
        
        function dispLReg(obj, iStrategy)
        if size(obj.LReg{1}{2},1) == 1  
             dispLReg1D(obj, iStrategy)
        elseif size(obj.LReg{1}{2},1) == 2
             dispLReg2D(obj, iStrategy)
        end
        end %end dispLReg
        
        
        function dispLReg1D(obj, iStrategy)
            %draw region in 1D between -1 and 1
            disp('dispLReg1D');
            try
                if size(obj.LReg,2) == 1, tryCatch, end
                
                rect = zeros(1,size(obj.LReg,2));
                for i = 2:size(obj.LReg,2)
                    Tmp = 2*obj.LReg{i}{2} - 1;
                    %                    Tmp = Tmp(1:2,:);
                    Tmp(2,:) = [0 0];
                    if obj.LReg{i}{1}(iStrategy) ~= 999 && obj.LReg{i}{1}(iStrategy) ~= 998
                         Tmp(2,:) = [0 -obj.LReg{i}{1}(iStrategy)];
                    end
                    hold on
                    if (Tmp(1,2)>Tmp(1,1))  && (Tmp(2,2)>Tmp(2,1))
                        rectangle('Position',[Tmp(1,1),Tmp(2,1),Tmp(1,2)-Tmp(1,1),Tmp(2,2)-Tmp(2,1)],'LineWidth',2,'LineStyle','-')
                        hold on
                    end
                    
                    
                end
                
                rect2 = rect/max(rect);
                
            catch
                disp('problem in dispLReg');
            end
            
            try
                for i = 2:size(obj.LReg,2)
                    Tmp = 2*obj.LReg{i}{2}-1;
                    
                    %                    Tmp = Tmp(1:2,:);
                    hold on
                    
%                     if obj.LReg{i}{1}(iStrategy) ~= 999  && obj.LReg{i}{1}(iStrategy) ~= 998 %&& nargin == 1
                        disp(['obj.LReg(' num2str(i) ') : [' num2str(obj.LReg{i}{2}(1,1)) '-' num2str(obj.LReg{i}{2}(1,2))  '] : ' num2str(obj.LReg{i}{1}(iStrategy))  ]);
%                     end
                end
            catch
                disp('problem in dispLReg');
            end
            
        end %end dispLReg1D 
        
        
        function dispLReg2D(obj, iStrategy)
            %draw region in 2D between -1 and 1
            disp('dispLReg2D');
            try
                if size(obj.LReg,2) == 1, tryCatch, end
                
                rect = zeros(1,size(obj.LReg,2));
                for i = 2:size(obj.LReg,2)
                    Tmp = 2*obj.LReg{i}{2} - 1;
                    Tmp = Tmp(1:2,:)
                    
                    hold on
                    if (Tmp(1,2)>Tmp(1,1))  && (Tmp(2,2)>Tmp(2,1))
                   rectangle('Position',[Tmp(1,1),Tmp(2,1),Tmp(1,2)-Tmp(1,1),Tmp(2,2)-Tmp(2,1)],'LineWidth',2,'LineStyle','-')
                    end
                   if obj.LReg{i}{1}(iStrategy) ~= 999 && obj.LReg{i}{1}(iStrategy) ~= 998
                       rect(i) = -obj.LReg{i}{1}(iStrategy);
                   end
               end
               
               rect2 = rect/max(rect);
               
               for i = 1:size(obj.LReg,2)
                   Tmp = 2*obj.LReg{i}{2}-1;
                   
                   Tmp = Tmp(1:2,:)
                   hold on
                   
                   if obj.LReg{i}{1}(iStrategy) ~= 999  && obj.LReg{i}{1}(iStrategy) ~= 998 %&& nargin == 1
                       try
                           if (Tmp(1,2)>Tmp(1,1))  && (Tmp(2,2)>Tmp(2,1))
                           rectangle('Position',[Tmp(1,1),Tmp(2,1),Tmp(1,2)-Tmp(1,1),Tmp(2,2)-Tmp(2,1)],'LineWidth',2,'LineStyle','-','FaceColor',rect2(i) *[0.8 0.8 0.8] + [0.1 0.1 0.1]*3);
                           rectangle('Position',[Tmp(1,1),Tmp(2,1),Tmp(1,2)-Tmp(1,1),Tmp(2,2)-Tmp(2,1)],'LineWidth',2,'LineStyle','-','FaceColor',rect2(i) *[0.8 0.8 0.8] + [0.1 0.1 0.1]);
                       
                           text((Tmp(1,2)-Tmp(1,1))/2+Tmp(1,1),(Tmp(2,2)-Tmp(2,1))/2+Tmp(2,1),[num2str(i) ' | ' num2str(obj.LReg{i}{1}(iStrategy))],'FontSize',10,'Color',[0 1 0]);
                       
                       disp(['obj.LReg(' num2str(i) ') : ' num2str(obj.LReg{i}{1}(iStrategy))  ]);
                       
                       hold on
                       text((Tmp(1,2)-Tmp(1,1))/2+Tmp(1,1),(Tmp(2,2)-Tmp(2,1))/2+Tmp(2,1)-0.02,num2str(i),'FontSize',10,'Color',[0 1 0]);
                           end
                       end
                   end
                   %pause
               end
               xlabel('x');
               ylabel('y');
           catch
               disp('problem in dispLReg');
           end
            
        end %end dispLReg2D
        
    end% end methods
    
    
    methods(Static=true)
        
        function LReg = createLReg(nbStrat)
            LReg{1} = zeros(1,nbStrat);
            % disp('create LReg');
        end
    end %static methods
end