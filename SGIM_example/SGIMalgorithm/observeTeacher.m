%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

function observeTeacher(varargin)
% nargin == 1 means Initialization
% =======================================================================%%
%                          Main Program for imitation of the teacher      %
% =======================================================================%%


%% Initialization
variablesGlobales
saggGlobal


disp('IMITATE TEACHER');




%%=========================================================================
%%=======     Choose The Action : demonstration of the teacher    =========
%%=========================================================================
demonstration   = teacherGivesDemonstration(epmem.teacher, mapp);


%%=========================================================================
%%==================     For Each Demonstration    ========================
%%=========================================================================

for idemo = 1:size(demonstration,1)
       
    demoi           = demonstration(idemo,:); 
    epmem.teacher.listTeacher = [epmem.teacher.listTeacher; size(epmem.listLearning,1) demoi];
    goalOutcome = convertGC(mapp, [demoi(1,end-1:end) 0])';

%%====================     Mastery Analysis     ===========================

	consequence = [];
    %TagRIACB = 0;
    demoi
    %epmem
    %mapp
    %curLowLevel
    consequence = run1observation(curLowLevel, demoi, epmem, mapp);
        
%%====================  update tree regions     ===========================
    
%     example.input    = goalOutcome';
%     example.output   = 0 ;
%     errorValue       = [0,consequence{1}];
%     treeRegions      = updateTree(treeRegions, example, errorValue, TagMax, listReg) ;   
%     QualityCandidate = [QualityCandidate [goalOutcome; consequence{1}]];
% 
%     try
%         for i = 1:length(consequence{2})
%             example.input = consequence{2}(i,:);
%             example.output = 0 ;
%             errorValue = [0,0];
%             treeRegions = updateTree(treeRegions, example, errorValue, TagMax, listReg) ;
%         end
%         
%         
%         Predictor = [Predictor, [goalOutcome; consequence{1}]];
%         for i = 1:length(consequence{2})
%             Predictor = [Predictor, [consequence{2}(i,:)'; 0]];
%         end
%     end
    
    
end

%====================end of the loop for each demonstration ===============
%==========================================================================
%========================================================================== 

disp('end imitate teacher');
end


                                                                           
                                                                           
                                                                           
