function ChxReg = ProportionalDerivativeMin(listRegMin)
% Return the number of a regions showing a high learning progress, according
% to probabilities which depends of the learning progress value
%global listRegMin;


if length(listRegMin) > 2
    %     LRegTmp(1) = 1111;
    if length(listRegMin) > 101
        LRegTmp = ones(1,100)*1111;
        
        cpt = 0;
        for i=length(listRegMin)-99:length(listRegMin)
            cpt = cpt + 1;
            LRegTmp(cpt) = listRegMin{i}{1};
        end
    else
        LRegTmp = ones(1,length(listRegMin)-1)*1111;
        for i=2:length(listRegMin)

            LRegTmp(i) = listRegMin{i}{1};

        end
    end
end

if length(LRegTmp) > 2

    indexMin = [];
    leng = ceil(length(LRegTmp));
    if leng > 10
        leng = 10;
    end
    
    for nbMin = 1:leng
        [val ind] = max(-LRegTmp); % min
        val = -val;

        LRegTmp(ind) = max(LRegTmp);
        if val ~= 999 && val ~= 0 && ~isnan(val) && val ~= Inf && val ~= -Inf
            indexMin = [indexMin [ind ; val]]; %#ok<AGROW>
        end
    end

    if size(indexMin,2) > 1

        indexMin(2,:) = indexMin(2,:) + max(-indexMin(2,:));

        indexMin(2,:) = indexMin(2,:) / sum(indexMin(2,:));
        indexMin(2,:) = indexMin(2,:) / max(indexMin(2,:));
        indexMin(2,:) = 1 - indexMin(2,:);


%%
        leng = size(indexMin,2);

        ProbFinalTmp = cell(1,leng);

        for i=1:leng
			
            if isnan(indexMin(2,i)) 
              indexMin(2,i) = 1;
            end
            ProbFinalTmp{i} = ones(1,ceil(indexMin(2,i)*50))*i; %#ok<AGROW>
        end
              
        leng2 = 0;
        for i=1:leng2
            leng2 = leng2 + length(ProbFinalTmp{i});
        end

        
%%
        
%         Prob = [];
%         for i = 1:size(indexMin,2)
% %             Prob{i} = ones(1,ceil(indexMin(2,i)*1000)); %#ok<AGROW>
%             Prob{i} = ones(1,ceil(indexMin(2,i)*2)); %#ok<AGROW>
%         end
% 
%         ProbFinalTmp = cell(1,size(Prob,2));
%         
%         for i=1:size(Prob,2)
%             ProbFinalTmp{i} = ones(1,length(Prob{i}))*i; %#ok<AGROW>
%         end
%         
%         leng = 0;
%         for i=1:size(Prob,2)
%             leng = leng + length(Prob{i});
%         end
        
        ProbFinal = [];
        
        Offset = 0;
        for i=1:leng
            for j = 1:length(ProbFinalTmp{i})
                ProbFinal(j+Offset) = i; %#ok<AGROW>
            end
            Offset = Offset + length(ProbFinalTmp{i});
        end
        
%         ProbFinal = [];
%         for i=1:size(Prob,2)
%             ProbFinal = [ProbFinal ones(1,length(Prob{i}))*i]; %#ok<AGROW>
%         end
% ProbFinalTmp
% builtin('plot',ProbFinal);

        alea = floor(rand*length(ProbFinal))+1;

        ind = indexMin(1,ProbFinal(alea));
        %
        %         figure(2)
        %         plot(ProbFinal)
        %         drawnow
        
        
        %           indexInf = find(indexMin(2,:) < alea);
        %           if length(indexInf) > 0
        %                indexInf = ceil(rand*indexInf(end));
        %                ind = indexMin(1,indexInf);
        %           else
        %                [ans ind] =  min(ListRegion(:,2));
        %           end

        %             plot(alea,0.2,'ro');
        %             hold on
        %             end

        %                  [ans ind] = min(ListRegion(:,2));
    else
        [ans ind] = firstMin(LRegTmp);
    end

else
    [ans ind] = firstMin(LRegTmp);
end
ChxReg = ind;
