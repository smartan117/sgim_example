function regTeach = teacherSet(filename,nbPoints,ipAddress, portUrbi, instanceVrep, WIN_PATH)
%% creates a teaching set from a data set in filename

%filename : the mat file that contains the data listLearning
%nbPoints : approximative and minimal number of points generated in the teaching set
%regTeach : list of regions
             % regTeach{i,j}{1} is a matrix that defines the [xmin xmax] of the region
             % regTeach{i,j}{2} is a learningEntity chosen in this region
             % regTeach{i,j} can be empty !
             
variablesGlobales

%load(filename)

x = listLearning(:,mapp.dimA +1);
y = listLearning(:,mapp.dimA +2);
z = listLearning(:,mapp.dimA +3);

xmin = -2;
xmax = 2;
ymin = -2;
ymax = 2;
thres = 2;

nbMin =0;
nbin = 0;
%newRegion = [];
listInRegion = [];
regTeach  = []; % list of the regions. 
             % regTeach{i,j}{1} is a matrix that defines the [xmin xmax] of the region
             % regTeach{i,j}{2} is a learningEntity chosen in this region
             
             
%%get the resolution of the regions with histogram  
ftouch = find(z<0.01);
listLearning = listLearning(ftouch,:);
x = x(ftouch);
y = y(ftouch);
z = z(ftouch);
xmin = min(x)
xmax = max(x)
ymin = min(y)
ymax = max(y)

while nbMin<nbPoints
    nbin = nbin + 1;
    [F, xs, ys] = histogram(x, y, 1, nbin,[xmin xmax],[ymin ymax]);
    nbMin = sum(sum(F>thres))
end
imagesc(xs,ys,F);

dx = 1.0001 * (xmax - xmin) / nbin;
dy = 1.0001 * (ymax - ymin) / nbin;

%get the regions and the learningEntity list belonging to the region
regTaught = [];
for i= 1:nbin+1
    for j= 1:nbin+1
        ind = find(xs(j)-dx/2 <= x & x <= xs(j)+dx/2 & ys(i)-dy/2 <= y & y <= ys(i)+dy/2 & z<0.01);
        if sum(ind)>=1
            %disp([num2str(i),' ', num2str(j)]);
            regTeach{j,i}{1} = [xs(j)-dx/2 xs(j)+dx/2; ys(i)-dy/2 ys(i)+dy/2]; 
            listInRegion{j,i} = listLearning(ind,:);
            imax = size(listInRegion{j,i},1);
            if imax>2
                r = randi(imax,1);
                regTaught = [regTaught;  xs(j)-dx/2, xs(j)+dx/2, ys(i)-dy/2, ys(i)+dy/2, 0, listInRegion{j,i}(r,1:27)];
            end
        end
    end
end

%% version we use a random learningEntity in each region
regTaught = [];
for i= 1:nbin+1
    for j= 1:nbin+1
        if ~isempty(listInRegion{j,i})
            imax = size(listInRegion{j,i},1);
            if imax>2
                r = randi(imax,1);
                regTaught = [regTaught;  xs(j), xs(j+1), ys(i), ys(i+1), 0, listInRegion{j,i}(r,1:27)];
            end
        end
    end
end    

%% simply choose in each region the 1st learningEntity and put it in a list
paramList =[];
for j= 1:nbin-1
    for i= 1:nbin-1
        if ~isempty(listInRegion{j,i})
            paramList = [paramList; listInRegion{j,i}(1,:)];
        end
    end
end


for joint = 1:6
            jointList{joint} = [];
end
            
listHist =[];            
nPL =size(paramList,1);
for i= 1:nPL
    tend = paramList(i,25);
    t = 0:0.1:tend;
    pos   = paramList(i,26:27);
    a = paramList(i,1:25);
    Vinv  = CaptureDemo.paramIntoTraj(a);
    for joint = 1:6
       % jointList{joint} = [jointList{joint} Vinv{joint}];
 listHist = [listHist; Vinv{1}', Vinv{2}', Vinv{3}' Vinv{4}' Vinv{5}' Vinv{6}'];
   end
               % end
            
end



%listHist = [jointList{1}', jointList{2}', jointList{3}' jointList{4}' jointList{5}' jointList{6}'];

%% version we use the learningEntity in each region within a range [xmin2
%% xmax2 ymin2 ymax2]
regTaught = [];

for i= 1:nbin+1
    for j= 1:nbin+1
        if ~isempty(listInRegion{j,i})
            kmax = size(listInRegion{j,i},1);
            for k =1:kmax
            if (~(listInRegion{j,i}(k,26)<xmin2 || listInRegion{j,i}(k,26)> xmax2 ||listInRegion{j,i}(k,27)<ymin2 || listInRegion{j,i}(k,27)> ymax2))
                regTaught = [regTaught;  xs(j), xs(j+1), ys(i), ys(i+1), 0, listInRegion{j,i}(k,1:27)];
                break;
            end
            end
        end
    end
end 


%% version we use a random learningEntity in each locality of a regTeachMai
regTaught = [];
[m,n] = size(regTeachMai)
cptMai =1;
for i= 1:m
    for j= 1:n
        if ~isempty(regTeachMai{i,j}) &&size(regTeachMai{i,j},2)>=2 && ~isempty(regTeachMai{i,j}{2})
            cptMai = cptMai +1;
            boundaries = regTeachMai{i,j}{1};
            x1 = boundaries(1,1);
            x2 = boundaries(1,2);
            y1 = boundaries(2,1);
            y2 = boundaries(2,2);
            ind = find(x1 <= x & x <= x2 & y1 <= y & y <= y2 & z<0.01);
            locality = listLearning(ind,:);
            imax = size(locality,1);
            if imax>1
                r = randi(imax,1);
                regTaught = [regTaught;  x1, x2, y1, y2, 0, locality(r,1:27)];
            end
        end
    end
end 

%% plot
clf
indL =find(listTotalAll(:,28)<0.01);
plot(listTotalAll(indL,26),listTotalAll(indL,27),'.r')
hold on
plot(regTaught(:,end-1), regTaught(:,end),'or')
axis square
xlim([-2 2])
ylim([-2 2])


%% for each region choose among the learningEntity list the one that has
%smallest variance
connVrep  = ConnexionRob(urbiIp, urbiPort, vrepIp, vrepInstance, WIN_PATH);
connectSeveral(connVrep, rFish)

pause(3)

regTaught = [];
[m,n] = size(regTeachMai)
cptMai =1;
for i= 1:m
    for j= 1:n   % for each region
        if ~isempty(regTeachMai{i,j}) &&size(regTeachMai{i,j},2)>=2 && ~isempty(regTeachMai{i,j}{2})
            cptMai = cptMai +1;
            boundaries = regTeachMai{i,j}{1};
            x1 = boundaries(1,1);
            x2 = boundaries(1,2);
            y1 = boundaries(2,1);
            y2 = boundaries(2,2);
            ind = find(x1 <= x & x <= x2 & y1 <= y & y <= y2 & z<0.01);
            locality = listLearning(ind,:);
            imax = size(locality,1);
             minStd = Inf;
            if imax > 1
                
                for mov =1:imax    %for each movement in the region
                    movParam = locality(mov,1:25)
                    listRepeat=[];
                    for rep =1:5  % repeat the movement 5 times
                        [dist pos] = experiment(movParam,-1);
                        if pos{3} < 0.05
                            listRepeat = [listRepeat; pos{1} pos{2}];
                        end
                    end
                    stdRepeat = sum(std(listRepeat));
                    moyRepeat = mean(listRepeat,1);
                    if minStd>stdRepeat
                        minStd = stdRepeat;
                        newRegTaught = [ x1, x2, y1, y2, 0, movParam moyRepeat];
                        disp( ['i=', num2str(i), ' ,j=', num2str(j), ': ', num2str(minStd)] );
                    end
                    if minStd<0.01
                        break;
                    end
                end  % end for each movement
                
                regTaught = [regTaught; newRegTaught];
                save teacherSetTemp4
            end
        end
    end
end


%% connect to urbi servers
%urbiClient = connectToVrep(ipAddress, portUrbi, instanceVrep, win_path);
% % 
% % clientList = [];
% % connVrep.instance0 = 3
% % instanceVrep = 1;
% for instanceV = connVrep.instance0:connVrep.instance0+connVrep.nbClient-1
%       port       = connVrep.port0 +instanceV*1000;
%       connVrep = connectToVrep(connVrep, port, instanceV, WIN_PATH);
%       initialiseTimeUrbi(urbiClient, rFish.tend, rFish.timestep);
%       connVrep.clientList = [connVrep.clientList; urbiClient];
% end
connectSeveral(connVrep, rFish);
pause

imax = size(listInRegion,1);
jmax = size(listInRegion,2);
gOutcome = zeros(1,3);

for i= 1:imax
    for j= 1:jmax %for each region 
        if ~isempty(listInRegion{i,j}) &&  size(regTeach{i,j},2) ==1 
            nbInRegion = size(listInRegion{i,j},1);
            minStd = Inf;
            for k =1:nbInRegion %for each learningEntity in the region
                movParam = listInRegion{i,j}(k,1:mapp.dimA);
                listRepeat=[];
                for rep =1:1
                    [dist pos] = experiment(movParam,-1);
                    if pos{3} < 0.05
                        listRepeat = [listRepeat; pos{1} pos{2}];
                    end
                end
                stdRepeat = sum(std(listRepeat));
                moyRepeat = mean(listRepeat,1);
                if minStd>stdRepeat 
                    minStd = stdRepeat;
                    regTeach{i,j}{2}= [movParam moyRepeat];
                    disp( ['i=', num2str(i), ' ,j=', num2str(j), ': ', num2str(minStd)] );
                end               
                if minStd<0.001
                    break;
                end
            
            end
             filename = ['../data/regTeach3_', num2str(portUrbi),'_',num2str(instanceVrep)];
             save(filename)
        end
    end
end

%plot regTeach
m = size(regTeach,1)
n = size(regTeach,2)
listTeacher = [];
for i=1:m
    for j=1:n % for each region
        if ~isempty(regTeach{i,j}) && size(regTeach{i,j},2)==2 && ~isempty(regTeach{i,j}{2})
            teaching = regTeach{i,j}{2};
            listTeacher = [listTeacher; teaching];
            plot(teaching(mapp.dimA+1), teaching(mapp.dimA+2), '*m');
            hold on;
        end
    end
end

end

function completes
%%script, pas a executer en simple fonction
m = size(regTeach,1)
n = size(regTeach,2)
listTeacher = [];
for i=1:m
    for j=1:n % for each region
        if ~isempty(regTeach{i,j}) &&size(regTeachT{i,j},2)==1
            regTeachT(i,j)=regTeach(i,j) ;
        end    
    end
end

end
        
function loadFilesAndComplete
dirName =  '~/Documents/Dropbox/Dropbox/data0408/'
filesReg = dir([dirName,'regTeach*mat'])

for i=1:numel(filesReg)
    filesReg(i).name
    dirName =  '~/Documents/Dropbox/Dropbox/data0408/';
    load([dirName,filesReg(i).name])
    m = size(regTeach,1)
    n = size(regTeach,2)
    listTeacher = [];
    for i=1:m
        for j=1:n % for each region
            if ~isempty(regTeach{i,j}) &&size(regTeachTemp{i,j},2)==1
                regTeachTemp(i,j)=regTeach(i,j) ;
            end
        end
    end
end

end


function testTeacherSet(regTeach,ipAddress, portUrbi, instanceVrep, win_path)
%tests the list given by regTeach by repeating the movements several times
variablesGlobales

m = size(regTeach,1)
n = size(regTeach,2)
listRepeatAll =[];

%% connect to urbi servers
%urbiClient = connectToVrep(ipAddress, portUrbi, instanceVrep, win_path);
clientList = [];
instanceVrep = 1;
for instanceV = connVrep.instance0:connVrep.instance0+NB_CLIENT-1
      port       = connVrep.port0 +instanceV*1000;
      urbiClient = connectToVrep(connVrep, port, instanceV, WIN_PATH);
      initialiseTimeUrbi(rFish, urbiClient);
      connVrep.clientList = [connVrep.clientList; urbiClient];
end
pause

for i=1:m
    for j=1:n % for each region
        if ~isempty(regTeach{i,j}) && (size(regTeach{i,j},2)==2)
            teaching = regTeach{i,j}{2};
            movParam = teaching(1:mapp.dimA);
            listRepeat=[];
            for rep =1:5 % repeat 5 times the same movement
                [dist pos] = experiment(movParam,-1);
                listRepeat = [listRepeat; pos{1} pos{2}];
            end
            color = rand(1,3);
            scatter(teaching(mapp.dimA+1), teaching(mapp.dimA+2), 20,color,'s');
            hold on;
            plot([teaching(mapp.dimA+1);listRepeat(:,1)],[teaching(mapp.dimA+2); listRepeat(:,2)], 'o-', 'Color', color);
            hold on;
            axis([-1 1 -1.5 1.5])
            listRepeatAll = [listRepeatAll; listRepeat];
        end
    end
end

end

function check3d(filename)
load filename

m = size(regTeachTemp,1)
n = size(regTeachTemp,2)
for i=1:m
    for j=1:n 
        % for each region
        if ~isempty(regTeachTemp{i,j}) && (size(regTeachTemp{i,j},2)==2)
            demo = regTeachTemp{i,j}{2};
            movParam = demo(1,1:mapp.dimA);
            [dist pos] = experiment(movParam,-1);
            if iscell(pos) && size(pos,2) == 3 && pos{3} < 0.05
                regTeach(i,j)=regTeachTemp(i,j) ;
            end
            
        end
    end
end

save regTeachFile regTeach
end

function plotTeacherEval


load listTeacherFile
load listEvalPoints

h2 = plot(evalPerf.listEvalPoints(:,1), evalPerf.listEvalPoints(:,2),'ob', 'MarkerSize',7, 'MarkerFaceColor','b')
hold on

%plot regTeach
m = size(regTeach,1)
n = size(regTeach,2)
listTeacher = [];
for i=1:m
    for j=1:n % for each region
        if ~isempty(regTeach{i,j}) && size(regTeach{i,j},2)==2 && ~isempty(regTeach{i,j}{2})
            teaching = regTeach{i,j}{2};
            listTeacher = [listTeacher; teaching];
            plot(teaching(mapp.dimA+1), teaching(mapp.dimA+2), 'dr', 'MarkerSize',10, 'MarkerFaceColor','r');
            hold on;
        end
    end
end

xlabel('y1');
ylabel('y2');
axis([-2 2 -2 2])
axis square

end
