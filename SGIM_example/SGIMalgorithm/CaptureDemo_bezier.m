classdef CaptureDemo
    
    
    methods(Static = true)
        
        %program to capture demonstrations given by human by the physical
        %robot
        function runCaptureDemo(ipAd)
            %% set path
            path(path,'/Users/mai/Programmes/Urbi/liburbi-matlab/')
            path(path,'/Users/mai/Programmes/Urbi/liburbi-matlab/lib')
            path(path,'/Users/mai/Programmes/Urbi/liburbi-matlab/lib/helpers')
            path(path,'/Users/mai/Programmes/Urbi/liburbi-matlab/lib/helpers/tcp_udp_ip')
            
            
            saggGlobal
            variablesGlobales
            
            %if isempty(ipAd)
            ipAd      = '127.0.0.1';
            %end
            instanceV = 1;
            nbClient  = 1;
            portUrbi = 51000;
            
            rFish     = FishingArm(6,4,0.01,3,0.3, WIN_PATH);
            connVrep  = ConnexionRob(nbClient,50000, instanceV, ipAd, WIN_PATH);
            connVrep  = connectToPhysical(connVrep, portUrbi, instanceV);
            pause(3);
            
            figure('Name', 'Task Space','Position',[0 0 1000 1000]);
            grid on
            axis([-2 2 -2 2]);
            hold on
            set(gca,'XTick',[-1:0.2:1],...
                'YTick',[-1:0.2:1])
            plot(0,0,'s','MarkerFaceColor','b')
            hold on
            grid on
            
            pause(2)
            
            for i=121:130
                urbiSend(connVrep.urbiClient, 'b;');
                urbiSendFile(connVrep.urbiClient,'FishingRod_monitorHook.u');
                
                for count =6:-1:1
                    pause(1)
                    disp( ['count down ', num2str(count)]);
                end
                outPos{i} = readPosObj(connVrep)
                urbiSend(connVrep.urbiClient,' vrep.simSetObjectPosition(mark,-1,[pos[0], pos[1], 0]);' )
                urbiGetVariable(connVrep.urbiClient,'pos' )
                urbiGetVariable(connVrep.urbiClient,'simEnd' )
                %    pause(6);
                urbiSend(connVrep.urbiClient, 'e;');
                urbiSend(connVrep.urbiClient,' vrep.simSetObjectPosition(mark,-1,[pos[0], pos[1], 0]);' )
                
                %%plotting
                for j=1:i-1
                    scatter(outPos{j}{1}, outPos{j}{2}, 40, [.5 0 0],'filled');
                    hold on
                    text(outPos{j}{1}, outPos{j}{2}, num2str(j));
                end
                
                scatter(outPos{i}{1}, outPos{i}{2}, 150, [.5 0 0],'filled');
                hold on
                text(outPos{i}{1}, outPos{i}{2}, num2str(i));
                
                grid on
                axis([-2 2 -2 2]);
                hold on
                set(gca,'XTick',[-1:0.2:1],...
                    'YTick',[-1:0.2:1])
                plot(0,0,'s','MarkerFaceColor','b')
                grid on
                axis([-2 2 -2 2]);
                hold off
                
                %% record trajectory
                b{i} =  [urbiGetVariable(connVrep.urbiClient,'Global.results' )]
            end
            urbiSend(connVrep.urbiClient, 'c;');
            
            
            save('demoTestMai', 'b','outPos');
            
        end
        
        %The vrep robot repeats exactly the sequence of joint positions
        %demonstrated
        function repeatExact(movDemo)
            %% connect to urbi
            saggGlobal
            variablesGlobales
            ipAd      = '127.0.0.1';
            instanceV = 1;
            nbClient  = 1;
            rFish     = FishingArm(6,4,0.01,tend,0.3, WIN_PATH);
            connVrep  = ConnexionRob(nbClient,50000, instanceV, ipAd, WIN_PATH);
            connVrep  = connectSeveral(connVrep, rFish);
            
            %%
            MxAllowSqD=1; % Max. allowed Square Distance between original and fitted data
            len = length(movDemo);
            %t = linspace(0,tend,len)'
            X =[];
            
            
            % copy and filter timestamp
            iPrev = 1;
            tPrev = 0;
            t0 = movDemo{1}{1};
            for i=1:length(movDemo)
                t2(i) = movDemo{i}{1}-t0;
                if t2(i)~=t2(iPrev)
                    for j=iPrev:i-1
                        t(j) = t2(j) + (j-iPrev)*(t2(i)-t2(iPrev))/(i-iPrev);
                    end
                    iPrev = i;
                    t(i) = t2(i);
                end
            end
            t= t';
            tend = t(end);
            
            %copy joint position data
            for joint = 1:6
                for i=1:length(t)
                    y1(i,joint) = movDemo{i}{joint +1};
                end
            end
            
            
            for joint = 1:6
                % Mise sous type String
                y='[';
                for i=1:length(t)-1
                    %     x=strcat(x,num2str(t(i)),',');
                    y=strcat(y,num2str(y1(i,joint)),',');
                end
                % x=strcat(x,num2str(t(end)),']');
                y=strcat(y,num2str(y1(end,joint)),']');
                
                % Envoie de la variable dans urbi
                % strcat(var,'= ',y,';')
                var = strcat('TargetPositionJoint',num2str(joint));
                urbiSend(connVrep.urbiClient,strcat(var,'=',y,';'));
            end
            
            urbiSendFile(connVrep.urbiClient,'StartSimulation.u');
            posHook   = getPosObj(connVrep)
            
        end
        
        %Bezier regression of a demonstration (sequence of joint positions
        function X = transposeDemo(movDemo)
             %%
            MxAllowSqD=1; % Max. allowed Square Distance between original and fitted data
            len = length(movDemo);
            %tend = movDemo{end}{1};
            %t = linspace(0,tend,len)'
            X =[];
            y1 = [];
            Mat = [];
            MatI = [];
            t = [];
            
            % copy and filter timestamp
            iPrev = 1;
            tPrev = 0;
            t0 = movDemo{1}{1};
            for i=1:length(movDemo)
                t2(i) = movDemo{i}{1}-t0;
                if t2(i) ~= t2(iPrev)
                    for j = iPrev:i-1
                        t(j,1) = t2(j) + (j-iPrev)*(t2(i)-t2(iPrev))/(i-iPrev);
                    end
                    iPrev = i;
                    t(i,1) = t2(i);
                end
            end
            tend = t(end,1);
            
         %   figure('Name', 'repeatDemo');
            %copy joint position data
            for joint = 1:6
                for i=1:length(t)
                    y1(i,1) = movDemo{i}{joint +1};
                end
                
                Mat = [t, y1];
                
                ei= length(t);
                ibi=[1;ei]; %first and last point are taken as initial break points
                
                [p0mat,p1mat,p2mat,p3mat,fbi]= MovementEncoder.bzapproxu(Mat,MxAllowSqD,ibi);
                X = [X p0mat(2),p1mat(2),p2mat(2),p3mat(2)];
                
                %check
%                 [MatI]=MovementEncoder.BezierInterpCPMatSegVec(p0mat,p1mat,p2mat,p3mat,fbi);
%                 
%                 subplot(2, 3, joint);
%                 plot(t,y1,'-r')
%                 hold on
%                 plot(MatI(:,1), MatI(:,2), '-b')
            end
            
            X = [X tend];
             
        end %end transposeDemo
        
        % vrep robot repeats the demonstrated movement after Bezier
        % regression
        function posHook = repeatDemo(movDemo)
            
            % %% connect to urbi
            saggGlobal
            variablesGlobales
            ipAd      = '127.0.0.1';
            instanceV = 1;
            nbClient  = 1;
            % rFish     = FishingArm(6,4,0.01,tend,0.1, WIN_PATH);
            % connVrep  = ConnexionRob(nbClient,50000, instanceV, ipAd, WIN_PATH);
            % connVrep  = connectSeveral(connVrep, rFish);
       
            X = CaptureDemo.transposeDemo(movDemo);
            tend = X(end);
            rFish     = FishingArm(6,4,0.01,tend,0.3, WIN_PATH);

            moveRobot(rFish, X, connVrep);
            posHook   = getPosObj(connVrep)
    
            
        end
        
        % vrep robot repeats, after Bezier regression,  all demonstration
        % movements of the data file filename.mat
        function  repeatDemoList(filename)
            load(filename)
            h = figure('Name','Difference Demo and Repeat')
            
            nbDemo = length(b)
            for i=1:nbDemo
                movDemo = b{i}
                posRepeat = CaptureDemo.repeatDemo(movDemo)
                outPos{i}
                
                figure(h)
                scatter(posRepeat{1}, posRepeat{2}, 'ob');
                text(posRepeat{1}, posRepeat{2}, num2str(i),'Color','blue');
                hold on
                scatter(outPos{i}{1}, outPos{i}{2}, 'or');
                hold on
                text(outPos{i}{1}, outPos{i}{2}, num2str(i),'Color','red');
                set(gca,'XTick',[-1:0.2:1],...
                    'YTick',[-1:0.2:1])
                plot(0,0,'s','MarkerFaceColor','b')
                grid on
                axis([-2 2 -2 2]);
            end
        end % end repeatDemoList
        
        % plots the demonstration set of filename.mat
        function plotDemo(filename)
            load(filename)
            nbDemo = length(b)
            
            figure('Name', 'Demonstrations in the Task Space','Position',[0 0 1000 1000]);
            grid on
            axis([-2 2 -2 2]);
            hold on
            set(gca,'XTick',[-1:0.2:1],...
                'YTick',[-1:0.2:1])
            plot(0,0,'s','MarkerFaceColor','b')
            hold on
            grid on
            
            for j=1:nbDemo
                scatter(outPos{j}{1}, outPos{j}{2}, 40, [.5 0 0],'filled');
                hold on
                text(outPos{j}{1}, outPos{j}{2}, num2str(j));
            end
            
            grid on
            axis([-2 2 -2 2]);
            hold on
            set(gca,'XTick',[-1:0.2:1],...
                'YTick',[-1:0.2:1])
            plot(0,0,'s','MarkerFaceColor','b')
            grid on
            axis([-2 2 -2 2]);
            hold off
        end
        
        % transform the demonstration set into a list 
        %(to make it compatible with the former format)
        function intoList(filename)
            nbin = 20;
            
            load(filename)
            nbDemo = length(b)-1
            minX = Inf;
            maxX = -Inf;
            minY = Inf;
            maxY = -Inf;
            listDemo = [];
            
            % determine the boundary of the explored space
            for i=1:nbDemo
                if (outPos{i}{3} < 0.01)
                    if (outPos{i}{1} > maxX) 
                        maxX = outPos{i}{1};
                    end
                    if (outPos{i}{1} < minX) 
                        minX = outPos{i}{1}
                        i
                    end
                    if (outPos{i}{2} > maxY)
                        maxY = outPos{i}{2};
                    end
                    if (outPos{i}{2} < minY)
                        minY = outPos{i}{2};
                    end
                    X = CaptureDemo.transposeDemo(b{i})
                    listDemo(i,:) = [X outPos{i}{1} outPos{i}{2}];
                end
            end
            
            % partition task space 
            dx = (maxX - minX)/nbin;
            dy = (maxY - minY)/nbin;
            for i= 1:nbin
                for j=1:nbin
                    x = minX + (i-1)*dx;
                    y = minY + (j-1)*dy;
                    regTeach{i,j}{1} =[x x+dx; y y+dy]; 
                end
            end
            
            for idemo=1:nbDemo-2
               if(~isempty(listDemo(idemo,:)))
                   x = listDemo(idemo,end-1);
                   y = listDemo(idemo,end);
                   i = floor( (x-minX)/dx )+1;
                   j = floor( (y-minY)/dy )+1;
                   regTeach{i,j}{2} = listDemo(idemo,:);
               end
            end
            
        end %end intoList
        
    end
    
    
end
