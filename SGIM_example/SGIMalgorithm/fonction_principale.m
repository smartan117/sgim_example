%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

function fonction_principale(MODE,must_eval)
% function principale launches the appropriate algorithm
% MODE : string : name of the algorithm to experiment with
%                 RIAC: sagg-riac (if no teacher) or sgim-d
%                 SGIMACTS:  sgim-acts (active version of sgim-d)
%                 REQUESTMIMIC: requests demo to copy movment parameters
%                 EMULATE: requests demo to copy the outcome, 
%                 SGIMIM  : sgim-d actively decides whether to imitate or
%                 explore auton
%                 TEACHERBEFORE : all demonstrations in the beginning, then
%                 sagg-riac
%                 IMITATION : observes the demo then repeats it numberIter
%                 times (with small variations on the mvt parameter).
%                 Competence with respect to demo outcome.
%                 OBSERVATION: observes the demo (non self experimentation)
%                 MIMIC :  observes the demo then repeats it numberIter
%                 times (with small variations on the mvt parameter).
%                 Competence with respect to self-determined outcome.
%                 RANDOMPARAM : random exploration of the parameter space
saggGlobal
variablesGlobales
global costStrat


if nargin < 2
    must_eval = true;
end

must_stop = false;
must_pause = false;
stop_file = '~/stop';
pause_file = '~/pause';

%==========================================================================
%=========================                    =============================
%=========================  Learning SAGG     =============================
%=========================                    =============================
%==========================================================================



%============================   RIAC      =================================
% explores autonomously, goal-oriented exploration
%==========================================================================
numberAction

if strcmp(MODE,'RIAC')
    disp('* ENTER SAGG RIAC MODE');
    
    while curLowLevel.nbExperiments <= numberAction && ~must_stop                   %tant que le nombre limite de mouvement n'est pas atteint
        
        [must_stop, must_pause] = check_stop_pause(stop_file, pause_file);

        if must_pause
            pause(5);
        else
        
            envSimu = randomizeContext(envSimu);

            Main_RIAC_CB;


            disp(['FONCTION_PRINCIPALE SAGG RIAC : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
            disp(['FONCTION_PRINCIPALE SAGG RIAC : * Number of Goals : ' num2str(size(QualityCandidate,1)) ])
        end
    end
    save(fileMemo); 
    
    %analyse the results
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    end
    save(fileMemo);   
    disp('* END SAGG RIAC MODE');
    
elseif strcmp(MODE,'SGIMD')
%========================== SGIM-D   ======================================
% explores autonomously, until when the teacher
% gives a demonstration, when it imitates
%==========================================================================

    disp('* ENTER SGIMD MODE');
    
    while curLowLevel.nbExperiments <= numberAction                               %tant que le nombre limite de mouvement n'est pas atteint
        envSimu = randomizeContext(envSimu);
        
        if curLowLevel.flagTeach == 0
            Main_RIAC_CB;
        elseif curLowLevel.flagTeach == 1
            idTeacher = randi(length(hTeacher)); %randomly changing teacher
            if hTeacher(idTeacher).present
                Main_ImitateTeacher(idTeacher);
            end
            curLowLevel.flagTeach = 0;
        end
        
        disp(['FONCTION_PRINCIPALE SGIMD : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
        disp(['FONCTION_PRINCIPALE SGIMD : * Number of Goals : ' num2str(size(QualityCandidate,1)) ])
    end
    
    %analyse the results
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    end
    save(fileMemo);
    disp('* END SGIMD MODE');
    
        
    
    
elseif strcmp(MODE,'SGIMACTS')
%========================== SGIM-ACTS  ====================================
% decides which strategy to adopt, using a regions mapping
%==========================================================================

    disp('* ENTER SGIMACTS MODE');
    hTeacher(1).present = 1;
    
   while curLowLevel.nbExperiments <= numberAction && ~must_stop                        %tant que le nombre limite de mouvement n'est pas atteint
       
       [must_stop, must_pause] = check_stop_pause(stop_file, pause_file);

        if must_pause
            pause(5);
        else
       
            envSimu = randomizeContext(envSimu);
            Main_SGIMACTS;


            disp(['FONCTION_PRINCIPALE SGIMACTS : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
            disp(['FONCTION_PRINCIPALE SGIMACTS : * Number of Goals : ' num2str( size(QualityCandidate,1) )])
        end
   end
    
   save(fileMemo); 
   
    %analyse the results
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    end
    save(fileMemo);
        
    disp('* END SGIMACTS MODE');
    
elseif strcmp(MODE,'REQUESTMIMIC')
%========================== REQUESTMIMIC  ====================================
% request a demonstration and mimic the action
%==========================================================================
    disp('* ENTER REQUESTMIMIC MODE');
    hTeacher(1).present = 1;
    
    %      global costStrat
    %            costStrat    = [1 1 1 1];
    
    %costStrat    = [1 1 ];
    
    while curLowLevel.nbExperiments <= numberAction                               %tant que le nombre limite de mouvement n'est pas atteint
        envSimu = randomizeContext(envSimu);
        Main_RequestMimic;
        
        
        disp(['FONCTION_PRINCIPALE REQUESTMIMIC : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
        disp(['FONCTION_PRINCIPALE REQUESTMIMIC : * Number of Goals : ' num2str( size(QualityCandidate,1) )])
        
    end
    
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    end
    
    
    save(fileMemo);
    
    disp('* END REQUESTMIMIC MODE');
    
    
elseif strcmp(MODE,'EMULATE')
%========================== EMULATE  ====================================
% emulate the observed outcome
%==========================================================================
    disp('* ENTER EMULATE MODE');
    hTeacher(1).present = 1;
    
    %global costStrat
    %            costStrat    = [1 1 1 1];
    
    %costStrat    = [1 1 ];
    
    while curLowLevel.nbExperiments <= numberAction                               %tant que le nombre limite de mouvement n'est pas atteint
        envSimu = randomizeContext(envSimu);
        Main_Emulate;
        
        
        disp(['FONCTION_PRINCIPALE EMULATE : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
        disp(['FONCTION_PRINCIPALE EMULATE : * Number of Goals : ' num2str( size(QualityCandidate,1) )])
        
    end
    
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    end
    save(fileMemo);
    
    disp('* END EMULATE MODE');
    
          
elseif strcmp(MODE,'SGIMIM')
%========================== SGIM-IM  ====================================
% Interactive Learning to monitor which makes most progress
%==========================================================================

    disp('* ENTER SGIMIM MODE');
    hTeacher(1).present = 1;  %or else error
    %     costS = 1/10; %cost of social learning over autonomous exploration
    %     nbMeta = 100;
    %     lambdaMeta = 0.95;
    % costS = 1/7; %cost of social learning over autonomous exploration
    % nbMeta = 75;
    % lambdaMeta = 0.97;
    costS = 1/100; %cost of social learning over autonomous exploration
    nbMeta = 20;
    % lambdaMeta = 1;
    %     for i =1:(nbMeta*10+1)
    %        lambdaMetaVec(i,1) = lambdaMeta^(10*nbMeta+1-i);
    %     end
    
    %     global PAL
    %     global PSL1
    %     global PSL2
    
    global ProgressL
    PAL = [];
    PSL1 = [];
    PSL2 = [];
    ProgressL =[];
    
    while curLowLevel.nbExperiments <= numberAction                               %tant que le nombre limite de mouvement n'est pas atteint
        envSimu = randomizeContext(envSimu);
        
        
        %% request help?
        selectStrategySGIMIM();
        
        
        disp(['FONCTION_PRINCIPALE SGIMIM : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
        disp(['FONCTION_PRINCIPALE SGIMIM : * Number of Goals : ' num2str( size(QualityCandidate,1) )])
        if mod(curLowLevel.nbExperiments,50) <= 1
            %            fileMemo = ['../data/MemoTeachRIAC',num2str(TEACHER),'_',date,'_', num2str(instance0)];
            save(fileMemo)
            fp_plotSGIMIM();
        end
    end
    
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    end
    save(fileMemo);
    %     title('goals chosen for obstacle');
    
    disp('* END SGIMIM MODE');
%{    
elseif strcmp(MODE,'SGIMIM2')
    disp('* ENTER SGIMIM 2 teachers MODE');
    hTeacher(1).present = 1;  %or else error
    %     costS = 1/10; %cost of social learning over autonomous exploration
    %     nbMeta = 100;
    %     lambdaMeta = 0.95;
    % costS = 1/7; %cost of social learning over autonomous exploration
    % nbMeta = 75;
    % lambdaMeta = 0.97;
    costS = 1/100; %cost of social learning over autonomous exploration
    nbMeta = 20;
    gamma = 0.1;
    
    % lambdaMeta = 1;
    %     for i =1:(nbMeta*10+1)
    %        lambdaMetaVec(i,1) = lambdaMeta^(10*nbMeta+1-i);
    %     end
    
    PAL = [];
    PSL1 = [];
    PSL2 = [];
    ProgressL = [];
    teacherid = 0;
    %indLastT = 0;
    %    nbLastT = 0;
    %yLastT = zeros(size(mapp.dimY));
    
    while curLowLevel.nbExperiments <= numberAction                               %tant que le nombre limite de mouvement n'est pas atteint
        envSimu = randomizeContext(envSimu);
        teacherid =  selectStrategySGIMIM2();
        
        
        
        nbGoal = nbGoal + 1;
        disp(['FONCTION_PRINCIPALE SGIMIM2 : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
        disp(['FONCTION_PRINCIPALE SGIMIM2 : * Number of Goals : ' num2str(nbGoal)])
        if mod(size(epmem.listGoal,1),5) <= 1
            %            fileMemo = ['../data/MemoTeachRIAC',num2str(TEACHER),'_',date,'_', num2str(instance0)];
            save(fileMemo)
            fp_plotSGIMIM();
        end
    end
    
    evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    save(fileMemo);
    fp_plotEndSGIMIM();
    
    disp('* END SGIMIM 2 teachers MODE');
%}    

elseif strcmp(MODE,'TEACHERBEFORE')
%========================= TEACHERBEFORE ==================================
%  RIAC  but all the teachings are given before
%==========================================================================

disp('* ENTER SAGG RIAC TEACHERBEFORE MODE');
    Main_ImitateTeacher(0);
    for i= 1:100
        Main_ImitateTeacher(1);
    end
    
    hTeacher(1).present = 0;
    Main_RIAC_CB(0);
    
    while curLowLevel.nbExperiments <= numberAction
        
        Main_RIAC_CB;
        disp(['FONCTION_PRINCIPALE TEACHERBEFORE : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
        disp(['FONCTION_PRINCIPALE TEACHERBEFORE : * Number of Goals : ' num2str( size(QualityCandidate,1) )])
        if mod(size(epmem.listGoal,1),5) <= 1
            %            fileMemo = ['../data/MemoTeachRIAC',num2str(TEACHER),'_',date,'_', num2str(instance0)];
            save(fileMemo)
        end
    end
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    end
    save(fileMemo);

    
elseif strcmp(MODE,'IMITATION')
%======================== IMITATION ONLY ==================================
%  Learn by imitation of the policies
%==========================================================================
    disp('* ENTER IMITATION MODE');
    
    %    hTeacher = HumanTeacher(hTeacher.present, hTeacher.frequency, 'regTaughtMai0805.mat');
    curLowLevel.nbImitate = hTeacher.frequency ;
    ndemo = 0;
    %Main_ImitateTeacher(1);
    
    while curLowLevel.nbExperiments <= numberAction && ~must_stop            %tant que le nombre limite de mouvement n'est pas atteint
        [must_stop, must_pause] = check_stop_pause(stop_file, pause_file);       
        
        if must_pause
            pause(5);
        else
            ndemo = ndemo + 1;
            flagTeach = 1;
            % NB_IMITATE = 0;
            Main_ImitateTeacher(1);
            disp(['FONCTION_PRINCIPALE IMITATE : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
        end
    end
    
    
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem, curLowLevel, envSimu, curLowLevel.nbExperiments);
    end
    save(fileMemo);
    
    disp('* END IMITATION MODE');
    
    
elseif strcmp(MODE,'OBSERVATION')
%==============================   OBSERVATION      ========================
%=========================================================================%
    disp('* ENTER OBSERVATION MODE');
    
    curLowLevel.nbImitate = 1 ;
    curLowLevel.randMax   =0;
    observeTeacher(0);
    
    while curLowLevel.nbExperiments <= numberAction+1                               %tant que le nombre limite de mouvement n'est pas atteint
        if (mod(curLowLevel.nbExperiments,hTeacher.frequency)==1)
            flagTeach = 1;
            % NB_IMITATE = 0;
            observeTeacher();
        end
        
        if(mod(curLowLevel.nbExperiments,1000) == 1)
            evalPerf = evaluationPerfOnce(evalPerf, epmem, curLowLevel, envSimu, curLowLevel.nbExperiments);
            save( [fileMemo,'_nb_',num2str(curLowLevel.nbExperiments)] );
        end
        
    end
    
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem, curLowLevel, envSimu, curLowLevel.nbExperiments);
    end
    save(fileMemo);
    
    disp('* END OBSERVATION MODE');
    
    
elseif strcmp(MODE,'MIMIC')
%=======================   MIMIC THE POLICY    ============================
%==========================================================================
     disp('* ENTER MIMIC MODE');
    hTeacher(1).present = 1;
    %            costStrat    = [1 1 1 1];
    
    %costStrat    = [1 1 ];
    
    while curLowLevel.nbExperiments <= numberAction                               %tant que le nombre limite de mouvement n'est pas atteint
        envSimu = randomizeContext(envSimu);
        Main_Mimic;
        
        
        disp(['FONCTION_PRINCIPALE MIMIC : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
        disp(['FONCTION_PRINCIPALE MIMIC : * Number of Goals : ' num2str( size(QualityCandidate,1) )])
        
    end
    
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    end
    save(fileMemo);
    
    disp('* END MIMIC MODE');
    
   
elseif strcmp(MODE,'RANDOMPARAM')
%============   RANDOM SEARCH IN THE POLICY PARAMETER SPACE     ===========
%==========================================================================
     disp('* ENTER RANDOM ON MOVEMENT PARAMETERS');
    curLowLevel.gOutcome = 0;
    while curLowLevel.nbExperiments <= numberAction  && ~must_stop                             %tant que le nombre limite de mouvement n'est pas atteint
        [must_stop, must_pause] = check_stop_pause(stop_file, pause_file);

        if must_pause
            pause(5);
        else
            curLowLevel.progressReachedOutcome = [];
            task = randi(nbOutcomeTypes);
            envSimu = randomizeContext(envSimu);
            context = getContext(envSimu);
            %         envSimu.pucks(1)
            movParam   = randParameters(envSimu);
            movParam   = normaliseParam(envSimu, movParam);
            dist       = experiment(movParam, 100, context, task);
            if mod(size(epmem{task}.listLearning,1),1000) == 1
                %          fileMemo = ['../data/MemoRandomParam_',date,'_', num2str(instance0)];
                save( [fileMemo,'_nb_',num2str(curLowLevel.nbExperiments)] );
            end

            disp(['FONCTION_PRINCIPALE RANDOM ON POLICY PARAMETERS : * Number interation : ',num2str(curLowLevel.nbExperiments)]);
        end
    end
    
    %analyse the results
    if must_eval && evalPerf.mapEvalNN(end,end) < curLowLevel.nbExperiments
        evalPerf = evaluationPerfOnce(evalPerf, epmem, curLowLevel, envSimu);
    end
    save(fileMemo);
    disp('* END RANDOMPARAM MODE');
else
    disp('/\ BAD CHOICE IN FONCTION PRINCIPALE')
end

if must_eval
    visualiseExploration;
end
%fp_plotEnd();

%==========================================================================
end