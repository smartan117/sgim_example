%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

function Main_RIAC_CB(varargin)
%
% =======================================================================%%
%                          Main Program R-IAC Competence Based            %
% =======================================================================%%


%% Initialization
variablesGlobales
saggGlobal

disp('MAIN RIAC CB');


%%=========================================================================
%%=======================     Choose The Goal     =======================
%%=========================================================================

envSimu       = randomizeContext(envSimu);
context       =  getContext(envSimu);
context01     = (context +1)/2;
goalTaskType  =  randi(nbOutcomeTypes);


alea = rand; ...
    % Choose a Random Action in the whole space MODE2 ======================================
TagMax = 1;

goalTaskType   = randi([1,nbOutcomeTypes]);
goalOutcome = randMinMax(mapp{goalTaskType}.dimY, 1, 0, 1);
iStrat  = 1;



if alea <ProbMode1
    % Select the Action with Proportional Derivative MODE1 =================
    [goalOutcome iStrat goalTaskType] =  selectGoalMode1(partReg, context01, mapp);
    
elseif alea < ProbMode1+ProbMode3
    % Select the Action With Max in Exploration MODE3 ==============
    [goalOutcome iStrat goalTaskType] = selectGoalMode3(partReg, context01, mapp );
else
    disp('MAINRIACCB: SLECT GOAL RANDOMLY');
end





%%=========================================================================
%%====================     Mastery Analysis     ===========================
%%=========================================================================
iStrat = 1;
consequence = {}; %[]
curLowLevel.curTask = goalTaskType;
curLowLevel.gOutcome = convertCG(mapp{goalTaskType}, goalOutcome');
if(isempty(epmem{goalTaskType}.listLearning))
    nbIter      = curLowLevel.numberIteration ;
    curLowLevel.numberIteration = 1;
    consequence = run1goal(curLowLevel,curLowLevel.gOutcome, context, epmem, mapp, envSimu)
    curLowLevel.numberIteration = nbIter;
end
consequence = run1goal(curLowLevel,curLowLevel.gOutcome, context, epmem, mapp, envSimu)


%%=========================================================================
%%====================  update tree regions     ===========================
%%=========================================================================

example          = goalOutcome(:);
example          = example(:);
errorValue       = consequence{1};
try
    partReg          = updatePartitionRegions(partReg,  example, errorValue, TagMax, iStrat, curLowLevel.curTask) ;
catch
    disp('erreur');
end
QualityCandidate = [QualityCandidate; [goalOutcome(:)' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY,1) consequence{1} curLowLevel.nbExperiments goalTaskType iStrat]];

Predictor = [Predictor; [goalOutcome' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) errorValue curLowLevel.nbExperiments goalTaskType iStrat]];


if size(epmem{curLowLevel.curTask}.listGoal,1)>2 && curLowLevel.nbExperiments>2
    ProgressL = [ProgressL; consequence{3} iStrat  curLowLevel.nbExperiments];
end


for iTask=1:length(consequence{2})
    
    nReachedOutcomes2 = size(consequence{2}{iTask},1);
    nReachedOutcomes4 = size(consequence{4}{iTask},1);
    if (nReachedOutcomes2 ~= nReachedOutcomes4)
        error('MAIN_RIAC_CB : error consequence2(reached outcomes) and consequence4(progress for the outcomes) do not have the same size');
    end

    for i = 1:nReachedOutcomes2
        example    = convertGC(mapp{iTask},consequence{2}{iTask}(i,:));
        example    = example(:);

        if(any(example>100) || any(example<0))
            error('example out of bounds. It should take values between 0 and 1');
        end

        errorValue = consequence{4}{iTask}(i,1);
        try
            partReg    = updatePartitionRegions(partReg, example, errorValue, TagMax, iStrat, iTask);
        catch
            disp('erreur');
        end
        Predictor  = [Predictor; [example' zeros(1,maxDimY - mapp{iTask}.dimY) errorValue curLowLevel.nbExperiments iTask iStrat]];
    end
    
end






%==========================================================================
%==========================================================================
%==========================================================================

end





