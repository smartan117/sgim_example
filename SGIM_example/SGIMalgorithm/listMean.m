function listMean()
global sizeData
sizeData = 200;

gOutcome1    = [-0.5 0.5 0];
posObjPrevious1 = [2.5 0 0];

%allMean(posObjPrevious1, gOutcome1, 'list1225_05_05',2860, 2863, 'Jeromeresults/list1225_05_05', 2860, 2864);
allMean(posObjPrevious1, gOutcome1, 'data/list0111-05_05',2860,2864);
gOutcome2    = [0.5 -0.5 0];
%allMean(posObjPrevious1, gOutcome2, 'list1228_05-05', 2860, 2862);
posObjPrevious2 = [2.0 0 0];
%allMean(posObjPrevious2, gOutcome1, 'Jeromeresults/list1230_05_05_g20_',2860,2864);
%allMean(posObjPrevious2, gOutcome1, 'Jeromeresults/list1230_05_05_g20rand_', 2860, 2864);

end 

function allMean(posObjPrevious, gOutcome, nameRoot1, port1, port2, nameRoot2, port3, port4)
global sizeData
global expEnviron
%expEnviron = ExpEnv(54000, 2860)
%gOutcome= [0.5 -0.5 0];
%posObjPrevious = [2.5 0 0]

%nameRoot = 'list1225_05_05' ;
allDist1    = zeros(sizeData,1);
allDist2    = zeros(sizeData,1);
count1 = 0;

for portNo= port1:port2
    try
    [meanDist1 meanDist2] = eachPort(nameRoot1, portNo, posObjPrevious, gOutcome);
    
    allDist1 = allDist1 + meanDist1;
    allDist2 = allDist2 + meanDist2;
    count1 = count1 + 1;
    catch exeption1
        msg = [' portNo ', num2str(portNo)];
%        error('LISTMEAN error : ', msg);
    end
end


%nameRoot = 'Jeromeresults/list1225_05_05';
if nargin == 8
    for portNo= port3:port4
        try
            [meanDist1 meanDist2] = eachPort(nameRoot2, portNo,posObjPrevious, gOutcome);
            
            allDist1 = allDist1 + meanDist;
            allDist2 = allDist2 + meanDist;
            count1 = count1 +1;
        catch exeption1
            msg = [' portNo ', num2str(portNo)];
            %        error('LISTMEAN error : ', msg);
        end
    end
end
% allDist
% allClosest
count1
allDist1    = allDist1/count1;
allDist2    = allDist2/count1;
for i=20:size(allDist1,1)
    minAllDist1(i)= min(allDist1(20:i,:));
    minAllDist2(i)= min(allDist2(20:i,:));
end



hline1 = plot(allDist1,'g-')
hold on
hline2 = plot(allDist2,'b-')
hold on
hline3 = plot(minAllDist1,'g--')
hold on 
hline4 = plot(minAllDist2,'b--')
hold on 
ylim([0 1])
ylabel('distance to goal')
xlabel('timeline')

set(hline1, 'Displayname', 'mean dist to the goal of the reexecuted push using interpolation');
set(hline2, 'Displayname', 'mean dist to the goal of the reexecuted push using nearest neighbour');
set(hline3, 'Displayname', 'minimum of the mean dist to the goal of the reexecuted push using interpolation');
set(hline4, 'Displayname', 'minimum of the mean dist to the goal of the reexecuted push using nearest neighbour');
legend('Location', 'northEast');
title('Mean distances to the goal over several simulations ', 'FontWeight','bold')

hold off
end


function [meanDist1 meanDist2] = eachPort(nameRoot, portNo, posObjPrevious, gOutcome)
filename1 = [nameRoot, num2str(portNo),'_'] ;
global sizeData

%gOutcome= [0.5 -0.5 0];
%posObjPrevious = [2.5 0 0];
reexecute1 = zeros(sizeData, 1);
reexecute2 = zeros(sizeData, 1);
reexecute3 = zeros(sizeData, 1);
reexecute4 = zeros(sizeData, 1);

sumClosest = zeros(sizeData,1);
sumDist    = zeros(sizeData,1);
sumDist1   = zeros(sizeData,1);
sumDist2   = zeros(sizeData,1);
sumDist3   = zeros(sizeData,1);
sumDist4   = zeros(sizeData,1);
count = 0;

for ct = 8:12
    matfile = [filename1, num2str(ct) ]
    if exist([matfile,'.mat'])
        try
            load([matfile,'.mat']);
            N = size(eval.listReexecute,1);
            for reexNo = 1:N-1
                reexecute1(eval.listReexecute(reexNo):eval.listReexecute(reexNo +1)) = eval.listReexecute1(reexNo) ;
                reexecute2(eval.listReexecute(reexNo):eval.listReexecute(reexNo +1)) = eval.listReexecute2(reexNo) ;
                reexecute3(eval.listReexecute(reexNo):eval.listReexecute(reexNo +1)) = eval.listReexecute3(reexNo) ;
                reexecute4(eval.listReexecute(reexNo):eval.listReexecute(reexNo +1)) = eval.listReexecute4(reexNo) ;
            end
            
            
            sumDist1 = sumDist1 + reexecute1(1:200,1) + reexecute4(1:200,1);
            sumDist2 = sumDist2  + reexecute2(1:200,1) + reexecute3(1:200,1);
            count = count +1;
        catch exception
            msg = [' portNo ', num2str(portNo),', ct ',num2str(ct)];
            %        error('EACHPORT error : ', msg);
        end
    end
end

count
meanDist1 = (sumDist1)/count/2;
meanDist2 = (sumDist2)/count/2;

% plot(meanDist1, '-g')
% hold on
% plot(meanDist2, '-r')
% hold on
% figure(1);
% plotClosestToGoal(posObjPrevious, gOutcome, meanClosest, expEnviron);
% figure(2);
% plot(meanDist)


end

