function listClosestToGoal = plotBestExperience(listClosestToGoal,posObjPrevious, goalDeltaTO, treePush, expEnv)

neighbourPush = nearestPush( posObjPrevious, goalDeltaTO, treePush, 1);
listClosestToGoal = [listClosestToGoal; neighbourPush(4:6)];
if ~isempty(listClosestToGoal)
    plotClosestToGoal(goalDeltaTO, listClosestToGoal, expEnv)
end

end
