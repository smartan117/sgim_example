function DataF = imgRand(EstimeGoal)
% Plot using sliding windows, the behavior of the system in 3Dimensions
%% mTimes
clear DataAll
clear DataAllPlot
% cptD = 1;
nbImg = 100;
% intervalD = floor(size(EstimeGoal,2)/nbImg):floor(size(EstimeGoal,2)/nbImg):size(EstimeGoal,2);
% D = floor(size(EstimeGoal,2)/nbImg)*2;

for cptPlot = 1:6
    cptD = 1;
    D = floor(size(EstimeGoal,2)/nbImg)*2;
    while D < size(EstimeGoal,2)
        Interval = 1:D ;
        D
        
        interval1 = 0:1:35;
        interval2 = -35:1:35;
        DataF = ones(size(interval1,2),size(interval2,2))*5;
        for i = interval1
            %         i
            for j = interval2
                nnidx = annquery(EstimeGoal(1:2,Interval),[i;j], 5);
                val = mean(EstimeGoal(2+cptPlot,nnidx));
                
                if cptPlot == 4
                    if val < 2 % ~isnan(val) ||
                        
                        DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
                    else
                        DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
                    end
                end
                
                if cptPlot == 2
                    if ~isnan(val)
                        DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
                    else
                        DataF(floor((i*1)+1),floor((j+35)*1+1)) = 0;
                    end
                end
                
                if cptPlot == 1 || cptPlot == 3 || cptPlot == 5 || cptPlot == 6
                    DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
                end
                
                
            end
        end
        DataAll{cptD} = DataF;
        cptD = cptD + 1;
        Interval = Interval + floor(size(EstimeGoal,2)/nbImg);
        D = Interval(end);
    end
    DataAllPlot{cptPlot} = DataAll;
end
save Data1500BeforeMovie2
save ImgBefore
% 
% close all
% figure
% 
% X = repmat(interval1,size(interval2,2),1);
% Y = repmat(interval2,size(interval1,2),1);
% 
% cptD = 1;
% for D = intervalD
%     
%     Z = DataAll{cptD};
%     % [X,Y,Z] = peaks(30);
%     subplot(121)
%     surfc(X',Y,Z)
%     shading interp
%     az = 0;
%     el = 90;
%     view(az,el)
%     pause(0.5)
%     cptD = cptD + 1;
% end


%% Derivative
cptD = 1;
intervalD = 10:2:size(EstimeGoal,2);
for D = intervalD
    D
    %     EstimeGoalTmp2 = EstimeGoal(:,1:D+2);
    interval1 = 0:1:35;
    interval2 = -35:1:35;
    DataF = ones(size(interval1,2),size(interval2,2))*5;
    for i = interval1
        %         i
        for j = interval2
            nnidx1 = annquery(EstimeGoal(1:2,end-10:end-5),[i;j], 5);
            nnidx2 = annquery(EstimeGoal(1:2,end-5:end),[i;j], 5);
            %             nnidx2 = annquery(EstimeGoalTmp2(1:2,:),[i;j], 5);
            val = mean(EstimeGoal(end,nnidx2+size(EstimeGoal,2)-5)-EstimeGoal(end,nnidx1) );
            if val < 50000
                DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
            end
            
            if isnan(val)
                DataF(floor((i*1)+1),floor((j+35)*1+1)) = 0;
            end
        end
    end
    DataAll{cptD} = DataF;
    cptD = cptD + 1;
end


close all
figure

X = repmat(interval1,size(interval2,2),1);
Y = repmat(interval2,size(interval1,2),1);

cptD = 1;
for D = intervalD

    
    Z = DataAll{cptD};
    % [X,Y,Z] = peaks(30);
    subplot(121)
    surfc(X',Y,Z)
    shading interp
    az = 0;
    el = 90;
    view(az,el)
    pause
    cptD = cptD + 1;
end





% 
% %
% interval1 = 0:1:35;
% interval2 = -35:1:35;
% DataF = ones(size(interval1,2),size(interval2,2))*250;
% for i = interval1
%     i
%     for j = interval2
%         nnidx = annquery(EstimeGoal(1:2,:),[i;j], 5);
%         val = mean(EstimeGoal(4,nnidx));
%         if val < 200
%             DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
%         end
%     end
% end
% 
% subplot(122)
% % close all
% % figure
% 
% X = repmat(interval1,size(interval2,2),1);
% Y = repmat(interval2,size(interval1,2),1);
% Z = DataF;
% % [X,Y,Z] = peaks(30);
% surfc(X',Y,Z)
% shading interp
% az = 0;
% el = 90;
% view(az,el)