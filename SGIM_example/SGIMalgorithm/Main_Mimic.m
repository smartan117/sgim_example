%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

function Main_Mimic(varargin)
% nargin == 1 means Initialization
%
% =======================================================================%%
%                        MIMIC :  observes the demo then repeats it numberIter
%                 times (with small variations on the mvt parameter).
%                 Competence with respect to self-determined outcome.
% =======================================================================%%


%% Initialization
variablesGlobales
saggGlobal
disp('MAIN Mimic');




%%=========================================================================
%%=======================     Choose The Goal     =======================
%%=========================================================================

envSimu = randomizeContext(envSimu);
context =  getContext(envSimu);
context01 = (context +1)/2;


alea = rand; ...
    % Choose a Random Action in the whole space MODE2 ======================================
TagMax = 1;

goalTaskType =  1 ;%randi(nbOutcomeTypes)
goalOutcome = randMinMax(mapp{goalTaskType}.dimY, 1, 0, 1)
iStrat = randi([1, partReg.nbStrategies]);


if alea <ProbMode1
    % Select the Action with Proportional Derivative MODE1 =================
    [goalOutcome iStrat goalTaskType] =  selectGoalMode1(partReg, context01, mapp);
    
elseif alea < ProbMode1+ProbMode3
    % Select the Action With Max in Exploration MODE3 ==============
    [goalOutcome iStrat goalTaskType] = selectGoalMode3(partReg, context01, mapp );
end



%%=========================================================================
%%====================     Mastery Analysis     ===========================
%%=========================================================================

consequence = [];


disp(['MAIN_Mimic  ']);

%iStrat =1;
    % STRATEGY MIMICKING
    % THE TEACHER 1 DEMONSTRATES ad to mimick
    hTeacherid =1;
    disp(['MAINSTRAT::MIMICKING TEACHER ',num2str(hTeacherid) ]);
    
    curLowLevel.curTask              = hTeacher(hTeacherid).task;
    demonstration                    = teacherGivesDemonstration(hTeacher(hTeacherid), mapp{curLowLevel.curTask},curLowLevel.curTask, envSimu)
    demoi                            = demonstration(1,:);
    if goalTaskType == hTeacher(hTeacherid).task % goal is the learner's 
        demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end) = goalOutcome;
    else 
        demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end) = mapp{hTeacher(hTeacherid).task}.yMax;
    end
            
    hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat goalTaskType hTeacherid demoi];
    goalOutcome                  = convertGC(mapp{curLowLevel.curTask}, demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end))';
    curLowLevel.gOutcome           = demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end);
    consequence                      = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
   


%%=========================================================================
%%====================  update tree regions     ===========================
%%=========================================================================
example          = goalOutcome(:);
example          = example(:);
errorValue       = consequence{1};
partReg          = updatePartitionRegions(partReg,  example, errorValue, TagMax, iStrat, curLowLevel.curTask) ;
QualityCandidate = [QualityCandidate; [goalOutcome(:)' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY,1) consequence{1} curLowLevel.nbExperiments goalTaskType iStrat]];

Predictor = [Predictor; [goalOutcome' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) errorValue curLowLevel.nbExperiments goalTaskType iStrat]];

    
if size(epmem{curLowLevel.curTask}.listGoal,1)>2 && curLowLevel.nbExperiments>2
    ProgressL = [ProgressL; consequence{3} iStrat  curLowLevel.nbExperiments];
end

nReachedOutcomes2 = size(consequence{2},1); 
nReachedOutcomes4 = size(consequence{4},1); 
if (nReachedOutcomes2 ~= nReachedOutcomes4)
    error('MAIN_RIAC_CB : error consequence2(reached outcomes) and consequence4(progress for the outcomes) do not have the same size');
end

for i = 1:nReachedOutcomes2
        example    = convertGC(mapp{goalTaskType},consequence{2}(i,:));
        example    = example(:);
        errorValue = consequence{4}(i,1);
        partReg = updatePartitionRegions(partReg,example, errorValue, TagMax, iStrat, curLowLevel.curTask) ;
        Predictor = [Predictor; [consequence{2}(i,:) zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) errorValue curLowLevel.nbExperiments goalTaskType iStrat]];
end



%==========================================================================
%==========================================================================
%==========================================================================

end





