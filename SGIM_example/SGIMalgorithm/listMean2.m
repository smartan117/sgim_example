function listMean2(filenameList, sizeData)

%global sizeData

if isempty(filenameList)
    filenameList = char('dataJerome2/list0204_2860_7.mat ',...
        'dataJerome2/list0204_2862_7.mat',...
        'dataJerome2/list0204_2863_7.mat',...
        'dataJerome2/list0204_2864_7.mat',...
        'dataJerome2/list0204_2866_10.mat',...
        'dataJerome2/list0204_2867_10.mat',...
        'data/list0204_debug2862_7.mat', ...
        'data/list0204_debug2865_7.mat',...
        'data/list0204_debug2865_8.mat');
end
if isempty(sizeData)
    sizeData = 750;
end

filenameList = cellstr(filenameList)
%filenameList =char(filenameList)
sumDistNearest =    zeros(1,sizeData);
sumDistLocal   =    zeros(1,sizeData);
reexecute1     = [];
reexecute2     = [];
reexecute3     = [];
reexecute4     = [];

for i = 1:size(filenameList,1)
    filename = char(filenameList(i,:))
    load(filename);
    N = size(listReexecute,1);
    for reexNo = 1:N-1
        reexecute1(listReexecute(reexNo):listReexecute(reexNo +1)) = listReexecute1(reexNo,1) ;
        reexecute2(listReexecute(reexNo):listReexecute(reexNo +1)) = listReexecute2(reexNo,1) ;
        reexecute3(listReexecute(reexNo):listReexecute(reexNo +1)) = listReexecute3(reexNo,1) ;
        reexecute4(listReexecute(reexNo):listReexecute(reexNo +1)) = listReexecute4(reexNo,1) ;
    end
    sumDistNearest =    sumDistNearest + reexecute1(1,1:sizeData) + reexecute2(1,1:sizeData);
    sumDistLocal   =    sumDistLocal   + reexecute3(1,1:sizeData) + reexecute4(1,1:sizeData);
end

sumDistNearest =    sumDistNearest /(2* size(filenameList,1));
sumDistLocal   =    sumDistLocal   /(2* size(filenameList,1));

for i=20:sizeData
    minDistNearest(i)= min(sumDistNearest(1,20:i));
    minDistLocal(i)  = min(sumDistLocal(1,20:i));
end

hline1 = plot(sumDistNearest,'-b');
hold on;
hline2 =plot(sumDistLocal,'-g');
hold on;
% hline3 = plot(minDistNearest,'g--');
% hold on; 
% hline4 = plot(minDistLocal,'b--');
% hold on; 

set(hline1, 'Displayname', 'mean dist to the goal of the reexecuted push using nearest neighbour');
set(hline2, 'Displayname', 'mean dist to the goal of the reexecuted push using interpolation');
% set(hline3, 'Displayname', 'minimum of the mean dist to the goal of the reexecuted push using nearest neighbour');
% set(hline4, 'Displayname', 'minimum of the mean dist to the goal of the reexecuted push using interpolation');

ylabel('distance to goal')
xlabel('memory size (number of experiences acquired)')
title('Mean distances to the goal over several simulations ', 'FontWeight','bold')
legend('Location', 'northEast');

hold off;