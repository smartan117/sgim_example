%puntumProximum.m
% Author: Sao Mai Nguyen
% date: August 2010
%
%point du robot le plus proche de l'obstacle
%pas encore implémenté
function pt = punctumProximum(jointPos, robot)
[x, y,rot] = fwdKine(robot, jointPos);
pt = [x, y, 0];
end


