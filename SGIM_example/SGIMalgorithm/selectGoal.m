function Goal = selectGoal(numG,affich)
global viewAxis
axis(viewAxis);
Goal = [];
for i = 1:numG
    GoalTmp = ginput(1);
    Goal = [Goal GoalTmp'];
    if affich == 1
        plot(Goal(1,:), Goal(2,:),'ks','LineWidt',3);
        axis(viewAxis);
    end
end