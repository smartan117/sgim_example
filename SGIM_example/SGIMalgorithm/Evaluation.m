%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France, 
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate 
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

classdef Evaluation
    
    properties
        listReexecute          %timestep when the evaluations took place
        listReexecute1         %distance of the reexecutions for the nearest neighbour
        listReexecute2         %distance of the reexecutions for the nearest neighbour
        listReexecute3         %distance of the reexecutions for interpolation
        listReexecute4         %distance of the reexecutions for interpolation
        listDistToGoal         %list of the distance to goal of the nearest nieghbour, cf. listClosestToGoal
        listEvalContext
        listEvalPoints         % benchmark set. of size nx1.
        listEvalWeight         % weights of each of the evaluation points of the benchmark set
        listEvalPointsD        % benchmark set on each dimension
        nbOutcomeTypes               % number of outcome types
        listDistInterp         % list of mean distance between the benchmark set and the execution of the interpolated action
        listDistNN             % list of mean distance between the benchmark set and the nearest-neighbour action
        listDistReexNN         % list of mean distance between the
                               % benchmark set and the execution of
                               % the nearest-neighbour action
        listDistListTeacher    % evaluation for listTeacher
        mapEvalInterp          % evaluation when executing the nearest-neighbour action
        mapEvalNN              % evaluation for nearest neighbour
        mapEvalReexNN          % evaluation when 
        mapEvalListTeacher     % evaluation for listTeacher
        listClosestToGoal
        plotEval               % boolean. 1 if you want to plot in bestExperience, else 0.
        figPerfOnce            %figure window
        figMapEval             %figure window
        figBestExp             %figure window
        figClosestToGoal       %figure window
        
        %{
        each outcome type iOutcome \in [1 nbOutcomeTypes], we define:
        - listEvalPoints{iOutcome} a list of outcomes to be produced
        - listEvalWeight{iOutcome} a list of weights for each of the outcomes listEvalPoints{iOutcome}{i}
        - listEvalPointsD{iOutcome} is a cell of size 1x(dimesion of task subspace), listEvalPointsD{iOutcome}{iDim} is a vector of the (unique) values of listEvalPoints{iOutcome}(iDim)
        %}
    end
    
    
    
    
    
    
    methods
        
        function obj = Evaluation(envSimu)
            obj.listReexecute     = [];      %timestep when the evaluations took place
            obj.listReexecute1    = [];      %distance of the reexecutions for the nearest neighbour
            obj.listReexecute2    = [];      %distance of the reexecutions for the nearest neighbour
            obj.listReexecute3    = [];      %distance of the reexecutions for interpolation
            obj.listReexecute4    = [];      %distance of the reexecutions for interpolation
            obj.listDistToGoal    = [];      %list of the distance to goal of the nearest nieghbour, cf. listClosestToGoal
            obj.listDistInterp    = [];      %error dist when evaluation by interpolation
            obj.listDistNN        = [];      %error dist evaluation with the nearest neighbour
            obj.listDistReexNN    = [];      %error dist evaluation by reexecuting the nearest neighbour
            obj.mapEvalInterp     = [];      %map of dist by interpolation
            obj.listClosestToGoal = [];
            obj.plotEval          = 1;       %boolean indicating if we plot the error
            
            obj.listEvalContext{1} = 30; %[-0.6 -0.2 0.2 0.6];
%             obj.listEvalContext{2} = [-1  -0.2 0.2 1];
%             obj.listEvalContext{3} = [-1    1];
            obj = evalPoints(envSimu, obj); % [-100:10:-50 -40:1:40 50:10:100 ];
        end
        
        
        
        %evaluate the performance at a certain point of the episodicMem
        %state, on the benchmark set
        function obj = evaluationPerfOnce(obj, episodicMem, curLowLevel, aH, timestamp)
        %episodicMem: episodic memory
        %curLowLevel: current low level
        %aH : simulation environment
        %timestamp : current time   
            if nargin <5
                timestamp = curLowLevel.nbExperiments;
            end
            interpol =0;
            
            disp(['EVALPERFONCE ',num2str(timestamp)]);
            %variablesGlobales
            sumDistInterp    = [];
            sumDistNN        = [];
            sumDistReexNN    = [];
            mapEvalLineInterp= [];
            mapEvalLineNN    = [];
            mapEvalLineReexNN    = [];
            
            numberEvalPoints = 0;
            numberEvalPointsDone = 0;
            for iOutcome =1:obj.nbOutcomeTypes
                numberEvalPoints = numberEvalPoints + size(obj.listEvalPoints{iOutcome},1);
            end
            
            
            %% measure performance for each goali of listEvalPoints            
            
            for iOutcome =1:obj.nbOutcomeTypes
                for  ig= 1:size(obj.listEvalPoints{iOutcome},1)
                    numberEvalPointsDone = numberEvalPointsDone + 1;
                    disp(['Evaluation: points ' num2str(numberEvalPointsDone) '/' num2str(numberEvalPoints)]);
                    goali       = obj.listEvalPoints{iOutcome}(ig,:);
                    %                 for x = [obj.listEvalContext{1}]
                    %                     for vx = [obj.listEvalContext{2}]
                    %                         for vy = [obj.listEvalContext{3}]
                    
                    aH = setContext(aH,[0 0.6 1]);
                    %                                aH = setContext(aH,[x vx vy]);
                    cont =  getContext(aH);
                    % pour tester avec nearest neighbour
                    neighbourParam    = nearestInv( episodicMem{iOutcome}, cont, goali, 1);
                    
                    %% TEST: using best locality instead of nearest neighbours
                    %[yBest, params, cBest] = bestLocality(episodicMem{iOutcome}, cont, goali);
                    %neighbourParam = [yBest params];
                    
                    if ~isempty(neighbourParam)
                        distNN             = distMapping(episodicMem{iOutcome}.mapping,neighbourParam(1,episodicMem{iOutcome}.mapping.dimYI),goali);
                        paramInterp = normaliseParam(aH, neighbourParam(1,episodicMem{iOutcome}.mapping.dimAI));
                        %distReexNN  = experiment(paramInterp,-1, cont, iOutcome, goali);
                        % les vrais resultats avec interpolation
                        if interpol
                            distInterp = localModel(episodicMem{iOutcome}, cont, iOutcome, goali, aH);
                        end
                    else
                        distNN     = distMapping(episodicMem{iOutcome}.mapping,[],goali);
                        %distReexNN = distMapping(episodicMem{iOutcome}.mapping,[],goali);
                        if interpol
                            distInterp = distMapping(episodicMem{iOutcome}.mapping,[],goali);
                        end
                    end
                    
                    
%                     if distInterp>distReexNN
%                        disp(['EVALPERFONCE : interpolation is worse than reexecution for outcome ',num2str(goali)]) 
%                     end
                    
                    sumDistNN          = [sumDistNN;  distNN];
                    mapEvalLineNN      = [mapEvalLineNN distNN];
                    
                    
                    %sumDistReexNN     = [sumDistReexNN ; distReexNN];
                    %mapEvalLineReexNN = [mapEvalLineReexNN distReexNN];

                    if interpol
                        sumDistInterp         = [sumDistInterp; distInterp];
                        mapEvalLineInterp     = [mapEvalLineInterp distInterp];
                    end
                    %end
                    %                     end
                    %                 end
                end %end for goali
            end
            
            global mapp
            nbegin = 1;
            for iOutcome =1:obj.nbOutcomeTypes
                nend = nbegin + size(obj.listEvalPoints{iOutcome},1)-1;
                mSumDistNN(iOutcome) = (sumDistNN(nbegin:nend)'*obj.listEvalWeight{iOutcome})/(sum(obj.listEvalWeight{iOutcome}));   % normalization before mapp{iOutcome}.distUnreached
                %mSumDistReexNN(iOutcome) = (sumDistReexNN(nbegin:nend)'*obj.listEvalWeight{iOutcome})/(sqrt((mapp{iOutcome}.yMin-mapp{iOutcome}.yMax)*(mapp{iOutcome}.yMin'-mapp{iOutcome}.yMax'))*sum(obj.listEvalWeight{iOutcome}));
                if interpol
                    mSumDistInterp(iOutcome)  = (sumDistInterp(nbegin:nend)'*obj.listEvalWeight{iOutcome})/(mapp{iOutcome}.distUnreached*sum(obj.listEvalWeight{iOutcome}));
                end
                nbegin = nend+1;
            end
            
            sumDistNN       = mean(mSumDistNN);
            obj.listDistNN  = [obj.listDistNN; sumDistNN];
            obj.mapEvalNN   = [obj.mapEvalNN; mapEvalLineNN timestamp];
            
            %sumDistReexNN  = mean(mSumDistReexNN);
            %obj.listDistReexNN = [obj.listDistReexNN; sumDistReexNN];
            %obj.mapEvalReexNN   = [obj.mapEvalReexNN; mapEvalLineReexNN timestamp];
            
            if interpol
                sumDistInterp       = mean(mSumDistInterp);
                obj.listDistInterp  = [obj.listDistInterp; sumDistInterp];
                obj.mapEvalInterp   = [obj.mapEvalInterp; mapEvalLineInterp timestamp];
                
                obj = plotListDist(obj);
            end
        end  %end function evaluationPerfOnce
        
        %evaluate the performance at a certain point of the episodicMem
        %state, on the benchmark set
        function obj = evaluationPerfTeaching(obj, episodicMem, aH, timestamp)
            
            if nargin <4
                timestamp = episodicMem.nbElements;
            end
            disp(['EVALPERFONCE ',num2str(timestamp)]);
            %variablesGlobales
            sumDistInterp     = [];
            sumDistNN    = [];
            sumDistReexNN    = [];
            mapEvalLine = [];
            mapEvalLineNN= [];
            % count3 = 0;
            %% measure performance for each goali of listEvalPoints
            %for i=1:obj.nbEvalPoints
            
            for   goali       = [obj.listEvalPoints]
                
                aH = setContext(aH,[0 0.8 0.5]);
                %                                aH = setContext(aH,[x vx vy]);
                cont =  getContext(aH);
                % pour tester avec nearest neighbour
                neighbourParam    = nearestInv( episodicMem, cont, goali, 1);
                
                %   distNN             = min(5,distSqrt(goali, neighbourParam(1,episodicMem.mapping.dimYI)));
                %                             sumDistNN          = [sumDistNN;  distNN];
                %                             mapEvalLineNN      = [mapEvalLineNN distNN];
                
                %                             paramInterp = normaliseParam(aH, neighbourParam(1,episodicMem.mapping.dimAI));
                %                             distReexNN  = experiment(paramInterp,3, cont, goali);
                %                             sumDistReexNN     = [sumDistReexNN ; distReexNN];
                
                
                % les vrais resultats avec interpolation
                distInterp             = localModel(episodicMem, cont, goali, aH);
                sumDistInterp          = [sumDistInterp; distInterp];
                mapEvalLine            = [mapEvalLine distInterp];
                %                         end
                %                     end
                %                 end
            end %end for goali
            
            %      sumDistReexNN  = mean(sumDistReexNN);
            %             obj.listDistReexNN = [obj.listDistReexNN; sumDistReexNN];
            
            %             sumDistNN  = mean(sumDistNN)
            %             obj.listDistNN = [obj.listDistNN; sumDistNN];
            %             obj.mapEvalNN   = [obj.mapEvalNN; mapEvalLineNN timestamp];
            
            sumDistInterp   = mean(sumDistInterp);
            obj.listDistListTeacher  = [obj.listDistListTeacher; sumDistInterp];
            obj.mapEvalListTeacher   = [obj.mapEvalListTeacher; mapEvalLine timestamp];
            
            %            obj = plotListDist(obj);
            
        end  %end function evaluationPerfTeaching
        
        
         function obj = evalAllDataOfFile(obj, fileName)
            
            reexperiment = 0; %boolean 0 or 1 to indicate if we evaluate quickly with nearestneighbours only (0) or also by reexecuting and localmodel(1)
            
            listTotal = [];
            mapEvalInterpTotal   = [];
            mapEvalNNTotal   = [];
            mapEvalReexNNTotal   = [];
            
            listDistInterpTotal  = [];
            listDistNNTotal = [];
            listDistReexNNTotal = [];
            dirName = []; % a enlever
            reexperiment = 0;
                   
            %% evaluate for each file
            %            for iFile=2:250:502
            for iFile=[100:100:1400]    %252:250:5002
                
                filename = [fileName,'_nb_', num2str(iFile),'.mat'];
                
                try
                    disp(filename)
                    load(filename, '-regexp', '^(?!evalPerf|connVrep)...')
                    
                    sumDistInterp    = [];
                    sumDistNN        = [];
                    sumDistReexNN    = [];
                    mapEvalLine      = [];
                    mapEvalLineNN    = [];
                    
                    obj = evalPoints(envSimu, obj);
                    %  connVrep = connectSeveral(connVrep, rFish);
                    
                    %% measure performance for each goali of listEvalPoints
                    for iOutcome =1:obj.nbOutcomeTypes
                        for  ig= 1:size(obj.listEvalPoints{iOutcome},1)
                            goali       = obj.listEvalPoints{iOutcome}(ig,:);
                            %                 for x = [obj.listEvalContext{1}]
                            %                     for vx = [obj.listEvalContext{2}]
                            %                         for vy = [obj.listEvalContext{3}]
                            
                            envSimu = setContext(envSimu,[0 0.6 1]);
                            %                                envSimu = setContext(envSimu,[x vx vy]);
                            cont =  getContext(envSimu);
                            % pour tester avec nearest neighbour
                            neighbourParam    = nearestInv( epmem{iOutcome}, cont, goali, 1);
                            if ~isempty(neighbourParam)
                                distNN             = distMapping(epmem{iOutcome}.mapping,neighbourParam(1,epmem{iOutcome}.mapping.dimYI),goali);
                                if reexperiment
                                    paramInterp = normaliseParam(envSimu, neighbourParam(1,epmem{iOutcome}.mapping.dimAI));
                                    distReexNN  = experiment(paramInterp,-1, cont, iOutcome, goali);
                                    % les vrais resultats avec interpolation
                                    distInterp = localModel(epmem{iOutcome}, cont, iOutcome, goali, envSimu);
                                end
                            else
                                distNN = distMapping(epmem{iOutcome}.mapping,[],goali);
                                if reexperiment
                                    distReexNN = distMapping(epmem{iOutcome}.mapping,[],goali);
                                    distInterp  = distMapping(epmem{iOutcome}.mapping,[],goali);
                                end
                            end
                            sumDistNN          = [sumDistNN;  distNN];
                            mapEvalLineNN      = [mapEvalLineNN distNN];
                            
                            
                             if reexperiment
                                sumDistReexNN     = [sumDistReexNN ; distReexNN];
                                sumDistInterp     = [sumDistInterp; distInterp];
                                mapEvalLineInterp = [mapEvalLineInterp distInterp];
                            end
                            %                         end
                            %                     end
                            %                 end
                        end %end for goali
                    end
                    
                    clear mapp
                    mapp{1}  = Mapping(54,2,1,[0 0],[1 1], 100);
                    nbegin = 1;
                    %compute for each outcome type the weighted sum of the error
                    for iOutcome =1:obj.nbOutcomeTypes
                        nend = nbegin + size(obj.listEvalPoints{iOutcome},1)-1;
                        mSumDistNN(iOutcome) = (sumDistNN(nbegin:nend)'*obj.listEvalWeight{iOutcome})/(mapp{iOutcome}.distUnreached*sum(obj.listEvalWeight{iOutcome}));
                        if reexperiment
                            mSumDistReexNN(iOutcome) = (sumDistReexNN(nbegin:nend)'*obj.listEvalWeight{iOutcome})/(sqrt((mapp{iOutcome}.yMin-mapp{iOutcome}.yMax)*(mapp{iOutcome}.yMin'-mapp{iOutcome}.yMax'))*sum(obj.listEvalWeight{iOutcome}));
                            mSumDistInterp(iOutcome)  = (sumDistInterp(nbegin:nend)'*obj.listEvalWeight{iOutcome})/(mapp{iOutcome}.distUnreached*sum(obj.listEvalWeight{iOutcome}));
                        end
                        nbegin = nend+1;
                    end
                    
                    sumDistNN       = mean(mSumDistNN.*mSumDistNN);
                    listDistNNTotal  = [listDistNNTotal; sumDistNN];
                    mapEvalNNTotal   = [mapEvalNNTotal; mapEvalLineNN iFile];
                    
                    if reexperiment
                        %             sumDistReexNNTotal  = mean(mSumDist3);
                        %             listDistReexNNTotal  = [listDistReexNNTotal; sumDistReexNN];
                        
                        sumDistInterp       = mean(mSumDistInterp);
                        %                     listDistInterpTotal  = [listDistInterpTotal; sumDistInterp];
                        %                     mapEvalInterpTotal   = [mapEvalInterpTotal; mapEvalLine timestamp];
                        
                    end
                    
                    %
                end
            end
            
            %figure
            hNN = plot(mapEvalNNTotal(:,end), listDistNNTotal,'-b');
            hold on;
            
            if reexperiment
                h3 = plot(mapEvalNNTotal(:,end), listDistInterpTotal,'-g');
                hold on;
                
                h1 = plot(mapEvalNNTotal(:,end), listDistNNTotal,'-r');
                hold on
            end
            
            
            set(gca,'yscale','log');
            xlabel('Timeline: assessment takes place every regular timestep');
            ylabel('Mean distance to the goals');
            title('Benchmark : evaluation of the performance of the robot on the benchmark data');
            legend('Assessment using the nearest neighbour');
            
            %                 legend('Assessment when reexecuting the nearest neighbour','Assessment using the nearest neighbour', 'Assessment using interpolation');
            
            
            %            load([dirName,fileName,'.mat'])
            evalPerf.mapEvalNN = mapEvalNNTotal;
            evalPerf.listDistReexNN  = listDistReexNNTotal;
            evalPerf.listDistNN = listDistNNTotal;
            evalPerf.listDistInterp = listDistInterpTotal;
            save([dirName, fileName,'_Perf121']);
            
            
            
        end %end function evalAllDataOfFile
        
        function obj = plotListDist(obj)
            global fileMemo
            global MODE
            interpol=0;
            
            if isempty(obj.figPerfOnce)
                if ~isempty(fileMemo)
                    obj.figPerfOnce = figure('Name',fileMemo);
                else
                    obj.figPerfOnce = figure('Name',[MODE,'Teacher', ...
                        num2str(hTeacher(1).present),'-frequency',num2str(hTeacher(1).frequency) ]);
                end
            else
                figure(obj.figPerfOnce);
            end
            
            h1 = plot(obj.mapEvalNN(:,end), obj.listDistNN,'-b');
            hold on;
            %h2 = plot(obj.mapEvalReexNN(:,end), obj.listDistReexNN,'-g');
            %hold on;
            if interpol
                h3 = plot(obj.mapEvalInterp(:,end), obj.listDistInterp,'-r');
            end
            hold on
            set(gca,'YGrid','on')
            set(gca,'yscale','log');
            % ylim([0 1])
            xlabel('Timeline: assessment takes place every regular timestep');
            ylabel('Mean distance to the goals');
            title('Benchmark : evaluation of the performance of the robot on the benchmark data');
%             legend('Assessment using the nearest neighbour','Assessment when reexecuting the nearest neighbour', 'Assessment using interpolation');
            %legend('Assessment using the nearest neighbour','Assessment when reexecuting the nearest neighbour');
            legend('Assessment using the nearest neighbour');
            drawnow
            
            nbTE =  min(7,size(obj.mapEvalInterp,1)-1);
            if nbTE> 5
                %                ymax = max([obj.listDistInterp(end-nbTE:end,1);obj.listDistNN(end-nbTE:end,1);obj.listDistReexNN(end-nbTE:end,1);]);
                ymax = max([obj.listDistNN(end-nbTE:end,1)]) ;
                ylim([0 1.5*ymax])
            end
        end %end function plotListDist
        
        
        function obj=plotMapEval(obj,nbE)
            global fileMemo
            global MODE
            if isempty(obj.figMapEval)
                if ~isempty(fileMemo)
                    obj.figMapEval = figure('Name',fileMemo);
                else
                    obj.figMapEval = figure('Name',[MODE,'Teacher', ...
                        num2str(hTeacher(1).present),'-frequency',num2str(hTeacher(1).frequency) ]);
                end
            else
                figure(obj.figMapEval);
            end
            
            
            nbTE =  size(obj.mapEvalNN,1);
            if nargin <2 || ~exist('nbE') || nbE > nbTE
                nbE = nbTE;
            end
            for iOutcome =1:obj.nbOutcomeTypes
                %subplot(1,obj.nbOutcomeTypes,iOutcome)
                if (size(obj.listEvalPoints{iOutcome},2)==1)
                    plotMapEval_1D(obj,nbE,iOutcome);
                elseif (size(obj.listEvalPoints{iOutcome},2)==2)
                    plotMapEval_2D(obj,nbE,iOutcome);
                end
            end
        end
        
        
        function plotMapEval_2D(obj,nbE,tache)
            interpol =0;
            x1 = obj.listEvalPointsD{tache}{1};
            y1 = obj.listEvalPointsD{tache}{2};
            nbegin = 1;
            for i=1:tache-1
                nbegin= nbegin + length(obj.listEvalPointsD{i}{1})*length(obj.listEvalPointsD{i}{2});
            end
            nend = nbegin + length(x1)*length(y1);
            
            %z1 = obj.mapEvalInterp(nbE,nbegin:nend-1)';
            z1 = obj.mapEvalReexNN(nbE,nbegin:nend-1)';
            x  = repmat(x1,length(y1),1);
            x  = x(:);
            y  = repmat(y1,1,length(x1))';
            y  = y(:);
            subplot(2+interpol,obj.nbOutcomeTypes,tache)
            plot3(x,y,z1,'ob');
            title(['Evaluation of space ' num2str(tache)]);
            hold on;
            F = TriScatteredInterp(x,y,z1);
            %                 ti = -min(x1):2:max(x1);
            [qx,qy] = meshgrid( x1, y1);
            qz = F(qx,qy);
            %figure
            hold on
            mesh(qx,qy,qz);
            hold on
            grid on
            surf(qx,qy,qz)
            colormap hsv
            alpha(.7)
            xlim([x1(1) x1(end)])
            ylim([y1(1) y1(end)])
            %                 zlim([0 1])
            xlabel('x');
            ylabel('y');
            zlabel('error')
        end
        
        
        function obj = plotMapEval_1D(obj, nbE,tache)
            global fileMemo
            global MODE
            interpol =0;
            
            nbTE =  size(obj.mapEvalNN,1);
            if nargin <2 || ~exist('nbE') || nbE > nbTE
                nbE = nbTE;
            end
            nbegin = 1;
            for i=1:tache-1
                nbegin= nbegin + length(obj.listEvalPointsD{i}{1})*length(obj.listEvalPointsD{i}{2});
            end
            nend = nbegin +length(obj.listEvalPointsD{tache}{1});
            x1   = obj.listEvalPointsD{tache}{1};
            %            z1   =  obj.mapEvalInterp(nbE,nbegin:nend-1)';
            z1   =  obj.mapEvalReexNN(nbE,nbegin:nend-1)';
            subplot(2+interpol,obj.nbOutcomeTypes,tache)
            hold on
            plot(x1,z1);
            xlim([x1(1) x1(end)]);
            xlabel('outcome');
            ylabel('error');
            title(['ReexNN error map at ', num2str(obj.mapEvalReexNN(nbE,end)), ' of space ', num2str(tache)]);
            
            if interpol
                subplot(3,obj.nbOutcomeTypes,obj.nbOutcomeTypes+tache)
                z1   =  obj.mapEvalInterp(nbE,nbegin:nend-1)';
                plot(x1,z1);
                xlim([x1(1) x1(end)]);
                xlabel('outcome');
                ylabel('error');
                title(['Interpolation error map at ', num2str(obj.mapEvalInterp(nbE,end)), ' of space ', num2str(tache)]);
            end
            
            subplot(2+interpol,1,2+interpol)
            hold on
            z1   =  obj.mapEvalNN(nbE,nbegin:nend-1)';
            plot(x1,z1);
            xlim([x1(1) x1(end)]);
            xlabel('outcome');
            ylabel('error');
            title(['Nearest neighbour error map at ', num2str(obj.mapEvalNN(nbE,end)), ' of space ', num2str(tache)]);
            
        end  %end function plotMapEval
        
        
        
        %% plotClosestToGoal and the distance to the goal
        function obj = bestExperience(obj, episodicMem, context, gOutcome, fishingArm)
            %variablesGlobales
            %constantesGlobales
            
            sizeData = episodicMem.nbElements;
            if sizeData > 1
                neighbourParam    = nearestInv( episodicMem, [context gOutcome], 1)
                obj.listClosestToGoal = [obj.listClosestToGoal; neighbourParam ];
                
                [dist1 pos1] = experiment(neighbourParam(1,episodicMem.treeInv.dimOutput), -1, context);
                [distNN pos2] = experiment(neighbourParam(1,episodicMem.treeInv.dimOutput), -1, context);
                
                [distReexNN pos3 paramInterp] = localModel(episodicMem, gOutcome, fishingArm);
                [dist4 pos4 param4] = localModel(episodicMem, gOutcome, fishingArm);
                
                
                obj.listReexecute  = [obj.listReexecute   ; sizeData gOutcome];
                obj.listReexecute1 = [obj.listReexecute1  ; dist1 pos1{1} pos1{2}];
                obj.listReexecute2 = [obj.listReexecute2  ; distNN pos2{1} pos2{2}];
                obj.listReexecute3 = [obj.listReexecute3  ; distReexNN pos3{1} pos3{2} paramInterp];
                obj.listReexecute4 = [obj.listReexecute4  ; dist4 pos4{1} pos4{2} param4];
                
                
                if ( ~isempty(obj.listReexecute) && obj.plotEval )
                    obj =  plotClosestToGoal(obj, episodicMem);
                end
                
                if obj.plotEval
                    if isempty(obj.figBestExp)
                        obj.figBestExp = figure('Name',['BestExperience ', num2str(hTeacher(1).present),'-frequency',num2str(hTeacher(1).frequency) ]); %figure(5)
                    else
                        figure(obj.figBestExp)
                    end
                    
                    bkcl = zeros(5,sizeData,3);
                    
                    for i= 1:(size(episodicMem.listGoal,1)-1)
                        bkcl(:, episodicMem.listGoal(i,1):episodicMem.listGoal(i+1,1), 1)= (episodicMem.listGoal(i,2)+ 3)/(2*3);
                        bkcl(:, episodicMem.listGoal(i,1):episodicMem.listGoal(i+1,1), 2)= (episodicMem.listGoal(i,3)+ 3)/(2*3);
                        bkcl(:, episodicMem.listGoal(i,1):episodicMem.listGoal(i+1,1), 3)= episodicMem.listGoal(i,1)/sizeData;
                    end
                    
                    bkcl(:, max(episodicMem.listGoal(end,1),1):episodicMem.nbElements, 1)= (episodicMem.listGoal(end,2)+ 3)/(2*3);
                    bkcl(:, max(episodicMem.listGoal(end,1),1):episodicMem.nbElements, 2)= (episodicMem.listGoal(end,3)+ 3)/(2*3);
                    bkcl(:, max(episodicMem.listGoal(end,1),1):episodicMem.nbElements, 3)= 0;
                    
                    
                    h = imagesc([0 episodicMem.nbElements], [0 1], bkcl); %h = imagesc([0 listGoal(end,1)], [0 1], bkcl);
                    hold on
                    set(h,'alphadata', 0.3)
                    hold on
                    axis xy
                    
                    plot(obj.listReexecute(:,1), obj.listReexecute1(:,1),'-', 'Color',[0 0 1] );
                    hold on
                    scatter(obj.listReexecute(:,1), obj.listReexecute1(:,1), 20,   'b', 'filled' )
                    hold on
                    plot(obj.listReexecute(:,1), obj.listReexecute2(:,1),'-', 'Color',[0 0.2 0.8]);
                    hold on;
                    scatter(obj.listReexecute(:,1), obj.listReexecute2(:,1), 20,   'b', 'filled' )
                    hold on
                    plot(obj.listReexecute(:,1), obj.listReexecute3(:,1),'-', 'Color',[0 1 0]);
                    hold on
                    scatter(obj.listReexecute(:,1), obj.listReexecute3(:,1), 20, 'g', 'filled' )
                    hold on
                    plot(obj.listReexecute(:,1), obj.listReexecute4(:,1),'-', 'Color',[0 0.8 0.2]);
                    hold on
                    scatter(obj.listReexecute(:,1), obj.listReexecute4(:,1), 20,   'g', 'filled' )
                    hold on
                    
                    plot(distSqrt(obj.listClosestToGoal(:,1:3), gOutcome), obj.listReexecute(:,1), 'r-');
                    hold on
                    
                    ylabel('distance of the observation to Goal')
                    xlabel('memory size (number of experiences acquired)')
                    title('Reexecute plot', 'FontWeight','bold')
                    ylim( [0 1.1] )
                    xlim([0 sizeData])
                    set(gca,'XTick',0:100:sizeData)
                    hold off
                    
                    
                    
                end
                
            end
            
        end %end function bestExperience
        
        
        %% plot nearestNeighbour and interpolation and the goal
        function obj = plotClosestToGoal(obj,episodicMem)
            
            if ~isempty(episodicMem.listGoal) && ~isempty(obj.listReexecute)
                if isempty(obj.figClosestToGoal)
                    obj.figClosestToGoal = figure('Name',['ClosestToGoal ', num2str(episodicMem.teacher.present),'-frequency',num2str(episodicMem.teacher.frequency) ]); %figure(5)
                else
                    figure(obj.figClosestToGoal)
                end
                hold off;
                colors = [obj.listReexecute(:,2) obj.listReexecute(:,3) zeros(size(obj.listReexecute(:,2))) ];
                hline0 = plot(episodicMem.listGoal(:,2),episodicMem.listGoal(:,3), 'or', 'LineWidth', 10);
                hold on;
                hline0 = plot(episodicMem.listGoal(:,2),episodicMem.listGoal(:,3), 'or', 'LineWidth', 15);
                hold on;
                hline1 = plot(obj.listReexecute1(:,2),obj.listReexecute1(:,3), 'b-');
                hold on
                hline2 = plot(obj.listReexecute2(:,2),obj.listReexecute2(:,3), 'b-');
                hold on
                hline3 = plot(obj.listReexecute3(:,2),obj.listReexecute3(:,3), 'g-');
                hold on
                hline4 = plot(obj.listReexecute4(:,2),obj.listReexecute4(:,3), 'g-');
                hold on
                hline11 = scatter(obj.listReexecute1(:,2),obj.listReexecute1(:,3), 10, colors);
                hold on
                hline12 = scatter(obj.listReexecute2(:,2),obj.listReexecute2(:,3), 10, colors);
                hold on
                hline13 = scatter(obj.listReexecute3(:,2),obj.listReexecute3(:,3), 10, colors);
                hold on
                hline14 = scatter(obj.listReexecute4(:,2),obj.listReexecute4(:,3), 10, colors);
                hold on
                hline21 = scatter(obj.listReexecute1(end,2),obj.listReexecute1(end,3), 20, 'm', 'filled');
                hold on
                hline22 = scatter(obj.listReexecute2(end,2),obj.listReexecute2(end,3), 20, 'm', 'filled');
                hold on
                hline23 = scatter(obj.listReexecute3(end,2),obj.listReexecute3(end,3), 20, 'm', 'filled');
                hold on
                hline24 = scatter(obj.listReexecute4(end,2),obj.listReexecute4(end,3), 20, 'm', 'filled');
                hold on
                
                xlabel('x')
                ylabel('y')
                set(hline0, 'Displayname', 'goal ');
                set(hline1, 'Displayname', 'displacements observed');
                set(hline2, 'Displayname', 'displacements observed');
                set(hline3, 'Displayname', 'displacements observed');
                set(hline4, 'Displayname', 'displacements observed');
                title('Positions of the Hook on reexecution', 'FontWeight','bold')
                hold off
            end
            
        end %end function plotClosestToGoal
        
    end % end methods
    
    
    methods (Static = true)
        
        function plotErrorFile(fname)
            load(fname)
            obj = evalPerf;
            figure
            h3 = plot(obj.mapEvalInterp(:,end), obj.listDistReexNN,'-g');
            hold on;
            h2 = plot(obj.mapEvalInterp(:,end), obj.listDistNN,'-b');
            hold on;
            h1 = plot(obj.mapEvalInterp(:,end), obj.listDistInterp,'-r');
            hold on
            set(gca,'YGrid','on')
            ylim([0 1])
            xlabel('Timeline: assessment takes place every regular timestep');
            ylabel('Mean distance to the goals');
            title('Benchmark : evaluation of the performance of the robot on the benchmark data');
            legend('Assessment when reexecuting the nearest neighbour','Assessment using the nearest neighbour', 'Assessment using interpolation');
            
        end
        
        
        function plotMapEvalFile(fname)
            load(fname)
            files = 251:1000:4001;
            nfiles = length(files);
            for indexF=1:nfiles
                iFile = files(indexF);
                filename = [ fname,'_nb_', num2str(iFile),'.mat']
                %load(filename,'-regexp', '^(?!connVrep).')
                
                subplot(nfiles,2, 2*indexF-1);
                %plotMapEval(evalPerf);
                x1 = evalPerf.listEvalPoints(:,1);
                y1= evalPerf.listEvalPoints(:,2);
                %                iFi;
                z1 = evalPerf.mapEvalInterp(ind,1:end-1)';
                plot3(x1,y1,z1,'ob');
                hold on;
                F = TriScatteredInterp(x1,y1,z1);
                ti = -2:.05:2;
                [qx,qy] = meshgrid(ti,ti);
                qz = F(qx,qy);
                %figure
                mesh(qx,qy,qz);
                grid on
                zlim([0 1])
                xlabel('x');
                ylabel('y');
                zlabel('error')
                
                subplot(nfiles,2, 2*indexF);
                if indexF >1
                    ind = epmem.listLearning(files(indexF-1):iFile,18)<0.01;
                else
                    ind = epmem.listLearning(1:iFile,18)<0.01;
                end
                plot(epmem.listLearning(ind,26), epmem.listLearning(ind,27),'o')
                xlabel('x');
                ylabel('y');
                set(gca,'YGrid','on')
                
                
            end
            
        end
        
        
        
        %% build a benchmark test from data in filename
        function listEvalPoints = evaluationSet(filename, nbPoints)
            %% generates randomly a uniformly distributed set of points in the region
            %% reached by the data set in filename
            
            % filename is a data file containing listLearning: a list of all reached
            % points
            % nbPoints is the number of points in the evaluation set we want to
            % generate
            
            load(filename)
            x = listTotalAll(:,26);
            y = listTotalAll(:,27);
            xmin = min(x);
            xmax = max(x);
            ymin = min(y);
            ymax = max(y);
            thres = 10;
            evalPerf = Evaluation();
            evalPerf.listEvalPoints =[];
            nbMin = 10;
            nbin = 0;
            nbPoints = 300;
            
            while nbMin<nbPoints
                nbin = nbin + 5;
                [F, xs, ys] = histogram(epmem.listLearning(:,mapp.dimA +1), epmem.listLearning(:,mapp.dimA+2), 1, nbin,[xmin xmax],[ymin ymax]);
                nbMin = sum(sum(F>thres))
            end
            %imagesc(xs,ys,F);
            minInBin = size(epmem.listLearning,1)/(30*nbMin);
            
            dx = abs(1.0001 * (xmax - xmin) / nbin);
            dy = abs(1.0001 * (ymax - ymin) / nbin);
            imax = numel(ys);
            jmax = numel(xs);
            
            for i= 0:nbin
                for j= 0:nbin
                    xminij = xmin +(j-0.5)*dx ;
                    xmaxij = xmin +(j+0.5)*dx ;
                    ymaxij = ymin +(i+0.5)*dy ;
                    yminij = ymin +(i-0.5)*dy ;
                    indBin = find(epmem.listLearning(:,26)<=xmaxij & epmem.listLearning(:,26)>=xminij & epmem.listLearning(:,27)<=ymaxij & epmem.listLearning(:,27)>=yminij & epmem.listLearning(:,28) <0.01 );
                    if numel(indBin)> thres %minInBin
                        disp([num2str(i), ' ', num2str(j)]);
                        newPoint = [xminij yminij]+ [dx dy].*rand(1,2) ;
                        evalPerf.listEvalPoints = [evalPerf.listEvalPoints; newPoint];
                    end
                end
            end
            
            figure
            ind = find(epmem.listLearning(:,28) <0.01);
            h1= plot(epmem.listLearning(ind,26), epmem.listLearning(ind,27), '.b')
            hold on
            h2 = plot(evalPerf.listEvalPoints(:,1), evalPerf.listEvalPoints(:,2),'or','MarkerFaceColor','r','MarkerSize',7)
            hold on
            xlabel('x');
            ylabel('y');
            title('Benchmark : the points used to assess the learning performance are generated randomly and uniformly in the reacheable space');
            legend('the reached points of the database used (76000 points)', 'points used to assess the learning performance');
            figure
            imagesc(xs,ys,F>thres)
            
        end %end function evaluationSet
        
        %% load all files from 1 directory and concatenate listLearning
        function loadAllFiles(directoryName, epmem)
            listTotalAll = [];
            filesInDir = dir([directoryName,'RIAC*5001.mat'])
            for i=1:numel(filesInDir)
                load([directoryName,filesInDir(i).name])
                listTotalAll = [listTotalAll; epmem.listLearning];
            end
            
            % epmem.listLearning = listTotalAll
            save listTotalAutoRIAC111026.mat  %listTotalAll
            
            
        end %end function loadAllFiles
        
        function test_newTree()
            n =size(epmem.listLearning);
            epmem.treeDirect   = createTreeDirect(mapp);
            epmem.treeInv      = createTreeInv(mapp);
            for i=1:n
                entity = epmem.listLearning;
                context = entity(epmem.mapping.dimCD);
                param   = entity(epmem.mapping.dimAD);
                reached = entity(epmem.mapping.dimYD);
                epmem.treeDirect = AddVectorToTree(epmem.treeDirect, [context param reached]);
                epmem.treeInv    = AddVectorToTree(epmem.treeInv, [context reached param]);
            end
        end
        
        
        
        function plotEvalEachTask
            obj = evalPerf;
            nbegin = 1;
            for i =1:16
                nbegin = 1;
                for iOutcome = 1:obj.nbOutcomeTypes
                    nend = nbegin + size(obj.listEvalPoints{iOutcome},1)-1;
                    obj.mapEvalInterp(i,882+iOutcome)= mean(obj.mapEvalInterp(i,nbegin:nend)*obj.listEvalWeight{iOutcome});
                    nbegin = nend;
                end
            end
            figure; plot(obj.mapEvalInterp(:,end-1))
            figure; plot(obj.mapEvalInterp(:,end))
        end
    end %end methods
    
    
end