function d = dist1(a,b)
d = sqrt(sum((a-b).^2));