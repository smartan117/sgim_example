function gotoOrigin(varargin)



affich = 0;
global dimInputArm
global ExtremityPreviousArm
global PositionArm
global PreviousPosition
global Sizes
global sizeArm
global Origin



if nargin == 0 % goto a auto selected random position around the rest position

    if isempty(Origin)
        Position = ones(sizeArm,1) * 0.5 + (rand(sizeArm,1) - 0.5) / 64; % each joint of max pi/16
    else
        Position = Origin;
    end
    %     InitPosAngl = [0.50 + (1:dimInputArm)/2000]';
%     % Position origine
%     Position = ones(dimInputArm,1).*InitPosAngl;
    [trajTmp] = plotArm2(Position'*2*pi - pi,Sizes,[0 0]',affich);
    p1 = trajTmp(:,end);
    
    ExtremityPreviousArm = p1;
    PositionArm = Position; %ones(dimInputArm,1).*InitPosAngl;
    PreviousPosition = PositionArm;
elseif nargin == 1 % goto position put in varargin
    Position = varargin{1};
    [trajTmp] = plotArm2(Position'*2*pi - pi,Sizes,[0 0]',affich);
    p1 = trajTmp(:,end);
    
    ExtremityPreviousArm = p1;
    PositionArm = Position;
    PreviousPosition = PositionArm;
elseif nargin == 2 %% add affich == 1
    Position = varargin{1};
    affich = 1;
    [trajTmp] = plotArm2(Position'*2*pi - pi,Sizes,[0 0]',affich);
    p1 = trajTmp(:,end);
    
    ExtremityPreviousArm = p1;
    PositionArm = Position;
    PreviousPosition = PositionArm;
end

