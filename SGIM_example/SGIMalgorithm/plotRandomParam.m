function plotRandomParam()
variablesGlobales
saggGlobal

%figure

subplot(1,3,1);
ind = find(epmem.listLearning(:,mapp.dimA +3) <0.01);
h1= plot(epmem.listLearning(ind,mapp.dimA +1), epmem.listLearning(ind,mapp.dimA +2), '.b')
hold on
xlabel('x');
ylabel('y');
axis([-Ymax(1) Ymax(1) -Ymax(2) Ymax(2)]);
title('Points reached by the fishing rod');

subplot(1,3,2);
ind = find(epmem.listLearning(:,mapp.dimA +3) <0.01 );
[F, xs, ys] = scattersmooth(epmem.listLearning(ind,mapp.dimA +1), epmem.listLearning(ind,mapp.dimA+2), 500, 2000,[-Ymax(1) Ymax(1)],[-Ymax(2) Ymax(2)]);
imagesc(xs,ys,F)
xlabel('x');
ylabel('y');
title('Distribution of the points reached by the fishing rod');


subplot(1,3,3);
h3 = plot(evalPerf.mapEval(:,end), evalPerf.listDistReexNN,'g');
hold on;
h2 = plot(evalPerf.mapEval(:,end), evalPerf.listDistNN,'b');
hold on;
h1 = plot(evalPerf.mapEval(:,end), evalPerf.listDistInterp,'r');
hold on
xlabel('Number of movements experimented');
ylabel('Mean distance to the goals');
title('Evaluation of the performance on the benchmark data');
legend('Assessment when reexecuting the nearest neighbour','Assessment using the nearest neighbour', 'Assessment using interpolation');


end
