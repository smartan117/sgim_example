function [DerivF Deriv] = ComputeDerivative(errors, iStrat)

%%
% Derivative normalized on the std of the curve compared to its local model
% errorsDim = 30:-1:1;
% noi = 0.1;
% errorsDim = errorsDim + randn(length(errorsDim),1)'*noi;


% plot(errorsDim)

if ~isempty(errors{iStrat})
    errorsDim = errors{iStrat}(1,:);
%    errorsDim = errorsDim(:)';
    nerrors = length(errorsDim);
    
    
    % Last Time Part
    windowWidth = min(5,nerrors-1);
    LTP = errorsDim(1,end-windowWidth:end);
    C =  mean(LTP);
    
    Deriv = -abs(C);
    DerivF = Deriv;
    
    
else
    DerivF = 998;
end

%{
%% test ComputeDerivative
errors{1} = -ones(1,10);
errors{2} = -2*ones(1,10);
[DerivF Deriv] = ComputeDerivative(errors,1);
% outputs DerivF =  -1, Deriv =  -1;

%}
end
