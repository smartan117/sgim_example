function [M Mstd] = plotData(listQualityF,interval,IntSelect, color)
%% 


for nbDim = 1%:4
    M = zeros(1,size(IntSelect,2));
    Mstd = zeros(1,size(IntSelect,2));
    for i = 1:size(IntSelect,2)
        
        M(i) = mean(listQualityF{i}(interval,nbDim));
        Mstd(i) = std(listQualityF{i}(interval,nbDim));
        
    end
%         subplot(2,2,nbDim)
    
%     switch nbDim
%         case 1
            title('Distance to reach')
            hold on
            plot(IntSelect,M,color)
%  
%         case 2
%             title('Velocity Constancy')
%             hold on
%             plot(IntSelect,M,color)
%         case 3
%             title('Extra Distance')
%             hold on
%             plot(IntSelect,M,color)
%         case 4
%             title('percent of efficient movement m-times')
%             hold on
%             plot(IntSelect,M,color)
%    
%     end

end