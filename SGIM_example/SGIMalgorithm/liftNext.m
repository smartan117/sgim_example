function lif = liftNext(varargin)
%%

global QualityCandidate
global previousLift
global listUpdates
eff = QualityCandidate(4,find(QualityCandidate(4,:) ~= 1));

try
    
    if nargin == 0 || nargin == 1 % nargin == 1 for considering values after previousLift
        eff = eff(previousLift:end);
    end
    
    
    if nargin == 1 % nargin = 2 for all values without previousLift
        
        figure
        subplot(211)
        plot(eff)
        y = smooth(1:length(eff),eff,0.9,'loess');
        
        subplot(212)
        plot(y,'.')
        
        %         subplot(413)
        %         plot(diff(y),'.')
        %         subplot(414)
        %         plot(diff(y,2)*10000,'.')
        %
    end
    
    
    if nargin == 2 % nargin = 2 for all values without previousLift
        
        %         figure
        %         subplot(411)
        %         plot(eff)
        %         y = smooth(1:length(eff),eff,0.2,'loess');
        %
        %         subplot(412)
        %         plot(y,'.')
        %
        %         subplot(413)
        %         plot(diff(y),'.')
        %         subplot(414)
        %         plot(diff(y,2)*10000,'.')
       
        indEff = find(QualityCandidate(4,:) ~= 1);
        indEffColor = indEff;
        
        indEffColor(find(indEff <= listUpdates(1))) = 1;
        
        if length(listUpdates) > 1
            for i = 2:length(listUpdates)
                indInf = find(indEff <= listUpdates(i));
                indEffF = find(indEff(indInf) > listUpdates(i-1));
                indEffColor(indEffF) = i;
            end

            
            indInf = find(indEff <= indEffColor(end));
            indEffF = find(indEff(indInf) > listUpdates(end));
            indEffColor(indEffF) = length(listUpdates)+1;
        else
            indInf = find(indEff <= indEffColor(end));
            indEffF = find(indEff(indInf) > listUpdates(1));
            indEffColor(indEffF) = 2;
        end
            
        
%         indEffColor(indEffF(end):end) = length(listUpdates)+1;
        %%%%%%%%
        figure
        
        for i = 1:length(listUpdates)+1
            subplot(211)
            plot(eff)
            %             y = smooth(1:length(eff),eff,0.6,'loess');
            y = smooth(1:length(find(indEffColor==i)),eff(indEffColor == i),0.95,'loess');
            subplot(212)
            hold on
            %             plot(find(indEffColor == i),y(indEffColor ==
            %             i),'.','Color',mod(i,2)*[1 1 1]*0.7 + 0.2*[1 1 1])
            plot(find(indEffColor == i),y,'.','Color',mod(i,2)*[1 1 1]*0.7 + 0.2*[1 1 1])
            %             hold on
            %             subplot(413)
            %             plot(diff(y),'.')
            %             subplot(414)
            %             plot(diff(y,2)*10000,'.')
        end
    end
    if length(eff) >= 200 % 100 elem needed to compute the second derivative
%         eff = eff(end-200:end); % Consider the last goals reached
        
        % Compute the derivative of error
%         meanEnd = mean(eff(100:200));
%         meanBeg = mean(eff(1:100));

        meanEnd = mean(eff(ceil(length(eff)/2):end));
        meanBeg = mean(eff(1:floor(length(eff)/2)));
        % Detect if the derivative is negative
        if meanEnd < meanBeg
            d2 = 1;
        else
            d2 = 0;
        end
        
        
        %         y = smooth(1:length(eff),eff,0.6,'loess');
        %         d2 = diff(y,2)*10000; % 0.5 in find y
        %         if length(find(d2 > 2)) > 2 || length(find(y < 0.6)) > length(eff)/10
        %         if length(find(d2 > 2)) > 2 && length(find(y < 0.65)) > length(eff)/40 %/////
%         if length(find(eff < 0.667)) > length(eff)/40 && d2 == 1%/////

        % Be evolving, we know more, but we can consider that the global learning
        % quality needed can decrease a bit, and increase, after each
        % constraint released
        if meanEnd < (0.68+ 0.2*length(listUpdates)) && d2 == 1%/////
%             save(['eff' num2str(length(QualityCandidate(4,find(QualityCandidate(4,:) ~= 1))))], 'eff');
%             disp(['eff' num2str(length(QualityCandidate(4,find(QualityCandidate(4,:) ~= 1)))) ' saved']);
            lif = 1;
            previousLift = previousLift + length(eff); % shift to the end if lif = 1
        else
            lif = 0;
        end
        
    else
        lif = 0;
    end
    
    
catch
    lif = 0;
    disp('exception in liftNExt')
end

