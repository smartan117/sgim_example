function outPos = waitDisconnect(client)

% z = 20;
% x = 20;
% while ( (length(z)<20) || ( (sum(sum(dist(z(end-19:end))))> 0.1)  && (sum(sum(dist(x(end-9:end))))> 0.1) )  )
%     posObj = urbiGetVariable(client,'vrep.simGetObjectPosition(hook, -1)');
%     if iscell(posObj) 
%         z =[ z posObj{3}];
%         x =[ x posObj{1}];
%         outPos = posObj;
%     else 
%         disp('break');
%         break;
%     end
% end

simEnd = 0;
time = 0;
TIME_LIM = 50;

while(simEnd ==0 && time< TIME_LIM)
    pause(0.1);
    simEnd = urbiGetVariable(client, 'simEnd');
    time = time+1;
    if ~isnumeric(simEnd) 
        simEnd = 0;
    end
end

 outPos = urbiGetVariable(client, 'pos');
 if ~iscell(outPos)
     outPos = urbiGetVariable(client, 'vrep.simGetObjectPosition(hook,-1)');
 end

% if (simEnd == 1) || (simEnd ==2 )
%         disp('time< TIME_LIM 1');
%     outPos = urbiGetVariable(client, 'pos')
% %z = urbiGetVariable(client, 'zpos')
% else
%         disp('time> TIME_LIM 2');
% 
%     outPos = urbiGetVariable(client, 'vrep.simGetObjectPosition(hook,-1)')
% end



disp('disconnect');
urbiSend(client,' vrep.simStopSimulation();')
urbiSend(client,' vrep.disconnect();')
%pause(1);
urbiDisconnect(client);
urbiSendFile(client, 'FishingRod_Initialisation.u')

end
