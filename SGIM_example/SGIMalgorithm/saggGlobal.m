%saggGlobal.m
%
% Copyright (c) 2013 Sao Mai Nguyen 
%               INRIA Bordeaux - Sud-Ouest
%               351 Cours de la Liberation, 33400, Talence,
%               France, http://flowers.inria.fr
%
% defines global variables that are essentiel for the learning


global nbOutcomeTypes % number of types of outcomes %int (usually equal to 1)

%global nbGoal
global numberAction
%global numberIteration
global costStrat
global ProbMode1
global ProbMode3
global sample
global TagMax; 
%global Y
global Ymax
global Ymin


global maxDimY     % maximum dimension of all the types of outcomes %cell array of a list of int. The size of the array is nbOutcomeTypes. Each cell is an array of the length of the dimension of the outcome (task) space

global MODE        % current algorithm that is exploited %string
global fileMemo    % file name of the log file %string

global figMode

% variables for SGIMIM2
global costS
global nbMeta
global gamma
global PAL
global PSL
global PSL1
global PSL2


