function fp_plotEnd()

variablesGlobales
saggGlobal


% figure
% hist(epmem.listLearning(:,end-1),[-202 -199 -1.1:0.1:1.1]);
% xlim([-1 1]);
% xlabel('outcome');
% ylabel('# listLearning');
% title('histogram of outcomes reached');
%
%figure
nbEvalStamp = size(evalPerf.mapEval,1);

%subplot(4,1,1)
plotMapEval(evalPerf,1)

%subplot(4,1,2)
plotMapEval(evalPerf,floor(nbEvalStamp/3))


%subplot(4,1,3)
plotMapEval(evalPerf,floor(2*nbEvalStamp/3))


%subplot(4,1,4)
plotMapEval(evalPerf)
%
% perExplo = percentageExploMode(epmem, [0 1 2])


end