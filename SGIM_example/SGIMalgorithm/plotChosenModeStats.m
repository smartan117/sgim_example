function plotChosenModeStats(filesDir)

filenameROOT = { '../dataAirHockey/2teachers*SGIMIMteacherid3_teacherf50000*2012.mat';};
folders = {'../dataAirHockey/';}
colors = [1 0 1  ];
legends = {'SGIMIM2'};
lines = {'-'};

[nbTypes ~] = size(filenameROOT)
[n1 ~]      = size(legends)
[n2 ~]      = size(folders)
size(colors,1)

if(nbTypes~= n1 || nbTypes~=n2 || nbTypes ~= size(colors,1))
    error('PLOTDATASTATS : error dimensions do not match');
end

%% ******     define the data files        ****** %%   
for iType =1: nbTypes
    filesList{iType} = dir(filenameROOT{iType,1} );
end

filesList
modesList = [0 1 11 12];

%%
chosenMode = zeros(13,20300);
for iType =1: nbTypes % for each type of simu
    for indFile = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(indFile).name]
        load(filename);
        size(hTeacher)
        size(PSL1)
        size(PSL2)
    for i=1:length(epmem.listLearning)
        chosenMode(epmem.listLearning(i,end)+1,i) =   chosenMode(epmem.listLearning(i,end)+1,i) +1;
    end
        clear PSL1
        clear PSL2
        clear hTeacher
    end
end

%%
nh=2000;

for i =1:size(chosenMode,2)-nh
    averageChosenMode(:,i) = sum(chosenMode(:,i:i+nh),2);
end


%%
figure
plot(averageChosenMode(1,:),'-b');
hold on
plot(averageChosenMode(2,:),'-r')
hold on
plot(averageChosenMode(12,:),'-g')
hold on
plot(averageChosenMode(13,:),'-c')
