function ProcessDataF(IndSelectNbExemplarF, listQualityFF, ColorPoints, ColorCurve)
%%


indFig = 0;
for indQuality = [1 2 3 5]
    
    indFig = indFig + 1;
    %     subplot(4,2,indFig)
    %     for i=1:size(IndSelectNbExemplarF,1)
    %         plot(IndSelectNbExemplarF(i,:), listQualityFF{indQuality}(i,:),ColorPoints)
    %         hold on
    %     end
    %     indFig = indFig + 1;
    subplot(2,2,indFig)
    % yy1 = smooth(1:6,listQualityFF(i,:),0.1,'rloess')
    
%     yy1 = smooth(listQualityFF{indQuality},'sgolay',0.5);
    % figure
    hold on
    % plot(IndSelectNbExemplarF(i,:),yy1,'r');
    
    X = IndSelectNbExemplarF';
    X = X(:);
    Y = listQualityFF{indQuality}';
    Y = Y(:);
    
 

    %%%
    
    [Xval Xind] = sort(X);
    Ysort = Y(Xind);
    
    Xval = Xval(1:end);
    Ysort = Ysort(1:end);
    plot(Xval, Ysort, [ColorPoints 'd'], 'LineWidth', 1)
%     yy1 = smooth(Xval,Ysort,0.9,'loess');
    yy1 = smooth(Xval,Ysort,0.9,'loess');
    hold on
    plot(Xval,yy1,ColorCurve,'LineWidth',4)
    
%     
%     
%     for i = 1:size(IndSelectNbExemplarF,1)
%         Tmp = listQualityFF{indQuality};
%         plot(IndSelectNbExemplarF(i,:),Tmp(i,:),ColorCurve)
%         
%     end

    
    
    % /////
    %     med = mean(IndSelectNbExemplarF);
    %     ind = med;
    %     for i = 1:size(med,2)
    %         indTmp = find(med(i) == Xval);
    %         ind(i) =  indTmp(ceil(size(indTmp,1)/2));
    %     end
    %
    %
    %
    %
    %     hold on
    %     plot(Xval,yy1,ColorCurve,'LineWidth',4)
    %     errorbar(median(IndSelectNbExemplarF),yy1(ind), std(listQualityFF{indQuality}),[ColorPoints '.'],'LineWidth',1)
    %     hold on
    % /////



    
    switch indQuality
        case 1
            title('Distance to reach')
            plot(Xval,yy1,ColorCurve,'LineWidth',4)
        case 2
            title('Velocity Constancy')
            plot(Xval,yy1,ColorCurve,'LineWidth',4)
        case 3
            title('Extra Distance')
            plot(Xval,yy1,ColorCurve,'LineWidth',4)
        case 4
            title('Percent Reaching')
            plot(Xval,yy1,ColorCurve,'LineWidth',4)
    end


    
end
%%




% save test


