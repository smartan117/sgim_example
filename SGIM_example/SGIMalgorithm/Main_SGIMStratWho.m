function Main_SGIMStratWho(varargin)
% nargin == 1 means Initialization
%
% Copyright (c) 2012 Sao Mai Nguyen
%               e-mail : nguyensmai@gmail.com
%               INRIA Bordeaux - Sud-Ouest
%               351 Cours de la Liberation, 33400, Talence,
%               France,
%               http://nguyensmai.free.fr/
%
% =======================================================================%%
%                        Main : Socially Guided Intrinsic Motivation      %
%                Who to imitate? according to regions mapping
% =======================================================================%%


%% Initialization
variablesGlobales
saggGlobal
disp('MAIN SGIMStratWho');



%%=========================================================================
%%=======================     Choose The Goal     =======================
%%=========================================================================

envSimu       = randomizeContext(envSimu);
context       =  getContext(envSimu);
context01     = (context +1)/2;



alea = rand; ...
    % Choose a Random Action in the whole space MODE2 ======================================
TagMax = 1;

goalTaskType =  randi(nbTaskTypes);
goalOutcome = randMinMax(mapp{goalTaskType}.dimY, 1, 0, 1);
iStrat = randi([1, partReg.nbStrategies]);


if alea <ProbMode1
    % Select the Action with Proportional Derivative MODE1 =================
    [goalOutcome iStrat goalTaskType] =  selectGoalMode1(partReg, context01, mapp);
    
elseif alea < ProbMode1+ProbMode3
    % Select the Action With Max in Exploration MODE3 ==============
    [goalOutcome iStrat goalTaskType] = selectGoalMode3(partReg, context01, mapp );
end



%%=========================================================================
%%====================     Mastery Analysis     ===========================
%%=========================================================================

consequence = [];

disp(['MAIN_SGIMSTRATWHO : strategy ',num2str(iStrat)]);
curLowLevel.curTask = goalTaskType;

if iStrat==1
    % STRATEGY AUTONOMOUS EXPLORATION
    % THE ROBOT DECIDES yg and a
    curLowLevel.curTask    = goalTaskType;
    curLowLevel.gOutcome = convertCG(mapp{goalTaskType}, goalOutcome');
    consequence            = run1goal(curLowLevel,curLowLevel.gOutcome, context, epmem, mapp, envSimu)
    
elseif iStrat==2
    % STRATEGY IMITATION
    % THE TEACHER 1 DEMONSTRATES yd and ad to mimick
    hTeacherid =1;
    disp(['MAINSTRAT::IMITATE TEACHER ',num2str(hTeacherid) ]);
    
    curLowLevel.curTask              = hTeacher(hTeacherid).task;
    demonstration                    =  teacherGivesDemonstration(hTeacher(hTeacherid), mapp{curLowLevel.curTask},curLowLevel.curTask, envSimu);
    demoi                            = demonstration(1,:);
    hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat goalTaskType hTeacherid demoi];
    goalOutcome                  = convertGC(mapp{curLowLevel.curTask}, demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end))';
    curLowLevel.gOutcome           = demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end);
    consequence                      = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
    
elseif iStrat==3
    % STRATEGY IMITATION
    % THE TEACHER 2 DEMONSTRATES yd and ad to mimick
    hTeacherid =2;
    disp(['MAINSTRAT::IMITATE TEACHER ',num2str(hTeacherid) ]);
    
    curLowLevel.curTask              = hTeacher(hTeacherid).task;
    demonstration                    =  teacherGivesDemonstration(hTeacher(hTeacherid), mapp{curLowLevel.curTask},curLowLevel.curTask, envSimu);
    demoi                            = demonstration(1,:);
    hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat goalTaskType hTeacherid demoi];
    goalOutcome                  = convertGC(mapp{curLowLevel.curTask}, demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end))';
    curLowLevel.gOutcome           = demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end);
    consequence                      = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
    
    
    % elseif iStrat==3
    %     % STRATEGY REQUEST ACTION FOR SELF-DETERMINED GOAL
    %     hTeacherid =1;
    %     disp(['MAINSTRAT::REQUEST DEMO',hTeacherid ]);
    %     hTeacher(hTeacherid).minRegTeach = floor(size(epmem.listLearning,1)/hTeacher(hTeacherid).frequency)+2;
    %     demonstration   = teacherAnswersGoalRequest(hTeacher(hTeacherid), curLowLevel.gOutcome, mapp);
    %     demoi = demonstration(1,:);
    %     hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; size(epmem.listLearning,1) demoi];
    %     goalOutcome = convertGC(mapp, demoi(1,end-mapp.dimY+1:end))';
    %     consequence     = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
    
end


%%=========================================================================
%%====================  update tree regions     ===========================
%%=========================================================================

example          = goalOutcome(:);
example          = example(:);
errorValue       = consequence{1};
partReg          = updatePartitionRegions(partReg,  example, errorValue, TagMax, iStrat, curLowLevel.curTask) ;
QualityCandidate = [QualityCandidate ;[goalOutcome(:)' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY,1) consequence{1} curLowLevel.nbExperiments goalTaskType iStrat]];

Predictor = [Predictor; [goalOutcome' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) errorValue]];

    
if size(epmem{curLowLevel.curTask}.listGoal,1)>2 && curLowLevel.nbExperiments>2
    ProgressL = [ProgressL; consequence{3} iStrat  curLowLevel.nbExperiments];
end

nReachedOutcomes2 = size(consequence{2},1); 
nReachedOutcomes4 = size(consequence{4},1); 
if (nReachedOutcomes2 ~= nReachedOutcomes4)
    error('MAIN_RIAC_CB : error consequence2(reached outcomes) and consequence4(progress for the outcomes) do not have the same size');
end
try
    for i = 1:nReachedOutcomes2
        example    = convertGC(mapp{goalTaskType},consequence{2}(i,:));
        example    = example(:);
        errorValue = consequence{4}(i,1);
        partReg = updatePartitionRegions(partReg,example, errorValue, TagMax, iStrat) ;
        Predictor = [Predictor; [consequence{2}(i,:) zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) 0]];
    end
end





%==========================================================================
%==========================================================================
%==========================================================================

end





