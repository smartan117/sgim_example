function plotReachedPoints()
%% plot all reached points of listLearning and the smoothed histogram
% colors are chosen so that the attempts for the same goal are drawn into
% the same color

variablesGlobales
saggGlobal

%sizeData = treeDirect.nbElements;
nGoal  = size(epmem.listGoal,1) - 1;
nGoal1 = 1; %max(nGoal-10,1);

if nGoal >2
   % figure
   % fig1 = figure;
%    hline0 = plot(epmem.listGoal(:,2),epmem.listGoal(:,3), 'or', 'LineWidth', 10);
    
    for i=nGoal1:nGoal
        j = epmem.listGoal(i,1);
        k = epmem.listGoal(i+1,1)-1;
        color = [epmem.listGoal(i,2) (epmem.listGoal(i,3)+1)/2 mod(i/100,1) ];
        color = min(max(color, 0), 1);
        if epmem.listLearning(j,end)>1 && epmem.listLearning(j,end-1)== 0
         %   scatter(epmem.listGoal(i,2),epmem.listGoal(i,3), 50, color, 'filled','s');
            hold on
          %  text( epmem.listGoal(i,2),epmem.listGoal(i,3),num2str(i) );
          ind = find(epmem.listLearning(j:k,mapp.dimA +3) <0.01 );
          scatter(epmem.listLearning(j+ind,mapp.dimA+1), epmem.listLearning(j+ind,mapp.dimA+2), 20,color,'s');
          hold on
        else
          %  scatter(epmem.listGoal(i,2),epmem.listGoal(i,3), 40, color, 'filled','o');
            hold on
         %   text( epmem.listGoal(i,2),epmem.listGoal(i,3),num2str(i) );
            ind = find(epmem.listLearning(j:k,mapp.dimA +3) <0.01 );
            scatter(epmem.listLearning(j+ind,mapp.dimA+1), epmem.listLearning(j+ind,mapp.dimA+2), 20,color,'o');
            hold on
        end
            
        hold on
    end

    xlabel('x');
    ylabel('y');
    title('Points reached by the fishing rod');
end

%hold off

end