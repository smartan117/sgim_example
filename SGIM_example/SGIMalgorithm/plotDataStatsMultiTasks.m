%
% Copyright (c) 2012 Sao Mai Nguyen
%               e-mail : nguyensmai@gmail.com
%               INRIA Bordeaux - Sud-Ouest
%               351 Cours de la Liberation, 33400, Talence,
%               France,
%               http://nguyensmai.free.fr/
%
colors = [
0.5 0.9 0.2; ...
0 0 1; ...
1   0.4 0.6; ...
0.5 0.0 0.2; ...
0.5 0.0 0.5; ...
0.8 0.4 1; ...
0.5 0.4 1; ...
0.25 0.4    0.5;...
1 0 0]
legends = { 'RANDOM';...
            'SAGG-RIAC';...
            'MIMIC teacher1';...
            'MIMIC teacher2';...
            'MIMIC teacher3';...
            'EMULATE teacher1';...
            'EMULATE teacher2';...
            'EMULATE teacher3';...
            'SGIM-ACTS'}
lines = {
'-d';...
'-*';...
'--x';...
':s';...
'-.o';...
'--x';...
':s';...
'-.o';...
'-p'}

nbTasks = size(evalPerf.listEvalPoints,2)

for iTask=1:nbTasks
    nbEvalPoints(iTask) =  size(evalPerf.listEvalPoints{iTask},1)
end




%% ********** plot mean evaluation performance  for each task     ***************** %%
figure
for iType = 1:nbTypes  % for each type of simu
    meanMapEvalTASK{iType}   = [];
    for iTask =1:nbTasks
        meanListDist3TASK{iType, iTask} = [];
    end
    
    namx =  size(mapEvalSTAT{iType,1},1)
    
    for iFileL = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(iFileL).name]
        size(mapEvalSTAT{iType,iFileL},1)
        meanMapEvalTASK{iType}   = [meanMapEvalTASK{iType} mapEvalSTAT{iType,iFileL}(1:namx,end)];
        
        countEvalPoints1 = 1;
        countEvalPoints2 = 1;
        for iTask =1:nbTasks
            countEvalPoints2 = countEvalPoints1 + nbEvalPoints(iTask) - 1;
            meanListDist3TASK{iType, iTask} = [meanListDist3TASK{iType, iTask} mean(mapEvalSTAT{iType,iFileL}(1:namx,countEvalPoints1:countEvalPoints2),2)];
            countEvalPoints1 = countEvalPoints2+1;
        end
    end
    
    countEvalPoints1 = 1;
    countEvalPoints2 = 1;
    for iTask =1:nbTasks
        countEvalPoints2 = countEvalPoints1 + nbEvalPoints(iTask) -1;
        varListDist3TASK{iType,iTask}    = std(meanListDist3TASK{iType,iTask}',0,1)';
        meanMapEvalTASK{iType,iTask}    = mean(meanMapEvalTASK{iType},2);
        meanListDist3TASK{iType,iTask}   = mean(meanListDist3TASK{iType,iTask},2);
        
        subplot(nbTasks,1,iTask)
        h11{iType} =		       plot(meanMapEvalTASK{iType,iTask} ,meanListDist3TASK{iType,iTask},'-d','Color',colors(iType,:), 'LineWidth',3,'MarkerSize',8)
        %errorbar(meanMapEval2TASK{iType,iTask} ,meanListDist2TASK{iType,iTask},varListDist2TASK{iType,iTask}, '-d','Color',colors(iType,:), 'LineWidth',3)
        hold on
        hold on
        %  errorbar(meanMapEvalTASK{iType} ,meanListDist2TASK{iType},varListDist2TASK{iType}, ':d','Color',colors(iType,:), 'LineWidth',3)
        hold on
        % errorbar(meanMapEvalTASK{iType} ,meanListDist3TASK{iType}, varListDist3TASK{iType}, '--d','Color',colors(iType,:), 'LineWidth',3)
        hold on
       % 				     set(h11{iType}, 'Displayname', [legends{iType}, ' (\Delta =', num2str(varListDist3STAT{iType}(end)),')']);
       title('plot Data Stats Multi Tasks');
        countEvalPoints1 = countEvalPoints2+1;
    end
    
    
end
hleg1= legend('Location', 'EastOutside');
set( hleg1, 'FontWeight','b');
set(hleg1,'FontSize',14)
set(hleg1,'FontName','FixedWidth')
set(gca,'YGrid','on')
%% interpolation mean only for each task

figureMean = figure
% Create axes
% axes1 = axes('Parent',figureMean,'YGrid','on',...
%  'YScale','log',...
%  'FontWeight','bold',...
%     'FontSize',12,...
%     'FontName','Arial');
% box(axes1,'on');
% hold(axes1,'all');
% xlim(axes1,[0 8020]);
%ylim(axes1,[10 120]);

for iTask =1:nbTasks
   subplot1 =  subplot(nbTasks,1,iTask,'Parent',figureMean,'YScale','log','YMinorTick','on');
 box(subplot1,'on');
hold(subplot1,'all');
   
    for iType = 1:nbTypes  % for each type of simu
	h11{iType,iTask} = plot(meanMapEvalTASK{iType,iTask} ,meanListDist3TASK{iType,iTask},lines{iType},'Color',colors(iType,:), 'LineWidth',3,'MarkerSize',10); 
%       h11{iType,iTask} = errorbar(meanMapEval2TASK{iType,iTask} ,meanListDist2TASK{iType,iTask},varListDist2TASK{iType,iTask},lines{iType},'Color',colors(iType,:), 'LineWidth',8,'MarkerSize',10)
        % h11{iType,iTask} =    errorbar(meanMapEvalTASK{iType,iTask} ,meanListDistTASK{iType,iTask},varListDistTASK{iType,iTask}, lines{iType},'Color',colors(iType,:), 'LineWidth',8,'MarkerSize',10)
        hold on
					       set(h11{iType,iTask}, 'Displayname', [legends{iType}, ' (\Delta =',num2str(varListDist3TASK{iType,iTask}(end)),')']);
    end
    
    
    
    % Create title
    title(['Mean Values of the  Performance Evaluation on the Benchmark Data for task ' iTask],...
        'FontSize',14,...
        'FontName','Arial Black');
    
    % Create xlabel
    xlabel('Number of Movements Experimented','FontWeight','bold','FontSize',14,...
        'FontName','Arial');
    
    % Create ylabel
    ylabel('Mean Distance to the Test Goals','FontWeight','bold','FontSize',14,...
        'FontName','Arial');
    
%     hleg1= legend('Location', 'BestOutside');
%     set( hleg1, 'FontWeight','b');
%     set(hleg1,'FontSize',14)
%     set(hleg1,'FontName','FixedWidth')
%     set(gca,'YGrid','on')
     xlim([0 8020]);
    
end


%% plot percentage of strategy and task SGIMSTRAT

nbTasks = partReg.nbTasks;
nbStrategies = 7 ;%partReg.nbStrategies;
iType = nbTypes;


s = zeros(10101, nbStrategies);
<<<<<<< HEAD
t = zeros(10101, nbOutcomeTypes);
=======
t = zeros(10101, nbTaskTypes);
>>>>>>> 7fdbab23d7ee9d3a038c50f1e2fe15cfb647d563
st = [];
for indFile = 1:size(filesList{iType} ,1)
    nQC    = size(listQualityCandidate{iType,indFile},1);
    tbegin = 1;
    for iQC =1:nQC-1
        tbegin = listQualityCandidate{iType,indFile}(iQC,end-2);
        iStrat = listQualityCandidate{iType,indFile}(iQC,end);
        iTask = listQualityCandidate{iType,indFile}(iQC,end-1);
        tend = listQualityCandidate{iType,indFile}(iQC+1, end-2)-1;
        st = [st; iStrat,iTask];
        s(tbegin:tend,iStrat) = s(tbegin:tend,iStrat) + 1;
        t(tbegin:tend,iTask)  = t(tbegin:tend,iTask) + 1;
    end
end
s = s/(size(filesList{iType} ,1));
t = t/(size(filesList{iType} ,1));


stratL = [1:7];
cstring='bgrkmyc';
correspondenceStratType = [2 3 6 4 7 5 8 9];
colorstrat(1,:) = colors(2,:);
colorstrat(2,:) = colors(3,:);
colorstrat(3,:) = colors(6,:);
colorstrat(4,:) = colors(4,:);
colorstrat(5,:) = colors(7,:);
colorstrat(6,:) = colors(5,:);
colorstrat(7,:) = colors(8,:);
colorstrat(8,:) = colors(9,:);
figure

for iStrat = 1:length(stratL)
    hStrat{iStrat} = plot(s(:,iStrat),cstring(iStrat))
    hold on
end

strTitle = [];
for iStrat = 1:length(stratL)
    strTitle = [strTitle , 'mode (', num2str(stratL(iStrat)), ') ', cstring(iStrat) ,', '];
end
 title(['chosen mode: ' , strTitle]);
    
   


span = 100;
ms = [];
for i=1:10000-span
    ms(i,:) =mean(s(i:i+span,:),1);
end
%%
figureStrat = figure
axes1 = axes('Parent',figureStrat,'YGrid','on',...
    'FontWeight','bold',...
    'FontSize',12,...
    'FontName','Arial');
box(axes1,'on');
hold(axes1,'all');

for iStrat = 1:length(stratL)
    hStrat{iStrat} = plot(ms(:,iStrat),'LineStyle', lines{correspondenceStratType(iStrat)}(1:end-1),'Color',colors(correspondenceStratType(iStrat),:), 'Linewidth',3)
    hold on
    set(hStrat{iStrat}, 'Displayname', legends{correspondenceStratType(iStrat)});
end

strTitle = [];
for iStrat = 1:length(stratL)
    strTitle = [strTitle , 'mode (', num2str(stratL(iStrat)), ') ', cstring(iStrat) ,', '];
end

 title(['chosen mode: ' , strTitle],...
    'FontSize',14,...
    'FontName','Arial Black');
xlim([0 8800])
% Create xlabel
xlabel('t','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

% Create ylabel
ylabel('%','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

hleg1= legend('Location', 'northWest');
set( hleg1, 'FontWeight','b');
set(hleg1,'FontSize',20)
set(hleg1,'FontName','FixedWidth')

%% mean number of demonstrations
iType = nbTypes;
nTeach1L = [];
nTeach2L = [];
nTeach3L = [];
for indFile = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(indFile).name]
        load(filename);
        nTeach1L = [nTeach1L; size( hTeacher(1).listTeacher,1)];
        nTeach2L = [nTeach2L; size( hTeacher(2).listTeacher,1)];
        nTeach3L = [nTeach3L; size( hTeacher(3).listTeacher,1)];
end
  
nTeach1 = mean(nTeach1L)
nTeach2 = mean(nTeach2L)
nTeach3 = mean(nTeach3L)
stdTeach1 = std(nTeach1L)
stdTeach2 = std(nTeach2L)
stdTeach3 = std(nTeach3L)
