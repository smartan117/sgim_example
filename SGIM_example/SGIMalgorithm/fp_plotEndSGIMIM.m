%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

function fp_plotEndSGIMIM()
% plotting results in case of SGIM-IM algorithm
variablesGlobales
saggGlobal

%     figure('Name',fileMemo);
%     plot3(QualityCandidate(1,:), QualityCandidate(2,:), ...
%         QualityCandidate(3,:),'.' )
%     hold on
%     F = TriScatteredInterp(QualityCandidate(1,:)', QualityCandidate(2,:)', ...
%         QualityCandidate(3,:)');
%     ti = 0:.02:1;
%     [qx,qy] = meshgrid(ti,ti);
%     qz = F(qx,qy);
%     mesh(qx,qy,qz);
%     hold on;
%     grid on
%     xlabel('x')
%     ylabel('y')
%     figure
%     plotListGoal_1D(epmem, mapp)
fig1 = figure
fig2 = figure
fig3 = figure('Name',['Modes', fileMemo])

<<<<<<< HEAD
for iTaskType=1:nbOutcomeTypes
    figure(fig1);
    subplot(1,nbOutcomeTypes,iTaskType)
plot(epmem{iTaskType}.listGoal(:,end),'o');

 figure(fig2);
    subplot(1,nbOutcomeTypes,iTaskType)
plotExploMode(epmem{iTaskType}, [0 1 11 12], 500);

 figure(fig3);
    subplot(1,nbOutcomeTypes,iTaskType)
=======
for iTaskType=1:nbTaskTypes
    figure(fig1);
    subplot(1,nbTaskTypes,iTaskType)
plot(epmem{iTaskType}.listGoal(:,end),'o');

 figure(fig2);
    subplot(1,nbTaskTypes,iTaskType)
plotExploMode(epmem{iTaskType}, [0 1 11 12], 500);

 figure(fig3);
    subplot(1,nbTaskTypes,iTaskType)
>>>>>>> 7fdbab23d7ee9d3a038c50f1e2fe15cfb647d563
indO = find(epmem{iTaskType}.listGoal(:,2)>0.75);
hist(epmem{iTaskType}.listGoal(indO,1))

title('goals chosen for obstacle');
end
end
