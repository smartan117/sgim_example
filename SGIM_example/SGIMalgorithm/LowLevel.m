%{
 Copyright (c) 2014 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate 
it if you could cite it as follows:


@phdthesis{Nguyen2013,
	Author = {Nguyen, Sao Mai},
	School = {INRIA},
	Title = {A Curious Robot Learner for Interactive Goal-Babbling: Strategically Choosing What, How, When and from Whom to Learn},
	Year = {2013}}

%}

classdef LowLevel < handle
    
    properties
        LearningEntity
        flagTeach
        gOutcome
        curContext
        nbImitate
        nbSave
        minRandom
        randMax
        bestDist;
        curTask;
        nbExperiments;
        progressReachedOutcome;
        numberIteration
    end
    
    methods
        function obj= LowLevel(nbIteration)
            obj.LearningEntity = [];
            obj.flagTeach      = 0;
            %             obj.gOutcome     = -1000*ones(1,mapping.dimY);
            %             obj.curContext     = -1000*ones(1,mapping.dimC);
            obj.curTask        = 1;
            obj.nbImitate      = 5;
            obj.nbSave         = 2;
            obj.minRandom      = 500;
            obj.randMax        = 0.01;
            obj.nbExperiments  = 0;
            obj.numberIteration    = nbIteration;
            obj.progressReachedOutcome = [];
        end
        
        
        %TO CHANGE check all outcome spaces and tasks
        %computes the progress made during the exploration
        %for the goal outcome and the reached outcomes
        % INPUT
        % epmem         : episodic memory
        % mappC         : mapping that gives all dimensions
        % OUTPUT
        % conseqOut{1} : progress measure for the self-determined goal
        % Outcome between beginnning and end: 0 if no progress, < 0 if progress
        % conseqOut{2} : list of reached Outcome (within the boundaries of the outcome space)
        % conseqOut{3} : difference in distance. >=0.
        % conseqOut{4} : progress for each of the reached outcomes conseqOut{2}.
        %
        function conseqOut= computeConsequence(obj,epmem, mappC, envSim, nStart)
            performance    = meanDistToGoal(epmem{obj.curTask},obj.curTask, obj.gOutcome, obj.curContext,3);
            
            %goalExprNorm   = convertGC(mappC{obj.curTask}, thisGoalExper);
            conseqOut{3}   = obj.bestDist - performance ;
            if performance<mappC{obj.curTask}.distYMax
%                 if conseqOut{3}< 10^-10
%                     conseqOut{1} = 0;
%                 else
                    conseqOut{1}= computeProgress(obj.bestDist, performance,epmem{obj.curTask}.nbElements,nStart, epmem{obj.curTask}.distY);
                    %                         conseqOut{1}   = 2*(-1 + sigmoid(2*conseqOut{3}/((epmem{obj.curTask}.nbElements - nStart)*epmem{obj.curTask}.distY)));
%                 end
            else
                conseqOut{1} = 0;
            end
            
            conseqOut{2}   =  {};
            conseqOut{4}   =  {};
            
            for iTask=1:length(epmem)
                goalExprNorm  = epmem{iTask}.listLearning(nStart:epmem{iTask}.nbElements,epmem{iTask}.mapping.dimYL); %all reached outcomes

                nbPRO = size(obj.progressReachedOutcome{iTask},1); %number of progress recorded during run1goal
                nbRO = size(goalExprNorm,1); %number of reached outcomes
                if(nbPRO ~= nbRO)
                    error('LOWLEVEL:run1goal incorrect size for reached outcomes');
                end
                iROreached =1;
                progressRO = [];
                goalExprNorm2 =[];
                for iRO = 1:nbPRO
                    if~(any(goalExprNorm(iRO,:)> epmem{iTask}.mapping.yMax) || any(goalExprNorm(iRO,:)< epmem{iTask}.mapping.yMin) )
                        goalExprNorm2(iROreached,:) = goalExprNorm(iRO,:);
    %                     if obj.progressReachedOutcome(iRO,:)< 10^-10
    %                         progressRO(iROreached,1) = -1;
    %                     else
                            progressRO(iROreached,1) = obj.progressReachedOutcome{iTask}(iRO,:);
                            %                            progressRO(iROreached,1)   = 2*(-1 + sigmoid(2*obj.progressReachedOutcome(iRO,:)/((epmem{obj.curTask}.nbElements - nStart)*epmem{obj.curTask}.distY)));
    %                     end
                        iROreached = iROreached +1;
                    end
                end

                nbPRO2 = size(progressRO,1);
                nbRO2 = size(goalExprNorm2,1);
                if(nbPRO2 ~= nbRO2)
                    error(['LOWLEVEL: incorrect size for reached outcomes 2 ',...
                        num2str(nbPRO) ,' ',num2str(nbRO),' ', num2str(nbPRO2) ,' ',num2str(nbRO2)]);
                end

                conseqOut{2}{iTask}   =  goalExprNorm2;
                conseqOut{4}{iTask}   =  progressRO;
            end
        end
        
        
        %% low-level imitation
        % INPUT
        % curContext    : context vector 1 x dimC
        % demonstration : demonstration vector 1 x (dimA+ dimC+dimY)
        % epmem         : episodic memory
        % mappC         : mapping that gives all dimensions
        % hTeacherid    : id of the teacher (integer)
        % OUTPUT
        % conseqOut{1} : progress measure for the self-determined goal
        % Outcome between beginnning and end: -1 if no progress, 0 if high progress
        % conseqOut{2} : list of reached Outcome (within the boundaries of the outcome space)
        % conseqOut{3} : difference in distance. >=0.
        % conseqOut{4} : progress for each of the reached outcomes conseqOut{2}.
        %
        function conseqOut = run1imitation(obj, curContext, demonstration, epmem, mappC, hTeacherid)
            variablesGlobales
            disp('LOWLEVEL : start run1imitation');
            movParamDemo     = demonstration(1,mappC{obj.curTask}.dimAD);
            
            obj.gOutcome     = demonstration(1,mappC{obj.curTask}.dimYD);
            
            disp(['Demonstration given: ' num2str(obj.gOutcome)]);
            
            %            obj.curContext   = demonstration(1,mapp.dimCD);
            obj.curContext   = curContext;
            obj.progressReachedOutcome = {}; %[];
            obj.flagTeach    = 1;
            nStart           = epmem{obj.curTask}.nbElements +1;
            obj.bestDist     = bestDistToGoal(epmem{obj.curTask}, obj.gOutcome, obj.curContext, 3);
            conseqOut        = [];
            
            if nStart >=1
                
                movParam            = movParamDemo ;
                dist                = experiment(movParam, 10+hTeacherid, obj.curContext, obj.curTask, obj.gOutcome);
                
                %% reexecute the demonstration NB_IMITATE times
                for i=1:obj.nbImitate
                    movParam            = movParamDemo + obj.randMax *(rand(size(movParamDemo))- 0.5) ;
                    dist       = experiment(movParam,  10+hTeacherid,  obj.curContext, obj.curTask, obj.gOutcome);
                end
                %----------------------computes the progress made during the exploration -----------------------
                
                conseqOut= computeConsequence(obj,epmem, mappC, envSimu, nStart);
                
            end
            
            obj.flagTeach = 0 ;
            disp('end run1imitation');
        end %end function run1imitation
        
        
        %% low-level observation
        function conseqOut = run1observation(obj, demonstration, epmem, mapp)
            disp('LOWLEVEL : start run1observation');
            mapp{obj.curTask}.dimA
            movParamDemo     = demonstration(1,1:mapp{obj.curTask}.dimA);
            obj.gOutcome   = [demonstration(mapp{obj.curTask}.dimA+1:mapp{obj.curTask}.dimA+2) 0];
            obj.progressReachedOutcome = {}; %[]
            obj.flagTeach    = 1;
            nStart           = epmem{obj.curTask}.nbElements +1;
            conseqOut      = [];
            
            if nStart >=1
                epmem{obj.curTask}.listGoal = [epmem{obj.curTask}.listGoal; nStart obj.gOutcome];
                
                
                %% add the demonstration as such in the robot's system
                context = 30;
                epmem{task} = addLearningEntity(epmem{task}, context, movParamDemo, ...
                    obj.gOutcome, 1);
                curLowLevel.nbExperiments = curLowLevel.nbExperiments +1;
                
                conseqOut{1} = 0;
            end
            
            obj.flagTeach = 0 ;
            disp('LOWLEVEL : end run1observation');
        end %end function run1observation
        
        
        %% behaviour when 1 goal has been fixed for a duration nbRep
        % INPUT
        % goal : self-determined goal outcome. vector 1 x dimY
        % curContext : context vector 1 x dimC
        % nbRep      : maximum number of policies tried for the goal
        % outcome
        % mappC : mapping that gives all dimensions
        % envSim : simulation environment
        % OUTPUT
        % conseqOut{1} : progress measure for the self-determined goal
        % Outcome between beginnning and end: -1 if no progress, 0 if high progress
        % conseqOut{2} : list of reached Outcome (within the boundaries of the outcome space)
        % conseqOut{3} : difference in distance. >=0.
        % conseqOut{4} : progress for each of the reached outcomes conseqOut{2}.
        %
        function conseqOut = run1goal(obj, goal, curContext, epmem, mappC, envSim)
            saggGlobal
            variablesGlobales
            obj.gOutcome = goal;
            obj.curContext = curContext;
            obj.progressReachedOutcome = {}; %[]
            obj.flagTeach  = 0;
            nStart         = epmem{obj.curTask}.nbElements +1;
            obj.bestDist   = bestDistToGoal(epmem{obj.curTask},  obj.gOutcome, obj.curContext, 3);
            conseqOut      = [];
            nbRep = obj.numberIteration;
            
            if nStart >=1
                epmem{obj.curTask}.listGoal = [epmem{obj.curTask}.listGoal; nStart obj.gOutcome];
                %--------------------- get nearer the goal with nbRep trials ------------
                
                while (obj.flagTeach==0) && (epmem{obj.curTask}.nbElements - nStart < nbRep)
                    %    disp(['run1goal ', num2str(instanceVrep)]);
                    obj  = run1reach(obj, epmem, envSim);
                end
                
                %----------------------computes the progress made during the exploration -----------------------
                conseqOut= computeConsequence(obj,epmem, mappC, envSim, nStart);
                
              
                
            end
            
        end %end function run1goal
        
        
        %% chooses between random exploration or optimisation to reach the
        %% given goal
        function obj  = run1reach(obj, epmem, envSim)
            %% constants and global variables
            NN         = 5 ;%floor(epmem{obj.curTask}.mapping.dimA/3);
            chooseMode  = rand(1);
            meanDist = -1;
            %variablesGlobales;
            obj.flagTeach    = 0;
            nbRep = obj.numberIteration;

            %determine the probability of random exploration
            if epmem{obj.curTask}.nbElements > obj.minRandom
                chooseCrit = 1;
                neighbour1  = nearestInv( epmem{obj.curTask}, obj.curContext, obj.gOutcome, NN);
                if size(neighbour1,1) >= NN
%                     meanDist     = mean(distSqrt(neighbour1(:,epmem{obj.curTask}.mapping.dimYI),obj.gOutcome));
                    meanDist = mean(distMapping(epmem{obj.curTask}.mapping, neighbour1(:,epmem{obj.curTask}.mapping.dimYI), obj.gOutcome));
                    %    distMeasure = meanDist/distSqrt(zeros(size(obj.gOutcome)), obj.gOutcome);
                    if meanDist <= epmem{obj.curTask}.mapping.distYMax
                        %                        distMeasure = (10*epmem{obj.curTask}.distY*meanDist/epmem{obj.curTask}.mapping.distYMax);
                        distMeasure = sigmoid((meanDist-epmem{obj.curTask}.errY)/epmem{obj.curTask}.distY);
                        chooseCrit  = 0.9*(distMeasure-0.5)+0.5; %max(0.05, min(0.95, distMeasure)); % choosecrit is the proba of randomExplore
                    end
                end
            else
                chooseCrit  = 1;
            end
            
            
            try
                epmem{obj.curTask}.listChooseCrit  = [epmem{obj.curTask}.listChooseCrit; chooseCrit obj.nbExperiments meanDist];
            catch
                disp('LOWLEVEL::run1reach : erreur');
            end
            
            if chooseMode> chooseCrit
                %exploreTowardsGoal
                disp(['LOWLEVEL::RUN1REACH:  local exploration for goal ', num2str(obj.gOutcome)]);
                [paramOut, distOut] = locallyExploreTowardsGoal(epmem{obj.curTask}, envSim,  obj.curContext, obj.curTask, obj.gOutcome, chooseCrit, nbRep);
                
       
            else
                %randomExplore
                disp(['LOWLEVEL::RUN1REACH:  explore whole space for goal ', num2str(obj.gOutcome)]);
                movParam   = randParameters(envSim);
                movParam   = normaliseParam(envSim, movParam);
                dist       = experiment(movParam, 1,  obj.curContext, obj.curTask, obj.gOutcome);
            end
            
        end  %end function run1reach
        
        
        
        
        
    end %end methods
    
    
    
    methods(Static = true)
        function test_run1goal()
            n1= curLowLevel.nbExperiments;
            taskType = 1;
            goal = [50 50];
            conseqOut = run1goal(curLowLevel, goal, 30, 100, epmem, mapp, envSimu);
            figure
            plot(distSqrt(goal,epmem{1}.listLearning(n1:end, (end-mapp{taskType}.dimY):end-1)))
        end
        
    end %end methods static
    
end