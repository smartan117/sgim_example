function  [H, xs, ys] = histogram(x, y, lambda, nbin, xrange, yrange)
% Plot a smoothed 2-D histogram and a selection of data points
% Input: vectors x and y
% Optional input: lambda (smoothness), nbin (# of bins), range of x and y
% If no range is set for x and y, their min and max will be used
%
% Paul Eilers and Jelle Goeman, 2003

m = length(x);
if nargin < 3
    lambda = 1;
end
if nargin < 4
    nbin = 100;
end
if nargin < 5
    xrange = [min(x) max(x)];
end
if nargin < 6
    yrange = [min(y) max(y)];
end

% Compute histogram
minx = xrange(1);
miny = yrange(1);
maxx = xrange(2);
maxy = yrange(2);
f = find(minx <= x & x <= maxx & miny <= y & y <= maxy);
x = x(f);
y = y(f);
[xi, xs] = binindex(x, nbin, minx, maxx);
[yi, ys] = binindex(y, nbin, miny, maxy);
H = full(sparse(yi, xi, 1, nbin, nbin));

%ys = rot90(ys,2);
% imagesc(xs, ys, F)
% C = hot(300);
% cstart = 0;
% colormap(C(cstart:end,:));
% colormap(hot) ;
% axis xy


% === support functions 

function [b, xb] = binindex(x, nbin, xmin, xmax)
% compute bin index and bin scale
%b is the index
%xb is the value
if nargin < 3
   xmin = min(x);
end
if nargin < 4
   xmax = max(x);
end
dx = 1.0001 * (xmax - xmin) / nbin;
b = floor(1 + (x - xmin) / dx);
xb = xmin + ((0:(nbin+2)))' * dx;


function Z = expsm(Y, lambda);
m = size(Y, 1);
E = eye(m);
D1 = diff(E);
D2 = diff(D1);
P = lambda ^ 2 * D2' * D2 + 2 * lambda *  D1' * D1;
Z = (E + P) \ Y;


