function plotDataStats(filesDir)
%
% Copyright (c) 2013 Sao Mai Nguyen
%               e-mail : nguyensmai@gmail.com
%               INRIA Bordeaux - Sud-Ouest
%               351 Cours de la Liberation, 33400, Talence,
%               France, 
%               http://nguyensmai.free.fr/
%

%% ******     define the data types        ****** %%   
% filenameROOT = { '../dataLancerPointer/*RANDOMPARAM*2013*_6001.mat';...
%     '../dataLancerPointer/*RIACteacherid*2013*_6001.mat';...
% %     '../dataLancerPointer/*REQUESTMIMICteacherid1_*2013*_6001.mat';...
% %     '../dataLancerPointer/*REQUESTMIMICteacherid2_*2013*_6001.mat';...
% %     '../dataLancerPointer/*REQUESTMIMICteacherid3_*2013*_6001.mat';...
% %     '../dataLancerPointer/*EMULATEteacherid1_*2013_nb_6001.mat';...
% %     '../dataLancerPointer/*EMULATEteacherid2_*2013_nb_6001.mat';...
% %     '../dataLancerPointer/*EMULATEteacherid3_*2013_nb_6001.mat';...
%     '../dataLancerPointer/*_SGIMSTRATteacherid21_*2013_nb_6001.mat';...
% };
% 
% folders = {'../dataLancerPointer/';...
%     '../dataLancerPointer/';...
% %     '../dataLancerPointer/';...
% %     '../dataLancerPointer/';...
% %     '../dataLancerPointer/';...
% %     '../dataLancerPointer/';...
% %     '../dataLancerPointer/';...
% %     '../dataLancerPointer/';...
%     '../dataLancerPointer/'};

filenameROOT = { '../dataLancerPointer/*RANDOMPARAM*2013*_6001.mat';...
    '../dataLancerPointer/*RIACteacherid0*2013*_6001.mat';...
  %  '../dataLancerPointer/*RIACteacherid3*2013*_6001.mat';...
     '../dataLancerPointer/*REQUESTMIMICteacherid1_*2013*_6001.mat';...
     '../dataLancerPointer/*REQUESTMIMICteacherid2_*2013*_6001.mat';...
     '../dataLancerPointer/*REQUESTMIMICteacherid3_*2013*_6001.mat';...
     '../dataLancerPointer/*EMULATEteacherid1_*2013_nb_6001.mat';...
     '../dataLancerPointer/*EMULATEteacherid2_*2013_nb_6001.mat';...
     '../dataLancerPointer/*EMULATEteacherid3_*2013_nb_6001.mat';...
    '../dataLancerPointer/*_SGIMSTRATteacherid21_*2013_nb_6001.mat';...
};

folders = {'../dataLancerPointer/';...
    '../dataLancerPointer/';...
     '../dataLancerPointer/';...
     '../dataLancerPointer/';...
     '../dataLancerPointer/';...
     '../dataLancerPointer/';...
     '../dataLancerPointer/';...
     '../dataLancerPointer/';...
    '../dataLancerPointer/'};

colors = [
0.5 0.9 0.2; ...
0 0 1; ...
 1   0.4 0.6; ...
 0.5 0.0 0.2; ...
 0.5 0.0 0.5; ...
 0.8 0.4 1; ...
 0.5 0.4 1; ...
 0.25 0.4    0.5;...
1 0 0]
legends = { 'RANDOM';...
            'SAGG-RIAC';...
            %'SGIM';...
             'MIMIC teacher1';...
             'MIMIC teacher2';...
             'MIMIC teacher3';...
             'EMULATE teacher1';...
             'EMULATE teacher2';...
             'EMULATE teacher3';...
            'SGIM-ACTS'}
lines = {
'-d';...
'-*';...
 '--x';...
 ':s';...
 '-.o';...
 '--x';...
 ':s';...
 '-.o';...
'-p'}

P=path;
path(P,'../../SGIMPlusieursTaches/')


%% ****** General variables ***  %%

[nbTypes ~] = size(filenameROOT)
[n1 ~]      = size(legends)
[n2 ~]      = size(folders)
size(colors,1)

if(nbTypes~= n1 || nbTypes~=n2 || nbTypes ~= size(colors,1))
    error('PLOTDATASTATS : error dimensions do not match');
end

%% ******     define the data files        ****** %%   
for iType =1: nbTypes
    filesList{iType} = dir(filenameROOT{iType,1} );
end

filesList
%% ******    record the data of each file     ******    %%
PLOT1 = 0

for iType =1: nbTypes % for each type of simu
    for indFile = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(indFile).name]
        load(filename);
        curLowLevel.nbExperiments
        listDist3STAT(iType,indFile)    = {evalPerf.listDistNN(:,1)};
       % listDist2STAT(iType,indFile)    = {evalPerf.listDistReexNN(:,1)};
       % mapEval2STAT(iType,indFile)      = {evalPerf.mapEval2(:,:)};
        %listDistSTAT(iType,indFile)     = {evalPerf.listDistInterp(:,1)};
        mapEvalSTAT(iType,indFile)      = {evalPerf.mapEvalNN(:,:)};
        listQualityCandidate(iType,indFile) = {QualityCandidate};
        listProgressL(iType,indFile)    = {ProgressL};
        
        for iTask =1: 2
        listGoalSTAT(iType,indFile,iTask)     = {epmem{iTask}.listGoal};
        listLearningSTAT(iType,indFile,iTask) = {epmem{iTask}.listLearning};
        end
        
        if PLOT1
            figure('Name',filename,'NumberTitle','off')
            plotAll(filesList{iType}(indFile).name);
        end
    end
    
end

 
%%
%{
%% plot the goal density
nfiles = max([size(filesList{2},1) ; size(filesList{3},1)]); 
for iType =2: nbTypes % for each type of simu
    for indFile = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(indFile).name]
        load(filename);
        Ymax =[ 2 2 100];
        subplot(nbTypes-1,nfiles,(iType-2)*nfiles+indFile);
        [F, xs, ys] = scattersmooth(epmem.listGoal(:,2), epmem.listGoal(:,3), 500, 1000,[-Ymax(1) Ymax(1)],[-Ymax(2) Ymax(2)]);
        imagesc(xs,ys,F);
        set(gca,'XTick',-2:1:2)
        set(gca,'YTick',-2:1:2)
        set(gca,'XTickLabel',{'-1';'-0.5';'0';'0.5';'1'})
        set(gca,'YTickLabel',{'-1';'-0.5';'0';'0.5';'1'})
    end
end
%}


%% ******     plot the reached points     ******    %%
        for iTask =1: 2

            for iType = 1:nbTypes

figReachedPoints = figure('Name','Reached Points','NumberTitle','off')
    h{iType} = [];
    for iFile = 1:size(filesList{iType} ,1) % for each data file
       % ind = find( listLearningSTAT(iType,iFile,iTask)(:,mapp{iTask}.dimA+3) <0.01);
        h{iType}= [h{iType}; plot( listLearningSTAT{iType,iFile,iTask}(:,mapp{iTask}.dimYL(1)), listLearningSTAT{iType,iFile,iTask}(:,mapp{iTask}.dimYL(2)), '.','Color',colors(iType,:),'MarkerSize',5)];
        hold on
    end
   % pause


xlabel('x');
ylabel('y');
title('Reached Points  ');

%     hGroup{iType} = hggroup;
%     set(h{iType},'Parent',hGroup{iType})
%     set(get(get(hGroup{iType},'Annotation'),'LegendInformation'),'IconDisplayStyle','on');
%     set(hGroup{iType}, 'Displayname', legends{iType});
end

legend('Location', 'northEast');

        end
%legend('SAGG-RANDOM','SAGG-RIAC','SGIM','Random on the input parameters')


%{
%% plot the convex hull of the reached points
%figure
for iType =1:nbTypes % for each type of simu
    x = [];
    y = [];
    for iFile = 1:size(filesList{iType} ,1) % for each data file
        ind = find(listLearningSTAT{iType,iFile}(:,mapp.dimA+3) <0.01);
        x = [x; listLearningSTAT{iType,iFile}(ind,mapp.dimA+1)];
        y = [y; listLearningSTAT{iType,iFile}(ind,mapp.dimA+2)];
    end
    k = convhull(x,y);
    h{iType} = plot(x(k),y(k),'-','Color',colors(iType,:))
    hold on
    set(h{iType}, 'Displayname', ['Convex hull of ',legends{iType}]);
    
end

xlabel('x');
ylabel('y');
title('Convex hull of the reached Points  ');
%set(h1, 'Displayname', 'Convex hull of SAGG RANDOM');
legend('Location', 'northEast');
%}


%% ******     plot evaluation performance      ******    %%
figReachedPoints = figure('Name','Evaluation of the performance on the benchmark data','NumberTitle','off')
clear h1
clear h2
clear h3
for iType = 1:nbTypes
    h3{iType} = [];
    h1{iType} = [];
    h2{iType} = [];
    hGroup{iType} = hggroup;
    hGroup1{iType} = hggroup;
    hGroup2{iType} = hggroup;

end

for iType =1: nbTypes % for each type of simu
    for iFile = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(iFile).name]
        hold on;
       % h1{iType} = [h1{iType}; plot(mapEval2STAT{iType,iFile}(:,end), listDist2STAT{iType,iFile},'--','Color',colors(iType,:))];
        %hold on;
 h2{iType} = [h2{iType}; plot(mapEvalSTAT{iType,iFile}(:,end), listDist3STAT{iType,iFile},'-','Color',colors(iType,:))];
         hold on
       % pause
    end
end


xlabel('Number of movements experimented');
ylabel('Mean distance to the goals');
title('Mean error on the benchmark data','FontSize',20,'FontWeight','b');

for iType = 1:nbTypes
    set(h3{iType},'Parent',hGroup{iType})
    set(h1{iType},'Parent',hGroup1{iType})
    set(h2{iType},'Parent',hGroup2{iType})
    set(get(get(hGroup{iType},'Annotation'),'LegendInformation'),'IconDisplayStyle','on');
    set(get(get(hGroup1{iType},'Annotation'),'LegendInformation'),'IconDisplayStyle','on');
    set(get(get(hGroup2{iType},'Annotation'),'LegendInformation'),'IconDisplayStyle','on');
    set(hGroup{iType}, 'Displayname', [legends{iType},' reexecute nearest neighbour']);
    set(hGroup1{iType}, 'Displayname', [legends{iType},' nearest neighbour']);
    set(hGroup2{iType}, 'Displayname', [legends{iType},' interpolation']);

end
hleg1= legend('Location', 'northEast');
set( hleg1, 'FontWeight','b')
set(hleg1,'FontName','FixedWidth')
        



%% ********** plot mean evaluation performance      ***************** %%

for iType = 1: 2%nbTypes  % for each type of simu
    meanMapEvalSTAT{iType}   = [];
    meanMapEval2STAT{iType}  = [];
    meanListDistSTAT{iType}  = [];
    meanListDist2STAT{iType} = [];
    meanListDist3STAT{iType} = [];
    namx =  size(mapEvalSTAT{iType,1},1); 

    for iFile = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(iFile).name]
        size(mapEvalSTAT{iType,iFile},1)
        meanMapEvalSTAT{iType}   = [meanMapEvalSTAT{iType} mapEvalSTAT{iType,iFile}(1:namx,end)];
       % meanMapEval2STAT{iType}  = [meanMapEval2STAT{iType} mapEval2STAT{iType,iFile}(1:namx,end)];
        %%meanListDistSTAT{iType}  = [meanListDistSTAT{iType} listDistSTAT{iType,iFile}(1:namx,:)];
        %meanListDist2STAT{iType} = [meanListDist2STAT{iType} listDist2STAT{iType,iFile}(1:namx,:)];
        meanListDist3STAT{iType} = [meanListDist3STAT{iType} listDist3STAT{iType,iFile}(1:namx,:)];
    end
    
    %varListDistSTAT{iType}    = std(meanListDistSTAT{iType}',0,1)';
    %varListDist2STAT{iType}   = std(meanListDist2STAT{iType}',0,1)';
    varListDist3STAT{iType}   = std(meanListDist3STAT{iType}',1)';
    meanMapEvalSTAT{iType}    = mean(meanMapEvalSTAT{iType},2);
    %meanListDistSTAT{iType}   = mean(meanListDistSTAT{iType},2);
    %meanMapEval2STAT{iType}   = mean(meanMapEval2STAT{iType},2);
    %meanListDist2STAT{iType}  = mean(meanListDist2STAT{iType},2);
    meanListDist3STAT{iType}  = mean(meanListDist3STAT{iType},2);
    %errorbar(meanMapEvalSTAT{iType} ,meanListDistSTAT{iType},varListDistSTAT{iType}, '-d','Color',colors(iType,:), 'LineWidth',3)
    %hold on
   % errorbar(meanMapEval2STAT{iType} ,meanListDist2STAT{iType},varListDist2STAT{iType}, ':d','Color',colors(iType,:), 'LineWidth',3)
    %hold on
 %  errorbar(meanMapEvalSTAT{iType} ,meanListDist3STAT{iType}, varListDist3STAT{iType}, '--d','Color',colors(iType,:), 'LineWidth',3)
    hold on
end


%% interpolation mean only
figureMean = figure
% Create axes
axes1 = axes('Parent',figureMean,'YGrid','on',...
    'YScale','log','YMinorTick','on',...
    'FontWeight','bold',...
    'FontSize',12,...
    'FontName','Arial');
box(axes1,'on');
hold(axes1,'all');
xlim(axes1,[0 8020]);
% ylim(axes1,[10 120]);
 
for iType = 1:2 %nbTypes  % for each type of simu
   %  h11{iType} =  plot(meanMapEval2STAT{iType} ,meanListDist2STAT{iType},...
    %      lines{iType}(1:end),'Color',colors(iType,:), 'LineWidth',3,'MarkerSize',6);
 h11{iType} =  plot(meanMapEvalSTAT{iType} ,meanListDist3STAT{iType},...
          lines{iType}(1:end),'Color',colors(iType,:), 'LineWidth',3,'MarkerSize',6);
hold on
				     set(h11{iType}, 'Displayname', [legends{iType}, ' (\Delta =', num2str(varListDist3STAT{iType}(end)),')']);
end




% Create title
title('Mean Values of the  Performance Evaluation on the Benchmark Data',...
    'FontSize',14,...
    'FontName','Arial Black');

% Create xlabel
xlabel('Number of Movements Experimented','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

% Create ylabel
ylabel('Mean Distance to the Test Goals','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

hleg1= legend('Location', 'northEast');
set( hleg1, 'FontWeight','b');
set(hleg1,'FontSize',14)
set(hleg1,'FontName','FixedWidth')
set(gca,'YGrid','on')
%axis([0 5020 0.1 0.7])

%% error wrt to #teachings
iTask = 2;
for iType =1: nbTypes % for each type of simu
        if(isempty(findstr(legends{iType},'RANDOM')) && isempty(findstr(legends{iType},'RIAC')))
%     figure('Name',legends{iType},'NumberTitle','off')
iType
teachNb{iType} = [];
teachErr{iType} = [];

    for indFile = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(indFile).name]
        load(filename);
        size(epmem{iTask}.listLearning,1)
        teachErr{iType}      = [teachErr{iType}; evalPerf.listDistInterp(:,1)];
        timestamp     = evalPerf.mapEval(:,end);
        for i =1:length(timestamp)
           timestampTeach = find(hTeacher(iTask).listTeacher(:,1)<timestamp(i));
           teachNb{iType} = [teachNb{iType}; length(timestampTeach)+1];
        end
       
    end
    end
    
end
figure('Name','error wrt to nb teachings');
for iType =1: nbTypes % for each type of simu
        if(isempty(findstr(legends{iType},'RANDOM')) && isempty(findstr(legends{iType},'RIAC')))

    if 1%PLOT1
        plot(teachNb{iType},teachErr{iType},'x','Color',colors(iType,:));
        hold on
    end
    end
end

%% regression
figureTeach = figure('Name','Mean Error with respect to the Number of Demonstrations Given'  );
axesT = axes('Parent',figureTeach,...
    'FontWeight','bold',...
    'LineWidth',3,...
        'YScale','log',...  
    'YGrid','on',...
    'FontSize',14,...
    'FontName','Arial');
box(axesT,'on');
hold(axesT,'all');
iTeach = [0:5:400]';

for iType =1: nbTypes % for each type of simu
    X =[];
    ia =[];
    ic =[];
    y =[];
        if(isempty(findstr(legends{iType},'RANDOM')) && isempty(findstr(legends{iType},'RIAC')))

   [X, ia, ic]  = unique(teachNb{iType}) ; %[ones(size(teachNb{iType})),  teachNb{iType} , teachNb{iType}.^2] ;%, teachNb{iType}.^3 ];
   for i =1:length(X)
       indNb = find(ic == i);
       y(i)  = mean([teachErr{iType}(indNb)]);
   end
   yinterp{iType} = interp1(X, y ,iTeach); 
   
     hr{iType} =   plot( iTeach, yinterp{iType},'Color',colors(iType,:),'LineWidth',8);

 % hr{iType} =   plot(X,y, 'o', iTeach, yinterp{iType},'Color',colors(iType,:),'LineWidth',8);
 hold on
    set(hr{iType}, 'Displayname', legends{iType});
    hold on
        end
end
xlim([min(iTeach) max(iTeach)])
title('Mean Error with respect to the Number of Demonstrations Given',...
    'FontSize',14,...
    'FontName','Arial Black');

% Create xlabel
xlabel('Number of Demonstrations Given','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

% Create ylabel
ylabel('Mean Distance to the Goals','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

hleg1= legend('Location', 'northEast');
set( hleg1, 'FontWeight','b');
set(hleg1,'FontSize',14)
set(hleg1,'FontName','FixedWidth')

% iTeach = [0:10:200]';
% X = [ones(size(iTeach)),  iTeach , iTeach.^2]; %, iTeach.^3 ];
% for iType =3: nbTypes % for each type of simu
%  y = theta{iType}'*X';
%  plot(iTeach,y,'-',
% 'Color',colors(iType,:));



%% plot percentage strategy and task SGIMWW
%    QualityCandidate = goalOutcome(:); 0; consequence{1}; curLowLevel.nbExperiments; goalTaskType; iStrat;

iType = nbTypes;
nbStrategies = 7;
<<<<<<< HEAD
<<<<<<< HEAD
nbOutcomeTypes = 2;
s = zeros(101101, nbStrategies);
t = zeros(101101, nbOutcomeTypes);
=======
=======
>>>>>>> 061dc3099435ce98fb583e47d33677bfdd603ca7
nbTaskTypes = 2;
s = zeros(101101, nbStrategies);
t = zeros(101101, nbTaskTypes);
>>>>>>> 7fdbab23d7ee9d3a038c50f1e2fe15cfb647d563
st = [];
for indFile = 1:size(filesList{iType} ,1)
    nQC    = size(listQualityCandidate{iType,indFile},1);
    tbegin = 1;
    for iQC =1:nQC-1
        tbegin = listQualityCandidate{iType,indFile}(iQC,end-2);
        iStrat = listQualityCandidate{iType,indFile}(iQC,end);
        iTask = listQualityCandidate{iType,indFile}(iQC,end-1);
        tend = listQualityCandidate{iType,indFile}(iQC +1, end-2)-1;
        st = [st; iStrat,iTask];
        s(tbegin:tend,iStrat) = s(tbegin:tend,iStrat) + 1;
        t(tbegin:tend,iTask)  = t(tbegin:tend,iTask) + 1;
    end
end
s = s/(size(filesList{iType} ,1));
t = t/(size(filesList{iType} ,1));

figure
h1 = plot(s(:,1),'--g')
hold on
h2 = plot(s(:,2),'--r')
hold on
h3 = plot(s(:,3),'--b')
set(h1, 'Displayname','Strategy: intrinsic motivation');
set(h2, 'Displayname','Strategy: imitate teacher1');
set(h3, 'Displayname','Strategy: imitate teacher2');


span = 200;
ms = [];
for i=1:10000-span
    ms(i,:) =mean(s(i:i+span,:),1);
end
figure
axes1 = axes('Parent',figureMean,'YGrid','on',...
    'FontWeight','bold',...
    'FontSize',12,...
    'FontName','Arial');
box(axes1,'on');
hold(axes1,'all');

h1 = plot(ms(:,1),'-g')
hold on
h2 = plot(ms(:,2),'-r')
hold on
h3 = plot(ms(:,3),'-b')
set(h1, 'Displayname','Strategy: intrinsic motivation');
set(h2, 'Displayname','Strategy: imitate teacher1');
set(h3, 'Displayname','Strategy: imitate teacher2');

title('Chosen Strategies',...
    'FontSize',14,...
    'FontName','Arial Black');

% Create xlabel
xlabel('t','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

% Create ylabel
ylabel('%','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

hleg1= legend('Location', 'northEast');
set( hleg1, 'FontWeight','b');
set(hleg1,'FontSize',14)
set(hleg1,'FontName','FixedWidth')

%% plot tasks
figure
plot(t(:,1),'--b')
hold on
plot(t(:,2),'--r')


span = 200;
ms = [];
for i=1:5000-span
    ms(i,:) =mean(t(i:i+span,:),1);
end


figureTask  =figure
axes1 = axes('Parent',figureTask,'YGrid','on',...
    'FontWeight','bold',...
    'FontSize',12,...
    'FontName','Arial');
box(axes1,'on');
hold(axes1,'all');

h1 = plot(ms(:,1),'-g')
hold on
h2 = plot(ms(:,2),'-r')

set(h1, 'Displayname','Task : Throwing ');
set(h2, 'Displayname','Task : Pointing');

title('Chosen Tasks',...
    'FontSize',14,...
    'FontName','Arial Black');

% Create xlabel
xlabel('t','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

% Create ylabel
ylabel('%','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

hleg1= legend('Location', 'northEast');
set( hleg1, 'FontWeight','b');
set(hleg1,'FontSize',14)
set(hleg1,'FontName','FixedWidth')



figConcordance = figure
[N C] = hist3(st,[ 7 2]);
axes1 = axes('Parent',figConcordance,'YTickLabel',{'throwing','pointing'},...
    'YTick',[1.25 1.75],...
    'XTickLabel',{'SAGG-RIAC','Imitate reacher1','EmulateTeacher1','Imitate Teacher2','Emulate Teacher2','Imitate Teacher3','Emulate Teacher3'},...
    'Position',[0.13 0.0858725761772853 0.796952141057934 0.839127423822715],...
    'FontSize',11);
% Uncomment the following line to preserve the X-limits of the axes
 xlim(axes1,[0.7 7.3]);
% Uncomment the following line to preserve the Y-limits of the axes
 ylim(axes1,[0.95 2.05]);
view(axes1,[-37.5 30]);
grid(axes1,'on');
hold(axes1,'all');
%hist3(st,[ 7 2]);
bar3(N/(sum(sum(N))) )

title('Concordance between Strategies and Tasks');

% Create xlabel
xlabel('strategy','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

% Create ylabel
ylabel('task','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

zlabel('# chosen','FontWeight','bold','FontSize',14,...
    'FontName','Arial');

%plotConcordanceTaskStrat(xdata1, ydata1, zdata1, cdata1)

%% temporaire 
% ProgressL =  consequence{3} iStrat  curLowLevel.nbExperiments
iType = nbTypes;
nbStrategies = 3;
s = zeros(10100, nbStrategies);
ms = zeros(10100, nbStrategies);
for indFile = 1:size(filesList{iType} ,1)
    nQC    = size(listProgressL{iType,indFile},1)
    for iQC =1:nQC-1
        iStrat = listProgressL{iType,indFile}(iQC,end-1);
        tbegin = listProgressL{iType,indFile}(iQC,end);
        tend   = listProgressL{iType,indFile}(iQC+1,end)-1;
        s(tbegin:tend,iStrat) = s(tbegin:tend,iStrat) + 1;
    end
end
s = s/(size(filesList{iType} ,1)*nbStrategies);

figure
plot(s(:,1),'--g')
hold on
plot(s(:,2),'--r')
hold on
plot(s(:,3),'--b')

span = 100;
for i=1:10000-span
    ms(i,:) =mean(s(i:i+span,:),1);
end
figure
plot(ms(:,1),'-g')
hold on
plot(ms(:,2),'-r')
hold on
plot(ms(:,3),'-b')
    

%% plot percentage mode SGIMIM
iType = nbTypes;
nh= 100;
s = zeros(2,8500);
				    cstring='grbcmyk';  %autono, (mimic,emulation)
                 for indFile = 1:11 %size(filesList{iType} ,1)
                 for i= 1:8500 -nh
                     % autonomous learning 
                     s(1,i) = s(1,i) +length(find(listLearningSTAT{iType,indFile}(i:i+ ...
                         nh,end)==1)) + ...
                     length(find(listLearningSTAT{iType,indFile}(i:i+ ...
                                                               nh,end)==2));
                     % social learning 
                     s(2,i) =  s(2,i) + length(find(listLearningSTAT{iType,indFile}(i:i+ ...
                                                               nh,end)==0));
                 end
                 end
          
                 
                 figureStrategy = figure( 'Name','Mean Values of the  Performance Evaluation on the Benchmark Data');
% Create axes
axes1 = axes('Parent',figureStrategy,'YGrid','on',...
    'FontWeight','bold',...
    'LineWidth',3,...
    'FontSize',14,...
    'FontName','Arial');
box(axes1,'on');
hold(axes1,'all');

 
                 %for imode = 1:length(modesL)
                 haut = plot(s(1,:)/(nh*indFile),cstring(1),'LineWidth',8)
                 hold on
                 set(haut, 'Displayname', 'Intrinsic Motivation Strategy');
                 hold on
                 hsoc = plot(s(2,:)/(nh*indFile),cstring(2),'LineWidth',8)
                 hold on
                 set(hsoc, 'Displayname', 'Social Learning Strategy');
                 hold on
                 %end
                 title(['intrinsic motivation strategy = ' cstring(1) ',  social learning strategy = ' cstring(2)]);   
                
 xlim(axes1,[0 length(s)-2*nh]);
ylim([0 1])

%% plot percentage mode SGIMIM2
iType = nbTypes;
nh =500;

s = zeros(3,14000);
cstring='grbcmyk';
for indFile = 1:size(filesList{iType} ,1)
    for i= 1:14000 - nh
        % autonomous learning
        s(1,i) = s(1,i) +length(find(listLearningSTAT{iType,indFile}(i:i+ ...
            nh,end)==1)) + ...
            length(find(listLearningSTAT{iType,indFile}(i:i+ ...
            nh,end)==0));
        % social learning
        s(2,i) =  s(2,i) + length(find(listLearningSTAT{iType,indFile}(i:i+ ...
            nh,end)==11));
        s(3,i) =  s(3,i) + length(find(listLearningSTAT{iType,indFile}(i:i+ ...
            nh,end)==12));
    end
end


figureStrategy = figure( 'Name','Mean Values of the  Performance Evaluation on the Benchmark Data');
% Create axes
axes1 = axes('Parent',figureStrategy,'YGrid','on',...
    'FontWeight','bold',...
    'LineWidth',3,...
    'FontSize',14,...
    'FontName','Arial');
box(axes1,'on');
hold(axes1,'all');


%for imode = 1:length(modesL)
haut = plot(s(1,:)/(nh*indFile),cstring(1),'LineWidth',8)
hold on
set(haut, 'Displayname', 'Intrinsic Motivation Strategy');
hold on
hsoc = plot(s(2,:)/(nh*indFile),cstring(2),'LineWidth',8)
hold on
set(hsoc, 'Displayname', 'Social Learning Strategy 1');
hold on
hsoc = plot(s(3,:)/(nh*indFile),cstring(3),'LineWidth',8)
hold on
set(hsoc, 'Displayname', 'Social Learning Strategy 2');
hold on
%end
title(['intrinsic motivation strategy = ' cstring(1) ',  social learning strategy = ' cstring(2)]);

xlim(axes1,[0 length(s)-2*nh]);
ylim([0 1])

%% progress
iType = nbTypes;
PSLL = [];
PALL = [];

                 for indFile = 1:11 
  filename = [ folders{iType},filesList{iType}(indFile).name]
        load(filename);
        PSLL= [PSLL; PSL];
        PALL= [PALL; PAL];
                 end
                 
                 
                 
                 iTeach = [0:500:10000]';

    X =[];
    ia =[];
    ic =[];
    y =[];

   [X, ia, ic]  = unique(PSLL(:,2)) ; %[ones(size(teachNb{iType})),  teachNb{iType} , teachNb{iType}.^2] ;%, teachNb{iType}.^3 ];
   for i =1:length(X)
       indNb = find(ic == i);
       y(i)  = mean([PSLL(indNb,1)]);
   end
   yinterpS = interp1(X, y ,iTeach); 
   
     hs =   plot( iTeach, yinterpS,'r'); %,'Color',colors(iType,:),'LineWidth',8);
hold on
      X =[];
    ia =[];
    ic =[];
    y =[];

   [X, ia, ic]  = unique(PALL(:,2)) ; %[ones(size(teachNb{iType})),  teachNb{iType} , teachNb{iType}.^2] ;%, teachNb{iType}.^3 ];
   for i =1:length(X)
       indNb = find(ic == i);
       y(i)  = mean([PALL(indNb,1)]);
   end
   yinterpA = interp1(X, y ,iTeach); 
   
     ha =   plot( iTeach, yinterpA,'g'); %,'Color',colors(iType,:),'LineWidth',8);

 % hr{iType} =   plot(X,y, 'o', iTeach, yinterp{iType},'Color',colors(iType,:),'LineWidth',8);



%% number of demonstrations SGIMIM
iType = nbTypes-1;
nbT = [];
 for indFile = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(indFile).name]
        load(filename);
        clf
      %  plotExploMode(epmem, [0 1 2], 500)
       % pause
       nbT(indFile) = size(hTeacher(1).listTeacher,1);
        
 end
 mean(nbT)
        
 
 %% number of demonstrations SGIMIM2
iType = nbTypes;
nbT1 = [];
nbT2 = [];
 for indFile = 1:size(filesList{iType} ,1) % for each data file
        filename = [ folders{iType},filesList{iType}(indFile).name]
        load(filename);
        clf
      %  plotExploMode(epmem, [0 1 2], 500)
       % pause
       nbT1(indFile) = size(hTeacher(1).listTeacher,1);
         nbT2(indFile) = size(hTeacher(2).listTeacher,1);
 end
 mean(nbT1)
 mean(nbT2)
        

end

 
