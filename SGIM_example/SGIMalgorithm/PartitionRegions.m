classdef PartitionRegions < handle
% This class handles the partition of the task space into regions. 
% The mapping of competence progress of the task space is necessary
% to orient the exploration by curiosity. 
% It uses InterestRegions and ListRegions.
%
% Copyright (c) 2013 Sao Mai Nguyen
%               e-mail : nguyensmai@gmail.com
%               http://nguyensmai.free.fr/
%
% Permission is granted to copy, distribute, and/or modify this program
% under the terms of the GNU General Public License, version 2 or any
% later version published by the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details
% 
% If you use this code in the context of a publication, I would appreciate 
% it if you could cite it as follows:
% 
% 
% @phdthesis{Nguyen2013,
% 	Author = {Nguyen, Sao Mai},
% 	School = {INRIA},
% 	Title = {A Curious Robot Learner for Interactive Goal-Babbling: Strategically Choosing What, How, When and from Whom to Learn},
% 	Year = {2013}}
%
    
    
    
    properties
        interestRegions
        listRegions
        watchAround       %amplitude of the randomization in mode 3
        nbStrategies
        nbTasks
    end %end properties
    
    
    
    methods
        
        
        function obj = PartitionRegions(C1Tmp, sampleTmp, numRegTmp, ...
                                        mappC, nbStrategies, ...
                                        nbOutcomeTypes, watchAr)
            for i=1:nbOutcomeTypes
                listIR(i)               = InterestRegions(C1Tmp, sampleTmp, numRegTmp, mappC{i}.dimY, nbStrategies);
                listLR(i)               = ListRegions(nbStrategies);
                listIR(i).activateChildren = 1;
            end
            nbOutcomeTypes
            obj.interestRegions = listIR;
            obj.listRegions     = listLR;
            obj.watchAround     = watchAr;
            obj.nbStrategies    = nbStrategies;
            obj.nbTasks         = nbOutcomeTypes;            
        end %end constructor
        
        function obj = updatePartitionRegions(obj, example, errorValue, ...
                                              TagMax, iStrategy, ...
                                              iTask)
            if iTask <= obj.nbTasks
                [obj.interestRegions(iTask)  obj.listRegions(iTask)] = updateTree(obj.interestRegions(iTask), ...
                                             example, errorValue, TagMax, obj.listRegions(iTask), iStrategy);
            else
                disp(['PARTITIONREGIONS::UPDATEPARTITIONREGIONS error ' ...
                      'unknown task' num2str(iTask)]);
            end
        end %end function updatePartitionRegions
        
        
        function [yg strat task] = selectGoalMode1(obj, context01, mappC)
            %% Select the Action with Proportional Derivative MODE1
            % a random point yg in the region with the highest progress
            disp('PARTITIONREGIONS::SELECTGOALMODE1');
            task = randi(obj.nbTasks);
            yg = randMinMax(mappC{task}.dimY, 1, 0, 1);
            strat = randi(obj.nbStrategies);
            if length(obj.listRegions(task).LReg) > 2                                        % disp('Enter Proportional Choice')
                [indMin stratMin taskMin] = ...
                    ProportionalDerivative(obj.listRegions, context01, mappC, obj.nbStrategies, obj.nbTasks);
                if ~isempty(indMin) &&  indMin>1 && ~isempty(stratMin) ...
                        && stratMin >=1 && stratMin<= obj.nbStrategies ...
                        && ~isempty(taskMin) && taskMin>=1 && taskMin<=obj.nbTasks
                    strat = stratMin;
                    task = taskMin;
%                     obj.listRegions
%                     obj.listRegions(taskMin).LReg
%                     obj.listRegions(taskMin).LReg{indMin}
                    limitMin = obj.listRegions(taskMin).LReg{indMin}{2};
                    yg = limitMin(mappC{taskMin}.dimYI,1) + ...
                        ( rand(mappC{taskMin}.dimY,1)*10)/10.*(limitMin(mappC{taskMin}.dimYI,2) - limitMin(mappC{taskMin}.dimYI,1));
                end
            end
            
            if(any(yg>100) || any(yg<0))
                            disp('PARTITIONREGIONS::SELECTGOALMODE1 error : goal selected is out of bounds');
                            error('PARTITIONREGIONS::SELECTGOALMODE1 error : goal selected is out of bounds');
            end
        end %end function selectGoalMode1
        
        function [yg strat task] = selectGoalMode3(obj, context01, mappC )
            %% Select the Action with Proportional Derivative MODE3
            %  yg in the region with the highest progress, which makes the
            %  most progress
            disp('PARTITIONREGIONS::SELECTGOALMODE3');
            task = randi(obj.nbTasks);
            yg = randMinMax(mappC{task}.dimY, 1, 0, 1);
            strat = randi(obj.nbStrategies);
            %try
                if length(obj.listRegions(task).LReg) > 2
                    [indMin stratMin taskMin]  = ...
                        ProportionalDerivative(obj.listRegions, context01, mappC, obj.nbStrategies, obj.nbTasks);
                    if (~isempty(indMin) &&  indMin>1 && ~isempty(stratMin) ...
                        && stratMin >=1 && stratMin<= obj.nbStrategies ...
                        && ~isempty(taskMin) && taskMin>=1 && taskMin<=obj.nbTasks)
                       
                        dimInput = mappC{taskMin}.dimY;
                        limitMin = obj.listRegions(taskMin).LReg{indMin}{2};
                        leaf     = obj.interestRegions(taskMin).findLeaf(...
                            obj.interestRegions(taskMin), limitMin(:,1) + ( rand(dimInput,1)*10)/10.*(limitMin(mappC{taskMin}.dimYI,2) - limitMin(mappC{taskMin}.dimYI,1)));
                        if size(leaf.errors,1) > leaf.sample
                            [valMax indMax] = min(leaf.errors{stratMin}(end-leaf.sample:end,2));
                            indMax = indMax+size(leaf.errors{stratMin},1)-leaf.sample;
                        else
                            [valMax indMax] = min(leaf.errors{stratMin});
                        end
                        
                        if ~isempty(leaf.data{stratMin}(:,indMax) )
                            y = leaf.data{stratMin}(mappC{taskMin}.dimYI,indMax) + randMinMax(mappC{taskMin}.dimY, 1, -obj.watchAround, obj.watchAround);
                            ii = 0;
                            while (sum(y <= zeros(mappC{taskMin}.dimY,1))~=0) || (sum(y >= ones(mappC{taskMin}.dimY,1))~=0)
                                ii = ii+1;
                                y = leaf.data{stratMin}(mappC{taskMin}.dimYI,indMax) + randMinMax(mappC{taskMin}.dimY, 1, -obj.watchAround, obj.watchAround);
                                if ii>100
                                    leaf.data{stratMin}
                                    disp('PartitionRegions pb loop');
                                    return;
                                end
                            end
                            yg = y;
                            strat = stratMin;
                            task  = taskMin;
                        end
                    end
                end
%             catch
%                 task = randi(obj.nbTasks);
%                 yg   = randMinMax(mappC{task}.dimY, 1, 0, 1);
%                 strat = randi(obj.nbStrategies);
%             end
            
if(any(yg>100) || any(yg<0))
    disp('PARTITIONREGIONS::SELECTGOALMODE3 error : goal selected is out of bounds');
    error('PARTITIONREGIONS::SELECTGOALMODE3 error : goal selected is out of bounds');
end
        end
        
        %display the min and max of regions according along the specified
        %dimension
        function limitsRegions(obj,dim)
            for it =1:obj.nbTasks
                limitsRegions(obj.listRegions(it),dim);
            end
        end  %end function limitsRegions
        
        %draw the regions in 1D or 2D
        function dispRegions(obj, iStrat)
            for it=1:obj.nbTasks
                subplot(1,obj.nbTasks,it);
                dispLReg(obj.listRegions(it),iStrat);
            end
        end %end dispRegions
        
    end %end methods
    
    
    methods(Static =true)
        
        function test1D()
            % for dimY = 1D
            % test creating 2 regions [0-0.5] and [0.5-1]
            clear all
            close all
            saggGlobal
            variablesGlobales
            DimensionIn = 14
            DimensionOut = 1
            DimensionCont = 3;
            dimParam      =  14;
           
            
            Ymax = [ 1];
            Ymin = [ -1];
            mapp{1}  = Mapping(DimensionIn,DimensionOut,DimensionCont,Ymin,Ymax);
            epmem{1} = EpisodicMemory(mapp,1,1,1);
            
            dimParam      =  14; 
            
            watchAround    = 0.01;
            sample         = 20;
            NumReg        = 1;                                                      % ------Parameters
            CriterRegions = 20;               %taille des regions

            partReg = PartitionRegions( CriterRegions,  sample, NumReg, mapp, 1, 1,watchAround);
          
            iStrategy =1;
            
            for i =1:1000%randperm(1000)
                example    = 0.5-i/2000;
                errorValue       = -(1000-i)/100000;
                partReg = updatePartitionRegions(partReg, example, errorValue, 1, iStrategy, 1) ;
                
                example    = 0.5+i/2000;
                errorValue       = -(1000-i)/1000;
                partReg = updatePartitionRegions(partReg, example, errorValue, 1, iStrategy, 1) ;
                
            end
            
            figure;
            dispRegions(partReg,1);
           
            limitsRegions(partReg.listRegions,1)
        end %end test1D
        
        
         function test1D2Strat()
            % for dimY = 1D for 2 strategies
            % test creating 2 regions [00.5] and [0.5-1]
            close all
            clear all
            saggGlobal
            variablesGlobales
            DimensionIn = 14
            DimensionOut = 1
            DimensionCont = 3;
            dimParam      =  14;
            
            
             Ymax = [ 1];
            Ymin = [ -1];
            mapp{1}  = Mapping(DimensionIn,DimensionOut,DimensionCont,Ymin,Ymax);
            epmem{1} = EpisodicMemory(mapp,1,1,1);
            
            dimParam      =  14;
            
            watchAround    = 0.01;
            sample         = [20 20];
            NumReg        = 1;                                                      % ------Parameters
            CriterRegions = 20;               %taille des regions

            global costStrat
            costStrat = [1 1];
            %      ProbMode1 = 0.7;
            %     ProbMode3 = 0.2;
            %     epmem = EpisodicMemory(mapp, hTeacher);
            
            partReg = PartitionRegions( CriterRegions,  sample, NumReg, mapp, 2,1, watchAround);
            
            
            for i =1:1000
                example          = i/2000;
                errorValue       = -(1000-i)/100000;
                partReg = updatePartitionRegions(partReg, example, errorValue, 1, 1,1) ;
            
                errorValue       = -(1000-i)/1000;
                partReg = updatePartitionRegions(partReg, example, errorValue, 1, 2,1) ;

                example          = 0.5+i/2000;
                errorValue       = -(1000-i)/2000;
                partReg = updatePartitionRegions(partReg, example, errorValue, 1, 1,1) ;
                errorValue       = -(1000-i)/10000;
                partReg = updatePartitionRegions(partReg, example, errorValue, 1, 2,1) ;
            end
            
            figure;
            dispRegions(partReg,1);
            figure;
            dispRegions(partReg,2);
            limitsRegions(partReg.listRegions,1)
            
            
            %{
displays
dispLReg1D
obj.LReg(2) : [0.5015-1] : 998
obj.LReg(3) : [0.5015-1] : -0.491
obj.LReg(4) : [0.5045-1] : 998
obj.LReg(5) : [0-0.5015] : -0.009825
obj.LReg(6) : [0.0045-0.5015] : 998
obj.LReg(7) : [0-0.0045] : -0.0025
dispLReg1D
obj.LReg(2) : [0.5015-1] : 998
obj.LReg(3) : [0.5015-1] : -0.0982
obj.LReg(4) : [0.5045-1] : 998
obj.LReg(5) : [0-0.5015] : -0.983
obj.LReg(6) : [0.0045-0.5015] : 998
obj.LReg(7) : [0-0.0045] : -0.0005
min is 0.5015 0.5015 0.5045 0 0.0045 0
max is 1 1 1 0.5015 0.5015 0.0045

            %}
        end %end test1D2Strat
        
        
        function test_selectGoalMode3()
            global costStrat
            costStrat = ones(1, partReg.nbStrategies)
            context01 = [1 1 1];
%             context =  getContext(envSimu);
%             context01 = (context +1)/2
            sel3 =[];
            for i=1:10000
                [y s t] = selectGoalMode3(partReg, context01, mapp );
                sel3= [sel3; y s ];
            end
            figure
            hist(sel3(:,1),[0:0.01:1])
        end
        
          function test_selectGoalMode1()
            global costStrat
            costStrat = ones(1, partReg.nbStrategies)
            context01 = [1 1 1];
            %             context =  getContext(envSimu);
            %             context01 = (context +1)/2
            sel1 =[];
            for i=1:10000
                [y s t] = selectGoalMode1(partReg, context01, mapp );
                if t==1
                sel1= [sel1; y(:)' s t];
                else
                sel1= [sel1; y 0 s t];                    
                end
            end
            figure
            subplot(2,1,1)
            title('task 1');
            ind1 = find(sel1(:,end)==1);
            hist3(sel1(ind1,1:2))
            xlabel('y');
            ylabel('strategy');
            subplot(2,1,2)
            title('task 2');
            ind2 = find(sel1(:,end)==2);
            hist(sel1(ind2,1))
            xlabel('y');
            ylabel('strategy');
        end
    end %end static methods
    
    
end

