classdef TestDM
    
    methods (Static = true)

        function [yBest, paramBest] = bestLocality(obj, yg, distM)
            neighbour1 = nearestInv( obj, yg, 30);
            dist1      = distSqrt(yg, neighbour1(:, obj.treeInv.dimInput));
            ind1       = find(dist1< distM);
            if isempty(ind1)
                ind1   = [1];
            end
            
            neighbour1 = neighbour1(ind1,:);
            nb1        = size(neighbour1,1);
            
            yBest   = [];
            paramBest   = [];
            
            if nb1>=1
                denom    = 0;
                num      = 0;
                param1   = neighbour1(:, obj.treeInv.dimOutput);
                y1       = neighbour1(:, obj.treeInv.dimInput);
                
                minVariance = Inf;
                indBest     = 1;
                
                
                for i=1:nb1
                    varM             = 0.05;
                    neighbour2       = nearestDirect(obj, param1(i,:), 30);
                    dist2            = distSqrt(param1(i,:), neighbour2(:, obj.treeDirect.dimInput));
                    ind2             = find(dist2< 24* distM);
                    
                    if isempty(ind2)
                        ind2   = [1];
                    end
                    
                    neighbour2       = neighbour2(ind2,:);
                    param2           = neighbour2(:, obj.treeDirect.dimInput);
                    y2               = neighbour2(:, obj.treeDirect.dimOutput);
                    variance2        = mean(var(y2));
                    varDist2         = distVar(yg, y1(i,:), variance2);
                    if ( (i==1) || (varDist2 < minVariance) )
                        minVariance  = varDist2;
                        indBest      = i;
                        yBest        = y2;
                        paramBest    = param2;
                        varianceBest = variance2;
                    end
                end
                
            end
        end
        
        function  yOut = directModel(paramIn,  gauss, distN, obj, rFish)            
            minVariance = Inf;
            indBest     = 1;
            
            
            varM             = 0.05;
            neighbour2       = nearestDirect(obj, paramIn, 30);
            y2               = neighbour2(:, obj.treeDirect.dimOutput);
            ind1             = find(y2(:,3) <0.01);  %only select experiences where the hook touches the water
            if isempty(ind1)
                ind1   = [1];
            end
            neighbour2       = neighbour2(ind1,:);
            dist2            = distSqrt(paramIn, neighbour2(:, obj.treeDirect.dimInput));
            ind2             = find(dist2< distN); %define a locality by distance
            
            if isempty(ind2)
                ind2   = [1];
            end
            
            neighbour2       = neighbour2(ind2,:);
            param2           = neighbour2(:, obj.treeDirect.dimInput);
            y2               = neighbour2(:, obj.treeDirect.dimOutput);
            
           % gauss  = (distM)^2; %distSqrt(yg, size(yg) ).^2;
            distSqrt(paramIn, param2);
            coef   = exp(  - (( distSqrt(paramIn, param2) ).^2 )./gauss);
            num    = sum(coef'*y2, 1);
            denom  = sum(coef);
            if(denom == 0)
                yOut = 2*(rand(1, 3)- 0.5);
            else
                yOut = num/denom;
            end
        end
        
        function test_directModel(filename1, filename2, n1, n2)
            % test data
            load(filename1)
            nMem = size(epmem.listLearning,1)
            listTest = epmem.listLearning(n1:n2,:);
            
            % memory data
            load(filename2)
            distN = 0.1;
            gauss  = (distM)^2;
            
            error = 0;
            for i= 1:n2-n1
                paramTest =listTest(i,1:mapp.dimA);
                yTest = listTest(i,(mapp.dimA+1):(mapp.dimA+mapp.dimY));
                yOut = TestDM.directModel(paramTest, gauss, distN, epmem, rFish);
                i
                error = error + distSqrt(yOut, yTest)
            end
            error
            
        end
        
        
         function test_findParam(filename1, filename2, n1, n2)
            % test data
            
            load(filename1)
            nMem = size(epmem.listLearning,1)
            listTest = epmem.listLearning(n1:n2,:);
            
            % memory data
            load(filename2)
            
            
            %  distMList = [0.01:0.05:0.21 0.25:0.2:0.45];
            gaussList = [0.1:0.2:2];
            distNList = [0.1:0.2:4.0]
            %             distMList = [0.005:0.005:0.2 0.25:0.05:1];
            %             gaussList = [0.005:0.005:0.2 0.25:0.05:1];
            errorList = [];
            %  for distM = distMList
            for gauss = gaussList  % for each set of parameter values
                for distN = distNList
                    error = 0;
                    for i= 1:n2-n1
                        paramTest = listTest(i,1:mapp.dimA);
                        yTest = listTest(i,(mapp.dimA+1): ...
                            (mapp.dimA+mapp.dimY));
                        yOut = TestDM.directModel(paramTest, gauss, distN, epmem, rFish);
                        error = error + distSqrt(yOut, yTest);
                    end
                    errorList = [errorList;  gauss, distN, error]
                    plot(distN,error)
                    hold on
                end
            end
            %  save testDMfindParam110926
            
            %  end
            save testDMfindParam110929_1
            
            %plot surface
            j = 1;
            i = 1;
            k = 1;
            for gauss = gaussList
                for distN = distNList
                    surfList(i,j) = errorList(k,3);
                    j = j+1;
                    k = k+1;
                end
                j = 1;
                i = i+1;
            end
            
            surf( distNList, gaussList, surfList)
            
            %find min
            minLoc = Inf;
            j = 2;
            i = 2;
           % for distM = distMList(2:end-4)
                for gauss = gaussList(2:end-4)
                    sumLoc = sum(sum(surfList(i-1:i+1,j-1:j+1)));
                    if sumLoc<minLoc
                        minLoc = sumLoc;
                        minD = distM;
                        minG = gauss;
                    end
                    j = j+1;
                end
                i = i+1;
                j= 2;
           % end
            
            
            
         end
        
        
         function test_findParam2(filesDir)
             
             n2 = 4501;
             
             n1 = 4001;
             filesSGIM1 = dir([filesDir,'SGIM_*_4501.mat'])
             filesSGIM2 = dir([filesDir,'SGIM_*_4001.mat'])

             for ifile = 1:size(filesSGIM1,1)
                 filename1 = [filesDir, filesSGIM1(ifile).name]
                 filename2 = [filesDir, filesSGIM2(ifile).name]
                 load(filename1)
                 nMem = size(epmem.listLearning,1)
                 listTest = epmem.listLearning(n1:n2,:);
                 
                 % memory data
                 load(filename2)
                 
                 
                 %  distMList = [0.01:0.05:0.21 0.25:0.2:0.45];
                 gaussList = [0.1:0.2:2];
                 distNList = [0.1:0.2:4.0]
                 %             distMList = [0.005:0.005:0.2 0.25:0.05:1];
                 %             gaussList = [0.005:0.005:0.2 0.25:0.05:1];
                 %errorList = [];
                 %  for distM = distMList
                 for gauss = gaussList  % for each set of parameter values
                     for distN = distNList
                         error = 0;
                         for i= 1:n2-n1
                             paramTest = listTest(i,1:mapp.dimA);
                             yTest = listTest(i,(mapp.dimA+1): ...
                                 (mapp.dimA+mapp.dimY));
                             yOut = TestDM.directModel(paramTest, gauss, distN, epmem, rFish);
                             error = error + distSqrt(yOut, yTest);
                         end
                         errorList = [errorList;  gauss, distN, error];
                         plot(distN,error)
                         hold on
                     end
                 end
                 %  save testDMfindParam110926
                 errorList
                 %  end
                 save testDMfindParam110929_1
             end
             
            j = 1;
            i = 1;
            k = 1;
            surfList = zeros(length(gaussList), length(distNList));
            while k< size(errorList,1) 
            for gauss = gaussList
                for distN = distNList
                    surfList(i,j) = surfList(i,j) + errorList(k,3);
                    j = j+1;
                    k = k+1;
                end
                j = 1;
                i = i+1;
            end
            i = 1;
            end
            
         end
         
    end % end methods static
end