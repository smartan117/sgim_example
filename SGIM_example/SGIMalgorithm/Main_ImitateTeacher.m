%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

function Main_ImitateTeacher(hTeacherid, varargin)
% nargin == 1 means Initialization
% =======================================================================%%
%                          Main Program for imitation of the teacher      %
% =======================================================================%%


%% Initialization
variablesGlobales
saggGlobal


disp(['IMITATE TEACHER ', num2str(hTeacherid)]);



iStrat = 1;
envSimu = randomizeContext(envSimu);
context =  getContext(envSimu);
context01 = (context +1)/2;
%%=========================================================================
%%=======     Choose The Action : demonstration of the teacher    =========
%%=========================================================================
goalTaskType = hTeacher(hTeacherid).task;
curLowLevel.curTask = goalTaskType;
demonstration   = teacherGivesDemonstration(hTeacher(hTeacherid), mapp{curLowLevel.curTask},curLowLevel.curTask, envSimu);

%%=========================================================================
%%==================     For Each Demonstration    ========================
%%=========================================================================

for idemo = 1:size(demonstration,1)
    
    demoi           = demonstration(idemo,:);
    hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat goalTaskType hTeacherid demoi];
    goalOutcome = convertGC(mapp{goalTaskType}, demoi(1,end-mapp{goalTaskType}.dimY+1:end))';
    
    %%====================     Mastery Analysis     ===========================
    
    consequence = [];
    curLowLevel.curTask = goalTaskType;
    consequence         = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
    
    %%====================  update tree regions     ===========================
    example          = goalOutcome(:);
    example          = example(:);
    errorValue       = consequence{1};
    try
        partReg          = updatePartitionRegions(partReg,  example, errorValue, TagMax, iStrat, curLowLevel.curTask) ;
    catch
        disp('erreur');
    end
    
    QualityCandidate = [QualityCandidate; [goalOutcome(:)' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY,1) consequence{1} curLowLevel.nbExperiments goalTaskType iStrat]];
    
    Predictor = [Predictor; [goalOutcome' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) errorValue curLowLevel.nbExperiments goalTaskType iStrat]];
    
    
    if size(epmem{curLowLevel.curTask}.listGoal,1)>2 && curLowLevel.nbExperiments>2
        ProgressL = [ProgressL; consequence{3} iStrat  curLowLevel.nbExperiments];
    end
    
    nReachedOutcomes2 = size(consequence{2},1);
    nReachedOutcomes4 = size(consequence{4},1);
    if (nReachedOutcomes2 ~= nReachedOutcomes4)
        error('MAIN_IMITATETEACHER : error consequence2(reached outcomes) and consequence4(progress for the outcomes) do not have the same size');
    end
    
    for iTask=1:length(consequence{2})
    
        nReachedOutcomes2 = size(consequence{2}{iTask},1);
        nReachedOutcomes4 = size(consequence{4}{iTask},1);
        if (nReachedOutcomes2 ~= nReachedOutcomes4)
            error('MAIN_RIAC_CB : error consequence2(reached outcomes) and consequence4(progress for the outcomes) do not have the same size');
        end

        for i = 1:nReachedOutcomes2
            example    = convertGC(mapp{iTask},consequence{2}{iTask}(i,:));
            example    = example(:);

            if(any(example>100) || any(example<0))
                error('example out of bounds. It should take values between 0 and 1');
            end

            errorValue = consequence{4}{iTask}(i,1);
            try
                partReg    = updatePartitionRegions(partReg, example, errorValue, TagMax, iStrat, iTask);
            catch
                disp('erreur');
            end
            Predictor  = [Predictor; [example' zeros(1,maxDimY - mapp{iTask}.dimY) errorValue curLowLevel.nbExperiments iTask iStrat]];
        end

    end
    
%     for i = 1:nReachedOutcomes2
%         example    = convertGC(mapp{goalTaskType},consequence{2}(i,:));
%         example    = example(:);
%         errorValue = consequence{4}(i,1);
%         try
%             partReg = updatePartitionRegions(partReg,example, errorValue, TagMax, iStrat, curLowLevel.curTask) ;
%         catch
%             disp('erreur');
%         end
%         Predictor = [Predictor; [consequence{2}(i,:) zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) errorValue curLowLevel.nbExperiments goalTaskType iStrat]];
%     end
    
    
end

%====================end of the loop for each demonstration ===============
%==========================================================================
%==========================================================================

disp('end imitate teacher');
end





