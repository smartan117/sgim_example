% distVar.m
%% Author: Sao Mai Nguyen
%% date: January 2011
%
% calculates a distance measure between deltaTO and gDeltaTO, taking
% account of the variance var

function outDist = distVar(gDeltaTO, deltaTO, variance )
alpha = 0.5 ;  % parameter to adjust
n1    = size(deltaTO ,1);
n2    = size(variance,1);
if n1~=n2 && n2 ~= 1
    error('Matrix dimensions must agree',...
        'DISTVAR error: input arguments deltaTO and variance have different sizes');
end

if n2 ==1
    outDist = mean(distSqrt(gDeltaTO, deltaTO)) + alpha*variance;
else
    outDist = distSqrt(gDeltaTO, deltaTO) + alpha*variance
end

end