classdef InterestRegions
   % Copyright (c) Sao Mai Nguyen,
   %               INRIA Bordeaux - Sud-Ouest
   %               351 Cours de la Liberation, 33400, Talence,
   %               France, http://nguyensmai.free.fr
   %
    
    
   properties
       data                   % data=goalOutcome 
       C1                     % criteria for spliting a region: nb max of samples
       errors                 % [0 performance]
       numReg
       sample                 %max number of data in a region
       derivative
       derivativeList
       tag
       tagAlea
       left                   % left tree (InterestRegion)
       right                  % right tree (InterestRegion)
       dim
       cut
       dist
       limit                  % table [min_dim1 max_dim1, min_dim2 max_dim2, ...] of dimension dimInput
       activateChildren       % flag has children (1) or not (-1)
%        numLReg
%        LReg
%        LRegMin
       dimInput               % int representing the dimension of the input space
       nbStrategies
   end
   
   
   
   
   methods
       
       function tree = InterestRegions(C1Tmp, sampleTmp, numRegTmp, dimInput, nbStrategies)
           % data x & y
           % length of the Region
           tree.C1     = C1Tmp ;
           tree.numReg = numRegTmp;
           tree.nbStrategies = nbStrategies;
           % Val for derivative
           tree.sample = sampleTmp ;
           for iStrat=1:nbStrategies
               tree.data{iStrat}   = [] ;
               tree.errors{iStrat} = [] ;
               tree.derivative{iStrat} = inf ;
               tree.derivativeList{iStrat} = [];
           end
           tree.tag = [];
           tree.tagAlea = [];
           
           % Pointer
           tree.left = -1 ;
           tree.right = -1 ;
           % Dimension
           tree.dim = 0;
           % Val
           tree.cut   = -1;
           tree.dist  = -1;
           tree.limit = repmat([0 1],dimInput,1);
           tree.activateChildren = -1;  
           tree.dimInput         = dimInput; 
       end % end function createTree
    
       
       
       
       function [t listReg] = updateTree(t, example, errorValue, TagMax, listReg, iStrategy)
           % example is vector(goalOutcome) and
           % errorValue is = consequence{1};
           %% Recursive research % ===================================================
           example = example(:);
           if (t.dim ~= 0)
               %% New updateTree =======================================
               
               %=== DEAL WITH THE ERRORVALUE
               % add the error value to the tree
               t.errors{iStrategy}(1,end + 1) = errorValue ;
               nerrors = size(t.errors{iStrategy},2) ;
               % if there are already a min nb of values
               if nerrors > t.sample(iStrategy)
                   t.errors{iStrategy} = t.errors{iStrategy}(:,2:end);
                   if t.left.derivative{iStrategy} == 998 || t.right.derivative{iStrategy} == 998 % the node has 2 deactivated children
                       % DIM 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%
                       % compute the derivative and update the node
                       errorsDim = t.errors{iStrategy}(1,1:end);
                       for iStrat=1:t.nbStrategies
                           t.derivative{iStrat} = [ComputeDerivative(t.errors, iStrat)];
                       end
                       listReg.LReg{listReg.numLReg}{1} = cell2mat(t.derivative);
%                        listReg.LReg{listReg.numLReg}{1}(iStrategy) = t.derivative{iStrategy};
                       %                       listReg.LReg{listReg.numLReg}{1}(iStrategy) = [t.derivative{iStrategy}];
                   else %activate the children nodes
                       t.left.activateChildren  = 1;
                       t.right.activateChildren = 1;
                       for iStrat=1:t.nbStrategies
                           listReg.LReg{listReg.numLReg}{1}(iStrat) = 999;
                       end
                   end
                   %         t.LReg{t.numLReg}{3} = mean(t.errors);
               end
               % End  =======================================
               
               
               %=== DEAL WITH EXAMPLE
               if example(t.dim) < t.cut
                   [t.left listReg]  = updateTree(t.left, example, errorValue, TagMax, listReg, iStrategy) ;
               else
                   [t.right listReg] = updateTree(t.right, example, errorValue, TagMax, listReg, iStrategy) ;
               end
           else
               %% End Recursive research ===============================================
               if (~isempty(t.data{iStrategy}))
                   nexamples = size(t.data{iStrategy}, 2) ;
               else
                   nexamples = 0 ;
               end ;
               
               nerrors = size(t.errors{iStrategy},2);
               t.data{iStrategy}(:, nexamples + 1)  = example(:) ;
               t.errors{iStrategy}(1,nerrors + 1) = errorValue ;
               
               
               if TagMax == 1
                   t.tag(nerrors+1) = 1;
               else
                   t.tag(nerrors+1) = 0;
               end
               
               % DIM 2 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
               errorsDim = t.errors{iStrategy};
               if length(errorsDim) > t.sample(iStrategy)
                   for iStrat=1:t.nbStrategies
                       t.derivative{iStrat} = [ComputeDerivative(t.errors, iStrat)];
                       t.derivativeList{iStrategy}{end+1} = t.derivative{iStrategy} ;
                   end
                   if  t.activateChildren == 1
                        listReg.LReg{listReg.numLReg}  = [cell2mat(t.derivative), {t.limit}];
                   end
               end
               
               surfaceRegion = 1;
               for idim = 1:t.dimInput
                   size1 = t.limit(idim,2) - t.limit(idim,1);
                   surfaceRegion = surfaceRegion * size1;
               end
               
               if (nexamples+1 > t.C1(iStrategy))  && t.activateChildren == 1 && surfaceRegion > 1/10000 % We cut the node, but not activating the regions
                   [cutResult, dimension, data1, data2, e1, e2, t1, t2, dist] = Split2(t.data{iStrategy}, t.errors{iStrategy}, t.tag, 3, t.dimInput);

                   if ~isnan(dist)
                       t.dim           = dimension;
                       listReg.numLReg = listReg.numLReg + 1;
                       
                       t.left  = InterestRegions(t.C1, t.sample, listReg.numLReg, t.dimInput, t.nbStrategies) ;
                       t.right = InterestRegions(t.C1, t.sample, listReg.numLReg, t.dimInput, t.nbStrategies) ;
                       
                       for iStrat=1:t.nbStrategies
                           if ~isempty(t.data{iStrat})
                           [data1, data2, e1, e2, t1, t2] = split(t.data{iStrat}, t.errors{iStrat}, t.tag, cutResult, dimension);
                           t.left.data{iStrat}            = data1;
                           t.left.errors{iStrat}          = e1;
                           t.left.derivative{iStrat}      = 998 ;
                           t.left.derivativeList{iStrat}  = t.derivativeList{iStrat};
                           t.right.data{iStrat}           = data2;
                           t.right.errors{iStrat}         = e2;
                           t.right.derivative{iStrat}     = 998 ;
                           t.right.derivativeList{iStrat} = t.derivativeList{iStrat};
                           end
                       end
                       
                       
                       t.right.dist = dist;
                       t.right.tag = [t.tag t2];
                       t.right.limit = t.limit;
                       t.right.limit(dimension,1) = cutResult;
                       listReg.LReg{listReg.numLReg}  = [cell2mat(t.right.derivative), {t.right.limit}];
                       %                        listReg.LReg{listReg.numLReg}  = [999*ones(1,t.nbStrategies), {t.right.limit}];
                       %                        listReg.LReg{listReg.numLReg}{1}(iStrategy)  = t.right.derivative{iStrategy};
                       %                        listReg.LReg{listReg.numLReg}{2}  = t.right.limit;
                       listReg.numLReg = listReg.numLReg + 1;
                       t.left.dist  = dist;
                       t.left.tag   = [t.tag t1];
                       t.left.limit = t.limit;
                       t.left.limit(dimension,2) = cutResult;
                       listReg.LReg{listReg.numLReg}  = [cell2mat(t.left.derivative), {t.left.limit}];
                       %                        listReg.LReg{listReg.numLReg}  = [999*ones(1,t.nbStrategies), {t.left.limit}];
                       %                        listReg.LReg{listReg.numLReg}{1}(iStrategy)  = t.left.derivative{iStrategy};
                       %                        listReg.LReg{listReg.numLRe
                       %                        g}{2}  = t.left.limit;
                       t.cut = cutResult ;
                   end
               end
               
           end
           
       end %end function updateTree
       
       
       
       
   end %end methods
    
   methods (Static = true)
       
       function leaf = findLeaf(tree, v)
           %Find the leaf of tree which contains the learning input v
           
           if tree.dim ~= 0
               if  v(tree.dim) < tree.cut
                   leaf = InterestRegions.findLeaf(tree.left, v) ;
               else
                   leaf = InterestRegions.findLeaf(tree.right, v) ;
               end 
           else
               leaf = tree ;
           end ;
           
       end  %end function findLeaf
       
       
       
       function test_Update()
           iStrat = 1;
           example   = 0.1*ones(partReg.interestRegions(1).dimInput,1);
           errorValue       = 1;
           TagMax = 1;
           [partReg.interestRegions(1) partReg.listRegions(1)] = ...
               updateTree(partReg.interestRegions(1), example, errorValue, TagMax, partReg.listRegions(1), iStrat) ;
 
       end
       
   end %end methods static



end