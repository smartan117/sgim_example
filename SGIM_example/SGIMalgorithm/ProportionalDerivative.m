function [idRegion, stratRegion taskRegion] = ProportionalDerivative(listRegions, ...
                                                      context, mapp, nbStrategy, nbTask)
%% Returns a region randomly according to its progress
%
% Copyright (c) 2013 Sao Mai Nguyen
%               e-mail : nguyensmai@gmail.com
%               http://nguyensmai.free.fr/
%
% Permission is granted to copy, distribute, and/or modify this program
% under the terms of the GNU General Public License, version 2 or any
% later version published by the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details
% 
% If you use this code in the context of a publication, I would appreciate 
% it if you could cite it as follows:
% 
% 
% @phdthesis{Nguyen2013,
% 	Author = {Nguyen, Sao Mai},
% 	School = {INRIA},
% 	Title = {A Curious Robot Learner for Interactive Goal-Babbling: Strategically Choosing What, How, When and from Whom to Learn},
% 	Year = {2013}}
%
%nbStrategy = 1;
global costStrat

nbLRegTmp =0;
for iTask=1:nbTask
    nbLRegTmp = nbLRegTmp + nbStrategy*length(listRegions(iTask).LReg);
end

LRegTmp  = zeros(nbLRegTmp,4);
c = 0;                     %counts the number of valid regions
s = zeros(1,nbStrategy);   %counts the number of strategies
t = zeros(1,nbTask);       %counts the number of tasks

%% list all valid regions
if length(costStrat)~=nbStrategy
    error('PROPORTIONALDERIVATIVE:: error with the dimension of costStrat');
end

for iTask =1:nbTask
for iStrategy =1:nbStrategy
    for i=2:length(listRegions(iTask).LReg)
        val = listRegions(iTask).LReg{i}{1}(iStrategy);
        if val ~= 999 && val ~= 998 && ~isnan(val) && val ~= Inf && val ~= -Inf
            if val ~= 0
                c = c +1;
              %  LRegTmp(c,:) = [val*costStrat(iStrategy)/sqrt((mapp{iTask}.yMin-mapp{iTask}.yMax)*(mapp{iTask}.yMin'-mapp{iTask}.yMax')),...
               %     i,iStrategy, iTask];
                   LRegTmp(c,:) = [val/costStrat(iStrategy),...
                    i,iStrategy, iTask];
            end
            s(iStrategy) = 1;
            t(iTask)     = 1;
        end
    end
end
end

%LRegTmp(1:20,:)
% LRegTmp(1:20,1)
% 
% t
% s

%% choose one region with proba ~ progress
if ~any(t)
    idRegion    = 1;
    stratRegion = randi([1,nbStrategy]);
    taskRegion  = randi([1,nbTask]);
else
    if ~all(s)||~all(t)
        taskRegion  = randi([1,nbTask]);
        idRegion    = randi([1,length(listRegions(taskRegion).LReg)]);
        stratRegion = randi([1,nbStrategy]);
    else
        nbL = size(LRegTmp,1);
        LRegTmp = LRegTmp(randperm(nbL),:);
        LRegTmp = sortrows(LRegTmp,1);
        LRegTmp(:,1) = LRegTmp(:,1) - LRegTmp(end,1);
        if c>10
            random = rand(1)*sum(LRegTmp(1:10,1));
        else
            random = rand(1)*sum(LRegTmp(:,1));
        end
        
        X = 0;
        
        while random < 0
            X = X + 1;
            random = random - LRegTmp(X,1);
        end
        
        if X>0
            idRegion    = LRegTmp(X,2);
            stratRegion = LRegTmp(X,3);
            taskRegion  = LRegTmp(X,4);
        else
            %         idRegion = 1;
            %         stratRegion = 1;
            taskRegion  = randi([1,nbTask]);
            idRegion    = randi([1,length(taskRegion)]);
            stratRegion = randi([1,nbStrategy]);
        end
    end
end

%{
test

context =  getContext(envSimu);
context01 = (context +1)/2
distr = []
for i=1:10000
[idRegion, stratRegion taskRegion] = ProportionalDerivative(partReg.listRegions, context01, mapp, partReg.nbStrategies,partReg.nbTasks);
distr = [distr; idRegion, stratRegion taskRegion];
end

figure
hist3(distr(:,2:3))


lmin = [];
for i=1:size(distr,1)
lmin = [lmin; partReg.listRegions(distr(i,3)).LReg{distr(i,1)}{2}(1)];
end
figure
hist(lmin,[0:0.01:1])
lmax = [];
for i=1:length(distr)
lmax = [lmax; partReg.listRegions(distr(i,3)).LReg{distr(i,1)}{2}(2)];
end
figure
hist(lmax,[0:0.01:1])

figure
for i=1:length(distr)
plot([lmin(i) lmax(i)], [i i],'-')
hold on
end

for i=1:length(distr)
partReg.listRegions(distr(i,3)).LReg{distr(i,1)}{1}
end

%}

end