%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France, 
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate 
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

classdef HumanTeacher < handle
    %Human teacher for a single context
    properties
        present;
        frequency;
        listTeacher;
        %regTeach;
        regTaught; %table of [y1min,y1max,y2min,y2max,nbTaught, a, y]
        regDemos; %table of [nbTaught a y]
        teacherfile;
        task;
    end
    
    methods
        
        function obj = HumanTeacher(pres, freq,task, file )
            % class constructor
            obj.present     = pres;
            obj.frequency   = freq;
            obj.listTeacher = [];
            obj.task        = task;
                        
            if (pres<=0)
                obj.frequency = 0;
            end
            
            if nargin ==4
                if ~isempty(file) && isstr(file)
                    obj.teacherfile = file;
                    load(file)
                    nbTeaching = size(regTaught,1);
                    obj.regTaught = regTaught(randperm(nbTeaching),:);
                    obj.regDemos = regDemos(randperm(nbTeaching),:);
                    % obj.regTeach    = regTeach;
                end
            end
            disp('HUMAN TEACHER constructor end');
        end %end function
        
        
        
        function demonstration = teacherGivesDemonstration(obj, ...
                mapp,task, aH)
            % selects which demonstration to give to the robot
            % demonstration is a list of learningEntity =[movementParameters goal]
            % variablesGlobales
            
            disp(['$$$ HUMANTEACHER : TEACHER GIVES DEMONSTRATION ' ...
                'start $$$ ']);
            if task ==obj.task
                nbTeaching = size(obj.regTaught,1);
                if nbTeaching < 10000
                    obj.regTaught = sortrows(obj.regTaught(randperm(nbTeaching),:),2*(mapp.dimY)+1);
                    demonstration= obj.regTaught(1,2*(mapp.dimY)+2:end);
                    obj.regTaught(1,2*(mapp.dimY)+1) = obj.regTaught(1,2*(mapp.dimY)+1) +1;
                else
                    i = randi(nbTeaching) ;
                    demonstration= obj.regTaught(i,2*(mapp.dimY)+2:end);
                    obj.regTaught(i,2*(mapp.dimY)+1) = obj.regTaught(i,2*(mapp.dimY)+1) +1;
                end
            else
                demonstration = [randParameters(aH) ...
                    (rand(1,mapp.dimY).*(mapp.yMax-mapp.yMin)+mapp.yMin)];
            end
            disp('$$$ HUMANTEACHER : TEACHER GIVES DEMONSTRATION end $$$ ');
            
        end
        
%         function demonstration = teacherAnswersGoalRequest(obj, yReq, ...
%                 mapp, aH)
%             disp('$$$ HUMANTEACHER : TEACHER REQUEST DEMONSTRATION start $$$ ');
%             %yReq = convertGC(mapp,yReq);
%             yL = obj.regTaught(:,(end-mapp.dimY+1):end);
%             if size(yL,2) ~= numel(yReq)
%                 yReq = randMinMax(mapp.dimY, 1, 0, 1)'
%             end
%             [valmin,indmin] = min(distSqrt(yL,yReq));
%             demonstration= obj.regTaught(indmin,2*(mapp.dimY)+2:end);
%             obj.regTaught(indmin,2*(mapp.dimY)+1) = obj.regTaught(indmin,2*(mapp.dimY)+1) +1;
%             disp('$$$ HUMANTEACHER : TEACHER GIVES DEMONSTRATION end $$$ ');
%         end
        
        %% TO KEEP THINKING ABOUT
        function demonstration = teacherAnswersGoalRequest(obj, yReq, ...
                iTask, aH)
            disp('$$$ HUMANTEACHER : TEACHER REQUEST DEMONSTRATION start $$$ ');
            %yReq = convertGC(mapp,yReq);
            yL = obj.regDemos(:,aH.dimPolicies + 1 + aH.mappOutTasks{iTask});
            if size(yL,2) ~= numel(yReq)
                yReq = randMinMax(length(aH.dimOutcomes{iTask}), 1, 0, 1)'
            end
            [valmin,indmin] = min(distSqrt(yL,yReq));
            indY = (aH.dimPolicies + 1) + aH.mappOutTasks{iTask};
            ind = [2:(aH.dimPolicies + 1) indY];
            demonstration= obj.regDemos(indmin,ind);
            obj.regDemos(indmin,1) = obj.regDemos(indmin,1) +1;
            disp('$$$ HUMANTEACHER : TEACHER GIVES DEMONSTRATION end $$$ ');
        end
        
        function obj = updateExploredRegions(obj, learningEntity, mapp)
            %LearningEntity = [movParam posHook{1} posHook{2} posHook{3} exploType]
            y = learningEntity(1,mapp.dimYL);
            i = find( obj.regTaught(:,1)<=y &  obj.regTaught(:,2)>y );
            obj.regTaught(i,2*(mapp.dimY)+1) = obj.regTaught(i,2*(mapp.dimY)+1) +1;
        end
        
        
        
    end %end methods
    
    
    methods(Static = true)
        
        function plotRegTeach(filename)
            load(filename)
            [m,n] = size(regTeach)
            countReg = 0;
            for i= 1:m
                for j=1:n
                    if ~isempty(regTeach{i,j}) && size(regTeach{i,j},2)== 2
                        countReg = countReg + 1;
                        i
                        j
                        regTeach{i,j}{2}(end-1:end)
                        plot(regTeach{i,j}{2}(end-1), regTeach{i,j}{2}(end), 'o')
                        hold on
                    end
                end
            end
            countReg
        end %end plotRegTeach
        
        function plotRegTaught(filename)
            load(filename)
            [m,n] = size(regTaught)
            countReg = 0;
            % for i= 1:m
            plot(regTaught(:,end-1), regTaught(:,end), 'og')
            hold on
            axis square
            xlim([-2 2])
            ylim([-2 2])
            %end
            
        end %end plotDemo
        
        function testTGD()
            mapp = Mapping(25,3,[-2 -2 0],[2 2 0]);
            hTeacher = HumanTeacher(0, 10, 'listTeacherFileMai902.mat');
            hTeacher.regTaught = rand(4,25+6+1);
            hTeacher.regTaught(:,2*2+1) = randperm(4)';
            disp(hTeacher.regTaught);
            learningEntity = teacherGivesDemonstration(hTeacher,mapp)
            hTeacher = updateExploredRegions(hTeacher, learningEntity, mapp);
            disp(hTeacher.regTaught)
        end
        
        % for each demonstration, reexecute and compare if the output in
        % the task-space is the same
        function testTeachingData()
            listDemoi= [];
            nbTeaching = 10;%size(hTeacher.regTaught,1);
            task = 1;
            for i= 1:nbTeaching
                demoi = hTeacher.regTaught(i, 2*(mapp{task}.dimY)+2:end);
                listDemoi = [listDemoi; demoi];
                % demoa = demoi(1:mapp{task}.dimA);
                run1imitation(curLowLevel,30, demoi, epmem, mapp,1);
            end
            plot(listDemoi(:,end-1),listDemoi(:,end),'xb');
            hold on
            plot(epmem{task}.listLearning(:,mapp{task}.dimA+mapp{task}.dimC+1), ...
                epmem{task}.listLearning(:,mapp{task}.dimA+mapp{task}.dimC+2),'or');
            hold on
            title('testTeachingData');
            
        end
        
        function buildTeacherData(dirName, itask)
            %%load data
            filesReg = dir([dirName,'*6001.mat'])
            listLearning = [];
            for i=1:numel(filesReg)
                filesReg(i).name
                load([dirName,filesReg(i).name])
                listLearning = [listLearning; epmem{itask}.listLearning];
                
            end
            
            save humanTeacher_buildTeacherData_collectedListLearning
            
            outcomes = listLearning(:,mapp{task}.dimYL);
            nbOutcomes = size(listLearning,1);
            dimOutcomes = size(outcomes,2);
            %compute the grid
            for dimY =1:size(outcomes,2)
                x = outcomes(:,dimY);
                xmin(1,dimY) = min(x);
                xmax(1,dimY) = max(x);
            end
            ndx = 100;
            dx = (xmax -xmin)/ndx;

             [gridcell1 gridcell2] = ndgrid(linspace(xmin(1),xmax(1),ndx),linspace(xmin(2),xmax(2),ndx))
%             %for each grid cell
%             xInf = xmin;
%             xSup = xmin+dx;
            dimY = dimOutcomes;
            dimYmin = dimOutcomes;
            regTaught = [];
            
            for i1= 1:numel(gridcell1)-ndx
                     xInf(1,1) = gridcell1(i1);
                     xSup(1,1) = gridcell1(i1+1);
                     xInf(1,2) = gridcell2(i1);
                     xSup(1,2) = gridcell2(i1+ndx);
        %    while (all(xInf<xmax) && dimY <= dimOutcomes && dimY>0)
                ind = find( all(repmat(xInf,nbOutcomes,1) <= outcomes,2) & ...
                    all(outcomes <= repmat(xSup,nbOutcomes,1),2) );
                locality = listLearning(ind,:);
                imax = size(locality,1);
                
                if imax>1
                    r = randi(imax,1);
                    boundaries = zeros(1,2*dimOutcomes);
                    for j = 0:dimOutcomes-1
                        boundaries(1, 2*j+1:2*j+2)  = [xInf(j+1) xSup(j+1)];
                    end
                    regTaught = [regTaught;  boundaries, 0, listLearning(r,mapp{task}.dimAL)];
                end
                
            end
                
%                 %next cell
%                 if xSup(dimY)<xmax(dimY)
%                     xInf(dimY) = xInf(dimY) + dx(dimY);
%                     xSup(dimY) = xInf(dimY) + dx(dimY);
%                 elseif(dimY==dimYmin)
%                     xInf(dimY) = xmin(dimY);
%                     xSup(dimY) = xInf(dimY) + dx(dimY);
%                     dimYmin = dimYmin-1;
%                     dimY = dimOutcomes;
%                    % break;
%                     if (dimY>0)
%                     xInf(dimY) = xInf(dimY) + dx(dimY);
%                     xSup(dimY) = xInf(dimY) + dx(dimY);
%                     end
%                 else
%                     dimY = dimY -1;
%                     if (dimY>0)
%                         xInf(dimY) = xInf(dimY) + dx(dimY);
%                         xSup(dimY) = xInf(dimY) + dx(dimY);
%                     end
%                 end
                
                
          %  end %while
            
            save teacherSet
        end  % function teacherBuildData
        
    end %end static methods
    
end