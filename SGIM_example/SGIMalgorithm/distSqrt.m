% distSqrt
%% Author: Sao Mai Nguyen
%% date: December 2010
%%
% computes the distance between 2 points or a point and a list of points
%
% d = distSqrt(a,b)
% a : m1xn vector
% b : m2xn vector
% for the time being, at least one of m1 or m2 must be equal to 1
%

function d = distSqrt(a,b)

[mA, nA] = size(a);
[mB, nB] = size(b);

if nA ~= nB
    error(['DISTSQRT ERROR the two input arguments sizes are incompatible ',num2str(nA),' ' num2str(nB)]);
elseif mA == mB
        d = sum((a-b).^2);
elseif mA == 1
    for i=1:mB
        d(i) = sum((a-b(i,:)).^2);
    end
elseif mB == 1
    for i=1:mA
        d(i) = sum((a(i,:)-b).^2);
    end   
else
    error('DISTSQRT ERROR in the input arguments sizes');
end

d = sqrt(d');
end