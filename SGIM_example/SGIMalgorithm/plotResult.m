function plotResult(listConsequence) 
%%
subplot(611)
hold on
plot(listConsequence(1,:),'LineWidth',2);
plot(repmat(0,1,size(listConsequence,2)), 'r.','LineWidth',2)
title('Distance to reach')
subplot(612)
hold on
plot(listConsequence(2,:),'LineWidth',2);
plot(repmat(0,1,size(listConsequence,2)), 'r.','LineWidth',2)
title('Velocity Constancy')
subplot(613)
plot(listConsequence(3,:),'LineWidth',2);
hold on
plot(repmat(0,1,size(listConsequence,2)), 'r.','LineWidth',2)
title('Extra Distance')
subplot(614)
plot(listConsequence(4,:),'LineWidth',2);
hold on
plot(repmat(1,1,size(listConsequence,2)), 'r.','LineWidth',2)
title('percent of movement (efficient + cloudy exploration) m-times')
subplot(615)
plot(listConsequence(5,:),'LineWidth',2);
hold on
plot(repmat(1,1,size(listConsequence,2)), 'r.','LineWidth',2)
title('percent of efficient movement m-times')
subplot(616)
plot(listConsequence(6,:),'LineWidth',2);
hold on
plot(repmat(0,1,size(listConsequence,2)), 'r.','LineWidth',2)
title('Number of cloudy explorations')
drawnow;