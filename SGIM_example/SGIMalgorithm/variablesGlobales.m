%variablesGlobales.m
%
% Copyright (c) 2013 Sao Mai Nguyen 
%               INRIA Bordeaux - Sud-Ouest
%               351 Cours de la Liberation, 33400, Talence,
%               France, http://flowers.inria.fr
%
% defines global variables that are essentiel for the learning

global envSimu     % simulation environment
global evalPerf    % evaluation of the performance on a benchmark set (goals in the outcome space) %an instance of Evaluation.m

global curLowLevel % records the current state of the exploration for the low level (policy space exploration) %an instance of LowLevel
global mapp        % inverse mapping to be learned. defines the spaces and dimensions of the mapping %cell array of instances of Mapping.m. Also each instance is an attribute of epmem . The size of the cell array is the number of outcomes (task) types
global epmem       % episodic memory : records all the experiences of the learning %cell array of instances of EpisodicMemory.m. The size of the cell array is the number of outcomes (task) types . Each element of the array has an attribute which is an instance of Mapping corresponding to the task
global hTeacher    % teachers %cell array of instances of HumanTeacher.m. The size of the cell array is the number of available teachers.

global partReg     % partition of the outcome space into different regions based on interest meansures (competence progress) %an instance of PartitionRegions.m
global ProgressL   % records the competence progress measures in the chronological order %matrix where the lines are [progress strategyType timestamp]
global goalOutcome
global consequence

global Predictor
global QualityCandidate
