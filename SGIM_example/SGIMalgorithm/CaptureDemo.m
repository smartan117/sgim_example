classdef CaptureDemo
    
    
    methods(Static = true)
        
        %program to capture demonstrations given by human by the physical
        %robot
        function runCaptureDemo(ipAd)
            %% set path
%             path(path,'/Users/mai/Programmes/Urbi/liburbi-matlab/')
%             path(path,'/Users/mai/Programmes/Urbi/liburbi-matlab/lib')
%             path(path,'/Users/mai/Programmes/Urbi/liburbi-matlab/lib/helpers')
%             path(path,'/Users/mai/Programmes/Urbi/liburbi-matlab/lib/helpers/tcp_udp_ip')
             

            saggGlobal
            variablesGlobales
            
            %if isempty(ipAd)
            ipAd      = '0.0.0.0' ;%'127.0.0.1';
            vrepIp = '192.168.169.133';
            %vrepIp = '192.168.214.142';
            %end
            instanceV = 1;
            nbClient  = 1;
            portUrbi = 54000;
            
            rFish     = FishingArm(6,4,0.01,3,0.3, WIN_PATH);
            connVrep  = ConnexionRob(ipAd, portUrbi, vrepIp, instanceV, WIN_PATH);
%            connVrep  = ConnexionRob(nbClient,50000, instanceV, ipAd, WIN_PATH);
            connVrep  = connectToPhysical(connVrep, portUrbi, instanceV);
            disp('Connection done');
            pause(1);
            
            figure('Name', 'Task Space','Position',[0 0 1000 1000]);
            grid on
            axis([-2 2 -2 2]);
            hold on
            set(gca,'XTick',[-1:0.2:1],...
                'YTick',[-1:0.2:1])
            plot(0,0,'s','MarkerFaceColor','b')
            hold on
            % draw reachable space
            load('listTotalAutoRIAC111026.mat','listTotalAll')
            indL = find(listTotalAll(:,28)<0.01);
            plot(listTotalAll(indL,26),listTotalAll(indL,27),'.b')
            hold on

            grid on
            
            pause(7)
            
%%            
            for i=20:25
                urbiSend(connVrep.urbiClient, 'b;');
               % urbiSendFile(connVrep.urbiClient,'FishingRod_monitorHook.u');
                pause(1)
                
                for count =5:-1:1
                    pause(1)
                    disp( ['count down ', num2str(count)]);
                end
                disp('start');
               % outPos{i} = readPosObj(connVrep)
                %urbiSend(connVrep.urbiClient,' vrep.simSetObjectPosition(mark,-1,[pos[0], pos[1], 0]);' )
                %urbiGetVariable(connVrep.urbiClient,'simEnd' )
                pause(6);
                    
                
                urbiSend(connVrep.urbiClient, 'e;');

                %% record trajectory
                hookTraj =  [urbiGetVariable(connVrep.urbiClient,'Global.fishingRod' )];
                outPos{i} = CaptureDemo.touchWater(hookTraj);
                im = mod(i,21);
                %urbiSend(connVrep.urbiClient,' vrep.simSetObjectPosition(mark,-1,[pos[0], pos[1], 0]);' )
                urbiSend(connVrep.urbiClient,[' vrep.simSetObjectPosition(mark',num2str(im),',-1,[', num2str(outPos{i}{1},2),',',num2str(outPos{i}{2},2),', 0]);'] )

                b{i} =  [urbiGetVariable(connVrep.urbiClient,'Global.motorsPositions' )];
                
                %%plotting
                plot(listTotalAll(indL,26),listTotalAll(indL,27),'.b')
                hold on
                for j=1:i-1
                    scatter(outPos{j}{1}, outPos{j}{2}, 40, [.5 0 0],'filled');
                    hold on
                    text(outPos{j}{1}, outPos{j}{2}, num2str(j));
                end
                
                scatter(outPos{i}{1}, outPos{i}{2}, 150, [.5 0 0],'filled');
                hold on
                text(outPos{i}{1}, outPos{i}{2}, num2str(i));
                
                grid on
                axis([-2 2 -2 2]);
                hold on
                set(gca,'XTick',[-1:0.2:1],...
                    'YTick',[-1:0.2:1])
                plot(0,0,'s','MarkerFaceColor','b')
                grid on
                axis([-2 2 -2 2]);
                hold off
                
               
            end
            disp('End Capture');
            disp('Thank you');
            urbiSend(connVrep.urbiClient, 'c;');
  %%          
            
            save('demoTest', 'b','outPos');
            
        end
        
        % get from the trajectory of the hook, its position when it touches
        % the water surface
        function  pos = touchWater(traj)
            if isempty(traj) || ~iscell(traj) || size(traj,1) ~= 1 || size(traj,2) <2
                error('CAPTUREDEMO::TOUCHWATER: error in input traj');
            end
            if size(traj{1},2)~=4 % traj should be a cell of elements {timestamp x y z}
                error('CAPTUREDEMO::TOUCHWATER: error in input traj dimension');
            end
            
            n = size(traj,2);
            for i= 1:n
                if traj{i}{4} < 0
                    break
                end
            end
            
            pos = {traj{i}{2} traj{i}{3} traj{i}{4}};
            
        end
        
        
        %The vrep robot repeats exactly the sequence of joint positions
        %demonstrated
        function repeatExact(movDemo)
            %% connect to urbi
            saggGlobal
            variablesGlobales
            ipAd      = '127.0.0.1';
            instanceV = 1;
            nbClient  = 1;
            rFish     = FishingArm(6,4,0.01,tend,0.3, WIN_PATH);
            connVrep  = ConnexionRob(nbClient,50000, instanceV, ipAd, WIN_PATH);
            connVrep  = connectSeveral(connVrep, rFish);
            
            %%
            MxAllowSqD=1; % Max. allowed Square Distance between original and fitted data
            len = length(movDemo);
            %t = linspace(0,tend,len)'
            X =[];
            
            
            % copy and filter timestamp
            iPrev = 1;
            tPrev = 0;
            t0 = movDemo{1}{1};
            for i=1:length(movDemo)
                t2(i) = movDemo{i}{1}-t0;
                if t2(i)~=t2(iPrev)
                    for j=iPrev:i-1
                        t(j) = t2(j) + (j-iPrev)*(t2(i)-t2(iPrev))/(i-iPrev);
                    end
                    iPrev = i;
                    t(i) = t2(i);
                end
            end
            t= t';
            tend = t(end);
            
            %copy joint position data
            for joint = 1:6
                for i=1:length(t)
                    y1(i,joint) = movDemo{i}{joint +1};
                end
            end
            
            
            for joint = 1:6
                % Mise sous type String
                y='[';
                for i=1:length(t)-1
                    %     x=strcat(x,num2str(t(i)),',');
                    y=strcat(y,num2str(y1(i,joint)),',');
                end
                % x=strcat(x,num2str(t(end)),']');
                y=strcat(y,num2str(y1(end,joint)),']');
                
                % Envoie de la variable dans urbi
                % strcat(var,'= ',y,';')
                var = strcat('TargetPositionJoint',num2str(joint));
                urbiSend(connVrep.urbiClient,strcat(var,'=',y,';'));
            end
            
            urbiSendFile(connVrep.urbiClient,'StartSimulation.u');
            posHook   = getPosObj(connVrep)
            
        end
        
        %Regression of a demonstration (sequence of joint positions)
        function X = transposeDemo(movDemo)
            %%
            len = length(movDemo);
            %tend = movDemo{end}{1};
            %t = linspace(0,tend,len)'
            X  = [];
            y1 = [];
            t  = [];
            
            % copy and filter timestamp
            iPrev = 1;
            tPrev = 0;
            t0 = movDemo{1}{1};
            for i=1:length(movDemo)
                t2(i) = movDemo{i}{1}-t0;
                if t2(i) ~= t2(iPrev)
                    for j = iPrev:i-1
                        t(j,1) = t2(j) + (j-iPrev)*(t2(i)-t2(iPrev))/(i-iPrev);
                    end
                    iPrev = i;
                    t(i,1) = t2(i);
                end
            end
            tend = t(end,1);
            
            %   figure('Name', 'repeatDemo');
            %copy joint position data
            %for joint = 1:6
            %    figure(joint)
            for joint = [1 3]
                for i=1:length(t)
                    y1(i,1) = movDemo{i}{joint +1};
                end
                
                %t
                [a, resnorm] = MovementEncoder.getMovementParameters(t,y1);
                X = [X a];
                %check
                color = rand(1,3);
                Vinv = MovementEncoder.getMovementTrajectory(a,0:0.1:t(end));
                plot(t,y1,'-','Color',color);
                hold on
                plot(linspace(0,tend,length(a)),a,'o','Color',color);
                hold on
                plot(0:0.1:t(end),Vinv, 'X', 'Color',color)
                hold on
            end
            
            X = [X tend];
            
        end %end transposeDemo
        
        % vrep robot repeats the demonstrated movement after Bezier
        % regression
        function posHook = repeatDemo(movDemo)
            
            % %% connect to urbi
            saggGlobal
            variablesGlobales
            ipAd      = '127.0.0.1';
            instanceV = 1;
            nbClient  = 1;
            % rFish     = FishingArm(6,4,0.01,tend,0.1, WIN_PATH);
            % connVrep  = ConnexionRob(nbClient,50000, instanceV, ipAd, WIN_PATH);
            % connVrep  = connectSeveral(connVrep, rFish);
       
            X = CaptureDemo.transposeDemo(movDemo);
            tend = X(end);
            rFish     = FishingArm(6,4,0.01,tend,0.3, WIN_PATH);

            moveRobot(rFish, X, connVrep);
            posHook   = getPosObj(connVrep)
    
            
        end
        
        % vrep robot repeats, after Bezier regression,  all demonstration
        % movements of the data file filename.mat
        function  repeatDemoList(filename)
            load(filename)
            h = figure('Name','Difference Demo and Repeat')
            
            nbDemo = length(b)
            for i=1:nbDemo
                movDemo = b{i}
                posRepeat = CaptureDemo.repeatDemo(movDemo)
                outPos{i}
                
                figure(h)
                scatter(posRepeat{1}, posRepeat{2}, 'ob');
                text(posRepeat{1}, posRepeat{2}, num2str(i),'Color','blue');
                hold on
                scatter(outPos{i}{1}, outPos{i}{2}, 'or');
                hold on
                text(outPos{i}{1}, outPos{i}{2}, num2str(i),'Color','red');
                set(gca,'XTick',[-1:0.2:1],...
                    'YTick',[-1:0.2:1])
                plot(0,0,'s','MarkerFaceColor','b')
                grid on
                axis([-2 2 -2 2]);
            end
        end % end repeatDemoList
        
        
        %plot the trajectories of each joint. 
        %filename is contains the data of demonstrations b, outPos (demo*
        %file)
        function plotTrajectories(filename)
            load(filename)
            nbdemo = length(b)-1
            for joint = 1:6
                histList{joint} = [];
            end
            
            for i=1:nbdemo
                if(outPos{1,i}{1,1} <0 && outPos{1,i}{1,2}>0)
                    movdemo = b{i};
                    len = length(movdemo);
                    %tend = movdemo{end}{1};
                    %t = linspace(0,tend,len)'
                    x  = [];
                    y1 = [];
                    t  = [];
                    
                    % copy and filter timestamp
                    iprev = 1;
                    tprev = 0;
                    t0 = movdemo{1}{1};
                    for i=1:length(movdemo)
                        t2(i) = movdemo{i}{1}-t0;
                        if t2(i) ~= t2(iprev)
                            for j = iprev:i-1
                                t(j,1) = t2(j) + (j-iprev)*(t2(i)-t2(iprev))/(i-iprev);
                            end
                            iprev = i;
                            t(i,1) = t2(i);
                        end
                    end
                    tend = t(end,1);
                    
                    %   figure('name', 'repeatdemo');
                    %copy joint position data
                    for joint = 1:6
                        % figure(joint)
                        y1 = [];
                        for i=1:length(t)
                            y1(i,1) = movdemo{i}{joint +1};
                        end
                        histList{joint} = [histList{joint}; y1];
                        subplot(2,3,joint)
                        color = rand(1,3);
                        plot(t,y1,'-','color',color);
                        hold on
                        title(['joint ',num2str(joint)])
                    end
                end
            end
            
            %%histogram
            figure
            x = -3:0.2:3;
            for joint =1:6
                subplot(2,3,joint)
                hist(histList{joint},x)
            end
            
        end
        
        
        % plots the demonstration set of filename.mat
        %filename is contains the data of demonstrations b, outPos (demo*
        %file)
        function plotDemo(filename)
            load(filename)
            nbDemo = length(b)
            
            figure('Name', 'Demonstrations in the Task Space','Position',[0 0 1000 1000]);
%             grid on
%             axis([-2 2 -2 2]);
%             hold on
%             set(gca,'XTick',[-1:0.2:1],...
%                 'YTick',[-1:0.2:1])
%             plot(0,0,'s','MarkerFaceColor','b')
%             hold on
%             grid on
            
            for j=1:nbDemo
                scatter(outPos{j}{1}, outPos{j}{2}, 60, [0 0.5 0],'filled');
                hold on
                %text(outPos{j}{1}, outPos{j}{2}, num2str(j));
            end
            
            %grid on
            axis([-2 2 -2 2]);
            hold on
            set(gca,'XTick',[-2:0.5:2],...
                'YTick',[-2:0.5:2])
            plot(0,0,'s','MarkerFaceColor','b')
            %grid on
            axis equal
            axis([-2 2 -2 2]);
            hold off
        end
        
        
        % transform the demonstration set into a list 
        %(to make it compatible with the new list format regTaught)
        function intoRegTaught(filename)
            load(filename)
            nbDemo = length(b)-1
            minX = Inf;
            maxX = -Inf;
            minY = Inf;
            maxY = -Inf;
            listDemo = [];
            regTaught = [];
            
            % determine the boundary of the explored space
             for i=1:nbDemo
                if (outPos{i}{3} < 0.01)
                    if (outPos{i}{1} > maxX) 
                        maxX = outPos{i}{1}
                        i
                    end
                    if (outPos{i}{1} < minX) 
                        minX = outPos{i}{1}
                        i
                    end
                    if (outPos{i}{2} > maxY)
                        maxY = outPos{i}{2}
                        i
                    end
                    if (outPos{i}{2} < minY)
                        minY = outPos{i}{2}
                        i
                    end
                    X = CaptureDemo.transposeDemo(b{i})
                    listDemo(i,:) = [X outPos{i}{1} outPos{i}{2}];
                end
             end
             
            dx = (maxX - minX)/nbin;
            dy = (maxY - minY)/nbin;
            
            totalcount = 0;
            for idemo=1:nbDemo-2
               if(~isempty(listDemo(idemo,:)))
                   x = listDemo(idemo,end-1);
                   y = listDemo(idemo,end);
                   i = floor( (x-minX)/dx )+1;
                   j = floor( (y-minY)/dy )+1;
                   xmin = minX + (i-1)*dx;
                   ymin = minY + (j-1)*dy;
                   if (i>0 && j> 0)
                       regTaught(idemo,:) = [xmin xmin+dx ymin ymin+dy 0 listDemo(idemo,:)];
                       disp([num2str(i),' ',num2str(j)])
                       totalcount = totalcount +1;
                   end
               end
            end
            
            %check
            plot(regTaught(:,end-1), regTaught(:,end),'.','MarkerSize',20)
            axis([-2 2 -2 2])
            save('regTaughtMai111028','regTaught');
         
            
        end
        
        % transform the demonstration set into a list 
        %(to make it compatible with the former format)
        function intoList(filename)
            nbin = 20;
            
            load(filename)
            nbDemo = length(b)-1
            minX = Inf;
            maxX = -Inf;
            minY = Inf;
            maxY = -Inf;
            listDemo = [];
            
            % determine the boundary of the explored space
            for i=1:nbDemo
                if (outPos{i}{3} < 0.01)
                    if (outPos{i}{1} > maxX) 
                        maxX = outPos{i}{1}
                        i
                    end
                    if (outPos{i}{1} < minX) 
                        minX = outPos{i}{1}
                        i
                    end
                    if (outPos{i}{2} > maxY)
                        maxY = outPos{i}{2}
                        i
                    end
                    if (outPos{i}{2} < minY)
                        minY = outPos{i}{2}
                        i
                    end
                    X = CaptureDemo.transposeDemo(b{i})
                    listDemo(i,:) = [X outPos{i}{1} outPos{i}{2}];
                end
            end
            
            % partition task space 
            dx = (maxX - minX)/nbin;
            dy = (maxY - minY)/nbin;
            for i= 1:nbin
                for j=1:nbin
                    x = minX + (i-1)*dx;
                    y = minY + (j-1)*dy;
                    regTeach{i,j}{1} =[x x+dx; y y+dy]; 
                end
            end
            
            totalcount = 0;
            for idemo=1:nbDemo-2
               if(~isempty(listDemo(idemo,:)))
                   x = listDemo(idemo,end-1);
                   y = listDemo(idemo,end);
                   i = floor( (x-minX)/dx )+1;
                   j = floor( (y-minY)/dy )+1;
                   if (i>0 && j> 0)
                       regTeach{i,j}{2} = listDemo(idemo,:);
                       disp([num2str(i),' ',num2str(j)])
                       totalcount = totalcount +1;
                   end
               end
            end
            
            %checking
            countReg = 0;
            for i= 1:nbin
                for j=1:nbin
                    if ~isempty(regTeach{i,j}) && size(regTeach{i,j},2)== 2
                        countReg = countReg + 1;
                    end
                end
            end
            countReg
      
            
        end %end intoList
        
        
        
        function Vinv = paramIntoTraj(param)
            tend = param(1,25);
            t = 0:0.005:tend;
            a1 = param(1,1:4);
            a2 = param(1,5:8);
            a3 = param(1,9:12);
            a4 = param(1,13:16);
            a5 = param(1,17:20);
            a6 = param(1,21:24);
            Vinv{1} = MovementEncoder.getMovementTrajectory(a1,t);
            Vinv{2} = MovementEncoder.getMovementTrajectory(a2,t);
            Vinv{3} = MovementEncoder.getMovementTrajectory(a3,t);
            Vinv{4} = MovementEncoder.getMovementTrajectory(a4,t);
            Vinv{5} = MovementEncoder.getMovementTrajectory(a5,t);
            Vinv{6} = MovementEncoder.getMovementTrajectory(a6,t);
            
        end
        
        function plotHist(paramList)
            %%
            for joint = 1:6
            histList{joint} = [];
            end
            
            nList = size(paramList,1)
            
            %plot trajectories and build list histList
            figure
            for i=1:nList
                pos   = paramList(i,26:27);
                %if (pos(1)<0 && pos(2)>0)
                param = paramList(i,1:25);
                Vinv  = CaptureDemo.paramIntoTraj(param);
                
                t = 0:0.005:param(1,25);
                for joint = 1:6
                    subplot(2,3,joint)
                    color = rand(1,3);
                    plot(t,Vinv{joint},'-','color',color);
                    hold on
                    title(['joint ',num2str(joint)])
                        
                    histList{joint} = [histList{joint} Vinv{joint}];
                end
               % end
            end
           
            %plot history
            figure
            x = -3:0.2:3;
            for joint =1:6
                subplot(2,3,joint)
                [f,x] = hist(histList{joint},x)
                bar(x,f/trapz(x,f));
                xlim([-3 3])
            end
            
            %space Percentage
             n=[];
            bin =[];
            nJoints = 6;
            edges = -3:0.05:3;
            for iJoint = 1:nJoints
                [n(:,iJoint),bin(:,iJoint)] = histc( histList{iJoint}, edges );
            end
            
        
            for iList = 1:nList
                hist6d(bin(iList,1),bin(iList,2),bin(iList,3),bin(iList,4),bin(iList,5),bin(iList,6))= 1;
            end
        
            per = sum(sum(sum(sum(sum(sum(hist6d))))))
            per/(length(edges)^6)
            %%
        end
        
        function spacePercentage()
            
            nJoints = 6;
            nbDemo = length(b)-1
            listHist =[];
            for iDemo=1:nbDemo
                movDemo = b{iDemo};
                len = length(movDemo);
                for t=1:length(movDemo)
                    listHist = [listHist ; movDemo{t}{2} movDemo{t}{3} movDemo{t}{4} movDemo{t}{5} movDemo{t}{6} movDemo{t}{7}]; %{joint +1}
                end
            end
            
            n=[];
            bin =[];
            edges = -3:0.25:3;
            for iJoint = 1:nJoints
                [n(:,iJoint),bin(:,iJoint)] = histc( listHist(:,iJoint), edges );
            end
            
        
            nList = size(listHist,1);
            for iList = 1:nList
                hist6d(bin(iList,1),bin(iList,2),bin(iList,3),bin(iList,4),bin(iList,5),bin(iList,6))= 1;
            end
        
            per = sum(sum(sum(sum(sum(sum(hist6d))))))
            per/(length(edges)^6)
        end %end spacePercentage
    end %end static methods
    
    
end
