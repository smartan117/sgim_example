classdef stack < handle
    properties
        untreated
        priority
    end
    
    methods (Static = false)
        
        function obj = stack()
            obj.untreated = [];
            obj.priority = [];
        end
      
        
        function [node] = pop(stack)
            node = stack.untreated(1);            
            stack.priority = stack.priority(2:end);
            stack.untreated = stack.untreated(2:end);
        end
        
        function push(stack,node,q)
            % save test
            
            
            if isempty(stack.untreated)
                stack.untreated = node;
                stack.priority = q;
            else
                %    i = 1;
                %    while i <= length(stack.priority)
                %        if stack.priority(i) < q
                %            i = i + 1;
                %        else
                % %            disp('test');
                %            break;
                %        end
                %    end
                nsp = length(stack.priority);
                for i=1:nsp
                    if stack.priority(i) >= q
                        break;
                    end
                end
                
                stack.priority = [stack.priority(1:i-1), q,  stack.priority(i:end)];
                stack.untreated(i:end+1) = [ node, stack.untreated(i:end)];
                
            end
        end
        
        
    end  %end methods
    %    events
    %       EventName
    %    end
end