%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France, 
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate 
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

classdef Mapping<handle
   
    properties
        dimA;  %dimension of the action space
        dimY;  %dimension of the goal space
        dimC;  %dimension of the context space
        dimAD;
        dimAI;
        dimAL;
        dimYD;
        dimYI;
        dimYL;
        dimCD; %indexes for the context space in the direct model
        dimCI; %indexes for the context space in the indirect model
        dimCL; %indexes for the learningEntity
        yMin;
        yMax;
        distYMax; %distSqrt(obj.yMax, obj.yMin)
        distUnreached; %
    end
    
    
    methods
        
        function obj = Mapping(a,y,c,min,max, distU)
            obj.dimA    = a;
            obj.dimY    = y;
            obj.dimC    = c;
            obj.dimAL   = [c+1:c+a];
            obj.dimYL   = [c+a+1:c+a+y];
            obj.dimCL   = [1:c];
            obj.dimAD   = [1:a];
            obj.dimAI   = [y+1:y+a];
            obj.dimYD   = [a+1:a+y];
            obj.dimYI   = [1:y];
            obj.dimCD   = [1:c];
            obj.dimCI   = [1:c];
            if( (size(max,2) ~= y) || (size(min,2) ~= y) )
                error('MAPPING error :incompatible sizes');
            end
            obj.yMin    = min;
            obj.yMax    = max;
            obj.distYMax = distSqrt(obj.yMax, obj.yMin);
            if nargin < 6 || isempty(distU)
                obj.distUnreached = 3*distSqrt(obj.yMax, obj.yMin);
            else
                obj.distUnreached = distU;
            end
        end
        
        
        function dim = dimInDirect(obj)
            dim = obj.dimA;
        end
        
        
        function dim = dimOutDirect(obj)
            dim = obj.dimY;
        end
        
        function correct = correctOutcome(obj, reached)
            correct = 1;
            try
                for i=1:size(reached,1)
                limSup  = all( reached(i,:)>=obj.yMin );
                limInf  = all( reached(i,:)<=obj.yMax );
                correct = correct && all([limSup limInf]);
                end
            catch
               disp(['error',num2str( reached(i,:)), ...
                   ' min ',num2str(obj.yMin),...
                   ' max ',num2str(obj.yMax)] ) 
            end
        end
        
        function outDist = distMapping(obj, reached, goal)
            outDist = obj.distUnreached;

            if nargin<2 || isempty(reached) 
                outDist = obj.distUnreached/obj.distYMax;
            elseif nargin<3 || isempty(goal)
                outDist = ones(size(reached,1),1);
            else
                 for i=1:size(reached,1)
                     if  ~correctOutcome(obj, reached(i,:))
                         outDist(i,1) = obj.distUnreached * ones(size(reached(i,:),1),1)/obj.distYMax;
                     else
                         outDist(i,1) = min(obj.distUnreached,distSqrt(goal,reached(i,:)))/obj.distYMax;
                     end
                 end
            end
            
%             reachedmax = obj.distYMax;
%             outDist = reachedmax;
%             
%             if ~isempty(reached)&& correctOutcome(obj,reached)
%                 if nargin>=2 && ~isempty(goal)
%                     for i=1:size(reached,1)
%                     outDistList(i) = min(reachedmax,distSqrt(goal,reached(i,:)));
%                     end
%                     outDist = mean(outDistList);
%                 end
%             end
            
        end %end function distMappping
            
        %% convert candidate action (normalized bet 0 and 1) to goal (bet Ymin and Ymax)
        function goal = convertCG(obj, action)
            [n,m] = size(action);
            goal =[];
            
            if ( (size(action,2) ~= obj.dimY) || (size(obj.yMax,2) ~= obj.dimY) )
                error(['CONVERTCG: incompatible sizes ',num2str(size(action,2)),'~=', num2str(size(obj.yMax,2))] );
            else
                for i =1:n
                    goali = [action(i,:) ].*(obj.yMax-obj.yMin) + obj.yMin;
                    goal = [goal; goali];
                end
            end
            
        end %end function covertCG
        
        
        %% convert goal (bet Ymin and Ymax) to candidate action (normalized
        %% bet 0 and 1)
        function cAction = convertGC(obj, pos)
            cAction =[];
%             size(obj.yMax,2)
%             size(pos,2)
            if size(pos,2) ~= size(obj.yMax,2)
                error('CONVERTCG: incompatible sizes');
            else
                n = size(pos,1);
                for i=1:n
                    act = (pos(i,:)- obj.yMin)./(obj.yMax-obj.yMin);
                    %        act = pos(i,:)./Ymax./[1 2 1] + [0 1 0]*0.5;
                    cAction = [cAction; act];
                end
            end
            
        end %end function covertCG
        
        
        %% create ANN Tree for direct model
        function treeDirect = createTreeDirect(obj)
            % Parameters of the tree to change here
            dimInDirect  = obj.dimAD;
            dimOutDirect = obj.dimYD;
            
            % BackTracking Parameter
             b = 50;
%           b = 50;
            %  Number of nearest neighbors asked when using search3
            nbEx = 10;
            % Add Vector Parameters %%%%%%%%%%%
            rmax = 0; % Minimal distance needed between the current added vector, and the nearest known one,
            % to decide the split of the considered node
            % If rmax = 0, we split the considered node once containing Nmax elements
            Nmax = 2000;% 1000; % Maximal number of element inside a leaf, where the minimal distance is inferior to rmax
            
            %TREE CREATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Number of Leafs created for each node split
            numberOfLeaf = length(dimInDirect) + length(dimOutDirect);
            treeDirect   = AnnTree(dimInDirect, dimOutDirect, numberOfLeaf, rmax, Nmax, b, nbEx );
        end %end function createTreeDirect
        
        
        %% create ANN Tree for inverse model
        function treeInv = createTreeInv(obj)
            % Parameters of the tree to change here
            dimInInv  = obj.dimYI;
            dimOutInv = obj.dimAI;
            
            % BackTracking Parameter
             b = 50; 
             %            b = 50;
            %  Number of nearest neighbors asked when using search3
            nbEx = 10;       
            % Add Vector Parameters %%%%%%%%%%%
            rmax = 0; % Minimal distance needed between the current added vector, and the nearest known one,
            % to decide the split of the considered node
            % If rmax = 0, we split the considered node once containing Nmax elements
            Nmax = 2000; %1000; % Maximal number of element inside a leaf, where the minimal distance is inferior to rmax
            
            % TREE CREATION
            % %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            % Number of Leafs created for each node split
            numberOfLeaf = length(dimInInv) + length(dimOutInv);
            treeInv      = AnnTree(dimInInv, dimOutInv, numberOfLeaf, rmax, Nmax, b, nbEx );
        end %end function createTreeInv
        
        
    end %end methods
    
    
    methods (Static = true)
        function nbErrors = test_convert()
           nbErrors = 0;
            for i = 1:100
                a = rand(1,mapp.dimY);
                p = convertCG(mapp,a);
                b = convertGC(mapp,p);
                if a~=b  || p>mapp.yMax || p< mapp.yMin || b>1 || b<0
                    nbErrors = nbErrors +1;
                end
            end
                
                nbErrors
        end %end test_convert
        
    end
end