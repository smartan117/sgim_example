function plotAll(nameArg)
variablesGlobales
saggGlobal
Ymax = [2 2];

if isempty(nameArg)
    figure
else
    figure('Name',nameArg,'NumberTitle','off')
end

subplot(2,4,1);
plotReachedPoints
disp('plotREachedPoints')
%axis([-Ymax(1) Ymax(1) -Ymax(2) Ymax(2)]);

subplot(2,4,5);
ind = find(epmem.listLearning(:,mapp.dimA +3) <0.01 );
[F, xs, ys] = scattersmooth(epmem.listLearning(ind,mapp.dimA +1), epmem.listLearning(ind,mapp.dimA+2),500, 2000,[-Ymax(1) Ymax(1)],[-Ymax(2) Ymax(2)]);
imagesc(xs,ys,F)
axis([-Ymax(1) Ymax(1) -Ymax(2) Ymax(2)]);
xlabel('x');
ylabel('y');
title('Distribution of the points reached by the fishing rod');

if ~isempty(epmem.listGoal)
    subplot(2,4,2);
    plot(epmem.listGoal(:,2), epmem.listGoal(:,3), 'o')
    axis([-Ymax(1) Ymax(1) -Ymax(2) Ymax(2)]);
    xlabel('x')
    ylabel('y')
    title('Goals chosen by SAGG-RIAC')
    
    subplot(2,4,6);
    [F, xs, ys] = scattersmooth(epmem.listGoal(:,2), epmem.listGoal(:,3), 500, 1000,[-Ymax(1) Ymax(1)],[-Ymax(2) Ymax(2)]);
    imagesc(xs,ys,F);
    xlabel('x')
    ylabel('y')
    title('Distribution of Goals determined by SAGG-RIAC', 'FontWeight','bold')
    
%     subplot(2,4,3);
%     plot(2*QualityCandidate(1,:)-1 , QualityCandidate(3,:),'.' )
%     xlabel('x')
%     ylabel('Competence');
%     axis([-1 1 -1 0]);
%     title('Distribution of competence against x axis', 'FontWeight','bold')
%     
%     subplot(2,4,7);
%     plot(2*QualityCandidate(2,:)-1, QualityCandidate(3,:),'.' )
%     xlabel('y')
%     ylabel('Competence');
%     axis([-1 1 -1 0]);
%     title('Distribution of competence against y axis', 'FontWeight','bold')
    
end

subplot(2,4,4);
h3 = plot(evalPerf.mapEval(:,end), evalPerf.listDist3,'g');
hold on;
h2 = plot(evalPerf.mapEval(:,end), evalPerf.listDist2,'b');
hold on;
h1 = plot(evalPerf.mapEval(:,end), evalPerf.listDist,'r');
hold on
xlabel('Number of movements experimented');
ylabel('Mean distance to the goals');
title('Evaluation of the performance on the benchmark data');
legend('Assessment when reexecuting the nearest neighbour','Assessment using the nearest neighbour', 'Assessment using interpolation');

subplot(2,4,8);
%dispLReg
title('Regions determined by SAGG-RIAC', 'FontWeight','bold')


% figure
% plot3(2*QualityCandidate(1,:)-1, 2*QualityCandidate(2,:)-1, QualityCandidate(3,:),'.' )
% xlabel('x')
% ylabel('y')
% zlabel('Competence');
% grid on
% title('Distribution of competence in the space', 'FontWeight','bold')


end

%%
% figure
% h3 = plot(GeneralMap1(:,end), GeneralDist1,'g');
% hold on
% h3 = plot(GeneralMap2(:,end), GeneralDist2,'r');
% hold on
% h3 = plot(GeneralMap3(:,end), GeneralDist3,'b');
% hold on
% h3 = plot(GeneralMap4(:,end), GeneralDist4,'c');
% hold on
% legend('MemoTeach0_01-Apr-2011_3','MemoTeach1_01-Apr-2011_1','MemoTeach1_01-Apr-2011_1', 'MemoRandomParam_0330_2');
% 
%  plot(GeneralMap1(:,end),GeneralVoisin1(:,end), 'g+');
% hold on
% h3 = plot(GeneralMap2(:,end),GeneralVoisin2(:,end), 'r+');
% hold on
% h3 = plot(GeneralMap3(:,end),GeneralVoisin3(:,end), 'b+');
% hold on
% h3 = plot(GeneralMap4(:,end),GeneralVoisin4(:,end),'c+');
% hold on
% 
% 
% h3 = plot(GeneralInterpolation1(:,end), GeneralDist1,'g-.-');
% hold on
% h3 = plot(GeneralInterpolation2(:,end), GeneralDist2,'r-.-');
% hold on
% h3 = plot(GeneralInterpolation3(:,end), GeneralDist3,'b-.-');
% hold on
% h3 = plot(GeneralInterpolation4(:,end), GeneralDist4,'c-.-');
% hold on