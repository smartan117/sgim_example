
function [data1, data2, e1, e2, t1, t2] = split(data, errors, tags, cutValue,dimension) 


% Copyright (c) 2009 Adrien Baranes and Pierre-Yves Oudeyer, INRIA Bordeaux - Sud-Ouest
%               351 Cours de la Liberation, 33400, Talence,
%               France, http://flowers.inria.fr
%
% data = [data_1, data_2,....]
% split ensemble of prototypes data in data1 and data2 using points
% pleft and pright

 [md, nd] = size(data) ;
 [me, ne] = size(errors) ;
 
 if (nd ~= ne) 
     error(['SPLIT: error : data and errors have incompatible width : data : ',num2str(nd), ' while errors: ',num2str(ne)]);
 end
 
 
% tmpLeft = (data./data) * cutValue
tmpLeft = repmat(cutValue,size(data));
% tmpLeft = cutValue(:, ones(1, n)) ;

BLeft = data - tmpLeft;
% BRight = data - tmpRight;
boolNorms  = BLeft < 0 ;
% BRight = Bleft < 0 ;



if size(boolNorms,1)>=dimension
data1 = data(:,boolNorms(dimension,:));
data2 = data(:,~boolNorms(dimension,:));
e1 = errors(:,boolNorms(dimension,:));
e2 = errors(:,~boolNorms(dimension,:));
t1 = tags(boolNorms(dimension,:));
t2 = tags(~boolNorms(dimension,:));
else
    
    e1=[];
    e2=[];
    data1 = [] ;
    data2 = [] ;
    t1 = [];
    t2 = [];
end

if  size(boolNorms,1)<dimension || (size(data1, 2) == 0)
    data1 = [] ;
    e1 = [] ;
end ;

if  size(boolNorms,1)<dimension || (size(data2, 2) == 0)     
    data2 = [] ;
    e2 = [] ;
end 

e1 = e1(:)';
e2 = e2(:)';

%{
dim = 5;
nbData = 10;
data   = randi(nbData,dim,nbData); %ones(dim,nbData)*diag([1:nbData]);
errors = -ones(1,nbData).*[1:nbData];
tags   = ones(1,nbData).*[1:nbData];
cutValue = 5;
dimension =2;
[data1, data2, e1, e2, t1, t2] = split(data, errors, tags, cutValue,dimension) 

%}


end

    
    