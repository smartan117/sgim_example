function stop = teacherInterrupts(x,optimValues,state)
variablesGlobales

stop = false;

if mod(curLowLevel.nbExperiments, hTeacher(1).frequency) ==0
    epmem{curLowLevel.curTask}.listFlagTeach = [epmem{curLowLevel.curTask}.listFlagTeach; ...
        size(epmem{curLowLevel.curTask}.listLearning,1) 0];
    curLowLevel.flagTeach = 1;
end

if curLowLevel.flagTeach == 1
    stop      = true;
end

end