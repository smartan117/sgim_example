%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                            APPROXIMATE NEAREST NEIGHBORS  CLASS                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Author: Sao Mai Nguyen
%% date: September 2010
%% rewritten from Adrien Baranes's source code


classdef AnnTree
    %description of the class
    %KNN tree structure to associate input and output vectors 
    
    properties
        rootNode;
        dimInput;     % dimension of input Data
        dimOutput;    % number of output Data
        numberOfLeaf; % Number of Leafs created for each node split
        rmax;         % Minimal distance needed between the current added vector, and the nearest known one
          % to decide the split of the considered node
          % If rmax = 0, we split the considered node once containing nmax
          % elements  
        nmax;         % Maximal number of element inside a leaf, where the minimal distance is inferior to rmax
        b;            % BackTracking Parameter
        nbEx;         % Number of nearest neighbors asked when using search3
        nbNodes;      % Number of nodes in the tree (counter)
        nbElements;   % Numer of eleemts in the tree (counter)
    end
    
    
    
    methods
        
        function obj = AnnTree(dimIn, dimOut, nbLeaf, rm, nm, b, nbExpl)
% constructor
% dimIn  : dimension of the input vector
% dimOut : dimension of the output vector
% nbLeaf : nb of Leaves created for each node split
% rm     : Minimal distance needed betw. the current added vector and the nearest known one
% nm     : Maximal number of elements inside a leaf
% b      : backtracking parameter. b higher => better precision but slower algorithm
% nbExpl : number of nearest neighbors asked when using serach3 (depricated, should not use it any more). 
            obj.dimInput     = dimIn;
            obj.dimOutput    = dimOut;
            obj.numberOfLeaf = nbLeaf;
            obj.rmax         = rm;
            obj.nmax         = nm;
            obj.b            = b;
            obj.nbEx         = nbExpl;
            obj.nbNodes      = 0 ;
            obj.nbElements   = 0 ;
            obj.rootNode     = node();            
            %create a new leaf called r
            r                = node();
            obj.rootNode.next = r;
            r.prev = obj.rootNode;
           
        end
        
        
        
        function obj = AddVectorToTree(obj, vector)
% add a vector to the tree
% vector : is the compound vector [input output] of a single example or matrix of vectors of m examples

            %check sizes and compatibility of data
           [m, n] = size(vector);
           if n~= size(obj.dimInput,2) + size(obj.dimOutput,2)
               disp(['ANNTREE.ADDVECTORTOTREE : vector ', num2str(n)]);
               obj.dimInput
               obj.dimOutput
               error('ANNTREE.ADDVECTORTOTREE error: sizes of data incompatible');
           else
               for i= 1:m
                   node = AddVectorToNodeM(obj.rootNode.next, ...
                       vector(i,:), ...
                       obj.nbNodes,...
                       obj.numberOfLeaf, ...
                       obj.rmax, ...
                       obj.nmax,...
                       obj.dimInput, ...
                       obj.dimOutput);
                  obj.nbNodes= node.num +1 ;
               end
               obj.nbElements   = obj.nbElements + m;
           end
           
       end
       
       function output = searchANNTree(obj, vectorInput)
% search in the tree for associations [vectorInput output]
% vectorInput : a vector or matrix of  size m x obj.dimInput

           output = searchNode(obj.rootNode, vectorInput, obj.b, obj.nbEx);
       end
        
    end
    
    
end

