%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

function selectStrategySGIMIM()
variablesGlobales
saggGlobal

hTeacher(1).present = 1;
curLowLevel.flagTeach = 0;
crit1 = 1/3;

%% request help?
if ~isempty(ProgressL) && size(ProgressL,1)> 10
    %compute the recent progress by autonomous exploration
    nbA = 0;
    ind = find(ProgressL(:,2)==0);
    if isempty(ind) %|| ind == 0
        progressA = 0;
    else
        nbA = min([length(ind)-1,nbMeta]);
        if nbA >= 0
            pA = ProgressL(ind(end-nbA:end),1); %.*lambdaMetaVec(end-nbA:end);
            progressA = mean(pA) ; %+  max(pA);
        end
    end
    
    %compute the recent progress by social learning 1
    nbS1 = 0;
    ind = find(ProgressL(:,2)==1);
    if isempty(ind) %|| ind == 0
        progressS1 = 0;
    else
        nbS1 = min([length(ind)-1,nbMeta]);
        if nbS1 >= 0
            pS1 = ProgressL(ind(end-nbS1:end),1); %.*lambdaMetaVec(end-nbS:end);
            progressS1 = mean(pS1) ;%+ max(pS);
        end
    end
    
    
    
    PSL1 = [PSL1; progressS1 nbElements(epmem{curLowLevel.curTask})];
    PAL = [PAL; progressA nbElements(epmem{curLowLevel.curTask})];
    
    disp(['FONCTION_PRINCIPALE:: MODE SGIMIM Request help? Autonomous: ' num2str(progressA),' Social1: ' ,num2str(progressS1)]);
    
    if ( (nbA > nbMeta/4) && (nbS1 > nbMeta/4)&&  (progressS1 ~= 0 || progressA ~= 0) ) && (progressS1*costS + progressA ~= 0)
        crit1 = progressS1*costS/( progressA + (progressS1)*costS);
        crit1 = max(min(crit1,0.9), 0.1);
        
    else
        crit1 = 1/3;
        
    end
    
end

if rand() < crit1
    curLowLevel.flagTeach = 1;
    teacherid = 1
end



%% execute high level social learning or autonomous exploration
if curLowLevel.flagTeach == 0
    Main_RIAC_CB;
    
elseif curLowLevel.flagTeach == 1
    
    if hTeacher(1).present
         Main_ImitateTeacher(teacherid);
    end
    curLowLevel.flagTeach = 0;
    
end

end
