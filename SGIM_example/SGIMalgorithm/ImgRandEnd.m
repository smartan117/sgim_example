function ImgRandEnd(EstimeGoal)
%% Plot in 3d all experimented values at the end

figure
for cptPlot = 1:6
    cptPlot
    subplot(2,3, cptPlot)
    
    
    intervalD = size(EstimeGoal,2);
    
    D = intervalD
    
    EstimeGoalTmp = EstimeGoal(:,end-floor(size(EstimeGoal,2)/10):end);
    
    interval1 = 0:1:35;
    interval2 = -35:1:35;
    DataF = ones(size(interval1,2),size(interval2,2))*0;
    for i = interval1
        %         i
        for j = interval2
            nnidx = annquery(EstimeGoalTmp(1:2,:),[i;j], 5);
            val = mean(EstimeGoalTmp(cptPlot+2,nnidx));
            
            if cptPlot == 4
                if val < 2 % ~isnan(val) ||
                    
                    DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
                else
                    DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
                end
            end
            
            if cptPlot == 2
                if ~isnan(val)
                    DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
                else
                    DataF(floor((i*1)+1),floor((j+35)*1+1)) = 0;
                end
            end
            
            if cptPlot == 1 || cptPlot == 3 || cptPlot == 5 || cptPlot == 6
                DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
            end
        end
    end
    
    
    
    
    
    X = repmat(interval1,size(interval2,2),1);
    Y = repmat(interval2,size(interval1,2),1);
    Z = DataF;
    % [X,Y,Z] = peaks(30);
    surfc(X',Y,Z)
    shading interp
    az = 0;
    el = 90;
    view(az,el)
    % pause(0.5)
    
    
    switch cptPlot
        case 1
            title('Distance to reach')
        case 2
            title('Velocity Constancy')
        case 3
            title('Extra Distance')
        case 4
            title('percent of movement (efficient + cloudy exploration) m-times')     
        case 5
            title('percent of efficient movement m-times')
        case 6
            title('Number of cloudy explorations')
    end
end

save ImgRandEnd1000
%%

figure

    alpha = 1/18;
    beta = 1/0.6;
    gamma = -1/0.7;
    delta = 1/35;



    iPlot = 1;
    
    intervalD = size(EstimeGoal,2);
    
    D = intervalD
    
    EstimeGoalTmp = EstimeGoal;
    
    interval1 = 0:1:35;
    interval2 = -35:1:35;
    DataF = ones(size(interval1,2),size(interval2,2))*5;
    for i = interval1
        %         i
        for j = interval2
            nnidx = annquery(EstimeGoalTmp(1:2,:),[i;j], 5);
            d = mean(EstimeGoalTmp(iPlot+2,nnidx));
            c = mean(EstimeGoalTmp(iPlot+3,nnidx));
            extraDist = mean(EstimeGoalTmp(iPlot+4,nnidx));
            mtimes = mean(EstimeGoalTmp(iPlot+5,nnidx));
            
            val = alpha * d + beta * c + gamma * extraDist + delta * mtimes;
%             val = mean(EstimeGoalTmp(iPlot+2,nnidx));
            if val < 50000
                DataF(floor((i*1)+1),floor((j+35)*1+1)) = val;
            end
            
            if isnan(val)
                DataF(floor((i*1)+1),floor((j+35)*1+1)) = 00;
            end
        end
    end
    
    
    
    
    
    X = repmat(interval1,size(interval2,2),1);
    Y = repmat(interval2,size(interval1,2),1);
    Z = DataF;
    % [X,Y,Z] = peaks(30);
    surfc(X',Y,Z)
    shading interp
    az = 0;
    el = 90;
    view(az,el)
    % pause(0.5)
    
    
  
end

