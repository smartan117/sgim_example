function [IndiceDistMaxDim, IndiceDistMaxCut] = maxiDist(dist)

if size(dist,1) == 1
    [val IndiceDistMaxCut] = max(dist);
    IndiceDistMaxDim = 1;
else
    [val1 ind] = max(dist);
    [val2 IndiceDistMaxCut] = max(val1);
    IndiceDistMaxDim = ind(IndiceDistMaxCut);
end


    
    