function plotSeveralData(filesDir)

%% ******     define the data files        ****** %%   

% filenameRANDOMPARAM = char('/Users/mai/Documents/Dropbox/Dropbox/GoodData/Jerome/RANDOMPARAM30withoutGauss07Alpha05_70_06-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Haylee/RANDOM_20_06-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Haylee1/RANDOM_10_06-Apr-2011_1.mat');
% 
% filenameSAGGRANDOM = char('/Users/mai/Documents/Dropbox/Dropbox/GoodData/Haylee1/SAGGRANDOM_10_05-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Haylee1/SAGGRANDOM_20_05-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Haylee1/SAGGRANDOM_30_06-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Haylee1/SAGGRANDOM_40_06-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Jerome/RANDOMPARAM30withoutGauss07Alpha05_70_06-Apr-2011_1.mat');
% 
% filenameSAGGwithout = char('/Users/mai/Documents/Dropbox/Dropbox/GoodData/Jerome/RIAC30withoutGauss07Alpha05_10_05-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Jerome/RIAC30withoutGauss07Alpha05_20_05-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Jerome/RIAC30withoutGauss07Alpha05_30_06-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Jerome/RIAC30withoutGauss07Alpha05_40_06-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Jerome/RIAC30withoutGauss07Alpha05_50_06-Apr-2011_1.mat');
% 
% %to complete
% filenameSGIM= char('/Users/mai/Documents/Dropbox/Dropbox/GoodData/Haylee1/RIACwith50Alpha05Gauss07_2_1_06-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/Haylee1/RIACwith50Alpha05Gauss071_06-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/MacPro/RIACwithAlpha050Gauss070_11_05-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/MacPro/RIACwithAlpha050Gauss070_21_06-Apr-2011_1.mat',...
%     '/Users/mai/Documents/Dropbox/Dropbox/GoodData/MacPro/RIACwithAlpha050Gauss070_31_06-Apr-2011_1.mat');

filesDir = '../data/';
%filesDir = '../GoodData/newData/';
filesRANDOMPARAM = dir([filesDir,'RANDOM*_*erf12.mat'])
filesSAGGwithout = dir([filesDir,'Imitation*_*5001.mat'])
filesSGIM = dir([filesDir,'0SGIM*_*5001.mat'])

%% ******    define parameters of plotting  ******    %%
colorRANDOMPARAM = [0   0   1]; %blue
%colorSAGGRANDOM  = [0   0.5 0.7];
colorSAGGwithout = [0 1 0];
colorSGIM   = [1   0   0]; %red

%% ******    record the data of each file     ******    %%
PLOT1 = 0
disp('RANDOMPARAM');
for ifile = 1:size(filesRANDOMPARAM,1)
    filename = [filesDir, filesRANDOMPARAM(ifile).name]
    load(filename);
    size(epmem.listLearning,1)
    listGoalRANDOMPARAM{ifile}     = epmem.listGoal;
    listDist3RANDOMPARAM{ifile}    = evalPerf.listDist3(:,1);
    listDist2RANDOMPARAM{ifile}    = evalPerf.listDist2(:,1);
    listDistRANDOMPARAM{ifile}     = evalPerf.listDist(:,1);
    mapEvalRANDOMPARAM{ifile}      = evalPerf.mapEval(:,:);
    listLearningRANDOMPARAM{ifile} = epmem.listLearning;
    if PLOT1
        figure('Name',filename,'NumberTitle','off')
        plotRandomParam();
    end
end
% pause

disp('WITHOUT');
for ifile = 1:size(filesSAGGwithout,1)
    filename = [filesDir, filesSAGGwithout(ifile).name]
    load(filename);
    size(epmem.listLearning,1)
    listGoalSAGGwithout{ifile}     = epmem.listGoal;
    listDist3SAGGwithout{ifile}    = evalPerf.listDist3(:,1);
    listDist2SAGGwithout{ifile}    = evalPerf.listDist2(:,1);
    listDistSAGGwithout{ifile}     = evalPerf.listDist(:,1);
    mapEvalSAGGwithout{ifile}      = evalPerf.mapEval(:,:);
    listLearningSAGGwithout{ifile} = epmem.listLearning;
    if PLOT1
        figure('Name',filename,'NumberTitle','off')
        plotAll(filesSAGGwithout(ifile).name);
    end
end
% pause

disp('WITH');
for ifile = 1:size(filesSGIM,1)
    ifile
    filename = [filesDir, filesSGIM(ifile).name]
    load(filename);
    size(epmem.listLearning,1)
    listGoalSGIM{ifile}     = epmem.listGoal(:,1);
    listDist3SGIM{ifile}    = evalPerf.listDist3(:,1);
    listDist2SGIM{ifile}    = evalPerf.listDist2(:,1);
    listDistSGIM{ifile}     = evalPerf.listDist(:,1);
    mapEvalSGIM{ifile}      = evalPerf.mapEval(:,:);
    listLearningSGIM{ifile} = epmem.listLearning;
    if PLOT1
        figure('Name',filename,'NumberTitle','off')
        plotAll(filesSGIM(ifile).name);
    end
end

 

%% plot the goal density
nfiles =  2 ; %max(size(filesSAGGwithout,1), size(filesSAGGwith,1))
figure()
for ifile = 1: 2%size(filesSAGGwithout,1)
    filename = [filesDir, filesSAGGwithout(ifile).name]
    load(filename);
    Ymax =[ 2 2 100];
    subplot(2,nfiles,ifile);
    [F, xs, ys] = scattersmooth(epmem.listGoal(:,2), epmem.listGoal(:,3), 500, 1000,[-Ymax(1) Ymax(1)],[-Ymax(2) Ymax(2)]);
    imagesc(xs,ys,F);
    set(gca,'XTick',-2:1:2)
    set(gca,'YTick',-2:1:2)
    set(gca,'XTickLabel',{'-1';'-0.5';'0';'0.5';'1'})
    set(gca,'YTickLabel',{'-1';'-0.5';'0';'0.5';'1'})
%   %  xlabel('x')
   % ylabel('y')
    %title(filesSAGGwithout(ifile).name, 'FontWeight','bold');
end

for ifile = 1:2%size(filesSGIM,1)
    filename = [filesDir, filesSGIM(ifile).name]
    load(filename);
    Ymax =[ 2 2 100];
     subplot(2,nfiles, nfiles + ifile);
    [F, xs, ys] = scattersmooth(epmem.listGoal(:,2), epmem.listGoal(:,3), 500, 1000,[-Ymax(1) Ymax(1)],[-Ymax(2) Ymax(2)]);
    imagesc(xs,ys,F);
    set(gca,'XTick',-2:1:2)
    set(gca,'YTick',-2:1:2)
    set(gca,'XTickLabel',{'-1';'-0.5';'0';'0.5';'1'})
    set(gca,'YTickLabel',{'-1';'-0.5';'0';'0.5';'1'})
%     %xlabel('x')
    %ylabel('y')
   % title(filesSGIM(ifile).name, 'FontWeight','bold');
end


%% ******     plot the reached points     ******    %%
figReachedPoints = figure('Name','Reached Points','NumberTitle','off')
h1 = [];
h2 = [];
h3 = [];
h4 = [];

for ifile = 1:size(filesSGIM,1)
    ind = find(listLearningSGIM{ifile}(:,mapp.dimA+3) <5);
    h3= [h3;plot(listLearningSGIM{ifile}(ind,mapp.dimA+1), listLearningSGIM{ifile}(ind,mapp.dimA+2), '.','Color',colorSGIM,'MarkerSize',5)];
    hold on
end
pause
for ifile = 1:size(filesSAGGwithout,1)
    ind = find(listLearningSAGGwithout{ifile}(:,mapp.dimA+3) <5);
    h2= [h2 ; plot(listLearningSAGGwithout{ifile}(ind,mapp.dimA+1), listLearningSAGGwithout{ifile}(ind,mapp.dimA+2), '.','Color',colorSAGGwithout,'MarkerSize',5)];
    hold on
end
pause
% for ifile = 1:size(filesSAGGRANDOM,1)
%     ind = find(listLearningSAGGRANDOM{ifile}(:,27) <0.05);
%     h1= [h1; plot(listLearningSAGGRANDOM{ifile}(ind,25), listLearningSAGGRANDOM{ifile}(ind,26), '.','Color',colorSAGGRANDOM,'MarkerSize',5)];
%     hold on
% end

for ifile = 1:size(filesRANDOMPARAM,1)
    ind = find(listLearningRANDOMPARAM{ifile}(:,mapp.dimA+3) <5);
    h4= [h4;plot(listLearningRANDOMPARAM{ifile}(ind,mapp.dimA+1), listLearningRANDOMPARAM{ifile}(ind,mapp.dimA+2), '.','Color',colorRANDOMPARAM,'MarkerSize',5)];
    hold on
end

xlabel('x');
ylabel('y');
title('Reached Points  ');
% h1Group = hggroup;
% set(h1,'Parent',h1Group)
h2Group = hggroup;
set(h2,'Parent',h2Group)
h3Group = hggroup;
set(h3,'Parent',h3Group)
h4Group = hggroup;
set(h4,'Parent',h4Group)
% set(get(get(h1Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h2Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h3Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h4Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
%set(h1Group, 'Displayname', 'SAGG RANDOM');
set(h2Group, 'Displayname', 'SAGG-RIAC');
set(h3Group, 'Displayname', 'SGIM');
set(h4Group, 'Displayname', 'Random on the input parameters');
legend('Location', 'northEast');
%legend('SAGG-RANDOM','SAGG-RIAC','SGIM','Random on the input parameters')

%% plot the convex hull of the reached points
%figure
x = [];
y = [];
for ifile = 1:size(filesRANDOMPARAM,1)
    ifile
    ind = find(listLearningRANDOMPARAM{ifile}(:,mapp.dimA+3) <0.05);
    x = [x; listLearningRANDOMPARAM{ifile}(ind,mapp.dimA+1)];
    y = [y; listLearningRANDOMPARAM{ifile}(ind,mapp.dimA+2)];
end
k = convhull(x,y);
h4 = plot(x(k),y(k),'-','Color',colorRANDOMPARAM)
hold on

% x = [];
% y = [];
% for ifile = 1:size(filesSAGGRANDOM,1)
%     ind = find(listLearningSAGGRANDOM{ifile}(:,27) <0.05);
%     x = [x; listLearningSAGGRANDOM{ifile}(ind,25)];
%     y = [y; listLearningSAGGRANDOM{ifile}(ind,26)];
% end
% k  = convhull(x,y);
% h1 = plot(x(k),y(k),'-','Color',colorSAGGRANDOM)
% hold on

x = [];
y = [];
for ifile = 1:size(filesSAGGwithout,1)
    ind = find(listLearningSAGGwithout{ifile}(:,mapp.dimA+3) <0.05);
    x = [x; listLearningSAGGwithout{ifile}(ind,mapp.dimA+1)];
    y = [y; listLearningSAGGwithout{ifile}(ind,mapp.dimA+2)];
end
k  = convhull(x,y);
h2 = plot(x(k),y(k),'-','Color',colorSAGGwithout)
hold on

x = [];
y = [];
for ifile = 1:size(filesSGIM,1)
    ind = find(listLearningSGIM{ifile}(:,mapp.dimA+3) <0.05);
    x = [x; listLearningSGIM{ifile}(ind,mapp.dimA+1)];
    y = [y; listLearningSGIM{ifile}(ind,mapp.dimA+2)];
end
k = convhull(x,y);
h3 = plot(x(k),y(k),'-','Color',colorSGIM)
hold on

xlabel('x');
ylabel('y');
title('Convex hull of the reached Points  ');
%set(h1, 'Displayname', 'Convex hull of SAGG RANDOM');
set(h2, 'Displayname', 'Convex hull of SAGG-RIAC');
set(h3, 'Displayname', 'Convex hull of SGIM');
set(h4, 'Displayname', 'Convex hull of Random on the input parameters');
legend('Location', 'northEast');


%% ******     plot evaluation performance      ******    %%
figReachedPoints = figure('Name','Evaluation of the performance on the benchmark data','NumberTitle','off')
h1 = [];
h2 = [];
h3 = [];
h4 = [];
h11 = [];
h21 = [];
h31 = [];
h41 = [];
h12 = [];
h22 = [];
h32 = [];
h42 = [];

%meanMapEvalSAGGRANDOM = mean(cell2mat(mapEvalRANDOMPARAM),2);


for ifile = 1:size(filesSGIM,1)
    filename = [filesDir, filesSGIM(ifile).name]
  %  h3 = [h3; plot(mapEvalSGIM{ifile}(:,end), listDist3SGIM{ifile},'-','Color',colorSGIM)];
    hold on;
   % h31 = [h31; plot(mapEvalSGIM{ifile}(:,end), listDist2SGIM{ifile},':*','Color',colorSGIM)];
    hold on;
    h32 = [h32; plot(mapEvalSGIM{ifile}(:,end), listDistSGIM{ifile},'--','Color',colorSGIM)];
    ylim([0 1])
    hold on
    pause
end

for ifile = 1:size(filesSAGGwithout,1)
        filename = [filesDir, filesSAGGwithout(ifile).name]
   % h2 = [h2; plot(mapEvalSAGGwithout{ifile}(:,end), listDist3SAGGwithout{ifile},'-','Color',colorSAGGwithout)];
    hold on;
    %h21 = [h21; plot(mapEvalSAGGwithout{ifile}(:,end), listDist2SAGGwithout{ifile},':*','Color',colorSAGGwithout)];
    hold on;
    h22 = [h22; plot(mapEvalSAGGwithout{ifile}(:,end), listDistSAGGwithout{ifile},'--','Color',colorSAGGwithout)];
    hold on
    pause
end

for ifile = 1:size(filesRANDOMPARAM,1)
    filename = [filesDir, filesRANDOMPARAM(ifile).name]
    %h4 = [h4; plot(mapEvalRANDOMPARAM{ifile}(:,end), listDist3RANDOMPARAM{ifile},'-','Color',colorRANDOMPARAM)];
    hold on;
    %h41 = [h41; plot(mapEvalRANDOMPARAM{ifile}(:,end), listDist2RANDOMPARAM{ifile},':*','Color',colorRANDOMPARAM)];
    hold on;
    h42 = [h42; plot(mapEvalRANDOMPARAM{ifile}(:,end), listDistRANDOMPARAM{ifile},'--','Color',colorRANDOMPARAM)];
    hold on
   pause
end

ylim([0 1])
xlabel('Number of movements experimented');
ylabel('Mean distance to the goals');
title('Mean error on the benchmark data','FontSize',20,'FontWeight','b');
h1Group = hggroup;
%set(h1,'Parent',h1Group)
h2Group = hggroup;
set(h2,'Parent',h2Group)
h3Group = hggroup;
set(h3,'Parent',h3Group)
h4Group = hggroup;
set(h4,'Parent',h4Group)
h21Group = hggroup;
set(h21,'Parent',h21Group)
h31Group = hggroup;
set(h31,'Parent',h31Group)
h41Group = hggroup;
set(h41,'Parent',h41Group)
h22Group = hggroup;
set(h22,'Parent',h22Group)
h32Group = hggroup;
set(h32,'Parent',h32Group)
h42Group = hggroup;
set(h42,'Parent',h42Group)
%set(get(get(h1Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h2Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h3Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h4Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h21Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h31Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h41Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h22Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h32Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
set(get(get(h42Group,'Annotation'),'LegendInformation'),'IconDisplayStyle','on'); 
%set(h1Group, 'Displayname', 'RANDOM parameters');
set(h2Group, 'Displayname', 'SAGG-RIAC reexecute nearest neighbour');
set(h3Group, 'Displayname', 'SGIM reexecute nearest neighbour');
set(h4Group, 'Displayname', 'Random on the input parameters : reexecute nearest neighbour');
set(h21Group, 'Displayname', 'SAGG-RIAC  nearest neighbour');
set(h31Group, 'Displayname', 'SGIM  nearest neighbour');
set(h41Group, 'Displayname', 'Random on the input parameters  nearest neighbour');
set(h22Group, 'Displayname', 'SAGG-RIAC interpolation');
set(h32Group, 'Displayname', 'SGIM interpolation');
set(h42Group, 'Displayname', 'Random on the input parameters interpolation');
hleg1= legend('Location', 'northEast');
set( hleg1, 'FontWeight','b')
set(hleg1,'FontName','FixedWidth')

%% ********** plot mean evaluation performance      ***************** %%
namx = 6; 
meanMapEvalRANDOMPARAM   = [];
meanListDistRANDOMPARAM = [];
meanListDist2RANDOMPARAM = [];
meanListDist3RANDOMPARAM = [];
for ifile = 1:size(filesRANDOMPARAM,1)
    filesRANDOMPARAM(ifile).name
    size(mapEvalRANDOMPARAM{ifile})
    meanMapEvalRANDOMPARAM   = [meanMapEvalRANDOMPARAM mapEvalRANDOMPARAM{ifile}(1:namx,end)];
    meanListDistRANDOMPARAM = [meanListDistRANDOMPARAM listDistRANDOMPARAM{ifile}(1:namx,:)];
    meanListDist2RANDOMPARAM = [meanListDist2RANDOMPARAM listDist2RANDOMPARAM{ifile}(1:namx,:)];
    meanListDist3RANDOMPARAM = [meanListDist3RANDOMPARAM listDist3RANDOMPARAM{ifile}(1:namx,:)];
end
varListDistRANDOMPARAM    = std(meanListDistRANDOMPARAM',1)';
varListDist2RANDOMPARAM    = std(meanListDist2RANDOMPARAM',1)';
varListDist3RANDOMPARAM    = std(meanListDist3RANDOMPARAM',1)';
meanMapEvalRANDOMPARAM   = mean(meanMapEvalRANDOMPARAM,2);
meanListDistRANDOMPARAM  = mean(meanListDistRANDOMPARAM,2);
meanListDist2RANDOMPARAM = mean(meanListDist2RANDOMPARAM,2);
meanListDist3RANDOMPARAM = mean(meanListDist3RANDOMPARAM,2);
errorbar(meanMapEvalRANDOMPARAM ,meanListDistRANDOMPARAM,varListDistRANDOMPARAM, '--d','Color',colorRANDOMPARAM, 'LineWidth',3)
hold on
errorbar(meanMapEvalRANDOMPARAM ,meanListDist2RANDOMPARAM,varListDist2RANDOMPARAM, ':d','Color',colorRANDOMPARAM, 'LineWidth',3)
hold on
errorbar(meanMapEvalRANDOMPARAM ,meanListDist3RANDOMPARAM, varListDist3RANDOMPARAM, '-d','Color',colorRANDOMPARAM, 'LineWidth',3)
hold on


namx = 7;
meanMapEvalSAGGwithout   = [];
meanListDistSAGGwithout = [];
meanListDist2SAGGwithout = [];
meanListDist3SAGGwithout = [];
for ifile = 1:size(filesSAGGwithout,1)
    size(mapEvalSAGGwithout{ifile})
    meanMapEvalSAGGwithout   = [meanMapEvalSAGGwithout mapEvalSAGGwithout{ifile}(1:namx,end)];
    meanListDistSAGGwithout = [meanListDistSAGGwithout listDistSAGGwithout{ifile}(1:namx,:)];
    meanListDist2SAGGwithout = [meanListDist2SAGGwithout listDist2SAGGwithout{ifile}(1:namx,:)];
    meanListDist3SAGGwithout = [meanListDist3SAGGwithout listDist3SAGGwithout{ifile}(1:namx,:)];
end
varListDistSAGGwithout    = std(meanListDistSAGGwithout',1)';
varListDist2SAGGwithout    = std(meanListDist2SAGGwithout',1)';
varListDist3SAGGwithout    = std(meanListDist3SAGGwithout',1)';
meanMapEvalSAGGwithout   = mean(meanMapEvalSAGGwithout,2);
meanListDistSAGGwithout  = mean(meanListDistSAGGwithout,2);
meanListDist2SAGGwithout = mean(meanListDist2SAGGwithout,2);
meanListDist3SAGGwithout = mean(meanListDist3SAGGwithout,2);
errorbar(meanMapEvalSAGGwithout ,meanListDistSAGGwithout,varListDistSAGGwithout, '--d','Color',colorSAGGwithout, 'LineWidth',3)
hold on
errorbar(meanMapEvalSAGGwithout ,meanListDist2SAGGwithout,varListDist2SAGGwithout, ':d','Color',colorSAGGwithout, 'LineWidth',3)
hold on
errorbar(meanMapEvalSAGGwithout ,meanListDist3SAGGwithout, varListDist3SAGGwithout, '-d','Color',colorSAGGwithout, 'LineWidth',3)
hold on

namx = 6;
meanMapEvalSGIM  = [];
meanListDistSGIM= [];
meanListDist2SGIM= [];
meanListDist3SGIM= [];
for ifile = 1:size(filesSGIM,1)
    ifile
    filesSGIM(ifile).name
    size(mapEvalSGIM{ifile})
    meanMapEvalSGIM  = [meanMapEvalSGIM, mapEvalSGIM{ifile}(1:namx,end)];
    meanListDistSGIM= [meanListDistSGIM, listDistSGIM{ifile}(1:namx,:)];
    meanListDist2SGIM= [meanListDist2SGIM, listDist2SGIM{ifile}(1:namx,:)];
    meanListDist3SGIM= [meanListDist3SGIM, listDist3SGIM{ifile}(1:namx,:)];
end
varMapEvalSGIM   = std(meanMapEvalSGIM,1);
varListDistSGIM   = std(meanListDistSGIM',1)';
varListDist2SGIM   = std(meanListDist2SGIM',1)';
varListDist3SGIM   = std(meanListDist3SGIM',1)';
meanMapEvalSGIM  = mean(meanMapEvalSGIM,2);
meanListDistSGIM = mean(meanListDistSGIM,2);
meanListDist2SGIM= mean(meanListDist2SGIM,2);
meanListDist3SGIM= mean(meanListDist3SGIM,2);
whos meanMapEvalSGIM
whos meanListDistSGIM
whos varListDistSGIM
errorbar(meanMapEvalSGIM,meanListDistSGIM,varListDistSGIM, '--d','Color',colorSGIM, 'LineWidth',3)
hold on
errorbar(meanMapEvalSGIM,meanListDist2SGIM,varListDist2SGIM, ':d','Color',colorSGIM, 'LineWidth',3)
hold on
errorbar(meanMapEvalSGIM,meanListDist3SGIM, varListDist3SGIM, '-d','Color',colorSGIM, 'LineWidth',3)
hold on


%% interpolation mean only
figure
h11 = errorbar(meanMapEvalSGIM,meanListDistSGIM,varListDistSGIM, '-d','Color',colorSGIM, 'LineWidth',6)
hold on
h12 = errorbar(meanMapEvalSAGGwithout ,meanListDistSAGGwithout,varListDistSAGGwithout, '-d','Color',colorSAGGwithout, 'LineWidth',6)
 hold on
 h13 =errorbar(meanMapEvalRANDOMPARAM ,meanListDistRANDOMPARAM,varListDistRANDOMPARAM, '-d','Color',colorRANDOMPARAM, 'LineWidth',6)
hold on
title('Mean of values of the evaluation of the performance on the benchmark data');
xlabel('Number of movements experimented');
ylabel('Mean distance to the goals');
set(h13, 'Displayname', 'Random Exploration');
set(h12, 'Displayname', 'SAGG-RIAC');
set(h11, 'Displayname', 'SGIM');
hleg1= legend('Location', 'northEast');
set( hleg1, 'FontWeight','b');
set(hleg1,'FontSize',14)
set(hleg1,'FontName','FixedWidth')
set(gca,'YGrid','on')
axis([0 5020 0.1 0.7])
end