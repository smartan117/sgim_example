%plotreexecute.m
%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}


function plotreexecute(posObjPrevious, gOutcome, listClosestToGoal, expEnv)
%plotting function
%
% plotDeltaTO(gOutcome, listClosestToGoal, expEnv)
% gOutcome : the goal, 1x3 matrix
% listClosestToGoal : list of deltaTobj experienced, mx3 matrix

if ~isempty(listClosestToGoal)
    
    subplot(2,2,1);
    hline1 = plot3(gOutcome(1), gOutcome(2), gOutcome(3), 'or', 'LineWidth',10);
    hold on
    
    m = size(listClosestToGoal,1);
    dist = distSqrt( [2.5 0 0 gOutcome], listClosestToGoal(:,[1:3 10:12]) )
    c3 = 1./(0.0000001 + dist);
    c3Moy= mean(c3);
    c3= min( c3/c3Moy, 1);
    hline00 = plot3(listClosestToGoal(:,10), listClosestToGoal(:,11), listClosestToGoal(:,12), 'o-' );
    hold on
    hline2 = scatter3( listClosestToGoal(:,10), listClosestToGoal(:,11), listClosestToGoal(:,12), 12,c3(:),'filled' );
    hold on
    hline3 = plot3(listClosestToGoal(m,10), listClosestToGoal(m,11), listClosestToGoal(m,12), 'dm', 'MarkerFaceColor', 'm', 'MarkerSize', 8);
    hold on
    
    
    %axis([-expEnv.rArm.length expEnv.rArm.length -expEnv.rArm.length expEnv.rArm.length -expEnv.rArm.length expEnv.rArm.length])
    axis([-2 2 -2 2 -pi/2 pi/2]);
    camproj('Persp')
%     xlabel('deltaTobj x')
%     ylabel('deltaTobj y')
%     zlabel('deltaTobj rot')
%     set(hline1, 'Displayname', 'Goal displacement');
    set(hline2, 'Displayname', 'displacements observed');
    set(hline3, 'Displayname', 'last displacement observed');
  %  legend('Location', 'northEast');
    title('Observed displacements of the object ', 'FontWeight','bold')
    grid on
    hold on;
    
    subplot(2,2,3);
    hline4 = plot(gOutcome(1), gOutcome(2), 'or', 'LineWidth',10);
    hold on
    hline5 = scatter( listClosestToGoal(:,10), listClosestToGoal(:,11), 12,c3(:),'filled' );
    hline5 = plot( listClosestToGoal(:,10), listClosestToGoal(:,11),'o');
    hold on
    hline6 = plot(listClosestToGoal(m,10), listClosestToGoal(m,11), 'dm', 'MarkerFaceColor', 'm', 'MarkerSize', 8);
    hold on
    axis([-1 1 -1 1])
    xlabel('deltaTobj x')
    ylabel('deltaTobj y')
    
    
    subplot(2,2,2);
    plot(dist, 'xb');
    hold on;
    %plot(dist(max(end-100,1):end,:))
    xlabel('trial number')
    ylabel('distance to the goal')
    title('distance of the Observed displacements of the object to the Goal', 'FontWeight','bold')
    hold on;
    
    %push realized
    subplot(2,2,4)
    if m>1
        quiver(listClosestToGoal(m-1,4), listClosestToGoal(m-1,5),listClosestToGoal(m-1,4)+listClosestToGoal(m-1,7), listClosestToGoal(m-1,5) +listClosestToGoal(m-1,8),'b');
        hold on
    end
    quiver(listClosestToGoal(m,4), listClosestToGoal(m,5),listClosestToGoal(m,4)+listClosestToGoal(m,7), listClosestToGoal(m,5) +listClosestToGoal(m,8),'r');
    hold on
    
end

end