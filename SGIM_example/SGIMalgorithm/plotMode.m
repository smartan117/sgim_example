function plotMode()
variablesGlobales
saggGlobal



if ~isempty(QualityCandidate)
    % compute the percentage of times each mode is chosen
    modesL = [1:partReg.nbStrategies];
    nh = 20; %floor(size(QualityCandidate,2)/10);
    cstring='brgcmyk';
    s = zeros(length(modesL),size(QualityCandidate,1) -nh);
    for i= 1:size(QualityCandidate,1) -nh
        for imode = 1:length(modesL)
            mode = modesL (imode);
            s(imode,i) = length(find(QualityCandidate(i:i+nh,end)==mode));
        end
    end
    
    %plot the percentages
    if (~exist('figMode') || isempty(figMode) )
        figMode = figure();
    else
        figMode = figure(figMode);
    end
    
    for imode = 1:length(modesL)
        plot(s(imode,:),cstring(imode))
        hold on
    end
    strTitle = [];
    for imode = 1:length(modesL)
        strTitle = [strTitle , 'mode (', num2str(modesL(imode)), ') ', cstring(imode) ,', '];
    end
    
    title(['chosen mode: ' , strTitle]);
    drawnow
end

end