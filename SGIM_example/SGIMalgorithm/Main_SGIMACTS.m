%{
 Copyright (c) 2014 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

function Main_SGIMACTS(varargin)
% nargin == 1 means Initialization
%
% =======================================================================%%
%                        Main : Socially Guided Intrinsic Motivation      %
%                Which strategy to choose? according to regions mapping
% =======================================================================%%


%% Initialization
variablesGlobales
saggGlobal
disp('MAIN SGIM-ACTS');




%%=========================================================================
%%=======================     Choose The Goal     =======================
%%=========================================================================

envSimu = randomizeContext(envSimu);
context =  getContext(envSimu);
context01 = (context +1)/2;


alea = rand; ...
    % Choose a Random Action in the whole space MODE2 ======================================
TagMax = 1;


goalTaskType =  randi(nbOutcomeTypes);
goalOutcome = randMinMax(mapp{goalTaskType}.dimY, 1, 0, 1);
iStrat = randi([1, partReg.nbStrategies]);


if alea <ProbMode1
    % Select the Action with Proportional Derivative MODE1 =================
    [goalOutcome iStrat goalTaskType] =  selectGoalMode1(partReg, context01, mapp);
    
elseif alea < ProbMode1+ProbMode3
    % Select the Action With Max in Exploration MODE3 ==============
    [goalOutcome iStrat goalTaskType] = selectGoalMode3(partReg, context01, mapp );
end



%%=========================================================================
%%====================     Mastery Analysis     ===========================
%%=========================================================================

disp(['MAIN_SGIMACTS: strategy ',num2str(iStrat)]);
consequence = [];
curLowLevel.curTask = goalTaskType;


if iStrat==1
    % STRATEGY AUTONOMOUS EXPLORATION
    % THE ROBOT DECIDES yg and a
    curLowLevel.curTask = goalTaskType;
    goalOutcome;
    curLowLevel.gOutcome = convertCG(mapp{goalTaskType}, goalOutcome');
    disp(['Goal chosen: ' num2str(curLowLevel.gOutcome)]);
    consequence            = run1goal(curLowLevel,curLowLevel.gOutcome, context, epmem, mapp, envSimu)
    
elseif iStrat <= length(hTeacher) + 1
    hTeacherid =iStrat - 1;
    disp(['MAIN_SGIMACTS::REQUEST TEACHER ',num2str(hTeacherid) ]);
    
    curLowLevel.curTask              = goalTaskType;
    curLowLevel.gOutcome = convertCG(mapp{goalTaskType}, goalOutcome');
    disp(['Goal chosen: ' num2str(curLowLevel.gOutcome)]);
    demonstration                    = teacherAnswersGoalRequest(hTeacher(hTeacherid), curLowLevel.gOutcome, curLowLevel.curTask, envSimu);
    demoi                            = demonstration(1,:);
    padding = envSimu.dimAllOutcomes + envSimu.dimPolicies - size(demoi,2);
    hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat goalTaskType hTeacherid demoi zeros(1, padding)];
    goalOutcome;    
    consequence                      = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
    
% elseif iStrat==2
%     % STRATEGY REQUEST DEMONTRATION TO TEACHER 1
%     hTeacherid =1;
%     disp(['MAIN_SGIMACTS::REQUEST TEACHER ',num2str(hTeacherid) ]);
%     
%     curLowLevel.curTask              = hTeacher(hTeacherid).task;
%     demonstration                    = teacherGivesDemonstration(hTeacher(hTeacherid), mapp{curLowLevel.curTask},curLowLevel.curTask, envSimu);
% %   demonstration                    = teacherAnswersGoalRequest(hTeacher(hTeacherid), curLowLevel.gOutcome, mapp{curLowLevel.curTask}, envSimu);
%     demoi                            = demonstration(1,:);
%     hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat hTeacher(hTeacherid).task hTeacherid demoi];
%     goalOutcome                      = convertGC(mapp{curLowLevel.curTask}, demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end))';
%     curLowLevel.gOutcome             = demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end);
%     consequence                      = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
% 
%     curLowLevel.curTask              = hTeacher(hTeacherid).task;
%     demonstration                    = teacherAnswersGoalRequest(hTeacher(hTeacherid), curLowLevel.gOutcome, mapp{curLowLevel.curTask}, envSimu);
%     demoi                            = demonstration(1,:);
%     hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat goalTaskType hTeacherid demoi];
%     goalOutcome
%     curLowLevel.gOutcome = convertCG(mapp{goalTaskType}, goalOutcome');
%     consequence                      = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
%     
%     
% elseif iStrat==3
%     % STRATEGY REQUEST DEMONTRATION TO TEACHER 2
%     hTeacherid =2;
%     disp(['MAIN_SGIMACTS::REQUEST TEACHER ',num2str(hTeacherid) ]);
%     
%     curLowLevel.curTask              = hTeacher(hTeacherid).task;
%     demonstration                    = teacherGivesDemonstration(hTeacher(hTeacherid), mapp{curLowLevel.curTask},curLowLevel.curTask, envSimu);
% %   demonstration                    = teacherAnswersGoalRequest(hTeacher(hTeacherid), curLowLevel.gOutcome, mapp{curLowLevel.curTask}, envSimu);
%     demoi                            = demonstration(1,:);
%     hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat hTeacher(hTeacherid).task hTeacherid demoi];
%     goalOutcome                      = convertGC(mapp{curLowLevel.curTask}, demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end))';
%     curLowLevel.gOutcome             = demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end);
%     consequence                      = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
% 
%     curLowLevel.curTask              = hTeacher(hTeacherid).task;
%     demonstration                    = teacherAnswersGoalRequest(hTeacher(hTeacherid), curLowLevel.gOutcome, mapp{curLowLevel.curTask}, envSimu);
%     demoi                            = demonstration(1,:);
%     hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat goalTaskType hTeacherid demoi];
%     goalOutcome
%     curLowLevel.gOutcome = convertCG(mapp{goalTaskType}, goalOutcome');
%     consequence                      = run1imitation(curLowLevel,context, demoi, epmem, mapp, hTeacherid);
     
% elseif iStrat==5
%     hTeacherid =2;
%     disp(['MAIN_SGIMACTS::EMULATE TEACHER ',num2str(hTeacherid) ]);
%     
%     curLowLevel.curTask              = hTeacher(hTeacherid).task;
%     demonstration                    =  teacherGivesDemonstration(hTeacher(hTeacherid), mapp{curLowLevel.curTask},curLowLevel.curTask, envSimu);
%     demoi                            = demonstration(1,:);
%     hTeacher(hTeacherid).listTeacher = [hTeacher(hTeacherid).listTeacher; curLowLevel.nbExperiments iStrat goalTaskType hTeacherid demoi];
%     goalOutcome                  = convertGC(mapp{curLowLevel.curTask}, demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end))';
%     curLowLevel.gOutcome           = demoi(1,end-mapp{curLowLevel.curTask}.dimY+1:end);
%     consequence                      = run1goal(curLowLevel,curLowLevel.gOutcome, context, epmem, mapp, envSimu)

else
    error('MAIN_SGIMACTS :: unknown strategy')
end


%%=========================================================================
%%====================  update tree regions     ===========================
%%=========================================================================
example          = goalOutcome(:);
example          = example(:);
errorValue       = consequence{1}; % -1 if no progress, 0 if high progress
partReg          = updatePartitionRegions(partReg,  example, errorValue, TagMax, iStrat, curLowLevel.curTask) ;
QualityCandidate = [QualityCandidate; [goalOutcome(:)' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY,1) consequence{1} curLowLevel.nbExperiments goalTaskType iStrat]];

disp(['Progress on goal: ' num2str(errorValue)]);

Predictor = [Predictor; [goalOutcome' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) errorValue curLowLevel.nbExperiments goalTaskType iStrat]];

    
if size(epmem{curLowLevel.curTask}.listGoal,1)>2 && curLowLevel.nbExperiments>2
    ProgressL = [ProgressL; consequence{3} iStrat  curLowLevel.nbExperiments];
end

nReachedOutcomes2 = size(consequence{2},1); 
nReachedOutcomes4 = size(consequence{4},1); 
if (nReachedOutcomes2 ~= nReachedOutcomes4)
    error('MAIN_SGIMACTS : error consequence2(reached outcomes) and consequence4(progress for the outcomes) do not have the same size');
end

% for i = 1:nReachedOutcomes2
%         example    = convertGC(mapp{goalTaskType},consequence{2}(i,:));
%         example    = example(:);
%         errorValue = consequence{4}(i,1);
%         partReg = updatePartitionRegions(partReg,example, errorValue, TagMax, iStrat, curLowLevel.curTask) ;
%         Predictor = [Predictor; [consequence{2}(i,:) zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) errorValue curLowLevel.nbExperiments goalTaskType iStrat]];
% end

for iTask=1:length(consequence{2})
    
    nReachedOutcomes2 = size(consequence{2}{iTask},1);
    nReachedOutcomes4 = size(consequence{4}{iTask},1);
    if (nReachedOutcomes2 ~= nReachedOutcomes4)
        error('MAIN_RIAC_CB : error consequence2(reached outcomes) and consequence4(progress for the outcomes) do not have the same size');
    end

    for i = 1:nReachedOutcomes2
        example    = convertGC(mapp{iTask},consequence{2}{iTask}(i,:));
        example    = example(:);

        if(any(example>100) || any(example<0))
            error('example out of bounds. It should take values between 0 and 1');
        end

        errorValue = consequence{4}{iTask}(i,1);
        try
            partReg    = updatePartitionRegions(partReg, example, errorValue, TagMax, iStrat, iTask);
        catch
            disp('erreur');
        end
        Predictor  = [Predictor; [example' zeros(1,maxDimY - mapp{iTask}.dimY) errorValue curLowLevel.nbExperiments iTask iStrat]];
    end

end



%==========================================================================
%==========================================================================
%==========================================================================

end





