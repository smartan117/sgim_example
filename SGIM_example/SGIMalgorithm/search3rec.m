function [outputVal candidatesOutput indDist] = search3rec(currentNode, vectorInput, candidates,nbExpl)

%global nbNN
nbNN = nbExpl;
if isempty(currentNode.next)

    [v indv indDist] = node.predictNearest(currentNode.elem',vectorInput', nbNN);
    outputVal = [currentNode.elem(indv,:) currentNode.output(indv,:)];
else
    distance = zeros(size(currentNode.next,2),1);
    for i = 1:size(currentNode.next,2)
        distance(i) = dist(vectorInput, currentNode.next(i).centroid);
    end
    [sortInd indSort] = sort(distance);
    bestChild = currentNode.next(indSort(1));
    others = currentNode.next(indSort(2:end));
    for i=1:size(others,2)
        push(candidates,others(i),i);
    end
    [outputVal candidatesOutput indDist] = search3rec(bestChild, vectorInput, candidates, nbExpl);
end
candidatesOutput = candidates;