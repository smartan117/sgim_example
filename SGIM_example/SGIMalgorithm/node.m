%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                            NODE (OF TREES)  CLASS                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
%% Author: Sao Mai Nguyen
%% date: September 2010
%% rewritten from Adrien Baranes's source code


classdef node < handle
    properties
        num = [];
        elem = [];
        output = [];
        next = [];
        prev = [];
        centroid  = [];

    end
    
    methods
        
        function nOutput = node(nInput, number)
% constructor

            if nargin > 0
                nOutput = node();
                nOutput.elem = nInput.elem;
                nOutput.output = nInput.output;
                nOutput.num = number + 1;
                for i=1:size(nInput.next)
                    nOutput.next = node(nInput.next(i), nb);
                    nOutput.next = nInput.next;
                    nOutput.prev = nInput.prev;
                    nOutput.num = number + 1;
                end
                nOutput.centroid = nInput.centroid;

            else
                nOutput.elem = [];
                nOutput.output = [];
                nOutput.next = [];
                nOutput.prev = [];
                nOutput.centroid  = [];
                nOutput.num = 0;
            end
        end
        
        
        function currentNode = AddVectorToNodeM(currentNode, vector, number, k, rmax, Nmax,dimInputTree,dimOutputTree)
% add a vector to the node or its children nodes
% vector node : vector to be added, of dimension dimInputTree+dimOutputTree
% Nmax:         % Maximal number of element inside a node, where the minimal distance is inferior to rmax
% rmax;         % Minimal distance needed between the current added vector, and the nearest known one
% k : number of children of each node

            if isempty(currentNode.next)
               
                v = node.predictNearest([currentNode.elem currentNode.output]' ,vector', Nmax)';
                %currentNode
                %currentNode.elem
                
                v = v(1,:);
                %vector
                if dist1(vector,v) >= rmax
                    %add vector to current node
                    currentNode.elem = [currentNode.elem; vector(dimInputTree)];
                    currentNode.output = [currentNode.output; vector(dimOutputTree)];
                    n = size(currentNode.elem,1);
                    if n >= Nmax
                        %disp('New Node')
                        new = node();
                        new.centroid = currentNode.centroid;
                       
                  
                        [indNew_nodes ctrs] = kmeans(currentNode.elem,k,'emptyaction','singleton');
                        % Test the number of cluster created
                        if ~sum(isnan(ctrs(:))) > 0
                            q = zeros(1,k);
                            for i = 1:k
                                % new Children
                                n = node(currentNode, number + i - 1 );
                                n.elem   = currentNode.elem(find(indNew_nodes == i),:);
                                n.output = currentNode.output(find(indNew_nodes == i),:);
                                n.centroid = ctrs(i,:);
                                n.prev = new;
                                new.next = [new.next n];
                            end
                            copy(currentNode,new);
                        else
                            for i = 1:k
                                % new Children
                                n = node(currentNode, number +i -1);
                                n.elem   = currentNode.elem(i+(i-1)*floor(Nmax/k):i*floor(Nmax/k),:);
                                n.output = currentNode.output(i+(i-1)*floor(Nmax/k):i*floor(Nmax/k),:);
                                n.centroid = ctrs(1,:);
                                n.prev = new;
                                new.next = [new.next n];
                            end
                            copy(currentNode,new);
                        end
                    end
                else
                    currentNode.elem   = [currentNode.elem  ; vector(dimInputTree)];
                    currentNode.output = [currentNode.output; vector(dimOutputTree)];
                end
            else
                d = zeros(k,1);
                for i=1:size(currentNode.next,2)
                    d(i) = distSqrt(vector(dimInputTree),currentNode.next(i).centroid);
                end
                [val indDistMin] = min(d);
                
                currentNode = AddVectorToNodeM(currentNode.next(indDistMin), vector,number, k, rmax, Nmax, dimInputTree,dimOutputTree);
            end
        end
        
        function outputVal = searchNode(obj,vectorInput,b,nbExpl)
% search in the node and its child nodes for associations [input output] so that input is closest to vectorInput
% vectorInput : vector of size dimInput
% b : backtraking parameter ( nb of child nodes to search in)
% nbExpl : number of nearest neighbours asked
% outputVal : matrix (dimInput +dimOutput) x nbExpl of the nbExpl nearest associations

            nbNN = nbExpl;
            candidates = stack();
            root = obj.next;
            push(candidates,root,1);
            
            outputValTmp = cell(b,1);
            indDist = cell(b,1);
            
            for i=1:b
                if size(candidates.untreated,2) > 0
                    [outputValTmp{i} candidates indDist{i}] = search3rec(pop(candidates), vectorInput, candidates,nbExpl);
                end
            end;
            
            outputValTmpF = [];
            indDistF = [];
            
            try
                for j = 1:b
                    outputValTmpF = [outputValTmpF; outputValTmp{j}];
                    indDistF = [indDistF indDist{j}];
                end
            end
            
            [valSort indSort] = sort(indDistF);
            nbNN = min(nbExpl, size(indSort,2) );
            outputVal = outputValTmpF(indSort(1:nbNN),:);
        end
        
        
        function copy(Out,In)            
            for i=1:size(Out.prev.next,2)
                if Out.prev.next(i) == Out
                    In.prev = Out.prev;
                    Out.prev.next(i) = In;
                end
            end
        end
        
        
        function [outputVal candidatesOutput indDist] = search3rec(currentNode, vectorInput, candidates,nbExpl)
		     % search in currentNode for an association [vectorInput ouputVal]
		     % currentNode : a node
       		     % vectorInput : a vector of size 1x dimInput
		     % candidates : list of nodes to search in
		     % nbExpl : nb of nearest neighbours asked
		     % outputVal : vector of size 1 x (dimOutput + dimInput)
		     % candidatesOutput :  updated list of nodes to search in
		     % indDist :   
 
            %global nbNN
            nbNN = nbExpl;
            if isempty(currentNode.next)
                [v indv indDist] = node.predictNearest(currentNode.elem',vectorInput', nbNN);
                if indv~=0
                    outputVal = [currentNode.elem(indv,:) currentNode.output(indv,:)];
                else
                    outputVal = inf;
                end
            else
                distance = zeros(size(currentNode.next,2),1);
                for i = 1:size(currentNode.next,2)
                    distance(i) = dist1(vectorInput, currentNode.next(i).centroid);
                end
                [sortInd indSort] = sort(distance);
                bestChild = currentNode.next(indSort(1));
                others = currentNode.next(indSort(2:end));
                for i=1:size(others,2)
                    push(candidates,others(i),i);
                end
                
                [outputVal candidatesOutput indDist] = search3rec(bestChild, vectorInput, candidates, nbExpl);
            end
            candidatesOutput = candidates; 
        end
        
        
    end %end methods
  
    
    methods(Static)
        function [output outputInd outputDist] = predictNearest(I, e, nbNN)
	    %  selects from the vectors I the nbNN vectors closest to e 
	    % vectors in I are vertical, e is a vertical vector dim x 1
            % output is a matrix dim x nbNN of the closest vectors 
	    % outputInd is a matrix 1 x nbNN of indexes of the vectors output in I
	    % outputDist is a vector of the distances from output to e.

            
            if (~isempty(I))
                
                % crude method for nearest neighbour
                
                [outputInd outputDist] = node.closest(I, e, nbNN) ;
                output = I(:,outputInd);
                % prediction based on rbf neural network
                %rbf = newrbe(I,O) ;
                %v = sim(rbf, e) ; i = 0;
                
                % prediction based on lwr
                %[v, i] = predictLinear(I, O, e, 5) ;
                
            else
                output = inf ;
                outputInd = [];
                outputDist = inf;
            end
            
        end % end predictNearest
        
        
        function [v vDist] = closest(A, e, nbNN)
            %  selects from the vectors A the nbNN vectors closest to e 
	    % vectors in A are vertical, e is a vertical vector dim x 1
            % v is a matrix 1 x nbNN of indexes of the closest vectors 
	    % and vDist is a vector of the distances from v to e.

            [m, n] = size(A) ;
            tmp = e(:, ones(1, n)) ;
            B = A - tmp ;
            B = B.*B ;
            if m>1
                %     NORMS = sqrt(sum(B)) ;
                NORMS = sum(B);
            else
                NORMS = B ;
            end ;
            [valIncrease, indIncrease] = sort(NORMS) ;
            
            if size(indIncrease,2) > nbNN
                v = indIncrease(1:nbNN);
                vDist = valIncrease(1:nbNN);
            else
                v = indIncrease(1:end);
                vDist = valIncrease(1:end);
%                 while size(v,2) < nbNN
%                     v = [v indIncrease(1)];
%                     vDist = [vDist valIncrease(1)];
%                 end
            end
            
        end% end closest
        
    end % end methods static
end
