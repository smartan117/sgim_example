%experiment.m
% This is one of the most used functions of the experiment.
% This function handles the execution of a policy in a defined context,
% and the interpretation of the outcome, ie its competence measure, and the memorisation of the learning data

% Copyright (c) 2013 Sao Mai Nguyen
%               e-mail : nguyensmai@gmail.com
%               http://nguyensmai.free.fr/
%
% Permission is granted to copy, distribute, and/or modify this program
% under the terms of the GNU General Public License, version 2 or any
% later version published by the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details
% 
% If you use this code in the context of a publication, I would appreciate 
% it if you could cite it as follows:
% 
% 
% @phdthesis{Nguyen2013,
% 	Author = {Nguyen, Sao Mai},
% 	School = {INRIA},
% 	Title = {A Curious Robot Learner for Interactive Goal-Babbling: Strategically Choosing What, How, When and from Whom to Learn},
% 	Year = {2013}}
%


function [outDist reached] = experiment(movParam, exploType, cont,iOutcome, goal)
%movParam  : policy parameter to execute
%exploType : exploration type (marker recorded in the episodic memory)
%cont      : context parameter
%iOutcome  : type of task (in the case of tasks of different nature)
%goal      : goal task to complete
%outDist   : distance between the reached outcome and the goal outcome.

variablesGlobales
saggGlobal
progressReachedOutcome = 0;
param = normaliseParam(envSimu,movParam);


%--------------------------------------------------------------------------
%--------------------   execute movement           ------------------------
%--------------------------------------------------------------------------

%disp('EXPERIMENT : movParam ');
%movParam  = normaliseParam(movParam,  rFish);
%envSimu.curs = randParameters(envSimu.curs);  %RANDOM MODE !!!
if nargin>=3 && ~isempty(cont)
    setContext(envSimu, cont);
end


context = getContext(envSimu);
reached = executePolicy(envSimu, param, 1);

%% TODO: handle the calculation of the distance
if nargin <5
    outDist = distMapping(mapp{1}, reached, []);
else
    outDist = distMapping(mapp{1}, reached, goal);
end
%--------------------------------------------------------------------------
%--------------------  update memory with new data         ----------------
%--------------------------------------------------------------------------

curLowLevel.progressReachedOutcome{iOutcome} = [];

%% new memory entity
if exploType >= 0
    curLowLevel.LearningEntity = [context movParam reached exploType];
    if exploType ~=100
        perfBefore = bestDistToGoal(epmem{iOutcome}, reached, context, 3);
    end
    epmem{1} = addLearningEntity(epmem{1}, context, movParam, ...
        reached, exploType);
    if exploType ~=100
        perfAfter = meanDistToGoal(epmem{iOutcome}, iOutcome, reached, context, 3);
%         if reached<0 && reached>-0.2
%             try
%                 figure(15)
%                 dispRegions(partReg,1);
%             end
%         end
        progress = computeProgress(perfBefore, perfAfter, curLowLevel.nbExperiments,curLowLevel.nbExperiments-curLowLevel.numberIteration,epmem{curLowLevel.curTask}.distY);
        curLowLevel.progressReachedOutcome{iOutcome}(end+1,1) =  progress;
    end
    curLowLevel.nbExperiments = curLowLevel.nbExperiments +1;
    
end


%--------------------------------------------------------------------------
%-------------  check if the teacher makes a demonstration  ---------------
%--------------------------------------------------------------------------

if hTeacher(1).present
    
    teacherInterrupts();
    if (curLowLevel.flagTeach ==1)
        epmem{1}.listFlagTeach= [epmem{1}.listFlagTeach; size(epmem{1}.listLearning,1) 1];
    end
    
end


%--------------------------------------------------------------------------
%----------- post-processing: display graphs and evaluation ---------------
%--------------------------------------------------------------------------

% display figure
% if evalPerf.affichePlot  && mod(epmem.nbElements,EVAL_EXP) == 0
%     plotReachedPoints;
%   %  plotExperience(epmem, mapp)
% end

%if  exploType <= 2 && mod(epmem.nbElements,EVAL_EXP) == 1
%    disp(' EXPERIMENT : DISPLAY REEXECUTE');
%    evalPerf = bestExperience(evalPerf, epmem, goal, rFish);
%end

% if nargin ==5
%     figure(10)
%     plot(curLowLevel.nbExperiments, outDist,'x', 'Color',([ goal 0 goal]+1)/2)
%     hold on
%     title('error per trial');
% end

if  (exploType >= 0 && (mod(curLowLevel.nbExperiments, 1000) == 1 || curLowLevel.nbExperiments == 1|| curLowLevel.nbExperiments == 10 || curLowLevel.nbExperiments == 50||curLowLevel.nbExperiments == 100||curLowLevel.nbExperiments == 750|| curLowLevel.nbExperiments == 500)  )
    save( [fileMemo,'_nb_',num2str(curLowLevel.nbExperiments)] );
    evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    visualiseExploration;
end



% disp(['EXPERIMENT : exploType ', num2str(exploType),' outDist ',num2str(outDist)]);

end

