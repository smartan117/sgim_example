close all
mFiles = dir('../data/Imitation1_*-2016.mat');
load(['../data/',mFiles(1).name]);
evalPerf = plotListDist(evalPerf);
saveas(evalPerf.figPerfOnce,'Imitation1Error','fig')

close all
mFiles = dir('../data/Imitation2_*-2016.mat');
load(['../data/',mFiles(1).name]);
evalPerf = plotListDist(evalPerf)
saveas(evalPerf.figPerfOnce,'Imitation2Error','fig')

close all
mFiles = dir('../data/Imitation3_*-2016.mat');
load(['../data/',mFiles(1).name]);
evalPerf = plotListDist(evalPerf)
saveas(evalPerf.figPerfOnce,'Imitation3Error','fig')

close all
mFiles = dir('../data/Imitation4_*-2016.mat');
load(['../data/',mFiles(1).name]);
evalPerf = plotListDist(evalPerf)
saveas(evalPerf.figPerfOnce,'Imitation4Error','fig')

close all
mFiles = dir('../data/Sgim1*-2016.mat');
load(['../data/',mFiles(1).name]);
evalPerf = plotListDist(evalPerf)
saveas(evalPerf.figPerfOnce,'Sgim1Error','fig')

close all
mFiles = dir('../data/Sgim2*-2016.mat');
load(['../data/',mFiles(1).name]);
evalPerf = plotListDist(evalPerf)
saveas(evalPerf.figPerfOnce,'Sgim2Error','fig')

close all
mFiles = dir('../data/Sgim3*-2016.mat');
load(['../data/',mFiles(1).name]);
evalPerf = plotListDist(evalPerf)
saveas(evalPerf.figPerfOnce,'Sgim3Error','fig')

close all
mFiles = dir('../data/Sgim4*-2016.mat');
load(['../data/',mFiles(1).name]);
evalPerf = plotListDist(evalPerf)
saveas(evalPerf.figPerfOnce,'Sgim4Error','fig')

%-------
%%
figureEnd =figure;
s1 = subplot(1,1,1)

figureCur= open('randomError.fig');
legend('random','random');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

% 
% figureCur= open('riacError.fig');
% legend('riac','riac');
% ax1 = gca;
% childCur = get(ax1,'children');
% copyobj(childCur,s1);


% figureCur= open('Saggriac1Error.fig');
% legend('Sagg-Riac','Sagg-Riac');
% ax1 = gca;
% childCur = get(ax1,'children');
% copyobj(childCur,s1);

figureCur= open('Saggriac2Error.fig');
legend('Sagg-Riac','Sagg-Riac');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figureCur= open('Sgimacts24Error.fig');
legend('sgimacts24','sgimacts24');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figureCur= open('Sgimacts34Error.fig');
legend('sgimacts34','sgimacts34');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figureCur= open('Imitation1Error.fig');
legend('Imitation1','Imitation1');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);


figureCur= open('Imitation2Error.fig');
legend('Imitation2','Imitation2');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figureCur= open('Imitation3Error.fig');
legend('Imitation3','Imitation3');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figureCur= open('Imitation4Error.fig');
legend('Imitation4','Imitation4');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);


figureCur= open('Sgim1Error.fig');
legend('Sgim1','Sgim1');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figureCur= open('Sgim2Error.fig');
legend('Sgim2','Sgim2');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figureCur= open('Sgim3Error.fig');
legend('Sgim3','Sgim3');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figureCur= open('Sgim4Error.fig');
legend('Sgim4','Sgim4');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figure(figureEnd)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
set(gca,'YGrid','on')
set(gca,'yscale','log');
