function outPredict = multiVariateRegression(X, y, xg)

m   = size(y,1);   %number of samples
%dimY= size(y,2); %number of features, dimension of Y

% Add intercept term to X
X = [ones(m, 1) X];

% Calculate the parameters from the normal equation
theta = normalEqn(X, y);
%[theta, J_history] = gradientDescent(X, y);

% Display normal equation's result
% fprintf('Theta computed from the normal equations: \n');
% fprintf(' %f \n', theta);
% fprintf('\n');


% Estimate the price of a 1650 sq-ft, 3 br house
% ====================== YOUR CODE HERE ======================
outPredict = [1  xg(:)']*theta; %theta'*[1 ; xg(:)]; % You should change this


end

function [theta] = normalEqn(X, y)
%NORMALEQN Computes the closed-form solution to linear regression 
%   NORMALEQN(X,y) computes the closed-form solution to linear 
%   regression using the normal equations.

theta = zeros(size(X, 2), size(y, 2));

% ====================== YOUR CODE HERE ======================
% Instructions: Complete the code to compute the closed form solution
%               to linear regression and put the result in theta.
%

% ---------------------- Sample Solution ----------------------


%theta= pinv(X'*X)* X'*y;
theta= (X'*X)\ (X'*y);

% -------------------------------------------------------------


% ============================================================

end


function [theta, J_history] = gradientDescent(X, y)
%GRADIENTDESCENT Performs gradient descent to learn theta
%   theta = GRADIENTDESENT(X, y, theta, alpha, num_iters) updates theta by 
%   taking num_iters gradient steps with learning rate alpha

% Initialize some useful values
m   = size(y,1);   %number of samples
n= size(y,2); %number of features, dimension of Y

theta = zeros(size(X, 2), size(y, 2));
alpha=0.01;
num_iters = 100;

% J_history = zeros(num_iters, 1);

for iter = 1:num_iters

    % ====================== YOUR CODE HERE ======================
    % Instructions: Perform a single gradient step on the parameter vector
    %               theta. 
    %
    % Hint: While debugging, it can be useful to print out the values
    %       of the cost function (computeCostMulti) and gradient here.
    %


    theta= theta- alpha/m*(X'*(X*theta-y));


    % ============================================================
% 
%     % Save the cost J in every iteration    
%     J= computeCostMulti(X, y, theta);
%     J_history(iter) =J; 

end

end
