classdef SimpleTest
    %change classdef name for your example
    
    % this class defines your environment, and how policies cause outcomes
    %
    % Copyright (c) 2013 Sao Mai Nguyen
    %               e-mail : nguyensmai@gmail.com
    %               INRIA Bordeaux - Sud-Ouest
    %               351 Cours de la Liberation, 33400, Talence,
    %               France,
    %               http://nguyensmai.free.fr/
    %
    
    
    %--------------------------------------------------------------------------
    %-------------------------  properties     --------------------------------
    %--------------------------------------------------------------------------
    
    properties
        dimPolicies;
        nbOutcomeTypes;   %total number of task types
        plotenv ;    %boolean. true if you want to display the environment with drawEnvironment, else false
        context;
    end %end properties
    
    methods
        
        %--------------------------------------------------------------------------
        %--------------------------  constructor     ------------------------------
        %--------------------------------------------------------------------------
 
        function obj = SimpleTest()
            % constructor
            obj.dimPolicies=100;
            obj.nbOutcomeTypes = 1;
            obj.plotenv = 0;
        end %end of constructor
        
        %--------------------------------------------------------------------------
        %--------------------------  methods     ------------------------------
        %--------------------------------------------------------------------------
         
        
        function obj = drawEnvironment(obj)
            % displays the environment setup environment
            if(obj.plotenv)
                ;
            else
                ;
            end
        end  %end drawEnvironment
        
        function eval = evalPoints(obj, eval)
            % sets a set of benchmark points for the evaluation
            % eval : Evaluation object,where we edit its listEvalPointsD, listEvalPoints, listEvalWeight.
            %        each outcome type iOutcome \in [1 nbTaches], in this function we define:
            %        - nbTaches :  number of outcome types
            %        - listEvalPoints{iOutcome} a list of outcomes to be produced
            %        - listEvalWeight{iOutcome} a list of weights for each of the outcomes listEvalPoints{iOutcome}{i}
            %        - listEvalPointsD{iOutcome} is a cell of size 1x(dimesion of task subspace), listEvalPointsD{iOutcome}{iDim} is a vector of the (unique) values of listEvalPoints{iOutcome}(iDim)
            % cf file ../SGIMPlusieursTaches/Evaluation.m for details on the class Evaluation
            
            eval.nbOutcomeTypes       = 1;
            
            for iOutcome = 1:eval.nbOutcomeTypes
                for iDim =1:1 %for each dimension of the type of task iOutcome
                    eval.listEvalPointsD{iOutcome}{iDim} = [-1:0.02:1]; % 1xn vector;
                end
                eval.listEvalPoints{iOutcome} = [-1:0.02:1]'; %nxdim Outcome vector
                eval.listEvalWeight{iOutcome} = ones(size(eval.listEvalPoints{iOutcome})); %1xn vector
                
            end
        end
        
        
        
        %--------------------------------------------------------------------------
        %----- generating contexts and policies and checking their format   -------
        %--------------------------------------------------------------------------
        
        
        function obj = randomizeContext(obj)
            % generate random contexts
            obj.context = 30;
        end % end randomizeContext
        
        function param  = randParameters(obj)
            % generate random policy parameters
            param = 2*(rand(1,obj.dimPolicies)-0.5);
        end %end randomizeContext
        
        
        function param = normaliseParam(obj, param)
            % check that policy parameters are allowed
            % (within the right range, are in the proper format)
            
            param = min(max(param,-1),1);
        end %end normaliseParam
        
        function context = getContext(obj)
            context =  30;
        end %end getContext
        
        function obj = setContext(obj, context)
            obj.context = 30;
        end
        
        
        %--------------------------------------------------------------------------
        %-------- model of the environment: outcome reached by an outcome ---------
        %--------------------------------------------------------------------------
        
        function reached = executePolicy(obj,movParam, iOutcome)
            % simulate the environment change with a policy
            % movParam : policy parameter
            % iOutcome : type of outcome to read
            % reached : the induced/reached outcome
            param = normaliseParam(obj,movParam);
            
            x1=param(1);
            x2=param(2);
            r =sqrt(x1^2+x2^2);
            
            f1=1/12;
            f2=7/12;
            reached0=-0.7;
            f3=1/12;
            reached1= 0.5;
            
            if r<f1           %reached \in [-1 -0.4]
                reached = 0.3*(2*(r-f1/2)/f1)^5-0.7 + 0.001*(rand(1)-0.5);
%                 reached     = -0.6/2*cos( 3*pi*(r/f1).^2 ) + reached0 +0.001*(rand(1)-0.5);
%                 reached     = -0.2*cos( 3*pi*(r/f1).^2 ) + reached0 +0.001*(rand(1)-0.5);
%                 reached     = reached + 0.2/f1*abs(r);
            elseif r<f2       %reached \in [-0.4 -0.2]
                reached     = -0.4 + 0.2/(f2-f1)*(r-f1)+ 0.001*(rand(1)-0.5);
            else
                r2= sqrt((x1-1)^2+(x2-1)^2);
                if r2<f3 && all(abs(param(3:end))<1/2)      % reached \in [0 1]
                    reached = cos( 3*pi*(r2/f3).^2 )/2 + reached1 +0.001*(rand(1)-0.5); %1 - 6*(r2*2)^2;
                else          % reached \in [-0.2 0]
                    reached = 0.2*(rand(1)-0.5) - 0.1;
                end
            end
            
            
            reached = max(min(reached, 1), -1);
        end  %end execute
        
    end %end methods
    
    methods(Static=true)
        
        function teacherDataset()
            dimPolicies =100;
            regTaught=[]; %to return: table of [y1min,y1max,y2min,y2max,nbTaught, a, y]
            
            f1=1/12;
            f2=7/12;
            reached0=-0.7;
            f3=1/12;
            reached1= 0.5;
 
            
            %perfect teacher : teacher 1
            for y=-1:0.05:-0.4
                x1= f1/2 + f1/2*sign(y+0.7)*(abs(y+0.7)/0.3)^(1/5); %f1*sqrt( acos(-4*(y-reached0))/(3*pi) );
                regTaught=[regTaught; y-0.025,y+0.025,0, x1,0, zeros(1,dimPolicies-2), y];
            end
            for y=-0.4:0.05:-0.2005
                x1=f1+(y+0.4)*(f2-f1)/0.2;
                regTaught=[regTaught; y-0.025,y+0.025,0, x1,0, zeros(1,dimPolicies-2), y];
            end
            for y=0:0.05:1
                s= f3*sqrt( acos(2*(y-reached1))/(3*pi) );
                x1=1-s;
                regTaught=[regTaught; y-0.025,y+0.025,0, x1,1, zeros(1,dimPolicies-2), y];
            end
            save('simpleTeacher1','regTaught')
            
            %partial teacher: teacher2
            regTaught=[]; %to return: table of [y1min,y1max,y2min,y2max,nbTaught, a, y]
            for y=-1:0.05:1
                x1= f1/2 + f1/2*sign(y+0.7)*(abs(y+0.7)/0.3)^(1/5); %f1*sqrt( acos(-4*(y-reached0))/(3*pi) );
                regTaught=[regTaught; y-0.025,y+0.025,0, x1,0, zeros(1,dimPolicies-2), y];
            end
             
            save('simpleTeacher2','regTaught')
           
            %partial teacher: teacher3
            regTaught=[]; %to return: table of [y1min,y1max,y2min,y2max,nbTaught, a, y]
            for y=-1:0.05:1
                x1=f1+(y+0.4)*(f2-f1)/0.2;
                regTaught=[regTaught; y-0.025,y+0.025,0, x1,0, zeros(1,dimPolicies-2), y];
            end
             
            save('simpleTeacher3','regTaught')
            
            %partial teacher: teacher4
            regTaught=[]; %to return: table of [y1min,y1max,y2min,y2max,nbTaught, a, y]
            reached11=-0.5;
            for y=-1:0.05:-0.05
                s= f3*sqrt( acos (2*(y-reached11))/(3*pi) );
                x1=1-s;
                regTaught=[regTaught; y-0.025,y+0.025,0, x1,1, zeros(1,dimPolicies-2), y];
            end
            for y=0:0.05:1
                s= f3*sqrt( acos (2*(y-reached1))/(3*pi) );
                x1=1-s;
                regTaught=[regTaught; y-0.025,y+0.025,0, x1,1, zeros(1,dimPolicies-2), y];
             end
             
            save('simpleTeacher4','regTaught')

        end
        
        
        function plotEnvironmentOutcomes()
            P=path;
            path(P,'../../SGIMalgorithm/')
            envSimu = SimpleTest();
            [X,Y] = meshgrid(-1:.005:1);
            param = zeros(1,envSimu.dimPolicies);
            Z = 0*X;
            for i=1:length(X(:))
                param(1,1) = X(i);
                param(1,2) = Y(i);
                Z(i) = executePolicy(envSimu,param, 1);
            end
            figure
            mesh(X,Y,Z)
            
            X2 = (-1:0.005:1);
            Z2 = 0*X2;
            for i=1:length(X2)
                param2(1,1) = X2(i);
                param2(1,2) = X2(i);
                Z2(i) = executePolicy(envSimu,param2, 1);
            end
            figure
            plot(X2,Z2,'-')
        end
        %--------------------------------------------------------------------------
        %------------------------  test functions ---------------------------------
        %--------------------------------------------------------------------------
        
        function test_EnvironmentSimulator()
            envSimu = EnvironmentSimulator()
            param   = randParameters(envSimu)
            executePolicy(envSimu, param)
        end
        
        function testTeacherDataset(filename)
            load(filename)
            envSimu = SimpleTest();
            nDemo=size(regTaught,1);
            error=zeros(1,nDemo);
            yDemo=zeros(1,nDemo);
            
            for iDemo=1:nDemo
               aDemo = regTaught(iDemo, (2*envSimu.nbOutcomeTypes+2):(end-envSimu.nbOutcomeTypes));
               yDemo(iDemo) = regTaught(iDemo, end-envSimu.nbOutcomeTypes+1:end);
               y = executePolicy(envSimu,aDemo, 1);
               error(iDemo)= y-yDemo(iDemo);
            end
            figure
            plot(yDemo,error,'x-');
            
        end
        
    end %end static methods
    
end
