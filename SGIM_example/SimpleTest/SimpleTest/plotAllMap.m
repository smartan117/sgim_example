function plotAllMap()
close all
figureEnd = figure();

addOneMap('../data/Random1_*-2016.mat', figureEnd);
addOneMap('../data/Saggriac2_*-2016.mat', figureEnd);

addOneMap('../data/Imitation1_*-2016.mat', figureEnd);
addOneMap('../data/Imitation2_*-2016.mat', figureEnd);
addOneMap('../data/Imitation3_*-2016.mat', figureEnd);
addOneMap('../data/Imitation4_*-2016.mat', figureEnd);

addOneMap('../data/Sgim1*-2016.mat', figureEnd);
addOneMap('../data/Sgim2*-2016.mat', figureEnd);
addOneMap('../data/Sgim3*-2016.mat', figureEnd);
addOneMap('../data/Sgim4*-2016.mat', figureEnd);

addOneMap('../data/Sgimacts24*-2016.mat', figureEnd);
addOneMap('../data/Sgimacts34*-2016.mat', figureEnd);

legend('Random', 'Sagg-Riac', ...
    'Imitation1', 'Imitation2', 'Imitation3', 'Imitation4', ...
    'Sgim-D1', 'Sgim-D2', 'Sgim-D3', 'Sgim-D4',...
    'Sgim-Acts24', 'Sgim-Acts34')

set(findall(gca, 'Type', 'Line'),'LineWidth',2);

end

function addOneMap(string, figureEnd)
mFiles = dir(string);
load(['../data/',mFiles(1).name]);
evalPerf.figMapEval = figureEnd;
evalPerf = plotMapEval(evalPerf,9);
end
