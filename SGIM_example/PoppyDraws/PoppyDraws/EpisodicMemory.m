%{
 Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              INRIA Bordeaux - Sud-Ouest
              351 Cours de la Liberation, 33400, Talence,
              France,
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:
@article{Nguyen2012PJBR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Paladyn Journal of Behavioural Robotics},
	Title = {Active choice of teachers, learning strategies and goals for a socially guided intrinsic motivation learner},
	Year = {2012}}

@article{Nguyen2014AR,
	Author = {Nguyen, Sao Mai and Oudeyer, Pierre-Yves},
	Journal = {Autonomous Robots},
	Title = {Socially Guided Intrinsic Motivation for Robot Learning of Motor Skills},
	Year = {2014}}
%}

classdef EpisodicMemory <handle
    
    properties
        listContext;
        listLearning;     %list of the [context param yreached typeOfExploration]= memory of the system
        listChooseCrit;   %list of the chooseCrit for choosing between random exporation or optimisation (fminsearch)
        listGoal;         %list of the [timestamp goal]
        listFlagTeach;
        mapping;
        gauss;            %parameters for local interpolation
        distA;
        distY;
        errY;
        figListGoal
        figListReached
    end
    
    
    methods
        
        function obj = EpisodicMemory(mapp,distA, distY, gauss, errY)
            obj.listLearning      = [];
            obj.listGoal          = [];
            obj.listChooseCrit    = [];
            obj.listFlagTeach     = [];
            obj.listContext       = [];
            obj.mapping           = mapp;
            obj.distA             = distA;
            obj.distY             = distY;
            obj.gauss             = gauss;
            obj.errY              = errY;
            
        end
        
        function iContext = whichContext(obj,context)
            ind1In =[];
            if ~isempty(obj.listContext)
                for i =1:obj.mapping.dimC
                    ind1In{i} = obj.listContext(:,i)==context(i);
                end
                iContext = ones(size(ind1In{1}));
                for i =1:obj.mapping.dimC
                    iContext = iContext.*ind1In{i};
                end
                iContext = find(iContext~=0);
            end
            if ~exist('iContext') || isempty(iContext)
                iContext = size(obj.listContext,1)+1;
            end
        end
        
        %% add learning entity [context, action, outcome] to the episodic memory
        function obj = addLearningEntity(obj, context, action, outcome, explo)
            learningEntity = [context action outcome explo];
            obj.listLearning      = [obj.listLearning ; learningEntity];
            
            vector1    = [ action outcome];
            vector2    = [ outcome action];
            
            iContext = whichContext(obj,context);
            if iContext > size(obj.listContext,1);
%             if (isempty(obj.treeDirectList) || (length(obj.treeDirectList)<iContext) )
%                 obj.treeDirectList{iContext} = createTreeDirect(obj.mapping);
                obj.listContext              = [obj.listContext; context(:)'];
            end 
%             if (isempty(obj.treeInvList) || (length(obj.treeInvList)<iContext) )
% %                obj.treeInvList{iContext}    = createTreeInv(obj.mapping);
%             end
            
%             obj.treeDirectList{iContext} = AddVectorToTree(obj.treeDirectList{iContext}, vector1);
%             obj.treeInvList{iContext}    = AddVectorToTree(obj.treeInvList{iContext}   , vector2);
            
        end
        
        function nEl = nbElements(obj)
            nEl = size(obj.listLearning,1);
        end
        
        %% returns the size of the memory
        function nC = nbContexts(obj)
            nC  = size(obj.listContext,1);
        end
        
        %% plot the list of all goal outcomes self-determined by the
        %%learner
        function obj = plotListGoal(obj,iOutcome)
            global fileMemo
            
            if ~isempty(obj.listGoal)
%                 if isempty(obj.figListGoal)
%                     if ~isempty(fileMemo)
%                         obj.figListGoal = figure('Name',fileMemo);
%                     else
%                         obj.figListGoal = figure('Name',[MODE,'Teacher', ...
%                             num2str(hTeacher(1).present),'-frequency',num2str(hTeacher(1).frequency) ]);
%                     end
%                 else
%                     figure(obj.figListGoal);
%                 end
                if obj.mapping.dimY==1
                    plotListGoal_1D(obj,iOutcome)
                elseif obj.mapping.dimY ==2
                    plotListGoal_2D(obj,iOutcome)
                elseif obj.mapping.dimY ==3
                    plotListGoal_3D(obj,iOutcome)
                end
            end
        end
        
        
        function plotListGoal_3D(obj,iOutcome)
            scatter3(obj.listGoal(:,end-2),...
                obj.listGoal(:,end-1),...
                obj.listGoal(:,end),...
                20,'s');
            xlabel('x')
            ylabel('y')
            zlabel('z')
            title(['histogram of goals self-determined space' num2str(iOutcome)]);
        end
        
        
        function plotListGoal_2D(obj,iOutcome)
            figure
            %             subplot(2,1,1);
            %             ygmin = min(obj.listGoal(:,end-1));
            %             ygmax = max(obj.listGoal(:,end-1));
            %             histrange = linspace(ygmin, ygmax, 200);
            %             dy = histrange(2)- histrange(1);
            %             histrange = [histrange(1)-2*dy,histrange(1)-dy, histrange, histrange(end)+dy, histrange(end)+2*dy];
            %             hist(obj.listGoal(:,end-1),histrange);
            %             xlim([ygmin, ygmax]);
            %             xlabel('goals1');
            %             ylabel('# goals');
            %
            %             subplot(2,1,2);
            %             ygmin = min(obj.listGoal(:,end));
            %             ygmax = max(obj.listGoal(:,end));
            %             histrange = linspace(ygmin, ygmax, 200);
            %             dy = histrange(2)- histrange(1);
            %             histrange = [histrange(1)-2*dy,histrange(1)-dy, histrange, histrange(end)+dy, histrange(end)+2*dy];
            %             hist(obj.listGoal(:,end),histrange);
            %             xlim([ygmin, ygmax]);
            hist3(obj.listGoal(:,end-1:end),'FaceAlpha',.65)
            xlim([obj.mapping.yMin(1) obj.mapping.yMax(1)])
            ylim([obj.mapping.yMin(2) obj.mapping.yMax(2)])
            xlabel('goals1');
            ylabel('goals2');
            zlabel('# goals');
            set(gcf,'renderer','opengl');
            title(['histogram of goals self-determined space ' num2str(iOutcome)]);
        end
        
        function plotListGoal_1D(obj,iOutcome)
            ygmin = min(obj.listGoal(:,end));
            ygmax = max(obj.listGoal(:,end));
            histrange = linspace(ygmin, ygmax, 40);
            dy = histrange(2)- histrange(1);
            histrange = [histrange(1)-2*dy,histrange(1)-dy, histrange, histrange(end)+dy, histrange(end)+2*dy];
            hist(obj.listGoal(:,end),histrange);
%             xlim([ygmin, ygmax]);
            xlim([obj.mapping.yMin(1) obj.mapping.yMax(1)])
            xlabel('goals');
            ylabel('# goals');
            title(['histogram of goals self-determined space ' num2str(iOutcome)]);
            
        end %end function plotListGoal
        
        function obj = plotReachedPoints(obj, iOutcome)
            if obj.mapping(1).dimY==1
                obj = plotReachedPoints1D(obj, iOutcome);
            elseif obj.mapping(1).dimY==2
                obj = plotReachedPoints2D(obj, iOutcome);
            end
        end
        
        function obj=plotReachedPoints1D(obj, iOutcome)
            plot(obj.listLearning(:,obj.mapping.dimYL(1)),zeros(size(obj.listLearning,1)),'.');
            xlim([obj.mapping.yMin obj.mapping.yMax]);
            ylim([-1 1]);
            title(['Points reached for outcome space ' num2str(iOutcome)]);
            xlabel('outcome');
        end
        
        function obj=plotReachedPoints2D(obj, iOutcome)
            plot(obj.listLearning(:,obj.mapping.dimYL(1)),obj.listLearning(:,obj.mapping.dimYL(2)),'.');
            xlim([obj.mapping.yMin(1) obj.mapping.yMax(1)]);
            ylim([obj.mapping.yMin(2) obj.mapping.yMax(2)]);
            title(['Points reached for outcome space ' num2str(iOutcome)]);
            xlabel('outcome 1');
            ylabel('outcome 2');
        end
        
        %% plot the list of all goal outcomes self-determined by the
        %%learner
        function obj= plotListReached(obj,iOutcome)
            global fileMemo
            if isempty(obj.figListReached)
                if ~isempty(fileMemo)
                    obj.figListReached = figure('Name',fileMemo);
                else
                    obj.figListReached = figure('Name',[MODE,'Teacher', ...
                        num2str(hTeacher(1).present),'-frequency',num2str(hTeacher(1).frequency) ]);
                end
            else
                figure(obj.figListReached);
            end
            
            if obj.mapping(1).dimY==1
                plotListReached_1D(obj,iOutcome)
            elseif obj.mapping(1).dimY==2
                %% WTF????
                scatter(obj.listLearning(:,obj.mapping(1).dimYL(1)),...
                    obj.listLearning(:,obj.mapping(1).dimYL(2)), 20,'s');
                xlim([obj.mapping(1).yMin(1), obj.mapping(1).yMax(1)]);
                ylim([obj.mapping(1).yMin(2), obj.mapping(1).yMax(2)]);
                title(['thing of realised outcomes space ' num2str(iOutcome)]);
                plotListReached_2D(obj,iOutcome)
            elseif obj.mapping(1).dimY==3
                plotListReached_3D(obj,iOutcome)
            end
        end
        
        function plotListReached_3D(obj,iOutcome)
            scatter3(obj.listLearning(:,obj.mapping(1).dimYL(1)),...
                obj.listLearning(:,obj.mapping(1).dimYL(2)),...
                obj.listLearning(:,obj.mapping(1).dimYL(3)),...
                20,'s');
            
            OutcomeMin = max([min([obj.listLearning(:,obj.mapping(1).dimYL);obj.mapping(1).yMin], [], 1); ...
                obj.mapping(1).yMin]);
            OutcomeMax = min([max([obj.listLearning(:,obj.mapping(1).dimYL); obj.mapping(1).yMax], [], 1); ...
                obj.mapping(1).yMax]);
            xlim([OutcomeMin(1), OutcomeMax(1)]);
            ylim([OutcomeMin(2), OutcomeMax(2)]);
            zlim([OutcomeMin(3), OutcomeMax(3)]);
            xlabel('x')
            ylabel('y')
            zlabel('z')
            title(['histogram of realised outcomes space ' num2str(iOutcome)]);
        end
        
        
        
        function plotListReached_2D(obj,iOutcome)
            
            ind1 = (obj.listLearning(:,end-2) > obj.mapping(1).yMin(1));
            ind2 = (obj.listLearning(:,end-2) < obj.mapping(1).yMax(1));
            ind3 = (obj.listLearning(:,end-1) > obj.mapping(1).yMin(2));
            ind4 = (obj.listLearning(:,end-1) < obj.mapping(1).yMax(2));
            if sum(ind1&ind2&ind3&ind4) > 0
                hist3(obj.listLearning(ind1&ind2&ind3&ind4,end-2:end-1),'FaceAlpha',.65)
            end
            xlabel('outcomes1');
            ylabel('outcomes2');
            zlabel('# outcomes');
            set(gcf,'renderer','opengl');
            title(['histogram of realised outcomes space ' num2str(iOutcome)]);
        end
        
        function plotListReached_1D(obj,iOutcome)
            ygmin = max(min([obj.listLearning(:,end-1); obj.mapping.yMin(1)]), obj.mapping.yMin(1));
            ygmax = min(max([obj.listLearning(:,end-1); obj.mapping.yMax(1)]), obj.mapping.yMax(1));
            histrange = linspace(ygmin, ygmax, 40);
            dy = histrange(2)- histrange(1);
            histrange = [histrange(1)-2*dy,histrange(1)-dy, histrange, histrange(end)+dy, histrange(end)+2*dy];
            hist(obj.listLearning(:,end-1),histrange);
%             xlim([ygmin, ygmax]);
            xlim([obj.mapping.yMin(1) obj.mapping.yMax(1)])
            xlabel('Reached outcomes');
            ylabel('# times reached');
            title(['histogram of realised outcomes space ' num2str(iOutcome)]);
            
        end %end function plotListReached
        
        %% plots the evolution in time of tendencies to use different
        %% modes of exploration
        function plotExploMode(obj, modesL,nh)
            if exist('modesL') && ~isempty(modesL)
                if nargin<3
                    nh= 500;
                end
                s = [];
                cstring='brgcmyk';
                for i= 1:nbElements(obj) -nh
                    for imode = 1:length(modesL)
                        mode = modesL (imode);
                        s(imode,i) = length(find(obj.listLearning(i:i+ ...
                            nh,end)==mode));
                    end
                end
                if ~isempty(s)
                    for imode = 1:length(modesL)
                        plot(s(imode,:),cstring(imode))
                        hold on
                    end
                end
                
                strTitle = [];
                for imode = 1:length(modesL)
                    strTitle = [strTitle , 'mode (', num2str(modesL(imode)), ') ', cstring(imode) ,', '];
                end
                
                
                title(['chosen mode: ' , strTitle]);
            end
        end
        
        %% gives statistics about the percentages of each explored mode
        function perOut = percentageExploMode(obj, modesL)
            perOut = [];
            if exist('modesL') && ~isempty(modesL)
                perOut=zeros(length(modesL),1);
                for imode = 1:length(modesL)
                    mode = modesL (imode);
                    ind = find(obj.listLearning(:,end)==mode);
                    perOut(imode) = length(ind);
                end
            end
            disp(['EpisodicMemory: percentageExploMode ', perOut])
        end
        
        %% plots the whole memory at low level active learning
        function plotExperience(obj)
            mapp = obj.mapping;
            nGoal  = size(obj.listGoal,1)-50
            nGoal1 = nGoal;
            
            if nGoal >0
                fig3 = figure(3);
                for i=nGoal1:nGoal-1
                    j = obj.listGoal(i,1);
                    k = obj.listGoal(i+1,1)-1;
                    color = [obj.listGoal(i,2) (obj.listGoal(i,3)+1)/2 mod(i/100,1) ];
                    color = min(max(color, 0), 1);
                    if obj.listLearning(j,end)>1
                        scatter(obj.listGoal(i,2),obj.listGoal(i,3), 50, color, 'filled','s');
                        hold on
                        text( obj.listGoal(i,2),obj.listGoal(i,3),num2str(i) );
                        scatter(obj.listLearning(j:k,mapp.dimA+1), obj.listLearning(j:k,mapp.dimA+2), 20,color,'s');
                        hold on
                    else
                        scatter(obj.listGoal(i,2),obj.listGoal(i,3), 40, color, 'filled','o');
                        hold on
                        text( obj.listGoal(i,2),obj.listGoal(i,3),num2str(i) );
                        scatter(obj.listLearning(j:k,mapp.dimA+1), obj.listLearning(j:k,mapp.dimA+2), 20,color,'o');
                        hold on
                    end
                    
                    plot([obj.listLearning(j:k,mapp.dimA+1); obj.listGoal(i,2)], [ obj.listLearning(j:k,mapp.dimA+2); obj.listGoal(i,3)], '-','Color', color);
                    hold on
                end
                
                i = nGoal
                j = obj.listGoal(i,1);
                k = obj.nbElements;
                color = [obj.listGoal(i,2) (obj.listGoal(i,3)+1)/2 mod(i/100,1) ];
                color = min(max(color, 0), 1);
                if obj.listLearning(j,end)>1
                    scatter(obj.listGoal(i,2),obj.listGoal(i,3), 50, color, 'filled','s');
                    hold on
                    text( obj.listGoal(i,2),obj.listGoal(i,3),num2str(i) );
                    scatter(obj.listLearning(j:k,mapp.dimA+1), obj.listLearning(j:k,mapp.dimA+2), 20,color,'s');
                    hold on
                else
                    scatter(obj.listGoal(i,2),obj.listGoal(i,3), 40, color, 'filled','o');
                    hold on
                    text( obj.listGoal(i,2),obj.listGoal(i,3),num2str(i) );
                    scatter(obj.listLearning(j:k,mapp.dimA+1), obj.listLearning(j:k,mapp.dimA+2), 20,color,'o');
                    hold on
                end
                
                plot([obj.listLearning(j:k,mapp.dimA+1); obj.listGoal(i,2)], [ obj.listLearning(j:k,mapp.dimA+2); obj.listGoal(i,3)], '-','Color', color);
                hold on
                xlabel('x');
                ylabel('y');
            end
            
            hold off
        end % end function plotExperience
        
        
        %% looks up un the tree to find the data which values are closest
        %% to param. Return NN vectors
        function [neighbours cDirect] = nearestDirect(obj, context, param, NN)
            iContext = whichContext(obj,context);
            if(iContext>obj.nbContexts)
                [~, iContext] = min(distSqrt(obj.listContext, context));
            end
            cDirect = obj.listContext(iContext,:);
            
            
            paramListLearning = obj.listLearning(:, obj.mapping.dimAL);
            distParam = distSqrt(param, paramListLearning);
            nmax = min([ NN, size(distParam,1)]);
            [dSort,iSort] = sort(distParam);
            neighbours = obj.listLearning(iSort(1:nmax), [obj.mapping.dimAL obj.mapping.dimYL ]);
            
            
            
            %            dimInputTree  = obj.treeDirectList{iContext}.dimInput;
            %             dimOutputTree = obj.treeDirectList{iContext}.dimOutput;
            %             % BackTracking Parameter
            %             b = 1;
            %             obj.treeDirectList{iContext}.nbEx = NN;
            %
            %
            %             neighbours = zeros(1, size(dimOutputTree,2)+size(dimInputTree,2));
            %
            %             % check the input parameters
            %
            %             m = size(param,1);
            %             n = size(param,2);
            %
            %             if n~= size(dimInputTree)
            %                 error('EPISODICMEMORY::NEARESTDIRECT error: dimInputTree and [posObj y] sizes do not correspond');
            %                 return;
            %             else
            %
            %
            %                 % neighbours = zeros(m ,size(dimOutputTree,2)+size(dimInputTree,2));
            %                 % OBTAIN APPROXIMATES NEAREST NEIGHBORS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            %                 ind = 0;
            %                 for i = 1:m
            %                     Tmp = searchANNTree(obj.treeDirectList{iContext},param(i,:)) ; % Tmp = nbEx Nearest Neighbors of xIn(i) and yIn(i)
            %                     N = min(NN, size(Tmp,1));
            %                     for j = 1:N
            %                         ind = ind + 1;
            %                         neighbours(ind,:) = Tmp(j,:);
            %                     end
            %                 end
            %             end
            
        end %end function nearestDirect
        
        
        
        %% looks up un the tree to find the data which values are closest to param. Return NN vectors
        function [neighbours cInv] = nearestInv(obj, context, y, NN)
            % INITIALIZATION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if isempty(obj.listContext)
                neighbours = [];
                cInv       =[];
            else
                
                iContext = whichContext(obj,context);
                if(iContext>obj.nbContexts)
                    if ~isempty(obj.listContext)
                        [~, iContext] = min(distSqrt(obj.listContext, context));
                    else
                        iContext = 1;
                    end
                end
                cInv = obj.listContext(iContext,:);
                
                if size(y,2) ~= obj.mapping.dimY
                    error('EPISODICMEMORY::NEARESTINV error in size of input argument y');
                end
                yListLearning = obj.listLearning(:, obj.mapping.dimYL);
                distY = distMapping(obj.mapping,yListLearning,y);
                nmax = min([ NN, size(distY,1)]);
                [dSort, iSort] = sort(distY);
                neighbours = obj.listLearning(iSort(1:nmax), [obj.mapping.dimYL obj.mapping.dimAL ]);
                
                
                
                %{
%                 dimInputTree  = obj.treeInvList{iContext}.dimInput;
%                 dimOutputTree = obj.treeInvList{iContext}.dimOutput;
%                 dim = size(dimInputTree,2) + size(dimOutputTree,2);
%
%                 b = obj.treeInvList{iContext}.b; % BackTracking Parameter
%                 obj.treeInvList{iContext}.nbEx = NN; %  Number of nearest neighbors asked when using search3
%
%                 [m, n]     = size(y);
%                 neighbours = zeros(m,dim );
%
%                 % check the input parameters
%                 if n ~= length(dimInputTree) %% error possible. check : n ~= dimInputTree
%                     y = [y 0];
%                    % error(['EPISODICMEMORY::NEARESTINV error: dimA and the dimInput of the tree are different. n=', num2str(n),' dimInputTree=',num2str(dimInputTree)]);
%                     % search for the nearest element
%                 end
%                     ind = 0;
%                     for i = 1:m
%                         Tmp = searchANNTree(obj.treeInvList{iContext}, y(i,:)) ;% Tmp = nbEx Nearest Neighbors of xIn(i) and yIn(i)
%                         NN = min(NN, size(Tmp,1) );
%                         for j = 1:NN
%                             ind = ind + 1;
%                             neighbours(ind,:) = Tmp(j,:);
%                         end
%                     end
                %}
            end
            
        end  % end function nearestInv
        
        
        %% find the best memory data (in the most stable locality) for
        %% interpolation
        function [yBest, paramBest, cBest] = bestLocality(obj, context, yg)
            [neighbour1, cInv]  = nearestInv( obj, context, yg, ceil(obj.mapping.dimA*2));
            
            %{
            yBest    = neighbour1(:, obj.mapping.dimYI);
            paramBest= neighbour1(:, obj.mapping.dimAI);
            cBest    = cInv;
%}
            
            if ~isempty(neighbour1)
                n1In       =  neighbour1(:, obj.mapping.dimYI);
                dist1      =  distMapping(obj.mapping,n1In,yg)/obj.mapping.distYMax ;
                ind1       =  find(dist1< obj.distY);
                if isempty(ind1)
                    indLim = min( 5, size(neighbour1,1) );
                    ind1   = [1:indLim];
                end
                neighbour1 = neighbour1(ind1,:);
            else
                error('EPISODICMEMORY::empty');
            end
            nb1        = size(neighbour1,1);
            

            yBest       = [];
            paramBest   = [];
            cBest       = cInv;
            
            if nb1>=1
                a1   = neighbour1(:, obj.mapping.dimAI); %[context actParam]
                
                
                %%
                minVariance = Inf;
                indBest     = 1;
                
                
                for i=1:nb1
                    [neighbour22 cDir] = nearestDirect(obj, cInv,a1(i,:), obj.mapping.dimA);
                    
                    if ~isempty(neighbour22)
                        dist2              = distSqrt(neighbour1(i,obj.mapping.dimAI), neighbour22(:, obj.mapping.dimAD))/sqrt(obj.mapping.dimA);
                        ind2               = find(dist2< obj.distA);
                        
                        if size(ind2,1)<4
                            ind2Lim = min( 4, size(dist2,1) );
                            ind2    = [1:ind2Lim];
                        end
                        neighbour2       = neighbour22(ind2,:);
                        
                    else
                        error('EPISODICMEMORY::empty');
                    end
                    
                    param2            = neighbour2(:, obj.mapping.dimAD);
                    y2                = neighbour2(:,  obj.mapping.dimYD);
                    n2                = size(y2,1);
                    errorModel=zeros(n2,1);
                    
                    %compute training error by the interpolation model
                    for j=1:n2
                        outParam = multiVariateRegression(y2([1:j-1 j+1:end],:), param2([1:j-1 j+1:end],:), y2(j,:));
                         errorModel(j) = distSqrt(outParam, param2(j,:));
                    end
                    varDist2=mean(errorModel);
                    
                    if ( (i==1) || (varDist2 < minVariance) )
                        minVariance  = varDist2;
                        indBest      = i;
                        yBest        = y2;
                        cBest        = cDir;
                        paramBest    = param2;
                    end
%{
                    dist2P            = distMapping(obj.mapping,y2,yg)/obj.mapping.distYMax ;
%                     [d2PSort,i2PSort] = sort(dist2P);
%                     partI2PSort       = min(length(i2PSort), ceil(obj.mapping.dimA/5));
%                     param2            = param2(i2PSort(1:partI2PSort),:);
%                     y2                = y2(i2PSort(1:partI2PSort),:);
                    variance2        = mean(var(y2));
                    varDist2         = mean(distSqrt(yg,y2))+ 2*norm(mean(repmat(yg,[size(y2,1),1])-y2,1)) + 2*variance2;
                    
                    %varDist2         = mean(distMapping(obj.mapping,y2, yg)) ; %+ variance2;
                    %                    varDist2         = mean(distSqrt(yg,y2)) + norm(mean(bsxfun(@minus, yg,y2),1)) + variance2;
                    if ( (i==1) || (varDist2 < minVariance) )
                        minVariance  = varDist2;
                        indBest      = i;
                        yBest        = y2;
                        cBest        = cDir;
                        paramBest    = param2;
                        varianceBest = variance2;
                    end
%}
                end
                
            end
            
        end
        
        %%  interpolation of the nearest pushes as a weighted sum. The weights are proportional to the error to the goal
        function outParam = interpolateParam( obj, context, yg, envSim)
            %NN=2*obj.mapping.dimA;
            %outParam = interpolateParam_nb( obj, context, yg, NN, envSim);
            outParam = interpolateParam_dist( obj, context, yg, envSim);
        end
        
        
        function outParam = interpolateParam_dist( obj, context, yg, envSim)
            %             interpolation using 5 neighbours of yg, then computing the variance
            %             of the corresponding parameters, before choosing the best
            %             etape 1: yg   ->   neighbour1 = [y1 param1]
            
            
            
            % cyBest is [context outcome]
            [yBest, paramBest, cBest] = bestLocality(obj, context, yg);
            cyBest = [repmat(cBest,size(yBest,1),1) yBest];
            
            outParam = multiVariateRegression(yBest, paramBest, yg);
            
            %{
if isempty(cyBest) || isempty(paramBest)
                outParam = [];
            else
                % dist1    = distMapping(obj.mapping, yBest(1,:), yg );
                diffDist = max(obj.gauss/1000,distMapping(obj.mapping,yBest, yg )/obj.mapping.distYMax);% - dist1;
%                 coef     = ((obj.gauss - diffDist)./(obj.gauss*diffDist)).^2 ;% exp(  - ( (diffDist/obj.mapping.distYMax).^2)./(2*obj.gauss));
                coef     = exp(  - ( (diffDist/obj.mapping.distYMax).^2)./(2*obj.gauss) );
                try
                    num      = sum(coef'*paramBest, 1);
                catch
                    disp('error')
                end
                    
                denom    = sum(coef);
                
                outParam = num/denom;
                outParam = normaliseParam(envSim, outParam);

            

                %{
                % plotting for TEST
                for i= 1:size(paramBest,1)
                    Xc = [0:0.05:paramBest(i,end)]';
                    for j= 1:envSim.nbJoints
                        figure(3+j)
                        Vinv2 = MovementEncoder.getMovementTrajectory(paramBest(i,(j-1)*4+1:j*4),Xc);
                        plot(linspace(0,Xc(end),4),paramBest(i,(j-1)*4+1:j*4),'og');
                        hold on
                        plot(Xc,Vinv2, 'Xg-')
                        hold on
                    end
                end
                Xc = [0:0.05:outParam(end)]';
                for j= 1:envSim.nbJoints
                    figure(3+j)
                    Vinv = MovementEncoder.getMovementTrajectory(outParam(1,(j-1)*4+1:j*4),Xc);
                    plot(linspace(0,Xc(end),4),outParam(1,(j-1)*4+1:j*4),'ob');
                    hold on;
                    plot(Xc,Vinv, 'Xb-')
                    hold off
                end
                %}
        end
            %}
        end  %end interpolateParam_dist
        
        
        
        function outParam = interpolateParam_nb( obj, context, yg, NN, envSim)
            % interpolation using 5 neighbours of yg, then computing the variance
            % of the corresponding parameters, before choosing the best
            % etape 1: yg   ->   neighbour1 = [y1 param1]
            neighbour1 = nearestInv( obj, context, yg, NN);
            % distSqrt(yg, neighbour1(:, obj.treeInv.dimInput));
            nb1        = size(neighbour1,1);
            
            
            if nb1>=1
                nMax=min(NN,nb1);
                yBest    = neighbour1(1:nMax, obj.mapping.dimYI);
                paramBest= neighbour1(1:nMax, obj.mapping.dimAI);
                outParam = multiVariateRegression(yBest, paramBest, yg);

               %{
                denom    = 0;
                num      = 0;
                param1   = neighbour1(:, obj.treeInv.dimOutput);
                y1       = neighbour1(:, obj.treeInv.dimInput);
                
                % etape 2 : param1   ->   neighbour2 = [param2 y2]
                %     neighbour2       = nearestDirect(param1, treeDirect, NN)
                %
                %     param2           = neighbour2(:, treeDirect.dimInput);
                %     y2         = neighbour2(:, treeDirect.dimOutput);
                %
                minVariance = Inf;
                indBest     = 1;
                yBest   = [];
                paramBest   = [];
                
                for i=1:nb1
                    neighbour2       = nearestDirect(obj, param1(i,:), NN);
                    param2           = neighbour2(:, obj.treeDirect.dimInput);
                    y2               = neighbour2(:, obj.treeDirect.dimOutput);
                    variance3        = mean(var(y2));  % mean(var(neighbour2)) plutot???
                    variance2        = distVar(yg, y1(i,:), variance3);
                    if variance2 < minVariance
                        minVariance  = variance2;
                        indBest      = i;
                        yBest        = y2;
                        paramBest    = param2;
                    end
                end
                
                coef   = exp(  - ((distMapping(obj.epmem, yBest, yg )).^2)./obj.gauss);
                num    = sum(coef'*paramBest, 1);
                denom  = sum(coef);
                
                outParam = num/denom;
                outParam = normaliseParam(outParam, envSim);
                %}
            else
                outParam = [];
            end
            
        end %end function interpolateParam
        
        
        %%  calculate local model around the goal
        function  [dist pos interpolParam] = localModel(obj,context, tache, goal, envSim)
            %computes inverse model and reexecutes the obtained policy to output the reached outcome
            % context : vector representing the context
            % tache   : integer representing the outcome type
            % goal    : parameter of the goal outcome
            % envSim  : simulation environment
            % disp('EPISODICMEMORY::LOCAL MODEL');
            dist = 10000;
            %parameters of the function, to adjust
            interpolParam    = interpolateParam( obj, context, goal, envSim);
            interpolParam    = normaliseParam(envSim, interpolParam);
            if ~isempty(interpolParam)
                [dist pos] = experiment(interpolParam, -1, context, tache, goal);
            end

%             neighbourParam    = nearestInv( obj, context, goal, 1);
%             interpolParam     = normaliseParam(envSim, neighbourParam(1,obj.mapping.dimAI));
%             [dist pos] = experiment(interpolParam, -1, context, tache, goal);

        end % end function localModel
        
        
        function [paramOut, distOut] = locallyExploreTowardsGoal(obj, envSim, curContext,curTask,gOutcome,chooseCrit,nbRep )
            [paramOut, distOut] = locallyExploreTowardsGoalFminSearch(obj, envSim, curContext,curTask,gOutcome,chooseCrit,nbRep );
        end
        
        function [paramOut, distOut] = locallyExploreTowardsGoalFminSearch(obj, envSim, curContext,curTask,gOutcome,chooseCrit,nbRep )
            %if you wish to use fminseearch from the optimisation
            %toolbox,  uncomment the code below
            variablesGlobales
            
            neighbour1  = nearestInv( obj, curContext, gOutcome, 1);
            newParam = interpolateParam( obj, curContext, gOutcome, envSim);
             newDist = distSqrt(  newParam , neighbour1(:,obj.mapping.dimAI)) ;
%            newDist = distMapping( obj.mapping, newParam , neighbour1(:,obj.mapping.dimAI)) ;
            if sum(newDist/obj.mapping.dimA < 10^-10)>0
                newParam = newParam + (10^-5)*rand(size(newParam));
            end
            exploType = 2;
            options = optimset('MaxIter',nbRep,'MaxFunEvals', nbRep);
            % options = optimset('TolFun',newDist(1)/10);
            if hTeacher(1).present
                options = optimset(options,'OutputFcn',@teacherInterrupts);
            end
            
            [paramOut distOut] = fminsearchTree(@(push)experiment(push,exploType,curContext,curTask,gOutcome), newParam', options);
            
        end
        
        function [paramOut, distOut] = locallyExploreTowardsGoalRandom(obj, envSim, curContext,curTask,gOutcome,chooseCrit,nbRep1 )

            nbRep = 5;
            paramOut=zeros(nbRep,obj.mapping.dimA);
            
            
            %                 disp('LOWLEVEL::RUN1REACH: explore towards goal');
            %                 [~, paramBest] =
            %                 bestLocality(epmem{obj.curTask}, obj.curContext, obj.gOutcome);
            %                 if size(paramBest,1)>=3
            %                     paramNN1 = paramBest(1,:);
            %                     paramNN2 = paramBest(2,:);
            %                     paramNN3 = paramBest(3,:);
            %                 else
            %                     paramNN1 = neighbour1(1,epmem{obj.curTask}.mapping.dimAI);
            %                     paramNN2 = neighbour1(2,epmem{obj.curTask}.mapping.dimAI);
            %                     paramNN3 = neighbour1(3,epmem{obj.curTask}.mapping.dimAI);
            %                 end
            
            [~, paramBest] = bestLocality(obj, curContext, gOutcome);
            if size(paramBest,1)>=3
                paramNN1 = paramBest(1,:);
                paramNN2 = paramBest(2,:);
                paramNN3 = paramBest(3,:);
            else
                neighbour1  = nearestInv( obj, curContext, gOutcome, 3);
                paramNN1 = neighbour1(1,obj.mapping.dimAI);
                paramNN2 = neighbour1(2,obj.mapping.dimAI);
                paramNN3 = neighbour1(3,obj.mapping.dimAI);
            end
            
            for i=1:nbRep
                t1 = 0.2*rand(size(paramNN1));
                t2 = 0.1*rand(size(paramNN1));
                movParam         = ((1-t1).*paramNN1 + t1.*paramNN2).*(1-t2) + t2.*paramNN3 ;
                paramOut(i,:)=movParam;
            end
            
             
            nbRep = size(paramOut,1);
            distOut=zeros(nbRep,1);
            for i=1:nbRep
                %                     t1 = 0.2*rand(size(paramNN1));
                %                     t2 = 0.5*rand(size(paramNN1));
                %                     rP = chooseCrit* (rand(size(paramNN1)) -0.5);
                %                     movParam         = ((1-t1).*paramNN1 + t1.*paramNN2).*(1-t2) + t2.*paramNN3 +rP ;
                movParam= paramOut(i,:);
                dist             = experiment(movParam, 2,  obj.curContext, obj.curTask, obj.gOutcome);
                distOut(i)       = dist;
            end
            
            
        end
        
        function distOut = bestDistToGoal(obj, goal, context, NN)
            neighbourParam = nearestInv( obj, context, goal, NN);
            %pm =  obj.mapping.distUnreached;
            pm = obj.mapping.distMapping();
            
            if ~isempty(neighbourParam)
                performanceL   = distMapping(obj.mapping, neighbourParam(:,obj.mapping.dimYI), goal);
                performance    = min(pm, mean(performanceL));
            else
                performance    = pm;
            end
            distOut = performance;
        end
        
        function distOut = meanDistToGoal(obj, iOutcome, goali, cont, NN)
            
            variablesGlobales;
            saggGlobal;

            %pm =  obj.mapping.distUnreached;
            pm = obj.mapping.distMapping();
            
            neighbourParam    = nearestInv( obj, cont, goali, NN);
            dist=zeros(1,NN);
            
            if ~isempty(neighbourParam)
                NN=size(neighbourParam,1);
                for i=1:NN
                    outcome       =  neighbourParam(i,obj.mapping.dimYI);
                    dist(i)       =  distMapping(mapp{iOutcome}, outcome, goali);
                end
                distOut = mean(dist);
            else
                distOut = pm;
            end
            
        end
        
        
    end %end methods
    
    
    methods (Static = true)
        
        function testOut = test_interpolateParamDist()
            saggGlobal
            variablesGlobales
            
            envSimu       = SimpleTest();
            DimensionIn   = envSimu.dimPolicies;
            DimensionOut  = 2
            DimensionCont = 1;
            
            
            Ymax     = [ 1 1];
            Ymin     = [ -1 -1];
            mapp{1}     = Mapping(DimensionIn,DimensionOut, DimensionCont, Ymin, Ymax);
            evalPerf = Evaluation(envSimu);
            epmem    = EpisodicMemory(mapp{1},0.1,0.1,0.01);
            
            yg          = [1  0];
            context1    = 1;
            movParam1   = ones(1,DimensionIn);
            %movParam1(1)= 0.1;
            y1          = [1  0];
            epmem       = addLearningEntity(epmem, context1, movParam1, ...
                y1, 1);
            
            
            [yBest, paramBest, cBest] = epmem.bestLocality( 1, yg);
            outParam1 = epmem.interpolateParam_dist(1, yg, envSimu)
            % should output outParam1 = [1 ... 1]
            
            movParam2   = ones(1,DimensionIn);
            movParam2(1)= 0.9;
            y2          = [0.9  0];
            epmem = addLearningEntity(epmem, context1, movParam2, ...
                y2, 1);
            
            
            movParam3   = ones(1,DimensionIn);
            movParam3(1)= 0.8;
            y3          = [0.8  0];
            epmem = addLearningEntity(epmem, context1, movParam3, ...
                y3, 1);
            
            
            [yBest, paramBest, cBest] = epmem.bestLocality( 1,yg);
            
            outParam3 = epmem.interpolateParam_dist(1, yg, envSimu)
            % should output outParam3 = [1 ... 1]
            
            outParam4 = epmem.interpolateParam_dist(1, y2, envSimu)
            outParam5 = epmem.interpolateParam_dist(1, y3, envSimu)
            y4=[0.85 0];
            outParam5 = epmem.interpolateParam_dist(1, y4, envSimu)
            y5=[0.5 0];
            outParam5 = epmem.interpolateParam_dist(1, y5, envSimu)
                
            if( isequal(outParam3, ones(1,DimensionIn)) && isequal(outParam1, ones(1,DimensionIn) ) )
                testOut = 1;
            else
                testOut = 0;
            end
        end %end function test_interpolateParamDist
        
        %enables testing new anntree parameters
        %function to fill in new trees with the listLearning.
        %function test_newTree()
        %    n =size(epmem.listLearning);
        %    epmem.treeDirect   = createTreeDirect(mapp);
        %    epmem.treeInv      = createTreeInv(mapp);
        %    for i=1:n
        %        entity = epmem.listLearning;
        %        context = entity(i,epmem.mapping.dimCD);
        %        param   = entity(i,epmem.mapping.dimAD);
        %        reached = entity(i,epmem.mapping.dimYD);
        %        epmem.treeDirect = AddVectorToTree(epmem.treeDirect, [context param reached]);
        %        epmem.treeInv    = AddVectorToTree(epmem.treeInv, [context reached param]);
        %    end
        %end
        
    end %end methods static
    
end
