function plotAllMap()
close all
figureEnd = figure();

addOneMap('../data/Random1_*-2016.mat', figureEnd);

legend('Random')

set(findall(gca, 'Type', 'Line'),'LineWidth',2);

end

function addOneMap(string, figureEnd)
mFiles = dir(string);
load(['../data/',mFiles(1).name]);
evalPerf.figMapEval = figureEnd;
evalPerf = plotMapEval(evalPerf,9);
end
