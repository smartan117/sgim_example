
figureEnd =figure;
s1 = subplot(1,1,1)

figureCur= open('randomError.fig');
legend('random','random');
ax1 = gca;
childCur = get(ax1,'children');
copyobj(childCur,s1);

figure(figureEnd)
set(findall(gca, 'Type', 'Line'),'LineWidth',2);
set(gca,'YGrid','on')
set(gca,'yscale','log');
