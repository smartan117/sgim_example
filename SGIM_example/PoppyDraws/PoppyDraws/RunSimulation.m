function RunSimulation( numberAct, numberIter,MODEin, teach, fteach, filename)
%% example of command:
%% RunSimulation(5000,50,'RANDOMPARAM',0,0,'Random1')
%{
Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILIT      Y or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate 
it if you could cite it as follows:


@phdthesis{Nguyen2013,
	Author = {Nguyen, Sao Mai},
	School = {INRIA},
	Title = {A Curious Robot Learner for Interactive Goal-Babbling: Strategically Choosing What, How, When and from Whom to Learn},
	Year = {2013}}


Main program to launch an experiment.
defines global variables and paths that need to be defined only once
then it launches experiments with Simulation.
Simulation and RunSimulation usually have the same arguments

numberAct : int : total number of movements/trials for the whole experiment
numberIter: int : number of movemnts/trials for each goal outcome
MODEin : string : name of the algorithm to experiment with
                RIAC: sagg-riac (if no teacher) or sgim-d
                SGIMACTS:  sgim-acts (active version of sgim-d)
                REQUESTMIMIC: requests demo to copy movment parameters
                EMULATE: requests demo to copy the outcome, 
                SGIMIM  : sgim-d actively decides whether to imitate or
                explore auton
                TEACHERBEFORE : all demonstrations in the beginning, then
                sagg-riac
                IMITATION : observes the demo then repeats it numberIter
                times (with small variations on the mvt parameter).
                Competence with respect to demo outcome.
                OBSERVATION: observes the demo (non self experimentation)
                MIMIC :  observes the demo then repeats it numberIter
                times (with small variations on the mvt parameter).
                Competence with respect to self-determined outcome.
                RANDOMPARAM : random exploration of the parameter space
teach  : int    : id of the teacher. 0 if no teacher
fteach : int    : frequency of the demonstrations in the case of a passive
                  learner. 0 if no teacher or for an active learner 
filename:string : name of the data files under which you wish to record
                  the experiment
%}

P=path;
P=path(P,'../../SGIMalgorithm/')
path(P,'../../../../jsonlab-1.2/jsonlab/')

if (nargin==0)
  % If no arguments are passed, default function
  RunSimulation(5000,50,'RANDOMPARAM',0,0,'Random1');
  return;
end

%initialisation : set a teacher to a default value
hTeacher(1) = HumanTeacher(0, 0, 1);

%defining global variables
saggGlobalclear
variablesGlobales
DMPglobales

MODE = MODEin
%TO CHANGE: setting the simulation environment
envSimu = PoppyDraws(length(centres), joints_limits)

Simulation( numberAct, numberIter,MODEin, teach, fteach, filename)


disp(['******************************************'])
disp ('EXPERIMENT FINISHED');
disp(['******************************************'])



end
