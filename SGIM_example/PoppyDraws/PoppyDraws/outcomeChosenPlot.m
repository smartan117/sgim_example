%% Computing sliding window iterations of tasks accross time
% load SGIM

% load('../data/Sgimacts1_SGIMACTSteacherid1_teacherf0_date15-Apr-2016');
% 
% t = [1; QualityCandidate(:,end-2)];
% allAttempts = zeros(QualityCandidate(end,end-2),3);
% 
% for i=1:(length(t)-1)
%     for j=t(i):(t(i+1)-1)
%         allAttempts(j,QualityCandidate(i,end-1)) = 1;
%     end
% end
% 
% tfix = 0:10:3000;
% perc = zeros(length(tfix),3);
% col = ['r' 'g' 'b' 'c' 'm' 'y'];
% 
% for i=1:length(tfix)
%     imin = max(tfix(i) - 100, 1);
%     imax = min(tfix(i) + 100, t(end));
%     for iTask = 1:3
%         n = sum(allAttempts(imin:imax,iTask));
%         perc(i, iTask) = n;
%     end
%     nn = sum(perc(i,:));
%     if nn > 0
%         perc(i,:) = perc(i,:) / nn;
%     end 
% end
% 
% l = [];
% figure;
% hold on;
% for i=1:3
%     plot(tfix,perc(:,i),col(i));
%     l = [l; ['Outcome ' num2str(i)]];
% end
% legend(l);
% title('Outcome spaces tried');
% xlabel('t') % x-axis label
% ylabel('%') % y-axis label
% hold off;


%% Load necessary files in path

P=path;
P=path(P,'../../SGIMalgorithm/')
path(P,'../../../../jsonlab-1.2/jsonlab/')


%% Computing sliding window choices of tasks accross time
% load SGIM

%load('../data/Sgimacts1_SGIMACTSteacherid1_teacherf0_date15-Apr-2016');
load('../data/Sgimacts1_SGIMACTSteacherid1_teacherf0_date30-May-2016');


t = [0; QualityCandidate(1:(end-1),end-2)];
tfix = 0:10:3000;
perc = zeros(length(tfix),3);

col = ['r' 'g' 'b' 'c' 'm' 'y'];

for i=1:length(tfix)
    it = (t < tfix(i) + 100)&(t > tfix(i) - 100);
    for iTask=1:3
        ind = (QualityCandidate(:,end-1) == iTask);
        perc(i, iTask) = sum(ind&it);
    end
    nn = sum(perc(i,:));
    if nn > 0
        perc(i,:) = perc(i,:) / nn;
    end
end

l = [];
figure;
hold on;
for i=1:3
    plot(tfix,perc(:,i),col(i));
    l = [l; ['Outcome ' num2str(i)]];
end
legend(l);
title('Outcome spaces chosen');
xlabel('t') % x-axis label
ylabel('%') % y-axis label
hold off;


%% Computing sliding window choices of strategies accross time
% load SGIM

t = [0; QualityCandidate(1:(end-1),end-2)];
tfix = 0:10:3000;
perc = zeros(length(tfix),2);

col = ['r' 'g' 'b' 'c' 'm' 'y'];

for i=1:length(tfix)
    it = (t < tfix(i) + 100)&(t > tfix(i) - 100);
    for istrat=1:2
        ind = (QualityCandidate(:,end) == istrat);
        perc(i, istrat) = sum(ind&it);
    end
    nn = sum(perc(i,:));
    if nn > 0
        perc(i,:) = perc(i,:) / nn;
    end
end

figure;
hold on;
for i=1:2
    plot(tfix,perc(:,i),col(i));
end
legend('Autonomous exploration','Imitate teacher 1');
title('Strategies chosen');
xlabel('t') % x-axis label
ylabel('%') % y-axis label
hold off;


%% Computing percentage choice of each task/strategy

perc3 = zeros(2, 3);
for iStrat=1:2
    for iTask=1:3
        ind1 = (QualityCandidate(:,end-1) == iTask);
        ind2 = (QualityCandidate(:,end) == iStrat);
        perc3(iStrat,iTask) = 100*sum(ind1&ind2)/size(QualityCandidate,1);
    end
end

xstr = {'T1', 'T2', 'T3'}; %6
ystr = {'Autonomous Exploration', 'Imitate teacher 1'}; %3

figure;
bar3(perc3);
title('Strategy chosen by task space');
set(gca, 'XTickLabel',xstr, 'XTick',1:numel(xstr));
set(gca, 'YTickLabel',ystr, 'YTick',1:numel(ystr));


%% Computing stochasticity and teacher 1 dataset
% load Imitation 1

%{
load('../data/Imitation1_IMITATIONteacherid1_teacherf5_date11-Apr-2016')


t = hTeacher.listTeacher(:,1) + 1;
demos = hTeacher.listTeacher(:,5:end);
allDemos = hTeacher.regTaught(:,2*(mapp{1}.dimY)+2:end);

demoID = zeros(size(t));

for i=1:length(demoID)
    demo = demos(i,:);
    [valmin,indmin] = min(distSqrt(allDemos,demo));
    demoID(i) = indmin;
end

figure;
n = 0;
m = 0;
for i = 1:size(hTeacher.regTaught,1)
    ind = (demoID == i);
    demoToConsidere = epmem{1}.listLearning(t(ind),epmem{1}.mapping(1).dimYL);
    demoToConsidere = demoToConsidere(1:20,:);
    ind2 = (demoToConsidere(:,1) < 2);

    hold on;
    demo = allDemos(i,(end-1:end));

    plot(demo(1), demo(2), 'bo');
    plot(demoToConsidere(ind2,1), demoToConsidere(ind2,2), 'r+'); 
    n = n + (20 - sum(ind2));
    m = m + 20;
end

xlabel('x');
ylabel('y');
title('Dataset of teacher 1');
legend('Outcomes of the dataset','Outcomes reached');
disp([num2str(n) ' points did not even touch the tab.']);
xlim([-1.1 1.1]);
ylim([-1.1 1.1]);

 %}

% %% Computing dataset of teacher 2
% 
% load('simpleTeacher2.mat');
% 
% allDemos = regDemos(:,(end-6):(end-3));
% 
% figure;
% hold on;
% for i = 1:size(allDemos,1)
%     quiver(allDemos(i,1),allDemos(i,2),allDemos(i,3)-allDemos(i,1),allDemos(i,4)-allDemos(i,2),0);
%     %plot([allDemos(i,1) allDemos(i,3)],[allDemos(i,2) allDemos(i,4)], 'r');
% end
% xlabel('x');
% ylabel('y');
% title('Dataset of teacher 2');
% xlim([-1.1 1.1]);
% ylim([-1.1 1.1]);
% 
% 
%% Plot Evaluation sets

%{

% Task 1
figure;
subplot(1,2,1);
hold on;
for i=1:size(evalPerf.listEvalPoints{1},1)
    plot(evalPerf.listEvalPoints{1}(i,1), evalPerf.listEvalPoints{1}(i,2), 'ro');
end
xlabel('x');
ylabel('y');
title('Evaluation set of task 1');
xlim([-1.1 1.1]);
ylim([-1.1 1.1]);

% Task 3 and 6
subplot(1,2,2);
hold on;
allDemos = evalPerf.listEvalPoints{2};
for i = 1:size(allDemos,1)
    quiver(allDemos(i,1),allDemos(i,2),allDemos(i,3)-allDemos(i,1),allDemos(i,4)-allDemos(i,2),0,'r');
    %plot([allDemos(i,1) allDemos(i,3)],[allDemos(i,2) allDemos(i,4)], 'r');
end
xlabel('x');
ylabel('y');
title('Evaluation set of tasks 2 and 3');
xlim([-1.1 1.1]);
ylim([-1.1 1.1]);

%}

%% Compare algorithms

load('../data/Sgimacts1_SGIMACTSteacherid1_teacherf0_date15-Apr-2016');

load('../data/Random1_RANDOMPARAMteacherid0_teacherf0_date26-Mar-2016_update','evalPerf');
distNN{1} = evalPerf.listDistNN;
tNN{1} = evalPerf.mapEvalNN(:,end);

fig1 = figure;
fig2 = figure;

figure(fig1);
col = ['k' 'r' 'g' 'b'];
hold on;

ind = 0;
for i=1:length(epmem)
    t = [0];
    y = [5];
    indN = ind + (1:size(evalPerf.listEvalPoints{i},1));
    for j=1:size(evalPerf.mapEvalNN,1)
        yaux = evalPerf.mapEvalNN(j,indN);
        y = [y; mean(yaux)];
        t = [t; evalPerf.mapEvalNN(j,end)];
    end
    figure(fig1);
    subplot(2,2,i);
    hold on;
    plot(t, y, col(1));
    ind = ind + size(evalPerf.listEvalPoints{i},1);
end

figure(fig2);
hold on;

T1 = evalPerf.mapEvalNN(:,end);
V1 = var(evalPerf.mapEvalNN(:,1:(end-1))')';
plot(T1, V1, col(1));

disp(['Final variance of algorithm random: ' num2str(V1(end))]);


load('../data/Saggriac1_RIACteacherid0_teacherf0_date16-Apr-2016','evalPerf');
distNN{2} = evalPerf.listDistNN;
tNN{2} = evalPerf.mapEvalNN(:,end);

figure(fig1);
hold on;

ind = 0;
for i=1:length(epmem)
    t = [0];
    y = [5];
    indN = ind + (1:size(evalPerf.listEvalPoints{i},1));
    for j=1:size(evalPerf.mapEvalNN,1)
        yaux = evalPerf.mapEvalNN(j,indN);
        y = [y; mean(yaux)];
        t = [t; evalPerf.mapEvalNN(j,end)];
    end
    figure(fig1);
    subplot(2,2,i);
    hold on;
    plot(t, y, col(2));
    ind = ind + size(evalPerf.listEvalPoints{i},1);    
end

figure(fig2);
hold on;

T2 = evalPerf.mapEvalNN(:,end);
V2 = var(evalPerf.mapEvalNN(:,1:(end-1))')';
plot(T2, V2, col(2));

disp(['Final variance of algorithm Sagg-RIAC: ' num2str(V2(end))]);


load('../data/Imitation1_IMITATIONteacherid1_teacherf5_date11-Apr-2016','evalPerf');
distNN{3} = evalPerf.listDistNN;
tNN{3} = evalPerf.mapEvalNN(:,end);

figure(fig1);
hold on;

ind = 0;
for i=1:length(epmem)
    t = [0];
    y = [5];
    indN = ind + (1:size(evalPerf.listEvalPoints{i},1));
    for j=1:size(evalPerf.mapEvalNN,1)
        yaux = evalPerf.mapEvalNN(j,indN);
        y = [y; mean(yaux)];
        t = [t; evalPerf.mapEvalNN(j,end)];
    end
    figure(fig1);
    subplot(2,2,i);
    hold on;
    plot(t, y, col(3));
    ind = ind + size(evalPerf.listEvalPoints{i},1);
end

figure(fig2);
hold on;

T3 = evalPerf.mapEvalNN(:,end);
V3 = var(evalPerf.mapEvalNN(:,1:(end-1))')';
plot(T3, V3, col(3));

disp(['Final variance of algorithm imitation: ' num2str(V3(end))]);



load('../data/Sgimacts1_SGIMACTSteacherid1_teacherf0_date15-Apr-2016','evalPerf');
distNN{4} = evalPerf.listDistNN;
tNN{4} = evalPerf.mapEvalNN(:,end);

figure(fig1);
hold on;

ind = 0;
for i=1:length(epmem)
    t = [0];
    y = [5];
    indN = ind + (1:size(evalPerf.listEvalPoints{i},1));
    for j=1:size(evalPerf.mapEvalNN,1)
        yaux = evalPerf.mapEvalNN(j,indN);
        y = [y; mean(yaux)];
        t = [t; evalPerf.mapEvalNN(j,end)];
    end
    figure(fig1);
    subplot(2,2,i);
    hold on;
    plot(t, y, col(4));
    ind = ind + size(evalPerf.listEvalPoints{i},1);
    xlabel('t');
    ylabel('Mean distance between nearest neighbour and evaluation');
    title(['Evaluation of outcome ' num2str(i)]);
    legend('Random','SAGG-RIAC','Imitation','SGIM-ACTSCL');
    set(gca,'YGrid','on')
    set(gca,'yscale','log');
end

figure(fig2);
hold on;

T4 = evalPerf.mapEvalNN(:,end);
V4 = var(evalPerf.mapEvalNN(:,1:(end-1))')';
plot(T4, V4, col(4));

disp(['Final variance of algorithm SGIM-ACTS: ' num2str(V4(end))]);

xlabel('t');
ylabel('Variance error');
title('Evaluation of all algorithms');
legend('Random','SAGG-RIAC','Imitation','SGIM-ACTSCL');

set(gca,'YGrid','on')
set(gca,'yscale','log');


figure;
col = ['k' 'r' 'g' 'b'];
hold on;
for i=1:length(distNN)
    hold on;
    plot([0; tNN{i}],[5; distNN{i}], col(i));
end
plot(T1, V1, col(1));
plot(T2, V2, col(2));
plot(T3, V3, col(3));
plot(T4, V4, col(4));

xlabel('t');
ylabel('Mean error');
title('Evaluation of all algorithms');
legend('Random','SAGG-RIAC','Imitation','SGIM-ACTSCL');

set(gca,'YGrid','on')
set(gca,'yscale','log');


%% Compare reached outcomes of imitation and SGIM

load('../data/Imitation1_IMITATIONteacherid1_teacherf5_date11-Apr-2016','epmem');

y = epmem{3}.listLearning(:,end-1);

ygmin = max(min([y; 1]), -1);
ygmax = min(max([y; -1]), 1);

fig1 = figure;

subplot(3,2,1);
epmem{1}.plotReachedPoints(1);

subplot(3,2,2);
histrange = linspace(ygmin, ygmax, 40);
dy = histrange(2)- histrange(1);
histrange = [histrange(1)-2*dy,histrange(1)-dy, histrange, histrange(end)+dy, histrange(end)+2*dy];
hist(y, histrange);

xlim([ygmin ygmax])
xlabel('Reached outcomes');
ylabel('# times reached');
title('Histogram of line length drawn for Imitation');

load('../data/Sgimacts1_SGIMACTSteacherid1_teacherf0_date15-Apr-2016','epmem');

y = epmem{3}.listLearning(:,end-1);

ygmin2 = max(min([y; 1]), -1);
ygmax2 = min(max([y; -1]), 1);

figure(fig1);
subplot(3,2,3);
epmem{1}.plotReachedPoints(1);

subplot(3,2,4);
histrange = linspace(ygmin2, ygmax2, 40);
dy = histrange(2)- histrange(1);
histrange = [histrange(1)-2*dy,histrange(1)-dy, histrange, histrange(end)+dy, histrange(end)+2*dy];
hist(y, histrange);

xlim([min(ygmin,ygmin2) max(ygmax2,ygmax)]);
xlabel('Reached outcomes');
ylabel('# times reached');
title('Histogram of line length drawn for SGIM-ACTSCL');

load('../data/Saggriac1_RIACteacherid0_teacherf0_date16-Apr-2016','epmem');

y = epmem{3}.listLearning(:,end-1);

ygmin3 = max(min([y; 1]), -1);
ygmax3 = min(max([y; -1]), 1);

figure(fig1);
subplot(3,2,5);
epmem{1}.plotReachedPoints(1);

subplot(3,2,6);
histrange = linspace(ygmin3, ygmax3, 40);
dy = histrange(2)- histrange(1);
histrange = [histrange(1)-2*dy,histrange(1)-dy, histrange, histrange(end)+dy, histrange(end)+2*dy];
hist(y, histrange);

xlim([min([ygmin ygmin2 ygmin3]) max([ygmax3 ygmax2 ygmax])]);
xlabel('Reached outcomes');
ylabel('# times reached');
title('Histogram of line length drawn for SAGG-RIAC');

subplot(3,2,2);
xlim([min([ygmin ygmin2 ygmin3]) max([ygmax3 ygmax2 ygmax])]);

subplot(3,2,4);
xlim([min([ygmin ygmin2 ygmin3]) max([ygmax3 ygmax2 ygmax])]);


