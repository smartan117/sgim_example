str = fileread('/home/nduminy/Programmation/Poppy/Moves/torso/haha.json');
dmp = json2matlab(str);

global joints_list;

context = zeros(1, 6);
params = zeros(1, 37);

params(1) = dmp.tau;

for i=1:length(joints_list)
    params((i-1)*6+2) = dmp.transformation_system.(joints_list{i}).goal;
    params([1 2 3 4 5] + 2 + (i-1)*6) = dmp.transformation_system.(joints_list{i}).forcing_term.weights;
    context(i) = dmp.transformation_system.(joints_list{i}).start;
end

outcomes = executePolicyRemotely(context, params);
disp(outcomes);