params = regDemos(:,2:(end-7));

id = [3:7 9:13 15:19 21:25 27:31 33:37];

params = params(:,id);

min_params = zeros(1,30);
max_params = zeros(1,30);

for i=1:30
    min_params(i) = min(params(:,i));
    max_params(i) = max(params(:,i));
end

min_params = min_params * 5000;
max_params = max_params * 5000;

abs_params = max(abs(min_params),abs(max_params));

plot(abs_params,'o');