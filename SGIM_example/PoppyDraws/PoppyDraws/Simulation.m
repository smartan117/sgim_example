function Simulation( numberAct, numberIter,MODE, teach, fteach, filename)
%
%% example of command:
%% Simulation(5000,50,'RANDOMPARAM',0,0,'Random1')
%{
Copyright (c) 2013 Sao Mai Nguyen
              e-mail : nguyensmai@gmail.com
              http://nguyensmai.free.fr/

Permission is granted to copy, distribute, and/or modify this program
under the terms of the GNU General Public License, version 2 or any
later version published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details

If you use this code in the context of a publication, I would appreciate
it if you could cite it as follows:


@phdthesis{Nguyen2013,
	Author = {Nguyen, Sao Mai},
	School = {INRIA},
	Title = {A Curious Robot Learner for Interactive Goal-Babbling: Strategically Choosing What, How, When and from Whom to Learn},
	Year = {2013}}


Simulation is one of the main program to launch an experiment.
It is called directly or by RunSimulation (which defines global variables and paths that need to be defined only once)
It resets all variables and calls fonction_principale to launch the real algorithm (in SGIMalgorithm folder)

Simulation and RunSimulation usually have the same arguments

numberAct : int : total number of movements/trials for the whole experiment
numberIter: int : number of movemnts/trials for each goal outcome
MODEin : string : name of the algorithm to experiment with
                RIAC: sagg-riac (if no teacher) or sgim-d
                SGIMACTS:  sgim-acts (active version of sgim-d)
                REQUESTMIMIC: requests demo to copy movment parameters
                EMULATE: requests demo to copy the outcome,
                SGIMIM  : sgim-d actively decides whether to imitate or
                explore auton
                TEACHERBEFORE : all demonstrations in the beginning, then
                sagg-riac
                IMITATION : observes the demo then repeats it numberIter
                times (with small variations on the mvt parameter).
                Competence with respect to demo outcome.
                OBSERVATION: observes the demo (non self experimentation)
                MIMIC :  observes the demo then repeats it numberIter
                times (with small variations on the mvt parameter).
                Competence with respect to self-determined outcome.
                RANDOMPARAM : random exploration of the parameter space
teach  : int    : id of the teacher. 0 if no teacher
fteach : int    : frequency of the demonstrations in the case of a passive
                  learner. 0 if no teacher or for an active learner
filename:string : name of the data files under which you wish to record
                  the experiment
%}

saggGlobal
variablesGlobales
World_Initialize



if isempty(filename)
    fileMemo = ['../data/MemoTeach_',MODE,'_teacherid',num2str(teach),'_teacherf',num2str(fteach),'_date',date];
else
    fileMemo = ['../data/',filename,'_',MODE,'teacherid',num2str(teach),'_teacherf',num2str(fteach),'_date',date];
end


%--------------------------------------------------------------------------
%------------------------  define global variables   ----------------------
%--------------------------------------------------------------------------

nbOutcomeTypes = envSimu.nbOutcomeTypes  %number of types of tasks/outcomes
DimensionIn = [];                         % TO CHANCE : dimension of the policy space
DimensionOut  = envSimu.dimOutcomes;      % TO CHANGE : dimension of the outcome space
DimensionCont = 1;                        % TO CHANGE : dimension of the context space
numberAction  = numberAct;                % number of policies experimented in total
% numberIter = numberIter;     % number of policies tried for each goal (for goal-directed exploration)


%policy: first pair is x, y, second is vx, vy
%outcome: first is x, second is y

epmem = {};
mapp = {};
for iTache = 1:nbOutcomeTypes
    DimensionIn{iTache} = envSimu.dimPolicies;
    Ymax{iTache} = ones(1, DimensionOut{iTache});
    Ymin{iTache} = -ones(1, DimensionOut{iTache});
    distYMax{iTache} = 5*distSqrt(Ymin{iTache},Ymax{iTache});
    
    mapp{iTache} = Mapping(DimensionIn{iTache},DimensionOut{iTache}, DimensionCont, Ymin{iTache}, Ymax{iTache}, distYMax{iTache});
    maxDimY = max([maxDimY, DimensionOut{iTache}]);
    errY = 0.1^(1/length(DimensionOut{iTache}));
    distY = 0.01^(1/length(DimensionOut{iTache}));
    epmem{iTache} = EpisodicMemory(mapp{iTache}, 0.2, distY, 0.05, errY); %parameters for local interpolation: (,distA, distY, gauss, errY)
    %epmem{iTache} = EpisodicMemory(mapp{iTache}, 0.2, 0.05, 0.05, 0.1); %parameters for local interpolation: distA, distY, gauss
end

%TO DO implement evaluation in envSimu
evalPerf = Evaluation(envSimu);

%--------------------------------------------------------------------------
%------------------------  define different teachers ----------------------
%--------------------------------------------------------------------------
curLowLevel = LowLevel(numberIter);

%TO DO think about the teacher of this dummy experiment
switch teach
    case 0
        hTeacher(1) = HumanTeacher(0, 0,1);
        %        hTeacher(1) = HumanTeacher(0, 0, 'teacherRiacAirHockey1.mat');
        curLowLevel.flagTeach = 0;
    case 1
        hTeacher(1) = HumanTeacher(1, fteach, 1,'simpleTeacher1.mat');
        curLowLevel.flagTeach = 0;
    case 2
        hTeacher(1) = HumanTeacher(1, fteach, 6,'simpleTeacher2.mat');
        curLowLevel.flagTeach = 0;
    case 3
        hTeacher(1) = HumanTeacher(1, fteach, 1,'simpleTeacher3.mat');
        curLowLevel.flagTeach = 0;
    case 4
        hTeacher(1) = HumanTeacher(1, fteach, 1,'simpleTeacher4.mat');
        curLowLevel.flagTeach = 0;
    case 12
        hTeacher(1) = HumanTeacher(1, fteach, 1,'simpleTeacher1.mat');
        hTeacher(2) = HumanTeacher(1, fteach, 6,'simpleTeacher2.mat');
        curLowLevel.flagTeach = 0;
    case 23
        hTeacher(1) = HumanTeacher(1, fteach, 1,'simpleTeacher2.mat');
        hTeacher(2) = HumanTeacher(1, fteach, 1,'simpleTeacher3.mat');
        curLowLevel.flagTeach = 0;
    case 24
        hTeacher(1) = HumanTeacher(1, fteach, 1,'simpleTeacher2.mat');
        hTeacher(2) = HumanTeacher(1, fteach, 1,'simpleTeacher4.mat');
        curLowLevel.flagTeach = 0;
    case 34
        hTeacher(1) = HumanTeacher(1, fteach, 1,'simpleTeacher3.mat');
        hTeacher(2) = HumanTeacher(1, fteach, 1,'simpleTeacher4.mat');
        curLowLevel.flagTeach = 0;
    otherwise
        disp('Unknown teacher');
end





%--------------------------------------------------------------------------
%  variables for the higher level exploration (outcome space and strategy)
%--------------------------------------------------------------------------



SearchNear    = 0;
nbTr          = 1;
nbNN          = 10;                                                     % Nearest Neighbors
%     SearchNearTab = [];
NumReg        = 1;                                                      % ------Parameters
%predictedWorld = [];
meanErrors     = [];
pos_before     = [0 0 0 0 0 0];
cptHistMem     = 1;
histMem        = [];
Predictor      = [];
watchAround    = 0.1;
niter          = 1;

% if strcmp(MODE,'SGIMSTRATWHO')
%     ProbMode1 = 0.7; %0.5; %0.7;
%     ProbMode3 = 0.25; %0.3; %0.2;
%     nbStrategies = 3;
%     costStrat    = [1 0.1 0.1];
%     sample         = [10 5 5];
% else
if  strcmp(MODE,'SGIMACTS')
    nbStrategies   = length(hTeacher)+1;   %3    %number of strategies : autonomous exploration + mimicry&emulation for each teacher
    costStrat      = 10*ones(1,nbStrategies);  %2 before
    costStrat(1)   = 1; %preference for self-exploration
    sample         = [10 10 10 ];
    ProbMode1 = 0.55; %0.5; %0.7;
    ProbMode3 = 0.3; %0.3; %0.2;
    % elseif  strcmp(MODE,'EMULATE')
    %     curLowLevel.nbImitate = fteach;
    %     nbStrategies   = 1;
    %     costStrat      = [1];
    %     sample         = [4];
    % elseif  strcmp(MODE,'REQUESTMIMIC')
    %     curLowLevel.nbImitate = fteach;
    %     nbStrategies   = 1;
    %     costStrat      = [1];
    %     sample         = [4];
    % elseif  strcmp(MODE,'RIAC')
    %      ProbMode1 = 0.7; %0.5; %0.7;
    %      ProbMode3 = 0.25; %0.3; %0.2;
    %     nbStrategies = 1;
    %     sample       = 10*ones(1,nbStrategies);
    %     costStrat    = ones(1,nbStrategies);
else
    nbStrategies = 1;
    sample       = 40*ones(1,nbStrategies);
    costStrat    = ones(1,nbStrategies);
    ProbMode1 = 0.65; %0.5; %0.7;
    ProbMode3 = 0.25; %0.3; %0.2;
end
CriterRegions = sample;

partReg = PartitionRegions(CriterRegions, sample, NumReg, mapp, nbStrategies, nbOutcomeTypes, watchAround)

%% Load previous data

%load('../data/Imitation1_IMITATIONteacherid1_teacherf5_date06-Apr-2016_nb_1751')
%load('../data/Saggriac1_RIACteacherid0_teacherf0_date27-Mar-2016_nb_2001','evalPerf')

%load('../data/Imitation1_IMITATIONteacherid1_teacherf5_date11-Apr-2016_nb_1001');
%evalPerf = evaluationPerfOnce(evalPerf, epmem, curLowLevel, envSimu);
%visualiseExploration;

%for iTache = 1:nbOutcomeTypes
%    epmem{iTache}.distY = 0.1;
%    epmem{iTache}.gauss = 0.05;
%    epmem{iTache}.errY = 0.15;
%end
%curLowLevel.numberIteration = numberIter;


%% Simulation

RandStream.setGlobalStream(RandStream('mt19937ar','seed',sum(100*clock)));
pause(1);

% load('../data/imitation_base.mat');
% 
% goal = [0.2; 0.5];   %[-0.6 0] [-0.2 0.4];
% curLowLevel.numberIteration = 500;
% fileMemo = '../data/explo_locale_2';
% 
% test_explo_local(1, goal);

% curLowLevel.nbImitate = 5;
% 
% for i=1:size(hTeacher(1).regTaught, 1)
%     disp(['Demonstration number: ' num2str(i) '/' num2str(size(hTeacher(1).regTaught, 1))]);
%     test_imitation(1, i);
% end

load('../data/Sgimacts1_SGIMACTSteacherid1_teacherf0_date30-May-2016_nb_2801.mat');


disp(['******************************************'])
disp(['Experiment of ' num2str(numberAction) ' Iterations'])
disp(['******************************************'])

fonction_principale(MODE);


disp(['******************************************'])
disp(['End of Experiment of ' num2str(numberAction) ' Iterations'])
disp(['******************************************'])


end
