saggGlobal
variablesGlobales
global costStrat


evalPerf = plotListDist(evalPerf);
%evalPerf = plotMapEval(evalPerf);
for i=1:length(epmem)
    epmem{i} = plotListReached(epmem{i},i);
end

figure;
epmem{1}.plotReachedPoints(1);

%============================   RIAC      =================================
% explores autonomously, goal-oriented exploration
%==========================================================================
if strcmp(MODE,'RIAC')
    
    for i=1:length(epmem)
        plotListGoal(epmem{i},i);;
    end
    
elseif strcmp(MODE,'SGIMD')
    %========================== SGIM-D   ======================================
    % explores autonomously, until when the teacher
    % gives a demonstration, when it imitates
    %==========================================================================
    plotListGoal(epmem{1});
    
    
    
    
elseif strcmp(MODE,'SGIMACTS')
%========================== SGIM-ACTS  ====================================
% decides which strategy to adopt, using a regions mapping
%==========================================================================

    for i=1:length(epmem)
        plotListGoal(epmem{i},i);;
    end    
    plotMode();
 
            
elseif strcmp(MODE,'REQUESTMIMIC')
%========================== SGIM-ACTS  ====================================
% request a demonstration and mimic the action
%==========================================================================

    
elseif strcmp(MODE,'EMULATE')
%========================== SGIM-ACTS  ====================================
% emulate the observed outcome
%==========================================================================

          
elseif strcmp(MODE,'SGIMIM')
%========================== SGIM-IM  ====================================
% Interactive Learning to monitor which makes most progress
%==========================================================================
  fp_plotEndSGIMIM();


elseif strcmp(MODE,'TEACHERBEFORE')
    %========================= TEACHERBEFORE ==================================
    %  RIAC  but all the teachings are given before
    %==========================================================================
    figure('Name',fileMemo);
    plot3(QualityCandidate(:,1), QualityCandidate(:,2), QualityCandidate(:,3),'.' )
    grid on
    xlabel('x')
    ylabel('y')
    disp('* END TEACHERBEFORE MODE');
    
elseif strcmp(MODE,'IMITATION')
    %======================== IMITATION ONLY ==================================
%  Learn by imitation of the policies
%==========================================================================
 
    
elseif strcmp(MODE,'OBSERVATION')
%==============================   OBSERVATION      ========================
%=========================================================================%
  
elseif strcmp(MODE,'MIMIC')
%=======================   MIMIC THE POLICY    ============================
%==========================================================================

   
elseif strcmp(MODE,'RANDOMPARAM')
%============   RANDOM SEARCH IN THE POLICY PARAMETER SPACE     ===========
%==========================================================================

else
    disp('/\ BAD CHOICE IN FONCTION PRINCIPALE')
end
