function stop_servers()
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

context = org.zeromq.ZMQ.context(1);
socket = context.socket(org.zeromq.ZMQ.REQ);
socket.connect('tcp://localhost:5555');

cmd = {};
cmd.command = 'Exit';

req = java.lang.String(matlab2json('',cmd));

exited = 0;

while ~exited
    socket.send(req.getBytes(),0);
    result = socket.recv(0);
    result = json2matlab(char(result)');
    if result.command == 'Exiting'
        exited = 1;
    end
end

socket.close();
context.term();

end

