clear all;
close all;
load('../data/Random1_RANDOMPARAMteacherid0_teacherf0_date26-Mar-2016.mat');

old_mem = epmem;

saggGlobal;
variablesGlobales;
DMPglobales;
envSimu = PoppyDraws(length(centres), joints_limits);

nbOutcomeTypes = envSimu.nbOutcomeTypes;  %number of types of tasks/outcomes
DimensionIn = [];                         % TO CHANCE : dimension of the policy space
DimensionOut  = envSimu.dimOutcomes;      % TO CHANGE : dimension of the outcome space
DimensionCont = 1;                        % TO CHANGE : dimension of the context space

epmem = {};
mapp = {};
for iTache = 1:nbOutcomeTypes
    DimensionIn{iTache} = envSimu.dimPolicies;
    Ymax{iTache} = ones(1, DimensionOut{iTache});
    Ymin{iTache} = -ones(1, DimensionOut{iTache});
    distYMax{iTache} = 5*distSqrt(Ymin{iTache},Ymax{iTache});
    
    mapp{iTache} = Mapping(DimensionIn{iTache},DimensionOut{iTache}, DimensionCont, Ymin{iTache}, Ymax{iTache}, distYMax{iTache});
    maxDimY = max([maxDimY, DimensionOut{iTache}]);
    errY = 0.1^(1/length(DimensionOut{iTache}));
    distY = 0.01^(1/length(DimensionOut{iTache}));
    epmem{iTache} = EpisodicMemory(mapp{iTache}, 0.2, distY, 0.05, errY); %parameters for local interpolation: (,distA, distY, gauss, errY)
    %epmem{iTache} = EpisodicMemory(mapp{iTache}, 0.2, 0.05, 0.05, 0.1); %parameters for local interpolation: distA, distY, gauss
end

%TO DO implement evaluation in envSimu
evalPerf = Evaluation(envSimu);

epmem{1}.listLearning = old_mem{1}.listLearning;
epmem{1}.listContext = old_mem{1}.listContext;
epmem{2}.listLearning = old_mem{3}.listLearning;
epmem{2}.listContext = old_mem{3}.listContext;
epmem{3}.listLearning = old_mem{6}.listLearning;
epmem{3}.listContext = old_mem{6}.listContext;

save('../data/Random1_RANDOMPARAMteacherid0_teacherf0_date26-Mar-2016_update.mat');

for t=[1 10 50 100 500 750 1000 1500 2000 2500 3000]
    load('../data/Random1_RANDOMPARAMteacherid0_teacherf0_date26-Mar-2016_update.mat','epmem');
    epmem{1}.listLearning = epmem{1}.listLearning(1:t,:);
    epmem{2}.listLearning = epmem{2}.listLearning(1:t,:);
    epmem{3}.listLearning = epmem{3}.listLearning(1:t,:);
    evalPerf = evaluationPerfOnce(evalPerf, epmem, curLowLevel, envSimu, t);
end
