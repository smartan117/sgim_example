classdef PoppyDraws
    %change classdef name for your example
    
    % this class defines your environment, and how policies cause outcomes
    %
    % Copyright (c) 2013 Sao Mai Nguyen
    %               e-mail : nguyensmai@gmail.com
    %               INRIA Bordeaux - Sud-Ouest
    %               351 Cours de la Liberation, 33400, Talence,
    %               France,
    %               http://nguyensmai.free.fr/
    %
    
    
    %--------------------------------------------------------------------------
    %-------------------------  properties     --------------------------------
    %--------------------------------------------------------------------------
    
    properties
        dimPolicies;
        dimOutcomes;
        nbOutcomeTypes;   %total number of task types
        paramsRange;  %range of each real parameter
        paramsMean;  %real value corresponding to a parameter equal to 0
        outcomesRange;  %range of each real outcome
        outcomesMean;  %real value corresponding to an outcome equal to 0
        plotenv ;    %boolean. true if you want to display the environment with drawEnvironment, else false
        context;
        mappOutTasks;
        dimAllOutcomes;
    end %end properties
    
    methods
        
        %--------------------------------------------------------------------------
        %--------------------------  constructor     ------------------------------
        %--------------------------------------------------------------------------
 
        function obj = PoppyDraws(nbfs, joints_limits)
            % constructor
            range_angles = joints_limits(:,2) - joints_limits(:,1);
            mean_angles = joints_limits(:,1) + range_angles/2;
            njoints = size(joints_limits,1);
            obj.dimPolicies = 1 + njoints*(nbfs + 1);
            obj.dimAllOutcomes = 7;
            obj.mappOutTasks = {};
            obj.mappOutTasks{1} = [1 2];
            %obj.mappOutTasks{2} = [3 4];
            obj.mappOutTasks{2} = 1:4;
            %obj.mappOutTasks{4} = [7];
            %obj.mappOutTasks{5} = [ 1 2 7];
            obj.mappOutTasks{3} = [1:4 7];
            %obj.dimOutcomes = {2 2 4 1 3 5};
            obj.dimOutcomes = {2 4 5};
            obj.nbOutcomeTypes = 3; %6;  %7
            obj.plotenv = 0;
            w = 2:(nbfs+1);
            j = 0:(njoints-1);
            obj.paramsRange = ones(1, obj.dimPolicies);
            obj.paramsMean = zeros(1, obj.dimPolicies);
            obj.paramsMean(1) = 5.5;
            obj.paramsMean(2 + j*(nbfs+1)) = mean_angles;
            obj.paramsRange(1) = 9;
            weights = w'*ones(1,njoints) + (nbfs+1)*ones(nbfs,1)*j + 1;
            ps = [2000 2000 2000 2000 10000];
            ps2 = [ps ps ps ps ps ps];
            obj.paramsRange(weights(:)) = ps2;  %10000
            obj.paramsRange(2 + j*(nbfs+1)) = range_angles;
            obj.outcomesRange = [1536 2048 1536 2048 5000 5000 6000];
            obj.outcomesMean = [768 1024 768 1024 768 1024 3000];
        end %end of constructor
        
        %--------------------------------------------------------------------------
        %--------------------------  methods     ------------------------------
        %--------------------------------------------------------------------------
         
        
        function obj = drawEnvironment(obj)
            % displays the environment setup environment
            if(obj.plotenv)
                ;
            else
                ;
            end
        end  %end drawEnvironment
        
        %TO DO Change to adapt the evaluation set
        function eval = evalPoints(obj, eval)
            % sets a set of benchmark points for the evaluation
            % eval : Evaluation object,where we edit its listEvalPointsD, listEvalPoints, listEvalWeight.
            %        each outcome type iOutcome \in [1 nbTaches], in this function we define:
            %        - nbTaches :  number of outcome types
            %        - listEvalPoints{iOutcome} a list of outcomes to be produced
            %        - listEvalWeight{iOutcome} a list of weights for each of the outcomes listEvalPoints{iOutcome}{i}
            %        - listEvalPointsD{iOutcome} is a cell of size 1x(dimesion of task subspace), listEvalPointsD{iOutcome}{iDim} is a vector of the (unique) values of listEvalPoints{iOutcome}(iDim)
            % cf file ../SGIMPlusieursTaches/Evaluation.m for details on the class Evaluation
            
            eval.nbOutcomeTypes       = 3;
            
            eval.listEvalPointsD{1}{1} = -1:0.1:1; % 1xn vector;
            eval.listEvalPointsD{1}{2} = -1:0.1:1; % 1xn vector;
            [X1,X2] = ndgrid(eval.listEvalPointsD{1}{1}, eval.listEvalPointsD{1}{2});
            eval.listEvalPoints{1} = [X1(:) X2(:)];
            eval.listEvalWeight{1} = ones(size(eval.listEvalPoints{1},1)); %1xn vector
            
%             eval.listEvalPointsD{1}{1} = [-1 -0.33 0.33 1]; % 1xn vector;
%             eval.listEvalPointsD{1}{2} = [-1 -0.5 0 0.5 1]; % 1xn vector;
%             [X1,X2] = ndgrid(eval.listEvalPointsD{1}{1}, eval.listEvalPointsD{1}{2});
%             eval.listEvalPoints{1} = [X1(:) X2(:)];
%             eval.listEvalWeight{1} = ones(size(eval.listEvalPoints{1},1)); %1xn vector
            
%             eval.listEvalPointsD{2}{1} = eval.listEvalPointsD{1}{1};
%             eval.listEvalPointsD{2}{2} = eval.listEvalPointsD{1}{2};
%             eval.listEvalPoints{2} = eval.listEvalPoints{1};
%             eval.listEvalWeight{2} = eval.listEvalWeight{1};

            eval.listEvalPointsD{2}{1} = -1:0.5:1;
            eval.listEvalPointsD{2}{2} = -1:0.5:1;
            eval.listEvalPointsD{2}{3} = -1:0.5:1;
            eval.listEvalPointsD{2}{4} = -1:0.5:1;
            [X1,X2,X3,X4] = ndgrid(eval.listEvalPointsD{2}{1}, eval.listEvalPointsD{2}{2}, ...
                eval.listEvalPointsD{2}{3}, eval.listEvalPointsD{2}{4});
            eval.listEvalPoints{2} = [X1(:) X2(:) X3(:) X4(:)];
            eval.listEvalWeight{2} = ones(size(eval.listEvalPoints{2},1)); %1xn vector
            
%             eval.listEvalPointsD{2}{1} = [-0.95 0 0.95];
%             eval.listEvalPointsD{2}{2} = [-0.95 -0.5 0 0.5 0.95];
%             eval.listEvalPointsD{2}{3} = [-0.95 0 0.95];
%             eval.listEvalPointsD{2}{4} = [-0.95 -0.5 0 0.5 0.95];
%             eval.listEvalPoints{2} = [[-0.95 -0.95 -0.95 0]; [-0.95 -0.95 0 -0.5]; ...
%                 [-0.95 -0.95 0.95 -0.95]; [0.95 -0.95 -0.95 -0.95]; [0.95 -0.95 0 -0.5]; ...
%                 [0.95 -0.95 0.95 0]; [0 -0.5 -0.95 -0.95]; [0 -0.5 0.95 -0.95]; ...
%                 [0 -0.5 0 0.5]; [-0.95 0 -0.95 -0.95]; [-0.95 0 0.95 0]; ...
%                 [-0.95 0 -0.95 0.95]; [0.95 0 0.95 -0.95]; [0.95 0 -0.95 0]; ...
%                 [0.95 0 0.95 0.95]; [0 0.5 0 -0.5]; [0 0.5 -0.95 0.95]; ...
%                 [0 0.5 0.95 0.95]; [-0.95 0.95 -0.95 0]; [-0.95 0.95 0 0.5]; ...
%                 [-0.95 0.95 0.95 0.95]; [0.95 0.95 0.95 0]; [0.95 0.95 0 0.5]; ...
%                 [0.95 0.95 -0.95 0.95]];
%             eval.listEvalWeight{2} = ones(size(eval.listEvalPoints{2},1)); %1xn vector            
            
%             eval.listEvalPointsD{4}{1} = [-1:0.1:0];
%             eval.listEvalPoints{4} = eval.listEvalPointsD{4}{1}(:);
%             eval.listEvalWeight{4} = ones(size(eval.listEvalPoints{4},1)); %1xn vector
%             
%             eval.listEvalPointsD{5}{1} = [-0.95 0 0.95];
%             eval.listEvalPointsD{5}{2} = [-0.95 -0.5 0 0.5 0.95];
%             eval.listEvalPointsD{5}{3} = [-1 -0.8];
%             eval.listEvalPoints{5} = [[-0.95 -0.95 -1]; [-0.95 -0.95 -0.8]; ...
%                 [0.95 -0.95 -1]; [0.95 -0.95 -0.8]; [0 -0.5 -1]; [0 -0.5 -0.8]; ...
%                 [-0.95 0 -1]; [-0.95 0 -0.8]; [0.95 0 -1]; [0.95 0 -0.8]; ...
%                 [0 0.5 -1]; [0 0.5 -0.8]; [-0.95 0.95 -1]; [-0.95 0.95 -0.8]; ...
%                 [0.95 0.95 -1]; [0.95 0.95 -0.8]];
%             eval.listEvalWeight{5} = ones(size(eval.listEvalPoints{5},1)); %1xn vector
            
            eval.listEvalPointsD{3}{1} = eval.listEvalPointsD{2}{1};
            eval.listEvalPointsD{3}{2} = eval.listEvalPointsD{2}{2};
            eval.listEvalPointsD{3}{3} = eval.listEvalPointsD{2}{3};
            eval.listEvalPointsD{3}{4} = eval.listEvalPointsD{2}{4};
            eval.listEvalPoints{3} = eval.listEvalPoints{2};
            dist = [];
            for i=1:length(eval.listEvalPoints{3})
                p1 = eval.listEvalPoints{3}(i,1:2);
                p2 = eval.listEvalPoints{3}(i,3:4);
                P1 = [768 1024] .* p1;
                P2 = [768 1024] .* p2;
                l = distSqrt(P1, P2);
                l = (l - 3000) / 3000;
                dist = [dist; l];
            end
            eval.listEvalPointsD{3}{5} = sort(unique(dist)');
            eval.listEvalPoints{3} = [eval.listEvalPoints{3} dist];
            eval.listEvalWeight{3} = ones(size(eval.listEvalPoints{3},1)); %1xn vector

%             eval.listEvalPointsD{3}{1} = [-0.95 0 0.95];
%             eval.listEvalPointsD{3}{2} = [-0.95 -0.5 0 0.5 0.95];
%             eval.listEvalPointsD{3}{3} = [-0.95 0 0.95];
%             eval.listEvalPointsD{3}{4} = [-0.95 -0.5 0 0.5 0.95];
%             eval.listEvalPoints{3} = [];
%             for i=1:length(eval.listEvalPoints{2})
%                 p1 = eval.listEvalPoints{2}(i,1:2);
%                 p2 = eval.listEvalPoints{2}(i,3:4);
%                 P1 = [768 1024] .* p1;
%                 P2 = [768 1024] .* p2;
%                 l = distSqrt(P1, P2);
%                 l = (l - 3000) / 3000;
%                 eval.listEvalPoints{3} = [eval.listEvalPoints{3}; [p1 p2 l]];
%             end
%             eval.listEvalPointsD{3}{5} = sort(unique(eval.listEvalPoints{3}(:,5))');
%             eval.listEvalWeight{3} = ones(size(eval.listEvalPoints{3},1)); %1xn vector
            
%             for iOutcome = 1:eval.nbOutcomeTypes
%                 for iDim =1:obj.dimOutcomes{iOutcome} %for each dimension of the type of task iOutcome
%                     eval.listEvalPointsD{iOutcome}{iDim} = -1:0.1:1; % 1xn vector;
%                 end
%                 if obj.dimOutcomes{iOutcome} == 1
%                     X1 = ndgrid(-1:0.1:1);
%                     eval.listEvalPoints{iOutcome} = X1(:);
%                 elseif obj.dimOutcomes{iOutcome} == 2
%                     [X1,X2] = ndgrid(-1:0.1:1);
%                     eval.listEvalPoints{iOutcome} = [X1(:) X2(:)];
%                 elseif obj.dimOutcomes{iOutcome} == 3
%                     [X1,X2,X3] = ndgrid(-1:0.1:1);
%                     eval.listEvalPoints{iOutcome} = [X1(:) X2(:) X3(:)];
%                 elseif obj.dimOutcomes{iOutcome} == 4
%                     [X1,X2,X3,X4] = ndgrid(-1:0.1:1);
%                     eval.listEvalPoints{iOutcome} = [X1(:) X2(:) X3(:) X4(:)];
%                 else
%                     [X1,X2,X3,X4,X5] = ndgrid(-1:0.1:1);
%                     eval.listEvalPoints{iOutcome} = [X1(:) X2(:) X3(:) X4(:) X5(:)];
%                 end
%                 
%                 eval.listEvalPoints{iOutcome} = [X(:) Y(:)]; %nxdim Outcome vector
%                 eval.listEvalWeight{iOutcome} = ones(size(eval.listEvalPoints{iOutcome})); %1xn vector
%                 
%             end
        end
        
        
        
        %--------------------------------------------------------------------------
        %----- generating contexts and policies and checking their format   -------
        %--------------------------------------------------------------------------
        
        
        function obj = randomizeContext(obj)
            % generate random contexts
            obj.context = 30;
        end % end randomizeContext
        
        function param  = randParameters(obj)
            % generate random policy parameters
            param = 2*(rand(1,obj.dimPolicies)-0.5);
        end %end randomizeContext
        
        
        function param = normaliseParam(obj, param)
            % check that policy parameters are allowed
            % (within the right range, are in the proper format)
            
            param = min(max(param,-1),1);
        end %end normaliseParam
        
        function context = getContext(obj)
            context =  30;
        end %end getContext
        
        function obj = setContext(obj, context)
            obj.context = 30;
        end
        
        
        %--------------------------------------------------------------------------
        %-------- model of the environment: outcome reached by an outcome ---------
        %--------------------------------------------------------------------------
        
        %TO DO change this method
        function reached = executePolicy(obj,movParam, outcomeType)
            % simulate the environment change with a policy
            % movParam : policy parameter
            % iOutcome : type of outcome to read
            % reached : the induced/reached outcome
            DMPglobales;
            
            param = normaliseParam(obj,movParam);
            real_param = param .* (obj.paramsRange/2) + obj.paramsMean;
            
            %TO UNCOMMENT WHEN PERFORMING REAL TESTS
            outcomes = executePolicyRemotely(zeros(1,length(joints_list)),real_param);
            %outcomes = {};
            
            %Add default values if outcome spaces not reached
            if ~isfield(outcomes,'Mstart')
                outcomes.Mstart = {};
                outcomes.Mstart.x = 1000000; %Inf bugs later
                outcomes.Mstart.y = 1000000;
            end                
            if ~isfield(outcomes,'Mend')
                outcomes.Mend = {};
                outcomes.Mend.x = 1000000;
                outcomes.Mend.y = 1000000;
            end
            if ~isfield(outcomes,'length')
                outcomes.length = -1000000;
            end
            if ~isfield(outcomes,'Object')
               outcomes.Object = {};
               outcomes.Object.x = 0;
               outcomes.Object.y = 0;
            end
            
            reached = [outcomes.Mstart.x outcomes.Mstart.y outcomes.Mend.x ...
                outcomes.Mend.y outcomes.Object.x outcomes.Object.y ...
                outcomes.length];
            
            reached = 2 * (reached - obj.outcomesMean) ./ obj.outcomesRange;
            
        end  %end execute
        
    end %end methods
    
    methods(Static=true)
        
        function createTeacherDataset(envSimu)
            global joints_list;
            regTaught = [];
            regDemos = [];
            directory0 = '/home/nduminy/Programmation/Poppy/Moves/teacher0';
            files = dir(directory0);
            for i=1:length(files)
                if files(i).name(1) ~= '.'
                    str = fileread([directory0 '/' files(i).name]);
                    data = json2matlab(str);
                    newReg = zeros(1, (envSimu.dimPolicies + envSimu.dimOutcomes{1}*3 + 1));
                    reached = [data.outcomeDemo.Mstart.x data.outcomeDemo.Mstart.y ...
                        data.outcomeDemo.Mend.x data.outcomeDemo.Mend.y ...
                        data.outcomeDemo.Object.x data.outcomeDemo.Object.y ...
                        data.outcomeDemo.length];
                    outcomes = 2 * (reached - envSimu.outcomesMean) ./ envSimu.outcomesRange;
                    reached = outcomes(1:2);
                    for j=0:(length(reached)-1)
                        newReg(2*j+1) = reached(j+1) - 0.025;
                        newReg(2*j+2) = reached(j+1) + 0.025;
                    end
                    newReg((length(newReg)-length(reached) + 1):length(newReg)) = reached;
                    params = zeros(1, envSimu.dimPolicies);
                    params(1) = data.dmp.tau;
                    for j=1:length(joints_list)
                        params((j-1)*(envSimu.dimPolicies-1)/6 + 2) = ...
                            data.dmp.transformation_system.(joints_list{j}).goal;
                        t = (j-1)*(envSimu.dimPolicies-1)/6 + 3;
                        params(t:(t-2+((envSimu.dimPolicies-1)/6))) = ...
                            data.dmp.transformation_system.(joints_list{j}).forcing_term.weights;
                    end
                    params = 2 * (params - envSimu.paramsMean) ./ envSimu.paramsRange;
                    newReg((length(newReg)-length(reached) - length(params)+1):(length(newReg)-length(reached))) = ...
                        params;
                    regTaught = [regTaught; newReg];
                    regDemos = [regDemos; 0 params outcomes];
                end
            end
            save('simpleTeacher1','regTaught','regDemos');
            regTaught = [];
            regDemos = [];
            directory0 = '/home/nduminy/Programmation/Poppy/Moves/teacher1';
            files = dir(directory0);
            for i=1:length(files)
                if files(i).name(1) ~= '.'
                    str = fileread([directory0 '/' files(i).name]);
                    data = json2matlab(str);
                    newReg = zeros(1, (envSimu.dimPolicies + envSimu.dimOutcomes{3}*3 + 1));
                    reached = [data.outcomeDemo.Mstart.x data.outcomeDemo.Mstart.y ...
                        data.outcomeDemo.Mend.x data.outcomeDemo.Mend.y ...
                        data.outcomeDemo.Object.x data.outcomeDemo.Object.y ...
                        data.outcomeDemo.length];
                    outcomes = 2 * (reached - envSimu.outcomesMean) ./ envSimu.outcomesRange;
                    reached = outcomes([1 2 3 4 7]);
                    for j=0:(length(reached)-1)
                        newReg(2*j+1) = reached(j+1) - 0.025;
                        newReg(2*j+2) = reached(j+1) + 0.025;
                    end
                    newReg((length(newReg)-length(reached) + 1):length(newReg)) = reached;
                    params = zeros(1, envSimu.dimPolicies);
                    params(1) = data.dmp.tau;
                    for j=1:length(joints_list)
                        params((j-1)*(envSimu.dimPolicies-1)/6 + 2) = ...
                            data.dmp.transformation_system.(joints_list{j}).goal;
                        t = (j-1)*(envSimu.dimPolicies-1)/6 + 3;
                        params(t:(t-2+((envSimu.dimPolicies-1)/6))) = ...
                            data.dmp.transformation_system.(joints_list{j}).forcing_term.weights;
                    end
                    params = 2 * (params - envSimu.paramsMean) ./ envSimu.paramsRange;
                    newReg((length(newReg)-length(reached) - length(params)+1):(length(newReg)-length(reached))) = ...
                        params;
                    regTaught = [regTaught; newReg];
                    regDemos = [regDemos; 0 params outcomes];
                end
            end
            save('simpleTeacher2','regTaught','regDemos');
%             regTaught = [];
%             directory0 = '/home/nduminy/Programmation/Poppy/Moves/teacher2';
%             files = dir(directory0);
%             for i=1:length(files)
%                 if files(i).name(1) ~= '.'
%                     str = fileread([directory0 '/' files(i).name]);
%                     data = json2matlab(str);
%                     newReg = zeros(1, (envSimu.dimPolicies + 3*length(envSimu.outcomesRange) + 1));
%                     reached = [data.outcomeDemo.Mstart.x data.outcomeDemo.Mstart.y ...
%                         data.outcomeDemo.Mend.x data.outcomeDemo.Mend.y ...
%                         data.outcomeDemo.Object.x data.outcomeDemo.Object.y ...
%                         data.outcomeDemo.length];
%                     reached = 2 * (reached - envSimu.outcomesMean) ./ envSimu.outcomesRange;
%                     for j=0:(length(reached)-1)
%                         newReg(2*j+1) = reached(j+1) - 0.025;
%                         newReg(2*j+2) = reached(j+1) + 0.025;
%                     end
%                     newReg((length(newReg)-length(reached) + 1):length(newReg)) = reached;
%                     params = zeros(1, envSimu.dimPolicies);
%                     params(1) = data.dmp.tau;
%                     for j=1:length(joints_list)
%                         params((j-1)*(envSimu.dimPolicies-1)/6 + 2) = ...
%                             data.dmp.transformation_system.(joints_list{j}).goal;
%                         t = (j-1)*(envSimu.dimPolicies-1)/6 + 3;
%                         params(t:(t-2+((envSimu.dimPolicies-1)/6))) = ...
%                             data.dmp.transformation_system.(joints_list{j}).forcing_term.weights;
%                     end
%                     params = 2 * (params - envSimu.paramsMean) ./ envSimu.paramsRange;
%                     newReg((length(newReg)-length(reached) - length(params)+1):(length(newReg)-length(reached))) = ...
%                         params;
%                     regTaught = [regTaught; newReg];
%                 end
%             end
%             save('simpleTeacher3','regTaught');
        end
        
        %TO DO change this method
%         function teacherDataset()
%             dimPolicies =4;
%             regTaught=[]; %to return: table of [y1min,y1max,y2min,y2max,nbTaught, a, y]
%             
%             f1=1/12;
%             f2=7/12;
%             reached0=-0.7;
%             f3=1/12;
%             reached1= 0.5;
%  
%             
%             %perfect teacher : teacher 1
%             for y=-1:0.05:-0.4
%                 x1= f1/2 + f1/2*sign(y+0.7)*(abs(y+0.7)/0.3)^(1/5); %f1*sqrt( acos(-4*(y-reached0))/(3*pi) );
%                 regTaught=[regTaught; y-0.025,y+0.025,0, x1,0, zeros(1,dimPolicies-2), y];
%             end
%             for y=-0.4:0.05:-0.2005
%                 x1=f1+(y+0.4)*(f2-f1)/0.2;
%                 regTaught=[regTaught; y-0.025,y+0.025,0, x1,0, zeros(1,dimPolicies-2), y];
%             end
%             for y=0:0.05:1
%                 s= f3*sqrt( acos(2*(y-reached1))/(3*pi) );
%                 x1=1-s;
%                 regTaught=[regTaught; y-0.025,y+0.025,0, x1,1, zeros(1,dimPolicies-2), y];
%             end
%             save('simpleTeacher1','regTaught')
%             
%             %partial teacher: teacher2
%             regTaught=[]; %to return: table of [y1min,y1max,y2min,y2max,nbTaught, a, y]
%             for y=-1:0.05:1
%                 x1= f1/2 + f1/2*sign(y+0.7)*(abs(y+0.7)/0.3)^(1/5); %f1*sqrt( acos(-4*(y-reached0))/(3*pi) );
%                 regTaught=[regTaught; y-0.025,y+0.025,0, x1,0, zeros(1,dimPolicies-2), y];
%             end
%              
%             save('simpleTeacher2','regTaught')
%            
%             %partial teacher: teacher3
%             regTaught=[]; %to return: table of [y1min,y1max,y2min,y2max,nbTaught, a, y]
%             for y=-1:0.05:1
%                 x1=f1+(y+0.4)*(f2-f1)/0.2;
%                 regTaught=[regTaught; y-0.025,y+0.025,0, x1,0, zeros(1,dimPolicies-2), y];
%             end
%              
%             save('simpleTeacher3','regTaught')
%             
%             %partial teacher: teacher4
%             regTaught=[]; %to return: table of [y1min,y1max,y2min,y2max,nbTaught, a, y]
%             reached11=-0.5;
%             for y=-1:0.05:-0.05
%                 s= f3*sqrt( acos (2*(y-reached11))/(3*pi) );
%                 x1=1-s;
%                 regTaught=[regTaught; y-0.025,y+0.025,0, x1,1, zeros(1,dimPolicies-2), y];
%             end
%             for y=0:0.05:1
%                 s= f3*sqrt( acos (2*(y-reached1))/(3*pi) );
%                 x1=1-s;
%                 regTaught=[regTaught; y-0.025,y+0.025,0, x1,1, zeros(1,dimPolicies-2), y];
%              end
%              
%             save('simpleTeacher4','regTaught')
% 
%         end
        
        
        function plotEnvironmentOutcomes()
            P=path;
            path(P,'../../SGIMalgorithm/')
            envSimu = SimpleTest();
            [X,Y] = meshgrid(-1:.005:1);
            param = zeros(1,envSimu.dimPolicies);
            Z = 0*X;
            for i=1:length(X(:))
                param(1,1) = X(i);
                param(1,2) = Y(i);
                Z(i) = executePolicy(envSimu,param, 1);
            end
            figure
            mesh(X,Y,Z)
            
            X2 = (-1:0.005:1);
            Z2 = 0*X2;
            for i=1:length(X2)
                param2(1,1) = X2(i);
                param2(1,2) = X2(i);
                Z2(i) = executePolicy(envSimu,param2, 1);
            end
            figure
            plot(X2,Z2,'-')
        end
        %--------------------------------------------------------------------------
        %------------------------  test functions ---------------------------------
        %--------------------------------------------------------------------------
        
        function test_EnvironmentSimulator()
            envSimu = EnvironmentSimulator()
            param   = randParameters(envSimu)
            executePolicy(envSimu, param)
        end
        
        function testTeacherDataset(filename)
            load(filename)
            envSimu = SimpleTest();
            nDemo=size(regTaught,1);
            error=zeros(1,nDemo);
            yDemo=zeros(1,nDemo);
            
            for iDemo=1:nDemo
               aDemo = regTaught(iDemo, (2*envSimu.nbOutcomeTypes+2):(end-envSimu.nbOutcomeTypes));
               yDemo(iDemo) = regTaught(iDemo, end-envSimu.nbOutcomeTypes+1:end);
               y = executePolicy(envSimu,aDemo, 1);
               error(iDemo)= y-yDemo(iDemo);
            end
            figure
            plot(yDemo,error,'x-');
            
        end
        
    end %end static methods
    
end
