function result = executePolicyRemotely(start, params)
%executePolicyRemotely
%   

context = org.zeromq.ZMQ.context(1);
socket = context.socket(org.zeromq.ZMQ.REQ);
socket.connect('tcp://localhost:5555');

policy = computeRequest(start, params);
req = java.lang.String(matlab2json('',policy,'Compact',1));

socket.send(req.getBytes(),0);
result = socket.recv(0);
result = json2matlab(char(result)');
result = result.data;

socket.close();
context.term();

end

function req = computeRequest(context, params)

global K D alpha centres widths joints_list;

req = {};
req.command = 'Execute';
req.data = {};

canonical_system = {};
canonical_system.alpha = alpha;
canonical_system.pattern = 'discrete';

tf = {};

nbfs = ((length(params) -1)/length(joints_list)) - 1;
weights = 1:nbfs;
weights = weights + 2;

for i=1:length(joints_list)
   data = {};
   data.D = D;
   data.K = K;
   data.forcing_term = {};
   data.forcing_term.centres = centres;
   data.forcing_term.widths = widths;
   data.forcing_term.weights = params((i-1)*(nbfs+1) + weights);
   data.start = context(i);
   data.goal = params((i-1)*(nbfs+1) + 2);
   data.pattern = 'discrete';
   tf.(joints_list{i}) = data;
end

req.data.canonical_system = canonical_system;
req.data.tau = params(1);
req.data.transformation_system = tf;

end


