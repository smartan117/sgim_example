k = 1;
for i=1:length(evalPerf.listEvalPoints)
    figure;
    ind = k:(k-1+length(evalPerf.listEvalPoints{i}));
    plot(evalPerf.mapEvalNN(:,end),mean(evalPerf.mapEvalNN(:,ind),2));
    title(['Evaluation of outcome space ' num2str(i)]);
    k = k + length(evalPerf.listEvalPoints{i});
    set(gca,'YGrid','on')
    set(gca,'yscale','log');
end