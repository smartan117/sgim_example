%% Loading path

P=path;
P=path(P,'../../SGIMalgorithm/')
path(P,'../../../../jsonlab-1.2/jsonlab/')


%% Actual code

%load('../data/Sgimacts1_SGIMACTSteacherid1_teacherf0_date30-May-2016.mat');
load('../data/Sgimacts1_SGIMACTSteacherid1_teacherf0_date15-Apr-2016.mat');

NumReg = 1;
nbStrategies = 2;
watchAround = 0.1;
sample = [10 10 10];

partReg2 = PartitionRegions(sample, sample, NumReg, mapp, nbStrategies, nbOutcomeTypes, watchAround);

it = 0;

for i=1:size(Predictor,1)

    example = Predictor(i,1:(end-4));
    
    errorValue = Predictor(i,6);
    iStrat = Predictor(i,end);
    iTask = Predictor(i,(end-1));
    
    if iTask == 1
        example = example(1:2);
    elseif iTask == 2
        example = example(1:4);
    end
    
    if it < Predictor(i,end-2)
        if it > 0
            
            regions = [];
            
            for task=1:nbOutcomeTypes
                for strat=1:nbStrategies
                    for j=2:length(partReg2.listRegions(task).LReg)
                        val = partReg2.listRegions(task).LReg{j}{1}(strat);
                        limits = partReg2.listRegions(task).LReg{j}{2}';
                        limits = limits(:);
                        str = [];
                        for k=1:length(limits)
                            str = [str ' ' num2str(limits(k))];
                        end
                        
                        if val ~= 999 && val ~= 998 && ~isnan(val) && val ~= Inf && val ~= -Inf
                            disp(['Region task ' num2str(task) ', strat ' num2str(strat) ...
                                ', limits ' str ', value: ' num2str(val)]);
                            ll = [limits' zeros(1,10-length(limits))];
                        
                            regions = [regions; [task strat j ll val/costStrat(strat)]];                        
                        end
                    end
                end
            end
            
            if size(regions,1) > 0
                [~,id] = sort(regions(:,end));            
                regions = regions(id,:);
            end

            if iStrat == 1
                it = it + 1;
                ind = (epmem{iTask}.listGoal(:,1) == it);
                goal = epmem{iTask}.listGoal(ind,2:end);
                goal = convertGC(mapp{iTask},goal);
                disp(['Task ' num2str(iTask) ' chosen, strat ' num2str(iStrat) ...
                    ', goal: ' num2str(goal)]);
            elseif iStrat == 2
                ind = (hTeacher.listTeacher(:,1) == it);
                goal = hTeacher.listTeacher(ind,((end-6):(end-2)));
                if iTask == 1
                    goal = goal(1:2);
                elseif iTask == 2
                    goal = goal(1:4);
                end
                goal = convertGC(mapp{iTask},goal);
                disp(['Task ' num2str(iTask) ' chosen, strat ' num2str(iStrat) ...
                    ', goal: ' num2str(goal)]);
            end
        end
        
        it = Predictor(i,end-2);
        %input('any touch to continue');
        
    end 
    
    
    partReg2 = updatePartitionRegions(partReg2, example', errorValue, ...
        TagMax, iStrat, iTask);
    %example in column
    
end

