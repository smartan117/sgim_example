function progress = computeProgress(perfBefore, perfAfter, nbElements,nStart,distY)
%INPUT
%perfBefore, perfAfter : distances (num >=0)
%nelements             : timestamp for perfAfter (int)
%nStart                : timestamp for perfBefore (int)
%distY                 : num >=0
%in our experiments perdBefore and perfAfter are distances
%OUTPUT
%progress               : progress measure (num <=0)
% progress = 2*(...
%     -1 +...
%     sigmoid(...
%     2*progressReachedOutcome/...
%     ((nbElements - nStart)*distY)...
%     )...
%     );

if perfBefore>perfAfter 
    % if progress, perfBefore> perfAfter and dPerf<0
    dPerf = perfAfter - perfBefore;
else
    dPerf=0;
end

% progress <=0
% progress = dPerf/(perfBefore*distY); 
progress = dPerf; 

end
