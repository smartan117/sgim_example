function test_explo_local(iTask, goal)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


%% Initialization
variablesGlobales
saggGlobal

%%=========================================================================
%%=======================     Choose The Goal     =======================
%%=========================================================================

envSimu       = randomizeContext(envSimu);
context       =  getContext(envSimu);
context01     = (context +1)/2;

goalOutcome = goal;
goalTaskType = iTask;

iStrat = 1;
consequence = {}; %[]
curLowLevel.curTask = goalTaskType;
curLowLevel.gOutcome = convertCG(mapp{goalTaskType}, goalOutcome');

consequence = run1goal(curLowLevel,curLowLevel.gOutcome, context, epmem, mapp, envSimu);


%%=========================================================================
%%====================  update tree regions     ===========================
%%=========================================================================

example          = goalOutcome(:);
example          = example(:);
errorValue       = consequence{1};
try
    partReg          = updatePartitionRegions(partReg,  example, errorValue, TagMax, iStrat, curLowLevel.curTask) ;
catch
    disp('erreur');
end
QualityCandidate = [QualityCandidate; [goalOutcome(:)' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY,1) consequence{1} curLowLevel.nbExperiments goalTaskType iStrat]];

Predictor = [Predictor; [goalOutcome' zeros(1,maxDimY - mapp{curLowLevel.curTask}.dimY) errorValue curLowLevel.nbExperiments goalTaskType iStrat]];


if size(epmem{curLowLevel.curTask}.listGoal,1)>2 && curLowLevel.nbExperiments>2
    ProgressL = [ProgressL; consequence{3} iStrat  curLowLevel.nbExperiments];
end


for iTask=1:length(consequence{2})
    
    nReachedOutcomes2 = size(consequence{2}{iTask},1);
    nReachedOutcomes4 = size(consequence{4}{iTask},1);
    if (nReachedOutcomes2 ~= nReachedOutcomes4)
        error('MAIN_RIAC_CB : error consequence2(reached outcomes) and consequence4(progress for the outcomes) do not have the same size');
    end

    for i = 1:nReachedOutcomes2
        example    = convertGC(mapp{iTask},consequence{2}{iTask}(i,:));
        example    = example(:);

        if(any(example>100) || any(example<0))
            error('example out of bounds. It should take values between 0 and 1');
        end

        errorValue = consequence{4}{iTask}(i,1);
        try
            partReg    = updatePartitionRegions(partReg, example, errorValue, TagMax, iStrat, iTask);
        catch
            disp('erreur');
        end
        Predictor  = [Predictor; [example' zeros(1,maxDimY - mapp{iTask}.dimY) errorValue curLowLevel.nbExperiments iTask iStrat]];
    end
    
end


save(fileMemo);


end

