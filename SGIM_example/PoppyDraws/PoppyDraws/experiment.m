%experiment.m
% This is one of the most used functions of the experiment.
% This function handles the execution of a policy in a defined context,
% and the interpretation of the outcome, ie its competence measure, and the memorisation of the learning data

% Copyright (c) 2013 Sao Mai Nguyen
%               e-mail : nguyensmai@gmail.com
%               http://nguyensmai.free.fr/
%
% Permission is granted to copy, distribute, and/or modify this program
% under the terms of the GNU General Public License, version 2 or any
% later version published by the Free Software Foundation.
% 
% This program is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
% Public License for more details
% 
% If you use this code in the context of a publication, I would appreciate 
% it if you could cite it as follows:
% 
% 
% @phdthesis{Nguyen2013,
% 	Author = {Nguyen, Sao Mai},
% 	School = {INRIA},
% 	Title = {A Curious Robot Learner for Interactive Goal-Babbling: Strategically Choosing What, How, When and from Whom to Learn},
% 	Year = {2013}}
%

%%TO DO think about adding an experiment to all outcome spaces intersected

function [outDist reached] = experiment(movParam, exploType, cont,iOutcome, goal)
%movParam  : policy parameter to execute
%exploType : exploration type (marker recorded in the episodic memory)
%cont      : context parameter
%iOutcome  : type of task (in the case of tasks of different nature)
%goal      : goal task to complete
%outDist   : distance between the reached outcome and the goal outcome.

disp('EXPERIMENT called');

variablesGlobales
saggGlobal
progressReachedOutcome = 0;
param = normaliseParam(envSimu,movParam);


%--------------------------------------------------------------------------
%--------------------   execute movement           ------------------------
%--------------------------------------------------------------------------

%disp('EXPERIMENT : movParam ');
%movParam  = normaliseParam(movParam,  rFish);
%envSimu.curs = randParameters(envSimu.curs);  %RANDOM MODE !!!
if nargin>=3 && ~isempty(cont)
    setContext(envSimu, cont);
end


context = getContext(envSimu);
outcomes = executePolicy(envSimu, param, iOutcome);
disp(['Outcomes reached: ' num2str(outcomes)]);

%TO CHANGE change so we care about outcomes of other tasks

reached{1} = outcomes(1:2);
%reached{2} = outcomes(3:4);
reached{2} = outcomes(1:4);
%reached{4} = [outcomes(7)];
%reached{5} = outcomes([1 2 7]);
reached{3} = outcomes([1 2 3 4 7]);
%reached{7} = outcomes(5:6);

%% TODO: handle the calculation of the distance
if nargin <5
    outDist = distMapping(mapp{iOutcome}, reached{iOutcome}, []);
else
    outDist = distMapping(mapp{iOutcome}, reached{iOutcome}, goal);
end

disp(['Distance with goal: ' num2str(outDist)]);

%--------------------------------------------------------------------------
%--------------------  update memory with new data         ----------------
%--------------------------------------------------------------------------

if length(curLowLevel.progressReachedOutcome) == 0
    for i=1:envSimu.nbOutcomeTypes
        curLowLevel.progressReachedOutcome{i} = [];
    end
end


for iTask=1:envSimu.nbOutcomeTypes
    %% new memory entity
    if exploType >= 0
        curLowLevel.LearningEntity = [context movParam reached{iTask} exploType];
        if exploType ~=100
            perfBefore = meanDistToGoal(epmem{iTask}, iTask, reached{iTask}, context, 3); %bestDistToGoal(epmem{iTask}, reached{iTask}, context, 3);  %Replace by meanDistToGoal ???
        end
        epmem{iTask} = addLearningEntity(epmem{iTask}, context, movParam, ...
            reached{iTask}, exploType);
        if exploType ~=100
            perfAfter = meanDistToGoal(epmem{iTask}, iTask, reached{iTask}, context, 3);
    %         if reached<0 && reached>-0.2
    %             try
    %                 figure(15)
    %                 dispRegions(partReg,1);
    %             end
    %         end
            progress = computeProgress(perfBefore, perfAfter, curLowLevel.nbExperiments,curLowLevel.nbExperiments-curLowLevel.numberIteration,epmem{iTask}.distY);
            disp(['Progress for reached point task ' num2str(iTask) ': ' num2str(progress)]);
            curLowLevel.progressReachedOutcome{iTask}(end+1,1) =  progress;
        end
    end
end

if exploType >= 0
    curLowLevel.nbExperiments = curLowLevel.nbExperiments +1;
end

reached = reached{iOutcome};

%--------------------------------------------------------------------------
%-------------  check if the teacher makes a demonstration  ---------------
%--------------------------------------------------------------------------

% Check whether at least one teacher is present or not
if hTeacher(1).present
    % Check if teacher 1 is interupting (used by SGIM-D when only 1 teacher
    % is present
    teacherInterrupts();
    if (curLowLevel.flagTeach ==1)
        epmem{iOutcome}.listFlagTeach= [epmem{iOutcome}.listFlagTeach; size(epmem{iOutcome}.listLearning,1) 1];
    end
    
end


%--------------------------------------------------------------------------
%----------- post-processing: display graphs and evaluation ---------------
%--------------------------------------------------------------------------

% display figure
% if evalPerf.affichePlot  && mod(epmem.nbElements,EVAL_EXP) == 0
%     plotReachedPoints;
%   %  plotExperience(epmem, mapp)
% end

%if  exploType <= 2 && mod(epmem.nbElements,EVAL_EXP) == 1
%    disp(' EXPERIMENT : DISPLAY REEXECUTE');
%    evalPerf = bestExperience(evalPerf, epmem, goal, rFish);
%end

% if nargin ==5
%     figure(10)
%     plot(curLowLevel.nbExperiments, outDist,'x', 'Color',([ goal 0 goal]+1)/2)
%     hold on
%     title('error per trial');
% end


if (exploType >= 0 && (mod(curLowLevel.nbExperiments, 100) == 1 || curLowLevel.nbExperiments == 1|| curLowLevel.nbExperiments == 10 || curLowLevel.nbExperiments == 50))
    save( [fileMemo,'_nb_',num2str(curLowLevel.nbExperiments)] );
end

if (exploType >= 0 && (mod(curLowLevel.nbExperiments, 1000) == 1 || curLowLevel.nbExperiments == 1|| curLowLevel.nbExperiments == 10 || curLowLevel.nbExperiments == 50 ...
        ||curLowLevel.nbExperiments == 100||curLowLevel.nbExperiments == 750|| curLowLevel.nbExperiments == 500 || curLowLevel.nbExperiments == 1500 || curLowLevel.nbExperiments == 2500 ...
        ||mod(curLowLevel.nbExperiments, 200) == 1))
    evalPerf = evaluationPerfOnce(evalPerf, epmem,curLowLevel, envSimu);
    visualiseExploration;
end



% disp(['EXPERIMENT : exploType ', num2str(exploType),' outDist ',num2str(outDist)]);

end

