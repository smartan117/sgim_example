load('../data/imitation_base.mat');

fig = figure;
epmem{1}.plotReachedPoints(1, fig);


load('../data/explo_locale_2_nb_594');

ind = (epmem{1}.listLearning(:,end) == 2);
goal = [-0.6 0];
dist = [];

y = epmem{1}.listLearning(ind,(end-2):(end-1));

for i=1:size(y,1)
    dist(i) = distMapping(mapp{1}, y(i,:), goal);
end

t = 1:size(y,1);

ind3 = (dist < 2);

figure;
plot(t(ind3),dist(ind3));
xlabel('Iterations');
ylabel('Error');
title('Evolution of error in local exploration');

ind2 = (dist > 2);
sum(ind2(1:100))
sum(ind2((end-100):end))
