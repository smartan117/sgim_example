global K D alpha centres widths joints_list joints_limits;
K = 150;
D = 25;
alpha = 6.9;
centres = [1.0 0.17817305177289844 0.03174563637806795 0.005656216913953108 ...
    0.0010077854290485113];
widths = [2.1494345654506155 67.70803205367747 2132.829572144119 ... 
    67184.9682502388 2116352.8571332926];

joints_list = {'r_shoulder_x', 'r_shoulder_y', 'r_arm_z', 'r_elbow_y', ...
    'bust_y', 'abs_z'};

joints_limits = [[-115 20]; [-210 65]; [-90 90]; [-90 57]; [-23 56]; [-58 102]];